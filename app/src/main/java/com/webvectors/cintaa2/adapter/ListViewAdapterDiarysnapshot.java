package com.webvectors.cintaa2.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.model.DiarySnapshotListSorting;

import java.util.List;


/**
 * Created by Shri on 13/07/2016.
 */
public class ListViewAdapterDiarysnapshot extends BaseAdapter {

    //public ArrayList<HashMap<String, String>> list;
    List<DiarySnapshotListSorting> list;
    Activity activity;
    TextView txtFirst;
    TextView txtSecond;
    TextView txtThird;
    TextView txtFourth;
    public ListViewAdapterDiarysnapshot(Activity activity, List<DiarySnapshotListSorting> list) {
        super();
        this.activity=activity;
        this.list=list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();
        Typeface customfont = Typeface.createFromAsset(activity.getAssets(), "lane.ttf");
        if(convertView == null){

            convertView=inflater.inflate(R.layout.colmn_row_diarysnapshot, null);
            txtFirst=(TextView) convertView.findViewById(R.id.tvDate);
            txtFirst.setTypeface(customfont);
            txtSecond=(TextView) convertView.findViewById(R.id.tvPhouse);
            txtSecond.setTypeface(customfont);
            txtThird=(TextView) convertView.findViewById(R.id.tvLoc);
            txtThird.setTypeface(customfont);
            txtFourth=(TextView) convertView.findViewById(R.id.tvShift);
            txtFourth.setTypeface(customfont);

        }
/*
        HashMap<String, String> map=list.get(position);
        txtFirst.setText(map.get(shoot_date));
        txtSecond.setText(map.get(house_name));
        txtThird.setText(map.get(shoot_location));
        txtFourth.setText(map.get(shift_time));
  */
        DiarySnapshotListSorting obj= list.get(position);
        txtFirst.setText(obj.date);
        txtSecond.setText(obj.productionHouse);
        txtThird.setText(obj.location);
        txtFourth.setText(obj.shiftTime);

        if (position % 2 == 0) {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        } else {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        }
        return convertView;
    }
}
