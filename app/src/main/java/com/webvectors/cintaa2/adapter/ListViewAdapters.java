package com.webvectors.cintaa2.adapter;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.webvectors.cintaa2.activity.InProgressDetailActivity;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.activity.SnapshotDetailsActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.webvectors.cintaa2.utility.Constants.house_name;
import static com.webvectors.cintaa2.utility.Constants.shift_time;
import static com.webvectors.cintaa2.utility.Constants.shoot_date;
import static com.webvectors.cintaa2.utility.Constants.shoot_location;

public class ListViewAdapters extends BaseAdapter{

    public ArrayList<HashMap<String, String>> list;
    Activity activity;

    List<String> entry_id;
    SharedPreferences preferences,preferencesforlist;
    SharedPreferences.Editor editor,editor1;
    public ListViewAdapters(Activity activity,ArrayList<HashMap<String, String>> list,List<String> entry_id){
        super();
        this.activity=activity;
        this.list=list;
        this.entry_id=entry_id;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater;
        Typeface customfont = Typeface.createFromAsset(activity.getAssets(), "lane.ttf");
        if(convertView == null){
            holder= new ViewHolder();
            inflater=activity.getLayoutInflater();
            convertView=inflater.inflate(R.layout.colmn_row, null);
            holder.txtdate=(TextView) convertView.findViewById(R.id.tvDate);
            holder.txtdate.setTypeface(customfont);
            holder.txtprodhouse=(TextView) convertView.findViewById(R.id.tvPhouse);
            holder.txtprodhouse.setTypeface(customfont);
            holder.txtlocation=(TextView) convertView.findViewById(R.id.tvLoc);
            holder.txtlocation.setTypeface(customfont);
            holder.txtshifttime=(TextView) convertView.findViewById(R.id.tvShift);
            holder.txtshifttime.setTypeface(customfont);
            holder.ivedit=(ImageView) convertView.findViewById(R.id.Ivedit);


            convertView.setTag(holder);

        }else
        {
            holder=(ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map=list.get(position);
        //holder.txtdate.setText(map.get(shoot_date));
        holder.txtdate.setText(list.get(position).get(shoot_date));
        holder.txtprodhouse.setText(map.get(house_name));
        holder.txtlocation.setText(map.get(shoot_location));
        holder.txtshifttime.setText(map.get(shift_time));
        holder.ivedit.setImageResource(R.drawable.edit);

        preferences= PreferenceManager.getDefaultSharedPreferences(activity);
        holder.txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor1= preferences.edit();
                String prev =  preferences.getString("Snapshot_list_item","xcz");
                if(!prev.equals("xcz")){
                    editor1.remove("Snapshot_list_item");
                }
                editor1.putString("Snapshot_list_item",entry_id.get(position));
                editor1.commit();
                //   Toast.makeText(activity, ""+entry_id.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,SnapshotDetailsActivity.class);
                activity.startActivity(intent);
            }
        });
        holder.txtprodhouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor1= preferences.edit();
                String prev =  preferences.getString("Snapshot_list_item","xcz");
                if(!prev.equals("xcz")){
                    editor1.remove("Snapshot_list_item");
                }
                editor1.putString("Snapshot_list_item",entry_id.get(position));
                editor1.commit();
                //  Toast.makeText(activity, ""+entry_id.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,SnapshotDetailsActivity.class);
                activity.startActivity(intent);
            }
        });
        holder.txtlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor1= preferences.edit();
                String prev =  preferences.getString("Snapshot_list_item","xcz");
                if(!prev.equals("xcz")){
                    editor1.remove("Snapshot_list_item");
                }
                editor1.putString("Snapshot_list_item",entry_id.get(position));
                editor1.commit();
                //   Toast.makeText(activity, ""+entry_id.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,SnapshotDetailsActivity.class);
                activity.startActivity(intent);
            }
        });
        holder.txtshifttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor1= preferences.edit();
                String prev =  preferences.getString("Snapshot_list_item","xcz");
                if(!prev.equals("xcz")){
                    editor1.remove("Snapshot_list_item");
                }
                editor1.putString("Snapshot_list_item",entry_id.get(position));
                editor1.commit();
                // Toast.makeText(activity, ""+entry_id.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity,SnapshotDetailsActivity.class);
                activity.startActivity(intent);
            }
        });
        holder.ivedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor=preferences.edit();
                //Toast.makeText(getContext(), ""+list_entry_id.get(position), Toast.LENGTH_SHORT).show();
                String prev =  preferences.getString("InProgress_list_item","xcz");
                if(!prev.equals("xcz")){
                    editor.remove("InProgress_list_item");
                }
                editor.putString("InProgress_list_item",entry_id.get(position)).commit();
                Intent intent=new Intent(activity,InProgressDetailActivity.class);
                activity.startActivity(intent);
            }

        });

        if (position % 2 == 0) {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        } else {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        }

        return convertView;
    }

    class ViewHolder
    {
        TextView txtdate;
        TextView txtprodhouse;
        TextView txtlocation;
        TextView txtshifttime;
        ImageView ivedit;
    }

}