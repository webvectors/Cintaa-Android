package com.webvectors.cintaa2.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.model.CalendarChildData;
import com.webvectors.cintaa2.model.CalendarHeaderData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ASUS on 02/08/2016.
 */
public class CalendarExpandableListAdapter extends BaseExpandableListAdapter {

    private HashMap<CalendarHeaderData,List<CalendarChildData>> listDataChild;
    private List<CalendarHeaderData> listDataHeader;
    private Context ctx;

    public CalendarExpandableListAdapter(HashMap<CalendarHeaderData, List<CalendarChildData>> listDataChild, List<CalendarHeaderData> listDataHeader, Context ctx) {
        this.listDataChild = listDataChild;
        this.listDataHeader = listDataHeader;
        this.ctx = ctx;
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    // FILL DATA IN PARENT
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
       // String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(), "lane.ttf");




        TextView tvStartTime = (TextView) convertView
                .findViewById(R.id.tvstartTime);
        tvStartTime.setTypeface(custom_font);
        TextView tvfilmName = (TextView) convertView
                .findViewById(R.id.tvfilmName);
        tvfilmName.setTypeface(custom_font);

        tvStartTime.setText(listDataHeader.get(groupPosition).startTime);

        tvfilmName.setText(listDataHeader.get(groupPosition).filmName);


        ////////////////////////
        TextView tvshootAT = (TextView) convertView
                .findViewById(R.id.tvshootAT);
        tvshootAT.setTypeface(custom_font);

        TextView tvShiftTime = (TextView) convertView
                .findViewById(R.id.tvShiftTime);
        tvShiftTime.setTypeface(custom_font);
        tvShiftTime.setText(listDataHeader.get(groupPosition).shiftTime);

        TextView tvlocation = (TextView) convertView
                .findViewById(R.id.tvlocation);
        tvlocation.setTypeface(custom_font);
        tvlocation.setText(listDataHeader.get(groupPosition).Location);

        final ImageView imgAlarmFill = (ImageView) convertView.findViewById(R.id.imgAlarmFill);
        final ImageView imgAlarm = (ImageView) convertView.findViewById(R.id.imgAlarm);
        imgAlarmFill.setVisibility(View.VISIBLE);
        imgAlarm.setVisibility(View.INVISIBLE);
        imgAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgAlarmFill.setVisibility(View.VISIBLE);
                imgAlarm.setVisibility(View.INVISIBLE);
            }
        });
        imgAlarmFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgAlarmFill.setVisibility(View.INVISIBLE);
                imgAlarm.setVisibility(View.VISIBLE);
            }
        });

        return convertView;
    }

    // FILL DATA IN CHILD
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }
        Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(), "lane.ttf");

        TextView tvshootAT = (TextView) convertView
                .findViewById(R.id.tvshootAT);
        tvshootAT.setTypeface(custom_font);

        TextView tvShiftTime = (TextView) convertView
                .findViewById(R.id.tvShiftTime);
        tvShiftTime.setTypeface(custom_font);
        tvShiftTime.setText(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).shiftTime);


        TextView tvlocation = (TextView) convertView
                .findViewById(R.id.tvlocation);
        tvlocation.setTypeface(custom_font);
        tvlocation.setText(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).Location);

        String entry_id =  listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).entry_id;

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
