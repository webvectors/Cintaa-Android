package com.webvectors.cintaa2.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

import java.util.List;

/**
 * Created by ASUS on 03/08/2016.
 */
public class ListViewPaymentDetails extends BaseAdapter {

    Context ctx;
    List<String> entry_id,member_id,film_tv_name,paymentAfter,shoot_date,rateType,rate,prodHouse,prodName;


    public ListViewPaymentDetails(Context ctx, List<String> entry_id,List<String> member_id,List<String> film_tv_name,List<String> paymentAfter,List<String> shoot_date,List<String> rateType,List<String> rate, List<String> prodHouse, List<String> prodName) {
        this.ctx = ctx;
        this.entry_id = entry_id;
        this.member_id = member_id;
        this.film_tv_name = film_tv_name;
        this.paymentAfter = paymentAfter;
        this.shoot_date = shoot_date;
        this.rateType = rateType;
        this.rate = rate;
        this.prodHouse = prodHouse;
        this.prodName = prodName;
    }

    @Override
    public int getCount() {
        return entry_id.size();
    }

    @Override
    public Object getItem(int position) {
        return entry_id.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater;
        if(v==null)
        {
            holder= new ViewHolder();
            inflater= (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            v= inflater.inflate(R.layout.list_payment_details,null);

            holder.tvtvfilmName=(TextView)v.findViewById(R.id.tvtvfilmName);
            holder.tvfilmName=(TextView)v.findViewById(R.id.tvfilmName);
            holder.tvtvprodHouse=(TextView)v.findViewById(R.id.tvtvprodHouse);
            holder.tvprodHouse=(TextView)v.findViewById(R.id.tvprodHouse);
            holder.tvtvProducer=(TextView)v.findViewById(R.id.tvtvProducer);
            holder.tvProducer=(TextView)v.findViewById(R.id.tvProducer);
            holder.tvtvRateValue=(TextView)v.findViewById(R.id.tvtvRateValue);
            holder.tvRateValue=(TextView)v.findViewById(R.id.tvRateValue);
            holder.tvtvPayAfter=(TextView)v.findViewById(R.id.tvtvPayAfter);
            holder.tvPayAfter=(TextView)v.findViewById(R.id.tvPayAfter);
            holder.tvtvshootDate=(TextView)v.findViewById(R.id.tvtvshootDate);
            holder.tvshootDate=(TextView)v.findViewById(R.id.tvshootDate);



            Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(), "lane.ttf");
            holder.tvtvfilmName.setTypeface(custom_font);
            holder.tvfilmName.setTypeface(custom_font);
            holder.tvtvprodHouse.setTypeface(custom_font);
            holder.tvprodHouse.setTypeface(custom_font);
            holder.tvtvProducer.setTypeface(custom_font);
            holder.tvProducer.setTypeface(custom_font);
            holder.tvtvRateValue.setTypeface(custom_font);
            holder.tvRateValue.setTypeface(custom_font);
            holder.tvtvPayAfter.setTypeface(custom_font);
            holder.tvPayAfter.setTypeface(custom_font);
            holder.tvtvshootDate.setTypeface(custom_font);
            holder.tvshootDate.setTypeface(custom_font);



            holder.tvtvfilmName.setPaintFlags(holder.tvtvfilmName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvprodHouse.setPaintFlags(holder.tvtvprodHouse.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvProducer.setPaintFlags(holder.tvtvProducer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvRateValue.setPaintFlags(holder.tvtvRateValue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvPayAfter.setPaintFlags(holder.tvtvPayAfter.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvshootDate.setPaintFlags(holder.tvtvshootDate.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            v.setTag(holder);
        }
        else
        {
            holder=(ViewHolder) v.getTag();
        }

        holder.tvfilmName.setText(film_tv_name.get(position));
        holder.tvRateValue.setText(rate.get(position)+" /- "+rateType.get(position));
        holder.tvprodHouse.setText(prodHouse.get(position));
        holder.tvProducer.setText(prodName.get(position));
        holder.tvPayAfter.setText(paymentAfter.get(position));
        holder.tvshootDate.setText(shoot_date.get(position));

        return v;
    }
    class ViewHolder{
        TextView tvtvfilmName, tvfilmName, tvtvprodHouse, tvprodHouse, tvtvProducer, tvProducer, tvtvRateValue, tvRateValue, tvtvPayAfter, tvPayAfter, tvtvshootDate, tvshootDate;
    }
}
