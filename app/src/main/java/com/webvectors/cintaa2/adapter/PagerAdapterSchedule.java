package com.webvectors.cintaa2.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.webvectors.cintaa2.fragment.CompletedFragment;
import com.webvectors.cintaa2.fragment.InprogessFragment;

/**
 * Created by Shri on 11/07/2016.
 */
public class PagerAdapterSchedule extends FragmentStatePagerAdapter {
    int mNumOfTbs1;

    public PagerAdapterSchedule(FragmentManager fm1,int tabCount1) {
        super(fm1);
        this.mNumOfTbs1=tabCount1;
    }
    @Override
    public android.support.v4.app.Fragment getItem(int position1) {

        switch (position1) {
            case 0:
                InprogessFragment tab1=new InprogessFragment();
                return tab1;

            case 1:
                CompletedFragment tab2=new CompletedFragment();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
