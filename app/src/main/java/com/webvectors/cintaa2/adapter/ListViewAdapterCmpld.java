package com.webvectors.cintaa2.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.webvectors.cintaa2.utility.Constants.house_name;
import static com.webvectors.cintaa2.utility.Constants.shift_time;
import static com.webvectors.cintaa2.utility.Constants.shoot_date;
import static com.webvectors.cintaa2.utility.Constants.shoot_location;

public class ListViewAdapterCmpld extends BaseAdapter{

    public ArrayList<HashMap<String, String>> list;
    Activity activity;

    public ListViewAdapterCmpld(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity=activity;
        this.list=list;
    }

    public ArrayList<HashMap<String, String>> getListArray(){
        return list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater=activity.getLayoutInflater();
        Typeface customfont = Typeface.createFromAsset(activity.getAssets(), "lane.ttf");
        if(convertView == null){
            holder= new ViewHolder();
            convertView=inflater.inflate(R.layout.colmn_row_completed, null);
            holder.txtFirst=(TextView) convertView.findViewById(R.id.tvDate);
            holder.txtFirst.setTypeface(customfont);
            holder.txtSecond=(TextView) convertView.findViewById(R.id.tvPhouse);
            holder.txtSecond.setTypeface(customfont);
            holder.txtThird=(TextView) convertView.findViewById(R.id.tvLoc);
            holder.txtThird.setTypeface(customfont);
            holder.txtFourth=(TextView) convertView.findViewById(R.id.tvShift);
            holder.txtFourth.setTypeface(customfont);

            convertView.setTag(holder);

        }else
        {
            holder=(ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map=list.get(position);
        holder.txtFirst.setText(map.get(shoot_date));
        holder.txtSecond.setText(map.get(house_name));
        holder.txtThird.setText(map.get(shoot_location));

        //..  changed "7:00 AM to 7:00 PM" to 7:00 AM
        String[] parts =  map.get(shift_time).split("to");
        String part1 = parts[0];

        holder.txtFourth.setText(part1);

        if (position % 2 == 0) {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        } else {
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.bgColor));
        }
        return convertView;
    }

    class ViewHolder
    {
        TextView txtFirst;
        TextView txtSecond;
        TextView txtThird;
        TextView txtFourth;
    }
}
