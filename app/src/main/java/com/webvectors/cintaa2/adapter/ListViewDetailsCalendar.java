package com.webvectors.cintaa2.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

import java.util.List;

/**
 * Created by ASUS on 03/08/2016.
 */
public class ListViewDetailsCalendar extends BaseAdapter {

    Context ctx;
    List<String> DShiftTime,DLocation,DFilm,DProdHouse,DCallTime;


    public ListViewDetailsCalendar(Context ctx, List<String> DShiftTime, List<String> DLocation, List<String> DFilm, List<String> DProdHouse,List<String> DCallTime) {
        this.ctx = ctx;
        this.DShiftTime = DShiftTime;
        this.DLocation = DLocation;
        this.DFilm = DFilm;
        this.DProdHouse = DProdHouse;
        this.DCallTime = DCallTime;
    }

    @Override
    public int getCount() {
        return DShiftTime.size();
    }

    @Override
    public Object getItem(int position) {
        return DShiftTime.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater;
        if(v==null)
        {
            holder= new ViewHolder();
            inflater= (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            v= inflater.inflate(R.layout.list_view_calendar_details,null);
            holder.tvShift=(TextView)v.findViewById(R.id.tvShiftTime);
            holder.tvFilm=(TextView)v.findViewById(R.id.tvFilmName);
            holder.ProdHouse=(TextView)v.findViewById(R.id.tvProductionHouse);
            holder.tvLocation=(TextView)v.findViewById(R.id.tvLocation);
            holder.tvFilm1=(TextView)v.findViewById(R.id.tvtvFilmName);
            holder.ProdHouse1=(TextView)v.findViewById(R.id.tvtvProductionHouse);
            holder.tvLocation1=(TextView)v.findViewById(R.id.tvtvLocation);
            holder.tvShift1=(TextView)v.findViewById(R.id.tvtvShiftTime);
            holder.tvCallTime=(TextView)v.findViewById(R.id.tvCallTime);
            holder.tvtvCallTime=(TextView)v.findViewById(R.id.tvtvCallTime);


            Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(), "lane.ttf");
            holder.tvShift.setTypeface(custom_font);
            holder.tvFilm.setTypeface(custom_font);
            holder.ProdHouse.setTypeface(custom_font);
            holder.tvLocation.setTypeface(custom_font);
            holder.tvShift1.setTypeface(custom_font);
            holder.tvFilm1.setTypeface(custom_font);
            holder.ProdHouse1.setTypeface(custom_font);
            holder.tvLocation1.setTypeface(custom_font);
            holder.tvCallTime.setTypeface(custom_font);
            holder.tvtvCallTime.setTypeface(custom_font);

            holder.tvShift1.setPaintFlags(holder.tvShift1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvFilm1.setPaintFlags(holder.tvFilm1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.ProdHouse1.setPaintFlags(holder.ProdHouse1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvLocation1.setPaintFlags(holder.tvLocation1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvtvCallTime.setPaintFlags(holder.tvtvCallTime.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            v.setTag(holder);

        }
        else
        {
            holder=(ViewHolder) v.getTag();
        }
        holder.tvShift.setText(DShiftTime.get(position));
        holder.tvFilm.setText(DFilm.get(position));
        holder.ProdHouse.setText(DProdHouse.get(position));
        holder.tvLocation.setText(DLocation.get(position));
        holder.tvCallTime.setText(DCallTime.get(position));
        return v;
    }
    class ViewHolder
    {
        TextView tvShift,tvLocation,ProdHouse,tvFilm,tvShift1,tvLocation1,ProdHouse1,tvFilm1,tvtvCallTime,tvCallTime;

    }
}
