package com.webvectors.cintaa2.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

import java.util.List;

/**
 * Created by ASUS on 10/08/2016.
 */
public class NotificationListViewAdapter extends BaseAdapter {

    List<String> notes;
    Context ctx;

    public NotificationListViewAdapter(Context ctx, List<String> notes) {
        this.notes = notes;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater;
        if(v==null)
        {
            holder= new ViewHolder();
            inflater= (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            v= inflater.inflate(R.layout.list_view_notifications,null);
            holder.tvnotes=(TextView)v.findViewById(R.id.tvnotes);


            Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(), "lane.ttf");
            holder.tvnotes.setTypeface(custom_font);




            v.setTag(holder);

        }
        else
        {
            holder=(ViewHolder) v.getTag();
        }
        holder.tvnotes.setText(notes.get(position));

        return v;
    }

    class ViewHolder
    {
        TextView tvnotes;

    }
}
