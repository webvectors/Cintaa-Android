package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ASUS on 16/09/2016.
 */
public class feedback_dialog_class extends DialogFragment {


    EditText etnewpwd,etconfirmpwd,etoldpwd;
    int check1=0;
    SharedPreferences preferences;
    AlertDialog.Builder builder;
    String member_id;
    ProgressDialog pd;
    Context context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id=  preferences.getString("member_id","abc");
        pd=new ProgressDialog(getActivity());
        context = getActivity();
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.feedback_dialog, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 1000f;
        // dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);


        dialog.setContentView(li.inflate(R.layout.feedback_dialog
                , null));
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        TextView tvhappy = (TextView) dialog.findViewById(R.id.tvhappy);
        TextView tvunhappy = (TextView) dialog.findViewById(R.id.tvunhappy);
        TextView tvremind = (TextView) dialog.findViewById(R.id.tvremind);
        ImageView ivclose = (ImageView) dialog.findViewById(R.id.ivclose);
        Button btsubmit = (Button) dialog.findViewById(R.id.btsubmit);
        final EditText etremark = (EditText) dialog.findViewById(R.id.etremark);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvhappy.setTypeface(custom_font);
        tvunhappy.setTypeface(custom_font);
        tvremind.setTypeface(custom_font);
        etremark.setTypeface(custom_font);
        btsubmit.setTypeface(custom_font);

        final String[] fed = new String[1];
        fed[0] = "Happy";
        final int[] f = {0};
        tvhappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etremark, InputMethodManager.SHOW_IMPLICIT);
                fed[0] = "Happy";
                f[0] =1;
               // feedback(happy,etremark.getText().toString().trim());
//                dialog.dismiss();
            }
        });
        tvunhappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etremark, InputMethodManager.SHOW_IMPLICIT);
                fed[0] = "Unhappy";
                f[0] =1;
               // feedback(unhappy,etremark.getText().toString().trim());
//                dialog.dismiss();
            }
        });
        tvremind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
              //  RemindAfter();
                new Feedback_Reminder_dialog().show(getFragmentManager(), "tag");
            }
        });
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(f[0] == 1){
                    feedback(fed[0],etremark.getText().toString().trim());
                    dialog.dismiss();
                }else {
                    showToast("Click Happy/Unhappy to Submit Your Feedback",Toast.LENGTH_SHORT);
                }

            }
        });
//        dialog.show();
        return dialog;
    }//onCreateDialog

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void feedback(String feed, String remark){


        preferences= PreferenceManager.getDefaultSharedPreferences(context);
        member_id=  preferences.getString("member_id","abc");
        String time = getCurrentTimeStamp();

        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("feed_date", time);
        params.put("feedback", feed);
        params.put("remark", remark);


        client.post(Config.URLfeedback, params, new JsonHttpResponseHandler() {

            //etprodhouname,etprodname

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    String s1 = response.getString("success");
                    showToast("Thank you for your Valuable Feedback", Toast.LENGTH_LONG);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Error in Connection", Toast.LENGTH_LONG);
            }
        });
        pd.dismiss();

    }

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(context, message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        toast.show();
    }

}
