package com.webvectors.cintaa2.Dialog_Classes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.webvectors.cintaa2.utility.GeneratePdf;
import com.webvectors.cintaa2.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ASUS on 08/11/2016.
 */

public class Backup_dialog extends DialogFragment {

    File CintaaDirectory;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Existing Completed Schedule Data will be Deleted and a Backup PDF will be mailed to you.");

        dialogBuilder.setCancelable(false);

        dialogBuilder.setPositiveButton("Backup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                generatePDF();

                dialog.cancel();
            }
        });

        dialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {

                dialog.cancel();
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();

        alertDialogObject.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                Button negativeButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                //negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                //final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                //final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.btn_backColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.btn_text);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.btn_backColor);
                if (Build.VERSION.SDK_INT >= 16) {
                    //  negativeButton.setBackground(negativeButtonDrawable);
                    //  positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                    // negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    //positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });
        //Show the dialog
        return alertDialogObject;
    }//onCreate

    private void generatePDF(){
        // Check for SD Card
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            showToast("Error! No SDCARD Found!", Toast.LENGTH_LONG);
            return;
        } else {
            CintaaDirectory= new File(Environment.getExternalStorageDirectory().toString() + "/CINTAA/Backup");
            if (!CintaaDirectory.exists()) {
                CintaaDirectory.mkdirs();
            }
            String filename = "CINTAA_Backup_"+getCurrentTimeStamp()+".pdf";
            final File file = new File(CintaaDirectory,filename );
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file);
                Document document = new Document(PageSize.A4);
                document.open();
                PdfWriter.getInstance(document, fos);
                GeneratePdf pdf=new GeneratePdf(getActivity());
                pdf.addPage(document,"CINTAA App",filename);
                //document.close();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

    }//generatePDF

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd_MM_yyyy_HHmmss", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentTimeStamp

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getContext(), message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        toast.show();
    }

}
