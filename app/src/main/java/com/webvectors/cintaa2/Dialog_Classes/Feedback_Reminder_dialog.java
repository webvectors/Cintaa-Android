package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ASUS on 16/09/2016.
 */
public class Feedback_Reminder_dialog extends DialogFragment {
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        //return super.onCreateDialog(savedInstanceState);
        LayoutInflater li = LayoutInflater.from(getActivity());
        final Dialog dialog1 = new Dialog(getActivity());
        dialog1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog1.getWindow().getAttributes();
        lp.dimAmount = 1000f;
        dialog1.setContentView(li.inflate(R.layout.remind_after_dialog, null));
        final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

        TextView tv30day,tv21day,tv7day,tv1day;
        tv30day = (TextView) dialog1.findViewById(R.id.tv30day);
        tv21day = (TextView) dialog1.findViewById(R.id.tv21day);
        tv7day = (TextView) dialog1.findViewById(R.id.tv7day);
        tv1day = (TextView) dialog1.findViewById(R.id.tv1day);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tv30day.setTypeface(custom_font);
        tv21day.setTypeface(custom_font);
        tv7day.setTypeface(custom_font);
        tv1day.setTypeface(custom_font);

        tv1day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currDate = getCurrentDate();

                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime( sdfDate.parse(currDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add( Calendar.DATE, 1 );
                String AfterDate=sdfDate.format(cal.getTime());

                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_FeedbackReminder set feed_date='"+AfterDate+"';");
                int n = sqLiteStatement.executeUpdateDelete();

                dialog1.dismiss();
            }
        });
        tv7day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currDate = getCurrentDate();

                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime( sdfDate.parse(currDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add( Calendar.DATE, 7 );
                String AfterDate=sdfDate.format(cal.getTime());

                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_FeedbackReminder set feed_date='"+AfterDate+"';");
                int n = sqLiteStatement.executeUpdateDelete();

                dialog1.dismiss();
            }
        });
        tv21day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currDate = getCurrentDate();

                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime( sdfDate.parse(currDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add( Calendar.DATE, 21 );
                String AfterDate=sdfDate.format(cal.getTime());

                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_FeedbackReminder set feed_date='"+AfterDate+"';");
                int n = sqLiteStatement.executeUpdateDelete();

                dialog1.dismiss();
            }
        });
        tv30day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currDate = getCurrentDate();

                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime( sdfDate.parse(currDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add( Calendar.DATE, 30 );
                String AfterDate=sdfDate.format(cal.getTime());

                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_FeedbackReminder set feed_date='"+AfterDate+"';");
                int n = sqLiteStatement.executeUpdateDelete();

                dialog1.dismiss();
            }
        });
//        dialog1.show();
        return dialog1;
    }//onCreateDialog

    public static String getCurrentDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentDate
}
