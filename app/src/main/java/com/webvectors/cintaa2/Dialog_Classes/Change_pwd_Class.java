package com.webvectors.cintaa2.Dialog_Classes;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.R;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ASUS on 16/09/2016.
 */
public class Change_pwd_Class extends DialogFragment {

    EditText etnewpwd,etconfirmpwd,etoldpwd;
    int check1=0;
    SharedPreferences preferences;
    AlertDialog.Builder builder;
    String member_id;
    ProgressDialog pd;
    Context context;
    Dialog dialog ;


  /* public interface YesNoListener {
        void onYes();

        void onNo();
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
       /* if (!(activity instanceof YesNoListener)) {
            throw new ClassCastException(activity.toString() + " must implement YesNoListener");
        }*/
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       // return super.onCreateDialog(savedInstanceState);

        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id=  preferences.getString("member_id","abc");
        pd=new ProgressDialog(getActivity());
        context = getActivity();

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.change_password_dialog, null);
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.change_password_dialog
                , null));
        // set prompts.xml to alertdialog builder
        //alertDialogBuilder.setView(promptsView);
        etnewpwd = (EditText) dialog.findViewById(R.id.etnewpwd);
        etconfirmpwd = (EditText) dialog.findViewById(R.id.etconfirmpwd);
        TextView tvchangepwd = (TextView) dialog.findViewById(R.id.tvchangepwd);
        TextView tvoldpwd = (TextView) dialog.findViewById(R.id.tvoldpwd);
        etoldpwd = (EditText) dialog.findViewById(R.id.etoldpwd);

        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        etconfirmpwd.setTypeface(custom_font);
        etnewpwd.setTypeface(custom_font);
        tvoldpwd.setTypeface(custom_font);
        etoldpwd.setTypeface(custom_font);
        tvchangepwd.setTypeface(custom_font);
        btnCancel.setTypeface(custom_font);
        btnOk.setTypeface(custom_font);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationsalertbuilder();
                if(check1==4)
                {
                    setpassword();


                }

            }
        });
        // set dialog message



        // create alert dialog






        // show it
        dialog.show();
        return dialog;

    }//OnCreateDialog

    public void validationsalertbuilder() {

        String old_pwd=  preferences.getString("password","abc");
        check1=0;
        if(!old_pwd.equals(etoldpwd.getText().toString().trim()))
        {
            etoldpwd.setError("Current Password is Wrong");
            showToast("Current Password is Wrong",Toast.LENGTH_LONG);
        }
        else {
            check1++;
        }

        if(!etnewpwd.getText().toString().equals(etconfirmpwd.getText().toString()))
        {
            etconfirmpwd.setError("New Password and Confirm Password Does Not Match");
            showToast("New Password and Confirm Password Does Not Match",Toast.LENGTH_LONG);
        }
        else {
            check1++;
        }
        if (etnewpwd.getText().toString().length() == 0  ) {
            etnewpwd.setError("New Password is Missing");
            showToast("New Password is Missing",Toast.LENGTH_LONG);
        }
        else{
            check1++;
        }
        if (etconfirmpwd.getText().toString().length()==0) {
            etconfirmpwd.setError("Confirm Password is Missing");
            showToast("Confirm Password is Missing",Toast.LENGTH_LONG);

        }else{check1++;}
        return;
    }

    public void setpassword()
    {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("password", etconfirmpwd.getText().toString().trim());
        final String new_pass = etconfirmpwd.getText().toString().trim();

        client.post(Config.URLchangepassword, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    // Toast.makeText(getActivity(), "Profile updated", Toast.LENGTH_LONG).show();
                    //   Intent intent=new Intent(getActivity(),RegisterLoginActivity.class);
                    // startActivity(intent);
                    SharedPreferences.Editor editor1;
                    editor1=preferences.edit();
                    editor1.putString("password",new_pass);
                    editor1.commit();

                    showToast("Password change successfully", Toast.LENGTH_LONG);
                    dialog.dismiss();
                    etnewpwd.setText("");
                    etconfirmpwd.setText("");

                    String s1 = response.getString("success");

                    //  Intent intent=new Intent(getActivity(),HomeActivity.class);
                    //  startActivity(intent);
                    //    Toast.makeText(getActivity(), s1, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                dialog.dismiss();
            }
        });
        pd.dismiss();

    }

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(context, message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        toast.show();
    }

}
