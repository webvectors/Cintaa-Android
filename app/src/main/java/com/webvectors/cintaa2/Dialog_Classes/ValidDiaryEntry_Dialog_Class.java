package com.webvectors.cintaa2.Dialog_Classes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Button;

import com.webvectors.cintaa2.R;

/**
 * Created by ASUS on 17/09/2016.
 */
public class ValidDiaryEntry_Dialog_Class extends DialogFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Shootlocation is missing, Auto Callin is Disabled for this Entry.");

        dialogBuilder.setCancelable(false);

        dialogBuilder.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();

        alertDialogObject.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                Button negativeButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.colorAccent);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.colorAccent);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.bgColor);
                if (Build.VERSION.SDK_INT >= 16) {
                      negativeButton.setBackground(negativeButtonDrawable);
                      positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                   negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                     negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });
        //Show the dialog
        return alertDialogObject;
    }//OnCreate
}
