package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.fragment.ScheduleEntryEditFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Admin on 12/27/2016.
 */

public class ScheduleNewEntryOrEditCurrentDialog extends DialogFragment {

    String entry_id,call_Status,scheduleCompetedStatus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String dateOfShoot = getArguments().getString("dateOfShoot");
        final ArrayList<String> entryId = getArguments().getStringArrayList("entryId");
        final ArrayList<String> filmName = getArguments().getStringArrayList("filmName");
        final ArrayList<String> callStatus = getArguments().getStringArrayList("callStatus");
        final ArrayList<String> scheduleStatus = getArguments().getStringArrayList("scheduleStatus");

        LayoutInflater li = LayoutInflater.from(getActivity());
        final Dialog dialog1 = new Dialog(getActivity());
        dialog1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog1.getWindow().getAttributes();
        lp.dimAmount = 1000f;
        dialog1.setContentView(li.inflate(R.layout.schedule_new_entry_or_edit_current_dialog, null));
        final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

        LinearLayout llfilmName;
        Button btnEdit,btnCreateNew;
        final TextView tvTitle,tvfilmName;
        tvfilmName = (TextView) dialog1.findViewById(R.id.tvfilmName);
        tvTitle = (TextView) dialog1.findViewById(R.id.tvTitle);
        llfilmName = (LinearLayout)dialog1.findViewById(R.id.llfilmName);
        btnCreateNew = (Button)dialog1.findViewById(R.id.btnCreateNew);
        btnEdit = (Button)dialog1.findViewById(R.id.btnEdit);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvTitle.setTypeface(custom_font);
        tvfilmName.setTypeface(custom_font);

        tvTitle.setText("Schedule: "+dateOfShoot);

        llfilmName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog1(filmName,entryId,callStatus,tvfilmName,scheduleStatus);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvfilmName.getText().toString().trim().length()>0){
                    if (scheduleCompetedStatus.equals("completed")){
                        showToast("This Entry is Completed",Toast.LENGTH_SHORT);
                    }else{
                        if(call_Status.equalsIgnoreCase("true")){
                            showToast("In Time Recorded, Cannot Edit This Entry",Toast.LENGTH_SHORT);
                            //Toast.makeText(getActivity(),"In Time Recorded, Cannot Edit This Entry",Toast.LENGTH_SHORT).show();
                        }else {
                            dialog1.dismiss();
                            Fragment fragment = new ScheduleEntryEditFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("scheduleEntryDate", dateOfShoot);
                            bundle.putString("entryID",entry_id);
                            bundle.putString("filmName",tvfilmName.getText().toString().trim());
                            fragment.setArguments(bundle);
                            getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                        }
                    }
                    
                }else {
                    Toast.makeText(getActivity(),"Select Film To Edit Schedule",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCreateNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                Fragment fragment = new ScheduleEntryEditFragment();
                Bundle bundle = new Bundle();
                bundle.putString("scheduleEntryDate", dateOfShoot);
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        });

        return dialog1;
    }//onCreateDialog

    private void showdialog1(final ArrayList<String> filmName, final ArrayList<String> entryId, final ArrayList<String> callStatus, final TextView tvfilmName, final ArrayList<String> schedule_Status)
    {
        try
        {
            View customTitle = View.inflate(getActivity(), R.layout.titlefilmname, null);
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setCustomTitle(customTitle);
            final String[] arr = new String[filmName.size()];
            filmName.toArray(arr);
            int selected = -1;
            String currentValue = tvfilmName.getText().toString().trim();
            for (int i = 0; i < arr.length; i++)
            {
                if (currentValue.equals(arr[i]))
                {
                    selected = i;
                    break;
                }
            }
            builder1.setSingleChoiceItems(arr, selected, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    tvfilmName.setText(arr[i]);
                    entry_id = entryId.get(i);
                    call_Status = callStatus.get(i);
                    scheduleCompetedStatus = schedule_Status.get(i);
                    dialogInterface.cancel();
                }
            });
            builder1.show();
        } catch (Exception e)
        {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialog1

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getActivity(), message, time);
        View toastView = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getActivity()), CommonHelper.convertDpToPx(5, getActivity()), CommonHelper.convertDpToPx(10, getActivity()), CommonHelper.convertDpToPx(5, getActivity()));
        toast.show();
    }
}
