package com.webvectors.cintaa2.Dialog_Classes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.webvectors.cintaa2.adapter.ListViewPaymentDetails;
import com.webvectors.cintaa2.R;

import java.util.ArrayList;

/**
 * Created by ASUS on 17/09/2016.
 */
public class PaymentAfterList_Dialog_Class extends DialogFragment {


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ArrayList<String> entry_id = getArguments().getStringArrayList("entry_id");
        final ArrayList<String> member_id = getArguments().getStringArrayList("member_id");
        final ArrayList<String> film_tv_name = getArguments().getStringArrayList("film_tv_name");
        final ArrayList<String> paymentAfter = getArguments().getStringArrayList("paymentAfter");
        final ArrayList<String> shoot_date = getArguments().getStringArrayList("shoot_date");
        final ArrayList<String> rateType = getArguments().getStringArrayList("rateType");
        final ArrayList<String> rate = getArguments().getStringArrayList("rate");

        final ArrayList<String> prodHouse = getArguments().getStringArrayList("prodHouse");
        final ArrayList<String> prodName = getArguments().getStringArrayList("prodName");

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.payment_reminder_data, null);
        TextView title = (TextView) promptsView.findViewById(R.id.tvtitle);
        title.setText(R.string.payment_reminder);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        title.setTypeface(custom_font);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        ListViewPaymentDetails customadaptor = new ListViewPaymentDetails(getActivity(),entry_id,member_id,film_tv_name,paymentAfter,shoot_date,rateType,rate,prodHouse,prodName);
        ListView listViewDetails = (ListView) promptsView.findViewById(R.id.lvDetails) ;
        listViewDetails.setAdapter(customadaptor);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                // get user input and set it to result
                // edit text

            }
        });

        /*alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                //  Button negativeButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                //negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                //final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                //final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.btn_backColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.btn_text);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.btn_backColor);
                if (Build.VERSION.SDK_INT >= 16) {
                    //  negativeButton.setBackground(negativeButtonDrawable);
                    //  positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    //     negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    //    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                    // negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    //positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    //    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    //   negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                //   negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });

        // show it
//        alertDialog.show();
        return alertDialog;
    }//onCreateDialog
}
