package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

/**
 * Created by ASUS on 16/09/2016.
 */
public class HelpCenterDialog extends DialogFragment {



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.change_password_dialog, null);
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.help_center_dialog
                , null));

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        TextView tvTitle,tvcontactwhatsapp,tvcontactcell,tvcontactemail;
        tvTitle = (TextView)dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(custom_font);
        tvcontactwhatsapp = (TextView)dialog.findViewById(R.id.tvcontactwhatsapp);
        tvcontactwhatsapp.setTypeface(custom_font);
        tvcontactcell = (TextView)dialog.findViewById(R.id.tvcontactcell);
        tvcontactcell.setTypeface(custom_font);
        tvcontactemail = (TextView)dialog.findViewById(R.id.tvcontactemail);
        tvcontactemail.setTypeface(custom_font);
        Button btnClose = (Button)dialog.findViewById(R.id.btnClose);
        btnClose.setTypeface(custom_font);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        // show it
        dialog.show();
        return dialog;

    }//onCreateDialog
}
