package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ASUS on 16/09/2016.
 */
public class GenerateReportDialogue extends DialogFragment {


    TextView tvTitle,tvsetTime,tvFrom,tvSetFrom,tvTo,tvSetTo;
    Button btnCancel,btnOk;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.change_password_dialog, null);
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.report_generate_dialogue, null));

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvTitle = (TextView)dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(custom_font);
        tvsetTime = (TextView)dialog.findViewById(R.id.tvsetTime);
        tvsetTime.setTypeface(custom_font);
        tvFrom = (TextView)dialog.findViewById(R.id.tvFrom);
        tvFrom.setTypeface(custom_font);
        tvSetFrom= (TextView)dialog.findViewById(R.id.tvSetFrom);
        tvSetFrom.setTypeface(custom_font);
        tvSetTo= (TextView)dialog.findViewById(R.id.tvSetTo);
        tvSetTo.setTypeface(custom_font);
        tvTo= (TextView)dialog.findViewById(R.id.tvTo);
        tvTo.setTypeface(custom_font);
        btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(custom_font);
        btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setTypeface(custom_font);

        final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        String currentDate = dateFormatter1.format(cal.getTime());
        tvSetTo.setText("" + currentDate);

        tvSetFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendarFrom();
            }
        });

        tvSetTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendarTo();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    dialog.dismiss();
                }
            }
        });

        // show it
        dialog.show();
        return dialog;

    }//onCreateDialog

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerd = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener()
    {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
        {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10)
            {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10)
            {
                datee = "0" + dayOfMonth;
            }

            //String date = datee + "/" + month + "/" + year;
            String date = year + "-" + month + "-" + datee;
            tvSetFrom.setText(date);
        }
    };

    public void showCalendarFrom()
    {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerd,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));


        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

    }//showCalendarFrom

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerTo = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener()
    {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
        {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10)
            {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10)
            {
                datee = "0" + dayOfMonth;
            }

            String date = year + "-" + month + "-" + datee;
            tvSetTo.setText(date);
        }
    };

    public void showCalendarTo()
    {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerTo,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));


        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

    }//showCalendarTo

    private Boolean validate(){
        int check = 0;
        if(tvSetFrom.getText().length()==0){
            tvSetFrom.setError("Set Start Date");
            showToast(" Set Start Date ",Toast.LENGTH_SHORT);
            ++check;
        }
        if(tvSetTo.getText().length()==0){
            tvSetTo.setError("Set End Date");
            showToast(" Set End Date ",Toast.LENGTH_SHORT);
            ++check;
        }
        if(check==0){
            String startDateStr,endDateStr;
            startDateStr = tvSetFrom.getText().toString().trim();
            endDateStr = tvSetTo.getText().toString().trim();

            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);//dd/MM/yyyy
            Date startDate = new Date();
            Date endDate = new Date();
            try {
                startDate = sdfDate.parse(startDateStr);
                endDate = sdfDate.parse(endDateStr);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }//SELECT shoot_date FROM tb_diary_entry WHERE STR_TO_DATE(shoot_date, '%d/%m/%Y') BETWEEN '2016-07-18' AND '2016-07-18';
            if (!startDate.before(endDate)){
                if(!startDate.equals(endDate)){
                    ++check;
                    showToast(" Start Date Should Be Before or Equal to End Date ",Toast.LENGTH_SHORT);
                }
            }
        }

        if (check>0){
            return false;
        }else {
            return true;
        }
    }//validate

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getContext(), message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        toast.show();
    }//showToast
}
