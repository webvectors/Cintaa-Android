package com.webvectors.cintaa2.Dialog_Classes;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Ankita.
 */
public class Generate_report_Class extends DialogFragment {

    List<String> list_entry_id = new ArrayList<>();
    List<String> film_nm = new ArrayList<>();
    private ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();

    SharedPreferences preferences;
    AlertDialog.Builder builder;
    String member_id;
    private String mStartDate, mEndDate;

    Context context;

    String filepath = "Cintaa";
    String filename = "CompletedShoots.pdf";
    private File pdfFile;
    private Dialog dialog;
    private ProgressDialog dialog2;
    private ProgressDialog pd;
    private EditText edtEndDate, edtStartDate;
    private LinearLayout llStartDate, llEndDate;
    private ArrayList<HashMap<String, String>> hashMaps2 = new ArrayList<>();
    private ArrayList<HashMap<String, String>> hashMaps1 = new ArrayList<>();
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteDB sqLiteDB;

    Cursor cursor;
    private ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();
    private String mEmail;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
       /* if (!(activity instanceof YesNoListener)) {
            throw new ClassCastException(activity.toString() + " must implement YesNoListener");
        }*/
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // return super.onCreateDialog(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        pd = new ProgressDialog(getActivity());
        context = getActivity();
        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.change_password_dialog, null);
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.generate_report, null));
        // set prompts.xml to alertdialog builder
        //alertDialogBuilder.setView(promptsView);
        edtEndDate = (EditText) dialog.findViewById(R.id.edtEndDate);
        llEndDate = (LinearLayout) dialog.findViewById(R.id.llEndDate);
        llStartDate = (LinearLayout) dialog.findViewById(R.id.llStartDate);
        edtStartDate = (EditText) dialog.findViewById(R.id.edtStartDate);
        llStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowStartTimePicker();
            }
        });
        llEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowEndTimePicker();
            }
        });
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        edtEndDate.setTypeface(custom_font);
        edtStartDate.setTypeface(custom_font);
        btnCancel.setTypeface(custom_font);
        btnOk.setTypeface(custom_font);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pdfFile = new File(getActivity().getExternalFilesDir(filepath), filename);

                selectdataDateWise();

                try {
                    pdfFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }


        });
        dialog.show();
        return dialog;

    }

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerStartDate = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            mStartDate = datee + "/" + month + "/" + year;
//            autosearch.setText(date);
            edtStartDate.setText(mStartDate);

            //....
//            Search();
//            SearchByDate(tvStartDate.getText().toString().trim());
//            ivsearchclear.setVisibility(View.VISIBLE);
//            ivsearch.setVisibility(View.VISIBLE);
//            llDateSearch.setVisibility(View.VISIBLE);
//            llsearch.setVisibility(View.GONE);
        }
    };
    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerLastDate = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            mEndDate = datee + "/" + month + "/" + year;
//            autosearch.setText(date);
            edtEndDate.setText(mEndDate);
            //....
//            Search();


//            llDateSearch.setVisibility(View.VISIBLE);
//            llsearch.setVisibility(View.GONE);

        }
    };

    private void ShowStartTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerStartDate,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowStartTimePicker

    private void ShowEndTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerLastDate,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowStartTimePicker

    private void selectdataDateWise() {
        GlobalClass.printLog(getActivity(), "-------mStartDate-----" + mStartDate);
        GlobalClass.printLog(getActivity(), "-------mEndDate-----" + mEndDate);
        if (mStartDate != null && mEndDate != null) {
            Date startDate, endDate;
            try {
                GlobalClass.printLog(getActivity(), "-------mStartDate-----" + mStartDate);
                GlobalClass.printLog(getActivity(), "-------mEndDate-----" + mEndDate);

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                startDate = formatter.parse(mStartDate);
                endDate = formatter.parse(mEndDate);

                if (endDate.after(startDate) || endDate.equals(startDate)) {

                    hashMaps2 = new ArrayList<>();
                    for (int i = 0; i < getDaysBetweenDates(startDate, endDate).size(); i++) {
                        hashMaps1.clear();
                        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String dateStr = getDaysBetweenDates(startDate, endDate).get(i).toString().trim();
                        Date date = inputFormat.parse(dateStr);
                        String str = outputFormat.format(date);
                        hashMaps1 = getSearchByDate(str);
//                                    hashMaps1.addAll(getSearchByDate(mEndDate));

                        hashMaps2.addAll(hashMaps1);
                    }

                    if (list_entry_id.size() > 0) {
                        GlobalClass.printLog(getActivity(), "-------hashMaps2--CCCCCCCCCC---" + hashMaps2.size());
                        new PdfGenerationTask().execute();

                    } else {
                        //On Result Found
                        showToast(getString(R.string.no_result_found), Toast.LENGTH_LONG);
                    }
                } else {
                    showToast(getString(R.string.startdate_should_before_enddate), Toast.LENGTH_SHORT);
                }

            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        } else {
            showToast(getString(R.string.please_select_proper_date), Toast.LENGTH_SHORT);
        }
    }

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate) || calendar.getTime().equals(enddate)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public void displaydetails(String entry_id, final PdfPTable table) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", entry_id);
        client.post(Config.URLcompletedscheduledetails, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("CompletedDetails");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
//                            tvflimnmstr = j1.getString("film_tv_name");
//                            tvphousestr = house_name;
//                            tvprodnmstr = j1.getString("producer_name");
//                            tvshootdatestr = j1.getString("shoot_date");
//                            tvlocationstr = j1.getString("shoot_location");
//                            tvcharnmstr = j1.getString("played_character");
//                            tvshifttimestr = j1.getString("shift_time");
//                            tvcallininstr = j1.getString("call_time");
//                            tvratestr = j1.getString("rate") + " " + j1.getString("rate_type");
//                            tvduedaysstr = j1.getString("due_days") + " days";
//                            String remark = j1.getString("remark");
//                            if (remark.equals("null")) {
//                                tvremarkstr = "No Remark";
//                            } else {
//                                tvremarkstr = remark;
//                            }
//                            tvcallinstr = j1.getString("callin_time");
//                            tvpackupstr = j1.getString("packup_time");
//                            tvauthoritynmstr = j1.getString("authority_nm");
//                            tvauthoritynostr = j1.getString("authority_number");
//                            ivauthoritysignstr = j1.getString("authority_sign");
//                            fees = j1.getString("fees");
//                            tvauthorityemailstr = j1.getString("house_email");
//                            rater = j1.getString("rate");
//
//                            tvauthosigndatestr = j1.getString("authority_sign_date");
                            /////////////////////////////////////////////////////////

                            Font dateFont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, BaseColor.BLACK);

                            table.addCell(createCell(j1.getString("shoot_date"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("film_tv_name"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("house_name"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("shoot_location"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("played_character"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("call_time"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("callin_time"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("packup_time"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("rate"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("rate"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("authority_nm"), 2, 1, Element.ALIGN_CENTER, dateFont));
                            table.addCell(createCell(j1.getString("authority_sign_date"), 2, 1, Element.ALIGN_CENTER, dateFont));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }


    private ArrayList<HashMap<String, String>> getSearchByDate(String date) {              //..Search By Date
        cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE shoot_date LIKE ?",
                new String[]{"%" + date + "%"});
        if (cursor.moveToFirst()) {
            hashMaps = new ArrayList<HashMap<String, String>>();
            list_entry_id.clear();
            do {
                HashMap<String, String> temp1 = new HashMap<String, String>();
                temp1.put("shoot_date", cursor.getString(0));
                temp1.put("house_name", cursor.getString(1));
                temp1.put("shoot_location", cursor.getString(2));
                temp1.put("shift_time", cursor.getString(3));
                hashMaps.add(temp1);
                list_entry_id.add(cursor.getString(4));
                GlobalClass.printLog(getActivity(), "---list_entry_id.size===AAAAAAAAAAA====---" + list_entry_id.size());

            } while (cursor.moveToNext());

        }
        return hashMaps;
    }

    private class PdfGenerationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog2 = new ProgressDialog(getActivity());
            dialog2.setMessage(getString(R.string.please_wait));
            dialog2.setCancelable(false);
            dialog2.show();
        }

        public class PDFBackground extends PdfPageEventHelper {

            @Override
            public void onEndPage(PdfWriter writer, Document document) {

                PdfContentByte canvas = writer.getDirectContentUnder();
                Drawable drawable = getResources().getDrawable(R.drawable.bg_plain);
                BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
                Bitmap bitmap = bitmapDrawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image backgroundImage;

                try {
                    backgroundImage = Image.getInstance(stream.toByteArray());
                    float width = document.getPageSize().getWidth();
                    float height = document.getPageSize().getHeight();

                    writer.getDirectContentUnder().addImage(backgroundImage, width, 0, 0, height, 0, 0);
                } catch (DocumentException | IOException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        protected String doInBackground(Void... params) {

//            left,Right,top,Bottom
            Document document = new Document(PageSize.A4.rotate(), 20, 20, 20, 50);
            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
//                set Background in PDF
                writer.setPageEvent(new PDFBackground());

                // For set title of PDF
                Font headingFont = new Font(Font.FontFamily.HELVETICA, 22, Font.BOLD, BaseColor.BLACK);
                Paragraph TitleName = new Paragraph(22);
                TitleName.setSpacingAfter(5);
                TitleName.setAlignment(Element.ALIGN_CENTER);
                TitleName.setFont(headingFont);
//                TitleName.setIndentationLeft(50);
//                TitleName.setIndentationRight(50);
                TitleName.add("COMPLETED SHOOT");

                document.open();
                document.add(TitleName);

                LineSeparator ls = new LineSeparator();
                ls.setLineColor(BaseColor.BLACK);
                document.add(new Chunk(ls));
                document.add(Chunk.NEWLINE);

                Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
                Font dateFont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, BaseColor.BLACK);
                float[] column = {1f, 10f, 0.1f, 4.5f};
                //create PDF table with the given widths
                PdfPTable table = new PdfPTable(4);
                table.setWidths(new int[]{1, 2, 2, 1});
                table.addCell(createCell("Shoot Date", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Film/TV serial Name", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Production House Name", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Shoot Location", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Character Name", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Call time", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("In time", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Packup Time", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Rate", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Conv.", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Authority Name", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Authority Sign Date&Time", 2, 1, Element.ALIGN_CENTER, titleFont));

                table.setSplitLate(false);
                table.setSplitRows(false);
                table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                table.getDefaultCell().setPadding(56);
                table.setTotalWidth(document.right() - document.left());
                table.setWidthPercentage(80);
                table.setLockedWidth(true);
//                table.setHeaderRows(1);
                table.setWidthPercentage(86f);

                GlobalClass.printLog(getActivity(), "---list_entry_id.size=======---" + list_entry_id.size());


                for (int i = 0; i < list_entry_id.size(); i++) {
                    GlobalClass.printLog(getActivity(), "------" + list_entry_id.get(i));
                    displaydetails(list_entry_id.get(i), table);
                }

//                for (int i = 0; i < hashMaps2.size(); i++) {
//                    HashMap<String, String> map = hashMaps2.get(i);
//                    String[] parts = map.get(shift_time).split("to");
//                    String part1 = parts[0];
//
//                    String shoot_date1 = map.get(shoot_date);
//                    String house_name1 = map.get(house_name);
//                    String shoot_location1 = map.get(shoot_location);
//                    table.addCell(createCell(shoot_date1, 2, 1, Element.ALIGN_CENTER, dateFont));
//                    table.addCell(createCell(house_name1, 2, 1, Element.ALIGN_CENTER, dateFont));
//                    table.addCell(createCell(shoot_location1, 2, 1, Element.ALIGN_CENTER, dateFont));
//                    table.addCell(createCell(part1, 2, 1, Element.ALIGN_CENTER, dateFont));
//                }
                document.add(table);
//                if (writer.getVerticalPosition(true) - 15 <= document.bottom()) {
//                    document.newPage();
//                }
                //add footer on the Last page of pdf
                PdfContentByte cb = writer.getDirectContent();
                Font footerFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
                //footer content
                String footerContent = "Copyright 2017 WebVectors Inc. All Rights Reserved.";
                String disclaimer = "Disclaimer: This PDF is generated using Cintaa mobile application.";

            /*
             * Footer
             */
                // copyRight String
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(footerContent, footerFont),
                        (document.left() + document.right()) / 2, 40, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer, footerFont),
                        (document.left() + document.right()) / 2, 25, 0);
//                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer_two, footerFont),
//                        (document.left() + document.right()) / 2, 10, 0);

                document.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            showToast("Report Gnerated", Toast.LENGTH_SHORT);

            mailPDF();
        }
    }

    private void mailPDF() {

        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_UserProfile", null);
        if (cursor.moveToFirst()) {
            do {
                mEmail = cursor.getString(7);
            } while (cursor.moveToNext());
        }
        //file should contain path of pdf file
        Uri path = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", pdfFile);

        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, mEmail);

        intent.putExtra(Intent.EXTRA_SUBJECT, "Completed Shoot Report");
        intent.putExtra(Intent.EXTRA_TEXT, mStartDate + " to " + mEndDate);
        intent.putExtra(Intent.EXTRA_STREAM, path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
        if (dialog2.isShowing() && dialog2 != null)
            dialog2.dismiss();
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    public PdfPCell createCell(String content, float borderWidth, int colspan, int alignment, Font fontStyle) {
        PdfPCell cell = new PdfPCell(new Phrase(content, fontStyle));
        cell.setBorderWidth(1);
        cell.setColspan(colspan);
        cell.setBorder(Rectangle.BOX);
        cell.setUseBorderPadding(true);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }

    private class MapComparator implements Comparator<Map<String, String>> {
        private final String key;

        MapComparator(String key) {
            this.key = key;
        }

        public int compare(Map<String, String> first,
                           Map<String, String> second) {
            // TODO: Null checking, both for maps and values
            String firstValue = first.get(key);
            String secondValue = second.get(key);
            return firstValue.compareTo(secondValue);
        }
    }

    public void showToast(String message, int time) {
        Toast toast = Toast.makeText(context, message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable(getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground(getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        toast.show();
    }

}
