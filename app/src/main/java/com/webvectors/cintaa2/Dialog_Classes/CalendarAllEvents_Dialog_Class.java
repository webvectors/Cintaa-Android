package com.webvectors.cintaa2.Dialog_Classes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.webvectors.cintaa2.adapter.ListViewDetailsCalendar;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 17/09/2016.
 */
public class CalendarAllEvents_Dialog_Class extends DialogFragment {

    List<String> DShiftTime,DLocation,DFilm,DProdHouse,DCallTime;
    String selectedDate;
    SharedPreferences preferences;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor, cur;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();
        final String dateOfShoot = getArguments().getString("dateOfShoot");
        final ArrayList<String> entryId = getArguments().getStringArrayList("entryId");

        DShiftTime = new ArrayList<>();
        DLocation = new ArrayList<>();
        DFilm = new ArrayList<>();
        DProdHouse = new ArrayList<>();
        DCallTime = new ArrayList<>();

        try{
            for(String id: entryId){
                cursor = null; cur = null;
                cursor = sqLiteDatabaseRead.rawQuery("select filmName, productionHouse, producerName, entryId, scheduleDate, scheduleStatus from tbScheduleDetails where entryId = '"+id+"'", null);
                if (cursor.moveToFirst()){
                    DShiftTime.add(cursor.getString(0));
                    DFilm.add(cursor.getString(1));
                    DLocation.add(cursor.getString(2));
                    DProdHouse.add(cursor.getString(2));


                    cur = sqLiteDatabaseRead.rawQuery("select call_time from sqtb_diary_entry where entry_id='"+id+"'",null);
                    if(cur.moveToFirst()){
                        DCallTime.add(cur.getString(0));
                    }else{
                        DCallTime.add(" NA");
                    }

                }

            }
        }catch (Exception e){

        }finally {
            if(cursor!=null)
                cursor.close();
        }




        /*preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        selectedDate = preferences.getString("CalendarSelected_date","");
        int list_size = preferences.getInt("CalendarEvents_List",0);
        for(int s=0; s<list_size;s++){
            DShiftTime.add(preferences.getString("CalendarEvents_List_DShiftTime_"+s,""));
            DLocation.add(preferences.getString("CalendarEvents_List_DLocation_"+s,""));
            DFilm.add(preferences.getString("CalendarEvents_List_DFilm_"+s,""));
            DProdHouse.add(preferences.getString("CalendarEvents_List_DProdHouse_"+s,""));
            DCallTime.add(preferences.getString("CalendarEvents_List_DCallTime_"+s,""));
        }*/

        selectedDate = dateOfShoot;

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.all_events_today, null);
        TextView title = (TextView) promptsView.findViewById(R.id.tvtitle);
        title.setText("Date: "+selectedDate);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        title.setTypeface(custom_font);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        ListViewDetailsCalendar customadaptor = new ListViewDetailsCalendar(getActivity(),DShiftTime,DLocation,DFilm,DProdHouse,DCallTime);
        ListView listViewDetails = (ListView) promptsView.findViewById(R.id.lvDetails) ;
        listViewDetails.setAdapter(customadaptor);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                // get user input and set it to result
                // edit text

            }
        });

        /*alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                //  Button negativeButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                //negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                //final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                //final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.btn_backColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.btn_text);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.btn_backColor);
                if (Build.VERSION.SDK_INT >= 16) {
                    //  negativeButton.setBackground(negativeButtonDrawable);
                    //  positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    //     negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    //    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                    // negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    //positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    //    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    //   negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                //   negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });

        // show it
//        alertDialog.show();
        return alertDialog;
    }//onCreateDialog
}
