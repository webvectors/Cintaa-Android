package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ASUS on 16/09/2016.
 */
public class CompleteEvents_Dialog_Class extends DialogFragment {

    String tvcallininstr,tvflimnmstr,ivauthoritysignstr, tvphousestr, tvprodnmstr, tvshootdatestr, tvlocationstr, tvcharnmstr, tvshifttimestr, tvratestr, tvduedaysstr, tvremarkstr,tvauthoritynmstr,tvcallinstr,tvpackupstr,tvauthosigndatestr,tvauthoritynostr,tvauthoritymailstr;
    SharedPreferences preferences;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);

        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());

        tvcallininstr = preferences.getString("tvcallininstr_cmplt","abc");
        tvflimnmstr = preferences.getString("tvflimnmstr_cmplt","abc");
        ivauthoritysignstr = preferences.getString("ivauthoritysignstr_cmplt","abc");
        tvphousestr = preferences.getString("tvphousestr_cmplt","abc");
        tvprodnmstr = preferences.getString("tvprodnmstr_cmplt","abc");
        tvshootdatestr = preferences.getString("tvshootdatestr_cmplt","abc");
        tvlocationstr = preferences.getString("tvlocationstr_cmplt","abc");
        tvcharnmstr = preferences.getString("tvcharnmstr_cmplt","abc");
        tvshifttimestr = preferences.getString("tvshifttimestr_cmplt","abc");
        tvratestr = preferences.getString("tvratestr_cmplt","abc");
        tvduedaysstr = preferences.getString("tvduedaysstr_cmplt","abc");
        tvremarkstr = preferences.getString("tvremarkstr_cmplt","abc");
        tvauthoritynmstr = preferences.getString("tvauthoritynmstr_cmplt","abc");
        tvcallinstr = preferences.getString("tvcallinstr_cmplt","abc");
        tvpackupstr = preferences.getString("tvpackupstr_cmplt","abc");
        tvauthosigndatestr = preferences.getString("tvauthosigndatestr_cmplt","abc");
        tvauthoritynostr = preferences.getString("tvauthoritynostr_cmplt","abc");
        tvauthoritymailstr = preferences.getString("tvauthorityemailstr_cmplt","abc");

        String fees = preferences.getString("fees_cmplt","abc");
        String rater_cmplt = preferences.getString("rater_cmplt","abc");


        LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.completescheduledetail, null);
        // entry_id= sharedPreferences.getString("Completed_list_item","xcz");
        //final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.completescheduledetail
                , null));
        // set prompts.xml to alertdialog builder
        // alertDialogBuilder.setView(promptsView);
        Typeface customfont = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        TextView tvcomfilmname = (TextView) dialog.findViewById(R.id.tvcomfilmname);
        tvcomfilmname.setTypeface(customfont);
        TextView tvconveyance = (TextView)dialog.findViewById(R.id.tvconveyance);
        tvconveyance.setTypeface(customfont);
        TextView  tvcomprodhouname = (TextView) dialog.findViewById(R.id.tvcomprodhouname);
        tvcomprodhouname.setTypeface(customfont);
        TextView tvcomprodname = (TextView) dialog.findViewById(R.id.tvcomprodname);
        tvcomprodname.setTypeface(customfont);
        TextView tvcomdateofshoot = (TextView) dialog.findViewById(R.id.tvcomdtshoot);
        tvcomdateofshoot.setTypeface(customfont);
        TextView  tvcomloc = (TextView) dialog.findViewById(R.id.tvcomshootloc);
        tvcomloc.setTypeface(customfont);
        TextView  tvcomcharname = (TextView) dialog.findViewById(R.id.tvcomcharname);
        tvcomcharname.setTypeface(customfont);
        TextView  tvcomshifttime = (TextView) dialog.findViewById(R.id.tvcomshifttime);
        tvcomshifttime.setTypeface(customfont);
        TextView tvcomdueafter = (TextView) dialog.findViewById(R.id.tvcomdueday);
        tvcomdueafter.setTypeface(customfont);
        TextView  tvcomremark = (TextView) dialog.findViewById(R.id.tvcomremark);
        tvcomremark.setTypeface(customfont);
        TextView  tvcomcallin = (TextView) dialog.findViewById(R.id.tvcomcallin);
        tvcomcallin.setTypeface(customfont);
        TextView  tvcompckup = (TextView) dialog.findViewById(R.id.tvcompackup);
        tvcompckup.setTypeface(customfont);
        TextView tvcomautname = (TextView) dialog.findViewById(R.id.tvcomauthoritynm);
        tvcomautname.setTypeface(customfont);
        TextView tvauthorityno= (TextView) dialog.findViewById(R.id.tvauthorityno);
        tvauthorityno.setTypeface(customfont);
        TextView tvauthorityemail= (TextView) dialog.findViewById(R.id.tvauthorityemail);
        tvauthorityemail.setTypeface(customfont);
        TextView  tvcomautsign = (TextView) dialog.findViewById(R.id.tvcomauthoritysign);
        tvcomautsign.setTypeface(customfont);
        TextView  tvcomautdate = (TextView) dialog.findViewById(R.id.tvcomauthoritysigndate);
        tvcomautdate.setTypeface(customfont);
        TextView  tvcomrate=(TextView)dialog.findViewById(R.id.tvcomrate);
        tvcomrate.setTypeface(customfont);
        TextView   tvheader = (TextView) dialog.findViewById(R.id.TVWelcome);
        tvheader.setTypeface(customfont);

        TextView   tvflimnm = (TextView) dialog.findViewById(R.id.tvfilmname);
        tvflimnm.setTypeface(customfont);
        TextView  tvphouse = (TextView) dialog.findViewById(R.id.tvprodhouname);
        tvphouse.setTypeface(customfont);
        tvphouse.setMovementMethod(new ScrollingMovementMethod());

        TextView   tvprodnm = (TextView) dialog.findViewById(R.id.tvprodname);
        tvprodnm.setTypeface(customfont);
        TextView   tvshootdate = (TextView) dialog.findViewById(R.id.tvdtshoot);
        tvshootdate.setTypeface(customfont);
        TextView  tvlocation = (TextView) dialog.findViewById(R.id.tvshootloc);
        tvlocation.setTypeface(customfont);
        TextView     tvcharnm = (TextView) dialog.findViewById(R.id.tvcharname);
        tvcharnm.setTypeface(customfont);
        TextView   tvshifttime = (TextView) dialog.findViewById(R.id.tvshifttime);
        tvshifttime.setTypeface(customfont);
        TextView tvrate = (TextView) dialog.findViewById(R.id.tvrate);
        TextView tvcallinin = (TextView) dialog.findViewById(R.id.tvcallinin);
        tvcallinin.setTypeface(customfont);
        TextView tvcomcallinin= (TextView) dialog.findViewById(R.id.tvcomcallinin);
        tvcomcallinin.setTypeface(customfont);
        tvrate.setTypeface(customfont);
        TextView  tvduedays = (TextView) dialog.findViewById(R.id.tvdueday);
        tvduedays.setTypeface(customfont);
        TextView   tvremark = (TextView) dialog.findViewById(R.id.tvremark);
        tvremark.setTypeface(customfont);
        TextView  tvcallin = (TextView) dialog.findViewById(R.id.tvcallin);
        tvcallin.setTypeface(customfont);
        TextView   tvpackup = (TextView) dialog.findViewById(R.id.tvpackup);
        tvpackup.setTypeface(customfont);
        TextView   tvauthoritynm = (TextView) dialog.findViewById(R.id.tvauthoritynm);
        tvauthoritynm.setTypeface(customfont);
        TextView tvcomauthorityno= (TextView) dialog.findViewById(R.id.tvcomauthorityno);
        tvcomauthorityno.setTypeface(customfont);
        TextView tvcomauthorityemail= (TextView) dialog.findViewById(R.id.tvcomauthorityemail);
        tvcomauthorityemail.setTypeface(customfont);
        ImageView ivauthoritysign= (ImageView) dialog.findViewById(R.id.ivauthoritysign);
        TextView  tvauthosigndate = (TextView) dialog.findViewById(R.id.tvauthoritysigndate);
        tvauthosigndate.setTypeface(customfont);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        dialog.setCancelable(false);
        btnOk.setTypeface(customfont);



        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvflimnm.setText(tvflimnmstr);
        tvphouse.setText(tvphousestr);
        tvprodnm.setText(tvprodnmstr);
        tvshootdate.setText(tvshootdatestr);
        tvlocation.setText(tvlocationstr);
        tvcharnm.setText(tvcharnmstr);
        tvshifttime.setText(tvshifttimestr);
        tvrate.setText(tvratestr);
        tvduedays.setText(tvduedaysstr);
        tvremark.setText(tvremarkstr);
        tvcallinin.setText(tvcallininstr);
        tvpackup.setText(tvpackupstr);
        tvauthoritynm.setText(tvauthoritynmstr);
        tvauthorityno.setText(tvauthoritynostr);
        tvauthorityemail.setText(tvauthoritymailstr);
        tvauthosigndate.setText(tvauthosigndatestr);
        tvcallin.setText(tvcallinstr);

        tvconveyance.setVisibility(View.GONE);
        if(rater_cmplt.length()>0)
        {
            try{
                double r = Double.parseDouble(rater_cmplt);
                if(r<= Config.conveyanceLimit){
                    tvconveyance.setText("Conveyance fees: "+fees+" INR");
                    tvconveyance.setVisibility(View.VISIBLE);
                }
            }catch (NumberFormatException e){}
        }else{
            tvrate.setVisibility(View.GONE);
        }

        /*new ImageLoadTask(ivauthoritysignstr, ivauthoritysign).execute();*/
        Picasso.with(getActivity())
                .load(ivauthoritysignstr)
                .resize(300,150).noFade().into(ivauthoritysign);

        return dialog;
    }//onCreateDialog

    public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

        private String url;
        private ImageView imageView;

        public ImageLoadTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageView.setImageBitmap(result);
        }

    }
}
