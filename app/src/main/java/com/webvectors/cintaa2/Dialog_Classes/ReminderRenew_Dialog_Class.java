package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webvectors.cintaa2.R;

/**
 * Created by ASUS on 16/09/2016.
 */
public class ReminderRenew_Dialog_Class extends DialogFragment {
    String date;
    int diff;
    SharedPreferences preferences;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);

        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        date =  preferences.getString("Reminder_date","abc");
        diff =  preferences.getInt("Reminder_diff",0);
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.reminder_to_renew
                , null));

        dialog.setCancelable(false);


        LinearLayout llreminderbg = (LinearLayout) dialog.findViewById(R.id.llbg);
        TextView tvreminder = (TextView) dialog.findViewById(R.id.tvreminder);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);




        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");

        btnOk.setTypeface(custom_font);
        tvreminder.setTypeface(custom_font);
        tvreminder.setTypeface(Typeface.DEFAULT_BOLD);

        tvreminder.setText("Please Renew your Membership before "+date);
        if(diff>7){
            tvreminder.setTextColor(getResources().getColor(R.color.RedReminder));
            llreminderbg.setBackgroundColor(getResources().getColor(R.color.GreenReminder));
        }else{
            tvreminder.setTextColor(getResources().getColor(R.color.colorAccent));
            llreminderbg.setBackgroundColor(getResources().getColor(R.color.RedReminder));
        }





        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // show it
//        dialog.show();
        return dialog;
    }//onCreateDialog
}
