package com.webvectors.cintaa2.Dialog_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.webvectors.cintaa2.adapter.NotificationListViewAdapter;
import com.webvectors.cintaa2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 16/09/2016.
 */
@SuppressWarnings("ConstantConditions")
public class AdminNotification_Dialog_Class extends DialogFragment {
    List<String> notes;
    SharedPreferences preferences;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }//onAttach

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        notes = new ArrayList<>();
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        int notes_size = preferences.getInt("Notification_Size",0);
        for(int s=0; s<notes_size;s++){
            notes.add(preferences.getString("AdminNotification_"+s,""));
        }

        LayoutInflater li = LayoutInflater.from(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.admin_notifications
                , null));

        TextView title = (TextView) dialog.findViewById(R.id.tvtitle);
        TextView noNotes = (TextView) dialog.findViewById(R.id.noNotes);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        ListView listViewNotification = (ListView) dialog.findViewById(R.id.lvnotification) ;
        dialog.setCancelable(false);
        title.setText("Notifications:");
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        title.setTypeface(custom_font);
        btnOk.setTypeface(custom_font);
        noNotes.setTypeface(custom_font);

        if(notes.size()>0){
            NotificationListViewAdapter adapter = new NotificationListViewAdapter(getActivity(),notes);
            listViewNotification.setAdapter(adapter);
            noNotes.setVisibility(View.GONE);
        }else{
            noNotes.setVisibility(View.VISIBLE);
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // show it
//        dialog.show();
        return dialog;
    }//onCreateDialog
}
