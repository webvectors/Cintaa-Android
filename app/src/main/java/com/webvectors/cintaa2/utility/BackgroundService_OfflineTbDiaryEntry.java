package com.webvectors.cintaa2.utility;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

/**
 * Created by ASUS on 29/09/2016.
 */
public class BackgroundService_OfflineTbDiaryEntry extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean isRunning = false;
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor, subCursor, chkcursor;
    Boolean isAvail;

    @Override
    public void onCreate() {
        isRunning = true;
        isAvail = false;
        sqLiteDB = new SQLiteDB(this);
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbDiaryEntry", "ON");
        editor.commit();
    }//onCreate

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {
        isRunning = true;
        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isRunning) {
                    for (int i = 0; isRunning; i++) {

                        cursor = sqLiteDatabaseRead.rawQuery("select entry_id, member_id, shoot_date, " +
                                "characterName, shoot_location, film_tv_name, shift_time, call_time, rate, paymentAfter" +
                                ",remark, schedule_status," +
                                "lat, lng,rateType from sqtb_diary_entry " +
                                "where ServerEntryStatus='no'", null);
                        if (cursor.moveToFirst() && cursor != null) {
                            isAvail = true;
                            do {
                                try {
                                    String entry_id, member_id, shoot_date, characterName, shoot_location, film_tv_name;
                                    String shift_time, call_time, rate, paymentAfter, remark, schedule_status, lat, lng, rateType;
                                    String new_house_name = null, new_producer_name = null;
                                    entry_id = cursor.getString(0);
                                    member_id = cursor.getString(1);
                                    shoot_date = cursor.getString(2);
                                    characterName = cursor.getString(3);
                                    shoot_location = cursor.getString(4);
                                    film_tv_name = cursor.getString(5);
                                    shift_time = cursor.getString(6);
                                    call_time = cursor.getString(7);
                                    rate = cursor.getString(8);
                                    paymentAfter = cursor.getString(9);
                                    remark = cursor.getString(10);
                                    schedule_status = cursor.getString(11);
                                    lat = cursor.getString(12);
                                    lng = cursor.getString(13);
                                    rateType = cursor.getString(14);

                                        /*if(shoot_location.trim().length()==0){
                                            shoot_location = GetAddress(lat,lng);
                                            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set shoot_location = '"+shoot_location+"' " +
                                                    "where entry_id='"+entry_id+"'; ");
                                            long result=sqLiteStatement.executeUpdateDelete();
                                        }*/
                                    try {
                                        subCursor = sqLiteDatabaseRead.rawQuery("select productionHouse, producerName from tbScheduleDetails " +
                                                "where entryId=?", new String[]{entry_id});
                                        if (subCursor.moveToFirst() && subCursor != null) {
                                            new_house_name = cursor.getString(0);
                                            new_producer_name = cursor.getString(1);
                                        }
                                    } finally {
                                        // this gets called even if there is an exception somewhere above
                                        if (subCursor != null)
                                            subCursor.close();
                                    }


                                    if (isNetworkConnected()) {
                                        try {
                                            chkcursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where entryId=?", new String[]{entry_id});
                                            if (chkcursor.moveToFirst()) {
                                                if (chkcursor.getString(0).equalsIgnoreCase("done")) {
                                                    new SendDataToServer(entry_id, member_id, shoot_date, characterName, shoot_location, film_tv_name, shift_time, call_time, rate, paymentAfter, remark, schedule_status, lat, lng, new_house_name, new_producer_name, rateType).execute();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        } finally {
                                            if (chkcursor != null)
                                                chkcursor.close();
                                        }
                                    }
                                } finally {
                                    // this gets called even if there is an exception somewhere above
                                    if (cursor != null)
                                        cursor.close();
                                }
                            } while (cursor.moveToNext() && cursor != null);
                        }
                        // tbAuthorise (, severStatus)
                        cursor = sqLiteDatabaseRead.rawQuery("select entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName, author_remark from tbAuthorise where severStatus='no'", null);
                        if (cursor.moveToFirst() && cursor != null) {
                            do {
                                try {
                                    String entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName, remark;
                                    entryId = cursor.getString(0);
                                    member_id = cursor.getString(1);
                                    name = cursor.getString(2);
                                    number = cursor.getString(3);
                                    sign = cursor.getString(4);
                                    encodedString = cursor.getString(5);
                                    signDate = cursor.getString(6);
                                    email = cursor.getString(7);
                                    prodHouse = cursor.getString(8);
                                    prodName = cursor.getString(9);
                                    remark = cursor.getString(10);
                                    if (isNetworkConnected()) {
                                        try {
                                            chkcursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where entryId=?", new String[]{entryId});
                                            if (chkcursor.moveToFirst()) {
                                                if (chkcursor.getString(0).equalsIgnoreCase("done")) {
                                                    new SendAuthorisationDataToServer(entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName, remark).execute();
                                                    //new SendDataToServer(entry_id, member_id, shoot_date, characterName, shoot_location, film_tv_name,shift_time, call_time, rate, paymentAfter ,remark, schedule_status,lat, lng, new_house_name, new_producer_name,rateType).execute();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        } finally {
                                            if (chkcursor != null)
                                                chkcursor.close();
                                        }
                                    }
                                } finally {
                                    // this gets called even if there is an exception somewhere above
                                    if (cursor != null)
                                        cursor.close();
                                }

                            } while (cursor.moveToNext() && cursor != null);
                        } else {
                            if (!isAvail) {
                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                if (!preferences.getString("ServiceCallinPackup", "OFF").equals("ON")) {
                                    Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService_OffLineCallinPackup.class);
                                    getApplicationContext().startService(serviceIntent);
                                }

                                StopService(startId);
                            }
                        }
                        try {
                            Thread.sleep(60000);//Thread Sleeps for One Minute
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }//end for
                }//end if(isRunning)
            }
        }).start();
        return Service.START_REDELIVER_INTENT;
    }//onStartCommand

    private void StopService(int startId) {
        this.stopSelf(startId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbDiaryEntry", "OFF");
        editor.commit();
    }//StopService

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbDiaryEntry", "OFF");
        editor.commit();
    }//onDestroy

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected

    private String GetAddress(String callin_lat, String callin_lng) throws IOException {
        double MyLat = Double.parseDouble(callin_lat), MyLong = Double.parseDouble(callin_lng);

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        List<Address> addresses;
        String address1, city, country, state, postalCode, knownName;
        addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
        address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        city = addresses.get(0).getLocality();
        state = addresses.get(0).getAdminArea();
        country = addresses.get(0).getCountryName();
        postalCode = addresses.get(0).getPostalCode();
        knownName = addresses.get(0).getFeatureName();

        String address = "NA";
        if (!postalCode.equals("null")) {
            return address = address1 + "; " + city + "; " + state + "; " + country + "- " + postalCode;
        } else {
            return address = address1 + "; " + city + "; " + state + "; " + country;
        }
    }//GetAddress

    class SendDataToServer extends AsyncTask<String, String, String> {
        String entry_id, member_id, rateType, shoot_date, characterName, shoot_location, film_tv_name, shift_time, call_time, rate, paymentAfter, remark, schedule_status, lat, lng, new_house_name, new_producer_name;

        public SendDataToServer(String entry_id, String member_id, String shoot_date, String characterName, String shoot_location, String film_tv_name, String shift_time, String call_time, String rate, String paymentAfter, String remark, String schedule_status, String lat, String lng, String new_house_name, String new_producer_name, String rateType) {
            this.entry_id = entry_id;
            this.member_id = member_id;
            this.shoot_date = shoot_date;
            this.characterName = characterName;
            this.shoot_location = shoot_location;
            this.film_tv_name = film_tv_name;
            this.shift_time = shift_time;
            this.call_time = call_time;
            this.rate = rate;
            this.paymentAfter = paymentAfter;
            this.remark = remark;
            this.schedule_status = schedule_status;
            this.lat = lat;
            this.lng = lng;
            this.new_house_name = new_house_name;
            this.new_producer_name = new_producer_name;
            this.rateType = rateType;

            if (this.shoot_location.length() == 0) {
                try {
                    this.shoot_location = GetAddress(lat, lng);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //showToast("Uploading CINTAA Record to Server..", Toast.LENGTH_LONG);
            doInBackground();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.UrlUpdateDiaryEntryInsert);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter

                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(entry_id, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("member_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(member_id, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("shoot_date", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(shoot_date, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("played_character", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(characterName, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("shoot_location", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(shoot_location, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("film_tv_name", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(film_tv_name, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("shift_time", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(shift_time, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("call_time", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(call_time, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("rate_type", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(rateType, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("rate", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(rate, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("due_days", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(paymentAfter, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("remark", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(remark, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("schedule_status", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(schedule_status, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("lat", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(lat, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("lng", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(lng, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("shoot_map_location", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(shoot_location, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("new_house_name", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(new_house_name, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("new_producer_name", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(new_producer_name, "UTF-8"));
                paramsString.append("&");

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse = reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
//                if(s.equals("{\"success\":1} ")){
                showToast("Record Updated to Server", Toast.LENGTH_LONG);
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set shoot_location='" + shoot_location + "', ServerEntryStatus = 'done' where entry_id = '" + entry_id + "'");
                long result = sqLiteStatement.executeUpdateDelete();
                //Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService_OffLineCallinPackup.class);
                //getApplicationContext().startService(serviceIntent);

                //}
            }
        }
    } //SendDataToServer

    class SendAuthorisationDataToServer extends AsyncTask<String, String, String> {
        String entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName, remark;

        public SendAuthorisationDataToServer(String entryId, String member_id, String name, String number, String sign, String encodedString, String signDate, String email, String prodHouse, String prodName, String remark) {
            this.entryId = entryId;
            this.member_id = member_id;
            this.name = name;
            this.number = number;
            this.sign = sign;
            this.encodedString = encodedString;
            this.signDate = signDate;
            this.email = email;
            this.prodHouse = prodHouse;
            this.prodName = prodName;
            this.remark = remark;
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.URLupdateauthoritysign);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter

                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("encoded_string", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(encodedString, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("img_name", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(sign, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(entryId, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("authority_sign_date", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(signDate, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("authority_nm", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(name, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("authority_number", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(number, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("authority_email", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(email, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("productionHouse", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(prodHouse, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("producerName", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(prodName, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("member_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(member_id, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("remark", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(remark, "UTF-8"));
                paramsString.append("&");


            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse = reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                showToast("Record Updated to Server", Toast.LENGTH_LONG);
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbAuthorise set severStatus='sendMail' where entryId = '" + entryId + "'");
                long result = sqLiteStatement.executeUpdateDelete();
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbScheduleDetails set " +
                        "scheduleStatus='completed' where entryId = '" + entryId + "'");
                result = sqLiteStatement.executeUpdateDelete();

            }
        }
    } //SendAuthorisationDataToServer


    public void showToast(String message, int time) {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable(getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground(getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }

}//BackgroundService
