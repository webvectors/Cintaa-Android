package com.webvectors.cintaa2.utility;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.webvectors.cintaa2.BuildConfig;

/**
 * Created by Ankita on 7/22/2017.
 */

public class GlobalClass {
    public static void printLog(Activity activity, String s) {
        if (BuildConfig.DEBUG) {
            Log.e("================ ", " :  " + s);
        }
    }


}
