package com.webvectors.cintaa2.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.fragment.CompletedEventsFragment;
import com.webvectors.cintaa2.model.CompletedShootData;
import com.webvectors.cintaa2.model.ReportData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class getDisplayData extends AsyncTask<String, Void, Void> {

    private String entry_id, member_id;
    private final Activity activity;
    private ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
    private ProgressDialog pd;
    ArrayList<CompletedShootData> completedShootDatas = new ArrayList<CompletedShootData>();

    public getDisplayData(Activity activity, ArrayList<HashMap<String, String>> hashMapArrayList, String member_id) {
        this.hashMapArrayList = hashMapArrayList;
        this.activity = activity;
        this.member_id = member_id;
    }

    @Override
    protected Void doInBackground(String... params) {
        for (int i = 0; i < hashMapArrayList.size(); i++) {
            displaydetails(hashMapArrayList.get(i).get("entry_id"));
        }
        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = new ProgressDialog(activity);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
        GlobalClass.printLog(activity, "--------------====================---post execute-----------------");
    }

    //... display details
    public void displaydetails(String entry_id) {

        final AsyncHttpClient client = new AsyncHttpClient();
        final RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", entry_id);
        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {


                client.post(Config.URLcompletedscheduledetails, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("CompletedDetails");
                            if (jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject j1 = jsonArray.getJSONObject(i);

                                    ReportData reportData = new ReportData();

                                    String house_name = j1.getString("house_name");
                                    String alies_name = j1.getString("alies_name");
                                    if (!alies_name.equals("null")) {
                                        house_name = house_name + "/" + alies_name;
                                    }
                                    String remark = j1.getString("remark");
                                    String mRemark;
                                    if (remark.equals("null")) {
                                        mRemark = "No Remark";
                                    } else {
                                        mRemark = remark;
                                    }
                                    reportData.setHouse_name(house_name);
                                    reportData.setFilm_tv_name(j1.getString("film_tv_name"));
                                    reportData.setProducer_name(j1.getString("producer_name"));
                                    reportData.setShoot_date(j1.getString("shoot_date"));
                                    reportData.setShoot_location(j1.getString("shoot_location"));
                                    reportData.setPlayed_character(j1.getString("played_character"));
                                    reportData.setShift_time(j1.getString("shift_time"));
                                    reportData.setCall_time(j1.getString("call_time"));
                                    reportData.setRater(j1.getString("rate") + " " + j1.getString("rate_type"));
                                    reportData.setDue_days(j1.getString("due_days") + " days");
                                    reportData.setCallin_time(j1.getString("callin_time"));
                                    reportData.setPackup_time(j1.getString("packup_time"));
                                    reportData.setAuthority_nm(j1.getString("authority_nm"));
                                    reportData.setAuthority_number(j1.getString("authority_number"));
                                    reportData.setAuthority_sign(j1.getString("authority_sign"));
                                    reportData.setFees(j1.getString("fees"));
                                    reportData.setHouse_email(j1.getString("house_email"));
                                    reportData.setRate(j1.getString("rate"));
                                    reportData.setAuthority_sign_date(j1.getString("authority_sign_date"));
                                    GlobalClass.printLog(activity, "--------------=========house_name----------------" + house_name);
                                    CompletedEventsFragment.reportDataArrayList.add(reportData);
                                    CompletedEventsFragment.getCompletedEventsFragment().checkSize();

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        if (pd != null) {
                            if (pd.isShowing()) {
                                pd.dismiss();
                            }
                        }
                    }
                });
            }
        };
        mainHandler.post(myRunnable);
    }
}
