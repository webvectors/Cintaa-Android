package com.webvectors.cintaa2.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;

import com.webvectors.cintaa2.activity.AlertStopActivity;

/**
 * Created by ASUS on 18/08/2016.
 */
public class AlertBroadcastReceiver extends BroadcastReceiver {

    MediaPlayer mp;


    @Override
    public void onReceive(Context context, Intent intent) {


        //////////////////////////////////

        String requestCode = intent.getExtras().getString("Alert_requestCode");
//        String entry_ids = intent.getExtras().getString("entry_ids");

        Intent mIntent = new Intent(context,AlertStopActivity.class); //Same as above two lines
        mIntent.putExtra("Alert_requestCode_ShowDetails",requestCode);
//        mIntent.putExtra("Alert_entry_ids",entry_ids);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mIntent);
        ///////////////////////////////////
       // Toast.makeText(context, "Event Alarm....", Toast.LENGTH_LONG).show();
    }
}
