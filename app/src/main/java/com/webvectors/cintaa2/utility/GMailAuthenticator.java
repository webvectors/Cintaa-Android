package com.webvectors.cintaa2.utility;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by ASUS on 10/11/2016.
 */

public class GMailAuthenticator extends Authenticator {

    String user;
    String pw;
    public GMailAuthenticator (String username, String password)
    {
        super();
        this.user = username;
        this.pw = password;
    }
    public PasswordAuthentication getPasswordAuthentication()
    {
        return new PasswordAuthentication(user, pw);
    }

}
