package com.webvectors.cintaa2.utility;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by ASUS on 29/09/2016.
 */
public class BackgroundService_OfflineTbSchedule extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean isRunning = false;
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    @Override
    public void onCreate() {
        isRunning = true;
        sqLiteDB = new SQLiteDB(this);
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbSchedule","ON");
        editor.commit();
    }//onCreate

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {
        isRunning = true;
        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isRunning) {
                    for (int i = 0; isRunning; i++) {
                        cursor=sqLiteDatabaseRead.rawQuery("select memberId, entryId, scheduleDate, filmName, " +
                                "productionHouse, producerName, scheduleStatus from tbScheduleDetails " +
                                "where ServerEntryStatus='no'",null);
                        if(cursor.moveToFirst() && cursor!=null)
                        {
                            do {
                                try{
                                    String memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus;
                                    memberId = cursor.getString(0);
                                    entryId = cursor.getString(1);
                                    scheduleDate = cursor.getString(2);
                                    filmName = cursor.getString(3);
                                    productionHouse = cursor.getString(4);
                                    producerName = cursor.getString(5);
                                    scheduleStatus = cursor.getString(6);

                                    if (isNetworkConnected()){
                                        try {
                                            new SendDataToServer(memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus).execute();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }catch (Exception e){}
                            }while (cursor.moveToNext()  && cursor!=null);
                        }else {
                            cursor=sqLiteDatabaseRead.rawQuery("select memberId, entryId, scheduleDate, filmName, " +
                                    "productionHouse, producerName, scheduleStatus from tbScheduleDetails " +
                                    "where ServerEntryStatus='update'",null);
                            if(cursor.moveToFirst() && cursor!=null)
                            {
                                do {
                                    try{
                                        String memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus;
                                        memberId = cursor.getString(0);
                                        entryId = cursor.getString(1);
                                        scheduleDate = cursor.getString(2);
                                        filmName = cursor.getString(3);
                                        productionHouse = cursor.getString(4);
                                        producerName = cursor.getString(5);
                                        scheduleStatus = cursor.getString(6);

                                        if (isNetworkConnected()){
                                            try {
                                                new UpdateDataToServer(memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus).execute();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }catch (Exception e){}
                                }while (cursor.moveToNext()  && cursor!=null);
                            }else {
                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                if(!preferences.getString("ServiceTbDiaryEntry","OFF").equals("ON")){
                                    Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService_OfflineTbDiaryEntry.class);
                                    getApplicationContext().startService(serviceIntent);
                                }
                                //isRunning = false;
                                StopService(startId);
                            }
                        }
                        try {
                            Thread.sleep(60000);//Thread Sleeps for One Minute
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }//end for
                }//end if(isRunning)
            }
        }).start();
        return Service.START_REDELIVER_INTENT;
    }//onStartCommand

    private void StopService(int startId) {
        this.stopSelf(startId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbSchedule","OFF");
        editor.commit();
    }//StopService

    @Override
    public void onDestroy() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceTbSchedule","OFF");
        editor.commit();
        super.onDestroy();
    }//onDestroy

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected

    class SendDataToServer extends AsyncTask<String,String,String> {
        String memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus;
        public SendDataToServer(String memberId, String entryId, String scheduleDate, String filmName, String productionHouse, String producerName, String scheduleStatus) {
            this.memberId = memberId;
            this.entryId = entryId;
            this.scheduleDate = scheduleDate;
            this.filmName = filmName;
            this.productionHouse = productionHouse;
            this.producerName = producerName;
            this.scheduleStatus = scheduleStatus;
            //showToast("Uploading CINTAA Record to Server..", Toast.LENGTH_LONG);
            doInBackground();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.URLinsertNewSchedule);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                            paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(entryId, "UTF-8"));
                            paramsString.append("&");

                            paramsString.append( URLEncoder.encode("member_id", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(memberId, "UTF-8"));
                            paramsString.append("&");

                            paramsString.append( URLEncoder.encode("ScheduleDate", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(scheduleDate, "UTF-8"));
                            paramsString.append("&");

                            paramsString.append( URLEncoder.encode("filmName", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(filmName, "UTF-8"));
                            paramsString.append("&");

                            paramsString.append( URLEncoder.encode("productionHouseName", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(productionHouse, "UTF-8"));
                            paramsString.append("&");

                            paramsString.append( URLEncoder.encode("producerName", "UTF-8"));
                            paramsString.append("=");
                            paramsString.append(URLEncoder.encode(producerName, "UTF-8"));
                            paramsString.append("&");
                } catch (Exception e) { e.printStackTrace(); }

                try {
                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse=reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (Exception e) { e.printStackTrace(); }

            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {}
                }
            }
            return null;
        }//doInBackground


        @Override
        protected void onPostExecute(String s) {
            if(s!=null){
  //              if(s.equals("{\"success\":1} ")){
                    showToast("Record Updated to Server", Toast.LENGTH_LONG);
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbScheduleDetails set ServerEntryStatus = 'done' where entryId = '"+entryId+"'");
                    long result=sqLiteStatement.executeUpdateDelete();
                 //   Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService_OfflineTbDiaryEntry.class);
                 //   getApplicationContext().startService(serviceIntent);
//                }

            }
        }

    } //SendDataToServer

    class UpdateDataToServer extends AsyncTask<String,String,String> {
        String memberId, entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus;
        public UpdateDataToServer(String memberId, String entryId, String scheduleDate, String filmName, String productionHouse, String producerName, String scheduleStatus) {
            this.memberId = memberId;
            this.entryId = entryId;
            this.scheduleDate = scheduleDate;
            this.filmName = filmName;
            this.productionHouse = productionHouse;
            this.producerName = producerName;
            this.scheduleStatus = scheduleStatus;
            //showToast("Uploading CINTAA Record to Server..", Toast.LENGTH_LONG);
            doInBackground();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.URLupdateSchedule);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(entryId, "UTF-8"));
                paramsString.append("&");

                paramsString.append( URLEncoder.encode("member_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(memberId, "UTF-8"));
                paramsString.append("&");

                paramsString.append( URLEncoder.encode("ScheduleDate", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(scheduleDate, "UTF-8"));
                paramsString.append("&");

                paramsString.append( URLEncoder.encode("filmName", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(filmName, "UTF-8"));
                paramsString.append("&");

                paramsString.append( URLEncoder.encode("productionHouseName", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(productionHouse, "UTF-8"));
                paramsString.append("&");

                paramsString.append( URLEncoder.encode("producerName", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(producerName, "UTF-8"));
                paramsString.append("&");
            } catch (Exception e) { e.printStackTrace(); }

            try {
                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse=reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (Exception e) { e.printStackTrace(); }

            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {}
                }
            }
            return null;
        }//doInBackground


        @Override
        protected void onPostExecute(String s) {
            if(s!=null){
                //              if(s.equals("{\"success\":1} ")){
                showToast("Record Updated to Server", Toast.LENGTH_LONG);
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbScheduleDetails set ServerEntryStatus = 'done' where entryId = '"+entryId+"'");
                long result=sqLiteStatement.executeUpdateDelete();
                //   Intent serviceIntent = new Intent(getApplicationContext(), BackgroundService_OfflineTbDiaryEntry.class);
                //   getApplicationContext().startService(serviceIntent);
//                }

            }
        }

    } //UpdateDataToServer

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }


}//BackgroundService
