package com.webvectors.cintaa2.utility;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Shri on 14/07/2016.
 */
public class Config {




    public static String mailLogoUrl = "http://cintaa.in/shoot_new/cintaaLogoMail.png";
    public static String urlFB = "https://www.facebook.com/cintaamumbai/";
    public static String urlTwitter = "https://twitter.com/cintaaofficial";
    public static String urlYoutube = "https://www.youtube.com/c/cintaatv";
//    public static String urlFB = "https://www.facebook.com/CINTA.Association/";
    //private static String BASE_URL = "http://192.168.43.209:8090/shoot_new/";

    /*
    Alies: complaint@cintaa.net
    info@cintaa.net
     */
// new server values
    public static String cintaaMail = "cintaa@web2101.cftclients.com";
    public static String cintaaMailPwd = "CU[NiU$^Z)=2";
    public static String profileUpdateMail = "cintaa@web2101.cftclients.com";
    public static String smtpHost = "host15.registrar-servers.com";
    public static String smtpPort = "465";
// new server URL
    private static String BASE_URL = "http://web2101.cftclients.com/app_api/";
    public static String imgPngUrl = BASE_URL + "AuthoritySign/";
    public static String URLupdateauthoritysign = BASE_URL + "UpdateAutoritySign.php"; //... new API

//    public static String URLupdateauthoritysign = BASE_URL + "updateautoritysign.php"; //...... old api

////... old server values
//    public static String cintaaMail = "info@cintaa.net";
//    public static String cintaaMailPwd = "cin##1234";
//    public static String profileUpdateMail = "info@cintaa.net";
//    public static String smtpHost = "smtp.gmail.com";
//    public static String smtpPort = "587";
////... Old Server URL
//    private static String BASE_URL = "http://cintaa.in/shoot_new/";
//    public static String imgPngUrl = "http://cintaa.in/shoot_new/AuthoritySign/";



    public static int conveyanceLimit = 5000;

    //Link to DOCX
    public static String URLMouDocx = BASE_URL + "mou.pdf";
    public static String URLInstructionDocx = BASE_URL + "instructions.pdf";

    //Link to Sites
    public static String URLContactUs = "http://www.cintaa.net/contact-us/";
    public static String URLwebite = "http://www.cintaa.net/";
    public static String URLprivacyPolicy = "http://www.cintaa.net/ privacy-policy/";
    //    public static String URLprivacyPolicy="http://www.cintaa.net/ privacy-policy/";
    public static String URLcomplains = "http://www.cintaa.net/";
    public static String URLhelpCenter = "http://www.cintaa.net/cintaa-help-center/";

    //Link to PHP
    public static String urlRegisterUserDetailInsert = BASE_URL + "RegisterUserDetailInsert.php";
    public static String urlselectIdandPw = BASE_URL + "selectIdandPw.php";
    public static String urlmyprofile = BASE_URL + "myprofile.php";
    public static String UrlUpdateDiaryEntryInsert = BASE_URL + "DiaryEntryUpdate.php";
    public static String UrlSelectCalenderEventDates = BASE_URL + "selectCalenderEventDates.php";
    public static String UrlSelectAllCalenderEvents = BASE_URL + "selectAllCalenderEvents.php";
    public static String UrlselectProductionHouseProducer = BASE_URL + "selectProductionHouseProducer.php";
    public static String UrlselectProductionHouseDropdownSnapshot = BASE_URL + "selectProductionHouseDropdownSnapshot.php";
    public static String URLinprogrescheduleentry = BASE_URL + "inprogresdheduleentry.php";
    public static String URLselectAllDiaryEntryList = BASE_URL + "selectAllDiaryEntryList.php";
    public static String URLmyprofilegetdata = BASE_URL + "myprofilegetdata.php";
    public static String URLprofileimgandname = BASE_URL + "MemberProfileInfo.php";
    public static String URLselectInProgressDetails = BASE_URL + "selectInProgressDetails.php";
    public static String URLupdateInProgressDetails = BASE_URL + "updateInProgressDetails.php";
    public static String URLcompletedscheduleentry = BASE_URL + "completedscheduleentry.php";
    public static String URLselectSnapshotDeatils = BASE_URL + "selectSnapshotDeatils.php";
    public static String URLcompletedscheduledetails = BASE_URL + "completedscheduledetails.php";
    public static String URLupdateProfileImage = BASE_URL + "updateProfileImage.php";
    public static String URLnameimgandemail = BASE_URL + "nameimgandemail.php";
    public static String URLchangepassword = BASE_URL + "changepassword.php";
    public static String URLfeedback = BASE_URL + "feedback.php";
    public static String URLShiftTime = BASE_URL + "selectShiftTime.php";
    public static String URLselectNotifications = "http://web2101.cftclients.com/app_api/selectNotifications.php";
    public static String URLselectRegisterDate = BASE_URL + "selectRegisterDate.php";

    public static String URLupdateRemovedp = BASE_URL + "updateRemovedp.php";
    public static String URLupdateCallinTime = BASE_URL + "updateCallinTime.php";
    public static String URLupdatePackupTime = BASE_URL + "updatePackupTime.php";
    public static String URLupdateAuthorityDetails = BASE_URL + "updateAuthorityDetails.php";
    public static String URLselectUserSession = BASE_URL + "selectUserSession.php";
    public static String URLgetBackup = BASE_URL + "getBackup.php";
    public static String URLdeleteAfterBackup = BASE_URL + "deleteAfterBackup.php";

    // New
    public static String URLinsertNewSchedule = BASE_URL + "insertNewSchedule.php";
    public static String URLupdateSchedule = BASE_URL + "updateSchedule.php";
    public static String URLDiaryEntryInsert = BASE_URL + "DiaryEntryInsert.php";
    public static String URLdeleteSchedule = BASE_URL + "deleteSchedule.php";
    public static String URLselectCheckFirstTimeLogin = BASE_URL + "selectCheckFirstTimeLogin.php";
    public static String URLcintaaSendSMS = BASE_URL + "cintaaSendSMS.php";
    public static String URLverifyOTP = BASE_URL + "verifyOTP.php";
    public static String URLresetOTP = BASE_URL + "resetOTP.php";

    //... For app under review dialog
    public static String VersionCheck = "http://cintaa.in/shoot_new/version.php";


    public static boolean isNetworkConnected(Activity context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

}
