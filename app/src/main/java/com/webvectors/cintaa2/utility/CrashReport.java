package com.webvectors.cintaa2.utility;

import android.support.multidex.MultiDexApplication;

import com.webvectors.cintaa2.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by ASUS on 22/10/2016.
 */

@ReportsCrashes(formUri = "",
        mailTo = "tejasbusiness@gmail.com",
        mode = ReportingInteractionMode.SILENT,
        resToastText = R.string.crash_toast_text)

public class CrashReport extends MultiDexApplication {



    private static CrashReport myApplication;

    @Override
    public void onCreate() {
        super.onCreate();

        myApplication = this;
        ACRA.init(this);

    }

}
