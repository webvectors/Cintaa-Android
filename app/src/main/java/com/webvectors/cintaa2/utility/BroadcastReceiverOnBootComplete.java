package com.webvectors.cintaa2.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.webvectors.cintaa2.utility.BackgroundService_OffLineCallinPackup;
import com.webvectors.cintaa2.utility.BackgroundService_OfflineTbDiaryEntry;
import com.webvectors.cintaa2.utility.BackgroundService_OfflineTbSchedule;

/**
 * Created by ASUS on 26/10/2016.
 */

public class BroadcastReceiverOnBootComplete extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            /*Intent serviceIntent = new Intent(context, BackgroundService.class);
            context.startService(serviceIntent);*/

            Intent serviceIntent = new Intent(context, BackgroundService_OfflineTbSchedule.class);
            context.startService(serviceIntent);

            serviceIntent = new Intent(context, BackgroundService_OfflineTbDiaryEntry.class);
            context.startService(serviceIntent);

            serviceIntent = new Intent(context, BackgroundService_OffLineCallinPackup.class);
            context.startService(serviceIntent);

        }
    }//onReceive
}
