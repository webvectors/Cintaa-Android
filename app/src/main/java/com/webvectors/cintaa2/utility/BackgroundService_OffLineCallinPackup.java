package com.webvectors.cintaa2.utility;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

/**
 * Created by ASUS on 29/09/2016.
 */
public class BackgroundService_OffLineCallinPackup extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // ProgressDialog pd;

    private boolean isRunning = false, isAvail = false;
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor, cursor2, chkcursor;

    String entry_id, callin_time, callin_lat, callin_lng, entry_id_p, packup_time, packup_lat, packup_lng;


    @Override
    public void onCreate() {
        isRunning = true;
        isAvail = false;

        sqLiteDB = new SQLiteDB(this);
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceCallinPackup", "ON");
        editor.commit();

        // Toast.makeText(this,"Service OnCreate",Toast.LENGTH_LONG).show();

    }//onCreate

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {

        isRunning = true;

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        new Thread(new Runnable() {
            @Override
            public void run() {

                if (isRunning) {

                    for (int i = 0; isRunning; i++) {

                        try {
                            //Store Callin to Server DB
                            //entry_id, callin_time, callin_lat, callin_lng, Status
                            cursor = sqLiteDatabaseRead.rawQuery("select entry_id, callin_time, callin_lat, callin_lng from sqtb_diary_Callin where Status='Pending'", null);
                            if (cursor != null)
                                if (cursor.moveToFirst()) {
                                    isAvail = false;
                                    do {
                                        try {
                                            entry_id = cursor.getString(0);
                                            callin_time = cursor.getString(1);
                                            callin_lat = cursor.getString(2);
                                            callin_lng = cursor.getString(3);
                                            if (isNetworkConnected()) {
                                                try {
                                                    //StoreCallinToServerDataBase(entry_id, callin_time, callin_lat, callin_lng);
                                                    String[] ary = {entry_id, callin_time, callin_lat, callin_lng};
                                                    chkcursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where entryId=?", new String[]{entry_id});
                                                    if (chkcursor.moveToFirst()) {
                                                        if (chkcursor.getString(0).equalsIgnoreCase("done")) {
                                                            new SendDataToServerCallin().execute();
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    } while (cursor.moveToNext() && cursor != null);
                                } else {
                                    isAvail = true;
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //Store Packup to Server DB
                        //entry_id, packup_time, packup_lat, packup_lng, Status sqtb_diary_Packup
                        cursor2 = sqLiteDatabaseRead.rawQuery("select entry_id, packup_time, packup_lat, packup_lng from sqtb_diary_Packup where Status='Pending'", null);
                        if (cursor2.moveToFirst() && cursor2 != null) {
                            do {
                                try {
                                    entry_id_p = cursor2.getString(0);
                                    packup_time = cursor2.getString(1);
                                    packup_lat = cursor2.getString(2);
                                    packup_lng = cursor2.getString(3);

                                    if (isNetworkConnected()) {
                                        try {
                                            chkcursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where entryId=?", new String[]{entry_id});
                                            if (chkcursor.moveToFirst()) {
                                                if (chkcursor.getString(0).equalsIgnoreCase("done")) {
                                                    new SendDataToServerPackup().execute();
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (Exception e) {
                                }
                            } while (cursor2.moveToNext() && cursor2 != null);
                        } else {
                            if (isAvail) {
                                //isRunning = false;
                                StopService(startId);
                            }
                        }


                        try {
                            //Thread.sleep(120000);//Thread Sleeps for Two Minute
                            //new HomeFragment().CheckOffLineCallinPackup();
                            Thread.sleep(60000);//Thread Sleeps for One Minute

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }//end for

                }//end if(isRunning)


            }
        }).start();


        return Service.START_REDELIVER_INTENT;
    }//onStartCommand

    private void StopService(int startId) {
        this.stopSelf(startId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceCallinPackup", "OFF");
        editor.commit();
    }//StopService


    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ServiceCallinPackup", "OFF");
        editor.commit();
        //  Toast.makeText(this,"Service onDestroyed",Toast.LENGTH_LONG).show();
    }//onDestroy

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected

    private String GetAddress(String callin_lat, String callin_lng) throws IOException {
        double MyLat = Double.parseDouble(callin_lat), MyLong = Double.parseDouble(callin_lng);
        String address = "GPS Fault";

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        ;
        List<Address> addresses;
        String address1, city, country, state, postalCode, knownName;
        addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
        if (addresses.size() > 0) {
            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            address = address1 + "; " + city + "; " + state + "; " + country;
        }

        return address;

    }//GetAddress

    class SendDataToServerCallin extends AsyncTask<String, String, String> {
        String PeopleID;

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                String address = GetAddress(callin_lat, callin_lng);

                URL url = new URL(Config.URLupdateCallinTime);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(entry_id, "UTF-8"));

                paramsString.append("&");

                paramsString.append(URLEncoder.encode("callin_time", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(callin_time, "UTF-8"));
                paramsString.append("&");


                paramsString.append(URLEncoder.encode("callin_location", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(address, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("callin_lat", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(callin_lat, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("callin_lng", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(callin_lng, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("callin_entry_type", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode("ManualOffline", "UTF-8"));
                paramsString.append("&");


            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse = reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground


        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                //if(s.equals("{\"success\":1} ")){
                showToast("In Time Updated to Server", Toast.LENGTH_LONG);
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_Callin set Status = 'done' where entry_id = '" + entry_id + "'");
                long result = sqLiteStatement.executeUpdateDelete();
                //}

            } else {

            }
        }

    } //SendDataToServerCallin

    class SendDataToServerPackup extends AsyncTask<String, String, String> {
        String PeopleID;

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                String address = GetAddress(callin_lat, callin_lng);

                URL url = new URL(Config.URLupdatePackupTime);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("entry_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(entry_id_p, "UTF-8"));

                paramsString.append("&");

                paramsString.append(URLEncoder.encode("packup_time", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(packup_time, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("packup_location", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(address, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("packup_lat", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(packup_lat, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("packup_lng", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(packup_lng, "UTF-8"));
                paramsString.append("&");

                paramsString.append(URLEncoder.encode("packup_entry_type", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode("ManualOffline", "UTF-8"));
                paramsString.append("&");


            } catch (Exception e) {
                e.printStackTrace();
            }


            try {

                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse = reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground


        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                if (s.equals("{\"success\":1} ")) {
                    showToast("Packup Time Updated to Server", Toast.LENGTH_LONG);

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_Packup set Status = 'done' where entry_id = '" + entry_id + "'");
                    long result = sqLiteStatement.executeUpdateDelete();
                }

            } else {

            }
        }

    } //SendDataToServerPackup


   /* private void StoreCallinToServerDataBase(final String entry_id, String callin_time, String callin_lat, String callin_lng) throws IOException {

        String address = GetAddress(callin_lat,callin_lng);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("callin_time", callin_time);
        params.put("callin_location", address);
        params.put("callin_lat", callin_lat);
        params.put("callin_lng", callin_lng);
        params.put("callin_entry_type", "ManualOffline");

        client.post(Config.URLupdateCallinTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    Toast.makeText(getApplicationContext(), "In Time Updated to Server", Toast.LENGTH_LONG).show();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_Callin set Status = 'done' where entry_id = '"+entry_id+"'");
                    long result=sqLiteStatement.executeUpdateDelete();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Toast.makeText(getApplicationContext(), "Could Not Connect to Network", Toast.LENGTH_LONG).show();
            }
        });
    }//StoreToServerDataBase*/

   /* private void StorePackupToServerDataBase(final String entry_id, String packup_time, String packup_lat, String packup_lng) throws IOException {
        String address = GetAddress(packup_lat,packup_lng);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("packup_time", packup_time);
        params.put("packup_location", address);
        params.put("packup_lat", packup_lat);
        params.put("packup_lng", packup_lng);
        params.put("packup_entry_type", "ManualOffline");

        client.post(Config.URLupdatePackupTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    Toast.makeText(getApplicationContext(), "Packup Time Updated to Server", Toast.LENGTH_LONG).show();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_Packup set Status = 'done' where entry_id = '"+entry_id+"'");
                    long result=sqLiteStatement.executeUpdateDelete();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Toast.makeText(getApplicationContext(), "Could Not Connect to Network", Toast.LENGTH_LONG).show();
            }
        });

    }//StorePackupToServerDataBase*/

    public void showToast(String message, int time) {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }


}//BackgroundService
