package com.webvectors.cintaa2.utility;

/**
 * Created by Shri on 12/07/2016.
 */
public class Constants {
    public static final String shoot_date="shoot_date";
    public static final String house_name="house_name";
    public static final String shoot_location="shoot_location";
    public static final String shift_time="shift_time";
}
