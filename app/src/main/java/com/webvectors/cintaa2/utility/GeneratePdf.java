package com.webvectors.cintaa2.utility;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ASUS on 08/11/2016.
 */

public class GeneratePdf {

    Context context;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    ProgressDialog pd;

    String filename;

    public GeneratePdf(Context context) {
        this.context = context;
    }

    private void showprogressdialog()
    {
        pd= new ProgressDialog(context);
        pd.setTitle(context.getString(R.string.please_wait));
        pd.setMessage(context.getString(R.string.backup_in_progress));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    public void addPage(final Document document, String pageTitleString, String filename) throws DocumentException
    {
        this.filename = filename;
        BackupAsync backup =  new BackupAsync(document,pageTitleString);
        backup.execute();
    //**************Sample Code Below:**********************
       /* try{
            document.open();
        Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
        Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
        Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
        // Start New Paragraph
        Paragraph pageTitle = new Paragraph();
        // Set Font in this Paragraph
        pageTitle.setFont(titleFont);
        pageTitle.setAlignment(Element.ALIGN_CENTER);
        pageTitle.setSpacingAfter(30f);
        // Add item into Paragraph
        pageTitle.add(pageTitleString);
        document.add(pageTitle);

        PdfPTable taskListTable = new PdfPTable(new float[] { 5, 5, 4,4 });
        // set table width a percentage of the page width
        taskListTable.setWidthPercentage(95f);
        taskListTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell taskCategoryCell = new PdfPCell(new Phrase("Category", catFont));
        PdfPCell taskNameCell = new PdfPCell(new Phrase("Task Name", catFont));
        PdfPCell startTimeCell = new PdfPCell(new Phrase("Start Time", catFont));
        PdfPCell endTimeCell = new PdfPCell(new Phrase("End Time", catFont));
        taskCategoryCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taskCategoryCell.setVerticalAlignment(Element.ALIGN_CENTER);
        taskCategoryCell.setPaddingBottom(8f);

        taskNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        taskNameCell.setPaddingBottom(8f);

        startTimeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        startTimeCell.setPaddingBottom(8f);

        endTimeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        endTimeCell.setPaddingBottom(8f);

        taskListTable.addCell(taskCategoryCell);
        taskListTable.addCell(taskNameCell);
        taskListTable.addCell(startTimeCell);
        taskListTable.addCell(endTimeCell);
        taskListTable.setHeaderRows(1);
        PdfPCell[] cells = taskListTable.getRow(0).getCells();
        for (int j=0;j<cells.length;j++){
            cells[j].setBackgroundColor(BaseColor.GRAY);
        }
        PdfPCell cell;
        int ip = 0;

        do {
            ip++;
            cell=  new PdfPCell(new Phrase("A1", normal));
            cell.setPaddingBottom(8f);
            cell.setPaddingLeft(8f);
            taskListTable.addCell(cell);

            cell=  new PdfPCell(new Phrase("A2", normal));
            cell.setPaddingBottom(8f);
            cell.setPaddingLeft(8f);
            taskListTable.addCell(cell);

            cell=  new PdfPCell(new Phrase("A3", normal));
            cell.setPaddingBottom(8f);
            cell.setPaddingLeft(8f);
            taskListTable.addCell(cell);

            cell=  new PdfPCell(new Phrase("A4", normal));
            cell.setPaddingBottom(8f);
            cell.setPaddingLeft(8f);
            taskListTable.addCell(cell);

            taskListTable.completeRow();
        } while (ip>2);
        document.add(taskListTable);
        // Create new Page in PDF
        document.newPage();
        document.close();
        //  new SendBackupMail(filename,context);
        // deleteServerBackup();
        Toast.makeText(context, "Backup Successfull", Toast.LENGTH_SHORT).show();

} catch (Exception e) {
        e.printStackTrace();
        }*/

    }//addPage

    class BackupAsync extends AsyncTask<String,String,String>  {

        Document document;
        String pageTitleString;

        ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();

        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        final String member_id=  preferences.getString("member_id","abc");



        public BackupAsync(Document document, String pageTitleString) {
            this.document = document;
            this.pageTitleString = pageTitleString;
        }


        @Override
        protected void onPreExecute() {
            showprogressdialog();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {



            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.URLgetBackup);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                paramsString.append( URLEncoder.encode("member_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(member_id, "UTF-8"));


            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse=reader.readLine();

                JSONObject response = new JSONObject(JsonResponse);
                JSONArray jsonArray = response.getJSONArray("CompletedBackup");
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            URL url = new URL(j1.getString("authority_sign"));
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("GET");
                            connection.setDoInput(true);
                            connection.connect();
                            InputStream input = connection.getInputStream();
                            Bitmap mBitmap = BitmapFactory.decodeStream(input);

                            File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA/Backup/sign");
                            if (!direct.exists()) {
                                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory()+"/CINTAA/Backup/sign");
                                wallpaperDirectory.mkdirs();
                            }
                            String image="" + i + ".png";
                            File outputFile = new File(new File(Environment.getExternalStorageDirectory()+"/CINTAA/Backup/sign"),image );
                            if (outputFile.exists()) {
                                outputFile.delete();
                            }

                            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                            mBitmap.compress(Bitmap.CompressFormat.PNG,100, fileOutputStream);
                            fileOutputStream.flush();
                            fileOutputStream.close();
                        }catch (Exception er){

                        }
                    }
                 }

                    try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;

            } catch (Exception e) {
                e.printStackTrace();
            }

            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground

        @Override
        protected void onPostExecute(String s) {



            try {
                JSONObject response = new JSONObject(s);
                JSONArray jsonArray = response.getJSONArray("CompletedBackup");
                if (jsonArray.length() > 0) {
                    document.open();
                    for (int i = 0; i < jsonArray.length(); i++) {
                    PdfPTable taskListTable = new PdfPTable(new float[] { 3,5 });
                    // set table width a percentage of the page width
                    taskListTable.setWidthPercentage(95f);
                    taskListTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell cell;
                    // Font Style for Document
                    Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);

                    // Start New Paragraph
                    Paragraph pageTitle = new Paragraph();
                    // Set Font in this Paragraph
                    pageTitle.setFont(titleFont);
                    pageTitle.setAlignment(Element.ALIGN_CENTER);
                    pageTitle.setSpacingAfter(30f);
                    // Add item into Paragraph
                    pageTitle.add(pageTitleString);
                    try {
                        document.add(pageTitle);
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }

                    Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
                    Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);


                    JSONObject j1 = jsonArray.getJSONObject(i);

                    String house_name = j1.getString("house_name");
                    String alies_name = j1.getString("alies_name");
                    if (!alies_name.equals("null")) {
                        house_name = house_name + "/" + alies_name;
                    }


                    cell=  new PdfPCell(new Phrase("Member ID:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(member_id, normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Film/TV Serial:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("film_tv_name"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Production House:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(house_name, normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Producer Name:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("producer_name"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Date of Shoot:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("shoot_date"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Location of Shoot:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("shoot_location"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Address of Shoot:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("shoot_address"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Character Name:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("played_character"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Shift Time:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("shift_time"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Call Time:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("call_time"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    String rate = j1.getString("rate") + " " + j1.getString("rate_type");

                    cell=  new PdfPCell(new Phrase("Rate:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(rate, normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    String due = j1.getString("due_days") + " days";

                    cell=  new PdfPCell(new Phrase("Due After:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(due, normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    String remark = j1.getString("remark");

                    if (remark.equals("null")){
                        remark="";
                    }

                    cell=  new PdfPCell(new Phrase("Remark:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(remark, normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                        if(j1.getString("rate").length()>0){
                            try {
                                double r = Double.parseDouble(j1.getString("rate"));
                                if(r<=Config.conveyanceLimit){
                                    cell=  new PdfPCell(new Phrase("Conveyance Fees:", catFont));
                                    cell.setPaddingBottom(8f);
                                    cell.setPaddingLeft(8f);
                                    taskListTable.addCell(cell);

                                    cell=  new PdfPCell(new Phrase(j1.getString("fees"), normal));
                                    cell.setPaddingBottom(8f);
                                    cell.setPaddingLeft(8f);
                                    taskListTable.addCell(cell);

                                    taskListTable.completeRow();
                                }
                            }catch (NumberFormatException e){}
                        }


                    cell=  new PdfPCell(new Phrase("In Time:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("callin_time"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Packup Time:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("packup_time"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Authority Name:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("authority_nm"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Authority Number:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("authority_number"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    cell=  new PdfPCell(new Phrase("Authority Sign:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                        try{
                            Image image = Image.getInstance(Environment.getExternalStorageDirectory()+"/CINTAA/Backup/sign/"+i+".png");
                            image.scalePercent(10f);

                            cell=  new PdfPCell(image);
                            cell.setPadding(3f);
                            taskListTable.addCell(cell);

                        }catch (Exception e){

                        }



                    taskListTable.completeRow();

                    String img= i + ".png";
                    File outputFile = new File(new File(Environment.getExternalStorageDirectory()+"/CINTAA/Backup/sign"),img );
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }

                    cell=  new PdfPCell(new Phrase("Date of Sign:", catFont));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    cell=  new PdfPCell(new Phrase(j1.getString("authority_sign_date"), normal));
                    cell.setPaddingBottom(8f);
                    cell.setPaddingLeft(8f);
                    taskListTable.addCell(cell);

                    taskListTable.completeRow();

                    document.add(taskListTable);
                    // Create new Page in PDF
                    document.newPage();
                }//EndFor

                    document.close();
                    //SendBackupMail sendBackupMail = new SendBackupMail(filename,context);
                   // sendBackupMail.execute();
                    //deleteServerBackup();
                    showToast("Backup Successfull", Toast.LENGTH_SHORT);

                    File CintaaDirectory= new File(Environment.getExternalStorageDirectory().toString() + "/CINTAA/Backup");
                    final File file = new File(CintaaDirectory,filename );
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile(file), "application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    Intent intent = Intent.createChooser(target, "Open File");
                    try {
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        // Instruct the user to install a PDF reader here, or something
                        showToast("No PDF Reader App Found",Toast.LENGTH_LONG);
                    }

                    }else{
                    showToast("No Backup Data Found", Toast.LENGTH_SHORT);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
               // Toast.makeText(context, "EXCP: "+e, Toast.LENGTH_SHORT).show();
                }
            if (pd != null) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        }//onPostExecute
    }//BackupAsync

    private void deleteServerBackup(){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        final String member_id=  preferences.getString("member_id","abc");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLdeleteAfterBackup, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
               // Toast.makeText(context, "could not connect", Toast.LENGTH_LONG).show();
            }
        });
    }//deleteServerBackup


    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(context, message, time);
        View toastView = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, context), CommonHelper.convertDpToPx(5, context), CommonHelper.convertDpToPx(10, context), CommonHelper.convertDpToPx(5, context));
        toast.show();
    }

}
