package com.webvectors.cintaa2.utility;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.activity.LoginActivity;
import com.webvectors.cintaa2.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ASUS on 17/08/2016.
 */
public class BackgroundService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // ProgressDialog pd;

    private boolean isRunning = false;
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;
    String last_callin;

    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient = null;
    LocationRequest mLocationRequest;
    String sourceLat = "0", sourceLon = "0";
    boolean flag = true, mGoogleApiClient_Once = true, entry_done = false;
    String DestLat = "0";
    String entryID;
    String DestLon = "0";



    @Override
    public void onCreate() {
        isRunning = true;

        sqLiteDB = new SQLiteDB(this);
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

       // Toast.makeText(this,"Service CREATE",Toast.LENGTH_SHORT).show();

        // mGoogleApiClient.connect();


    }//onCreate

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {

        isRunning = true;
        entry_done = false;
        flag = true;
        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        new Thread(new Runnable() {
            @Override
            public void run() {

                if (isRunning) {
                    String currDate = getCurrentDate();
                    List<String> entry_id, member_id, time, lat, lng, Task_callin;
                    List<Date> call_Time;

                    String AfterDate = null;
                    SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
                    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy


                    entry_id = new ArrayList<String>();
                    member_id = new ArrayList<String>();
                    time = new ArrayList<String>();
                    lat = new ArrayList<String>();
                    lng = new ArrayList<String>();
                    Task_callin = new ArrayList<String>();
                    call_Time = new ArrayList<>();

                    for (int i = 0; isRunning; i++) {
                        try {

                            entry_id.clear();
                            member_id.clear();
                            time.clear();
                            lat.clear();
                            lng.clear();
                            Task_callin.clear();
                            call_Time.clear();




                            cursor = sqLiteDatabaseRead.rawQuery("select entry_id,member_id,call_time,lat,lng,Task_callin from sqtb_diary_entry where shoot_date =? and Task_callin = 'undone' ORDER BY entry_id;", new String[]{currDate});
                            if (cursor.moveToFirst()) {
                                /////////////////////////
                                // String untildate="2011-10-08";//can take any date in current format
                                // SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(sdfDate.parse(currDate));
                                cal.add(Calendar.DATE, 2);
                                AfterDate = sdfDate.format(cal.getTime());
                                /////////////////////////////////

                                // isRunning = true;
                                do {
                                    entry_id.add(cursor.getString(0));
                                    member_id.add(cursor.getString(1));
                                    time.add(cursor.getString(2));
                                    lat.add(cursor.getString(3));
                                    lng.add(cursor.getString(4));
                                    Task_callin.add(cursor.getString(5));

                                } while (cursor.moveToNext());
                                //Get Last Callin for the day
                                last_callin = entry_id.get(entry_id.size() - 1);
                            } else {
                                break;
                            }
                            //Stop Service after two days of Event date
                            if (AfterDate.equals(currDate)) {
                               // isRunning = false;
                                StopService(startId);
                            }

                        } catch (Exception e) {
                        }


                        //Stop Service after the Last Call of the day is Recorded
                        if (entry_done) {
                            //isRunning = false;
                            StopService(startId);
                        }

                        try {
                            for (int j = 0; j < time.size() && time.size() >= 1; j++) {
								if(time.get(j)!=null)
	                                call_Time.add(sdfTime.parse(time.get(j)));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // Sort All The Callin Time in ascending order
                        Collections.sort(call_Time, new Comparator<Date>() {

                            @Override
                            public int compare(Date o1, Date o2) {
                                return o1.compareTo(o2);
                            }
                        });

                        Date currentTime = getCurrentTime();
                        for (int j = 0; j < call_Time.size(); j++) {
                            //Check if Callin is equals or after the current_time and Also check that whether for that callin time's InTime is marked.
                            if (((currentTime.equals(call_Time.get(j))) || (currentTime.after(call_Time.get(j))))) {
                                long diffr = call_Time.get(j).getTime() - currentTime.getTime();
                                if (diffr <= 0) {
                                    //String call = String.valueOf(call_Time.get(j));
                                    String call = sdfTime.format(call_Time.get(j));
                                    for (int k = 0; k < time.size() && time.size() >= 1; k++) {
                                        if (call.equalsIgnoreCase(time.get(k))  && lat.size()>k) {

                                                DestLat = lat.get(k);
                                                DestLon = lng.get(k);

											if(entry_id!=null && entry_id.size()>k)
	                                            entryID = entry_id.get(k);
                                            //Check users Location from destination. If InTime is not marked as "done" then only proceed further
                                            if (isRunning && Task_callin.get(j).equals("undone")) {
                                                buildGoogleApiClient();
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        try {
                            Thread.sleep(60000);//Thread Sleeps for One Minute

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }//end for
                        //isRunning = false;
                        StopService(startId);
                }//end if(isRunning)


            }
        }).start();


        return Service.START_REDELIVER_INTENT;
    }//onStartCommand

    private void StopService(int startId) {
        this.stopSelf(startId);
    }//StopService


    @Override
    public void onDestroy() {
        super.onDestroy();
        //  Toast.makeText(this,"Service onDestroyed",Toast.LENGTH_LONG).show();
       // mGoogleApiClient.disconnect();
    }

    /* SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");//dd/MM/yyyy
     SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");//dd/MM/yyyy
     */
    public static String getCurrentDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentDate


    public static Date getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("h:mm a", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        Date d = new Date();
        try {
            d = sdfDate.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return d;
    }//getCurrentTime


    /**************************GeoLocation***************************/


    @Override
    public void onConnected(Bundle bundle) {

        //   mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(120000); // Update location after every 2 minutes


        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;

        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
           // Toast.makeText(this,"SOURCE LAT LNG",Toast.LENGTH_SHORT).show();
            sourceLat = String.valueOf(mLastLocation.getLatitude());
            sourceLon = String.valueOf(mLastLocation.getLongitude());
            updateUI();
        }else{
            //turnGPSOn();
            showToast("GPS is OFF, Callin Not Recorded",Toast.LENGTH_LONG);
            NotificationCallinError();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        sourceLat = String.valueOf(location.getLatitude());
        sourceLon = String.valueOf(location.getLongitude());
        if(flag)
        {
            updateUI();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    synchronized void buildGoogleApiClient() {



        if(mGoogleApiClient_Once){

            mGoogleApiClient_Once=false;

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        mGoogleApiClient.connect();
    }

   /* @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }*/



    void updateUI() {
        /*txtOutputLat.setText(lat);
        txtOutputLon.setText(lon);*/
        double destLatitude, destLongitude,sourceLatitude,sourceLongitude;
        destLatitude = Double.parseDouble(DestLat);
        destLongitude = Double.parseDouble(DestLon);
        sourceLatitude = Double.parseDouble(sourceLat);
        sourceLongitude = Double.parseDouble(sourceLon);

        double d=calculateDistance(sourceLatitude,sourceLongitude,destLatitude,destLongitude);
      //  Toast.makeText(this,"Dist: "+d,Toast.LENGTH_SHORT).show();
        if(d<=501) //if Destinatin is 500 meter away. "1000 mtr = 1 km"
        {
            flag=false;
            if(isNetworkConnected()){
                //////////////////////////////////////////////////////////////
                Geocoder geocoder;
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addresses;
                String address1,city,country,state,postalCode,knownName,StateName,CityName,CountryName;

                try {
                    // Getting address from found locations.
                    addresses = geocoder.getFromLocation(sourceLatitude, sourceLongitude, 1);
                    address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    //postalCode = String.valueOf(addresses.get(0).getPostalCode());
                    knownName = addresses.get(0).getFeatureName();
                    // you can get more details other than this . like country code, state code, etc.

                    String address  = address1+"; "+city+"; "+state+"; "+country;

                   /* if(!postalCode.equalsIgnoreCase("null")){
                        address  = address1+"; "+city+"; "+state+"; "+country+"- "+postalCode;
                    }*/

                    String callTime = getCurrentTimeStamp();

                    storeCallinToDB( address, sourceLatitude, sourceLongitude,callTime);
                    //Toast.makeText(getApplicationContext(),address1+"; "+city+"; "+state+"; "+country+"- "+postalCode,Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    showToast("EXCP4: " + e.toString(),Toast.LENGTH_SHORT);
                    e.printStackTrace();
                }

            }else {
                showToast("\"Network connection is closed...Please enable data connection\"",Toast.LENGTH_LONG);
                NotificationCallinError();
            }



        }
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    //---------------Calculate Location-----------------//
    private Double calculateDistance(Double lat,Double lng,Double lat1,Double lng1)
    {
        Location locationA = new Location("point A");
        Location locationB = new Location("point B");
        //set lat/lng of first
        locationA.setLatitude(lat);
        locationA.setLongitude(lng);
        //set lat/lng of second
        locationB.setLatitude(lat1);
        locationB.setLongitude(lng1);
        Double distance1;
        distance1 = Double.valueOf(locationA.distanceTo(locationB));
        return distance1;
    }

    /******************Update Callin to Database**********************/
    private void storeCallinToDB(String address, double Lat, double Lng, final String Callin){
        //select entry_id Task_callin from sqtb_diary_entry where Task_callin = 'undone'
        cursor = sqLiteDatabaseRead.rawQuery("Select Task_callin from sqtb_diary_entry where entry_id =?;", new String[]{entryID});
        if (cursor.moveToFirst()) {
            String Status = cursor.getString(0);
            if(Status.equalsIgnoreCase("undone")){

                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                params.put("entry_id", entryID);
                params.put("callin_time", Callin);
                params.put("callin_location", address);
                params.put("callin_lat", Lat);
                params.put("callin_lng", Lng);
                params.put("callin_entry_type","Automatic");

                client.post(Config.URLupdateCallinTime, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        try {
                            String s1 = response.getString("success");

                            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                            sqLiteStatement.bindString(1,entryID);
                            sqLiteStatement.bindString(2,Callin);
                            sqLiteStatement.bindString(3, "0");
                            sqLiteStatement.bindString(4, "0");
                            sqLiteStatement.bindString(5,"done");
                            long res=sqLiteStatement.executeInsert();

                            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set Task_callin = 'done' where entry_id = '"+entryID+"'");
                            long result=sqLiteStatement.executeUpdateDelete();
                            showToast("Callin Recorded Successfully ", Toast.LENGTH_LONG);
                            NotificationCallinDone();
                            //isRunning = false;

                            if(last_callin.equals(entryID)){
                                entry_done = true;
                            }//Stop Service For Last Callin for the day
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // pd.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        showToast("Could Not Connect", Toast.LENGTH_LONG);
                        //  pd.dismiss();
                        NotificationCallinError();
                    }
                });

            }
        }


    }//storeCallinToDB

    private void NotificationCallinDone(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle("CINTAA")
                        .setContentText("In Time Recorded Successfully");

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        //Vibration
        mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        //LED
        mBuilder.setLights(Color.RED, 3000, 3000);

// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, LoginActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LoginActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("In Time Recorded Successfully"));
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        int mId = 0;
        mNotificationManager.notify(mId, mBuilder.build());
    }//NotificationCallinDone

    private void NotificationCallinError(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle("CINTAA")
                        .setContentText("In Time Not Recorded, Submit In Time Manually");

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        //Vibration
        mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        //LED
        mBuilder.setLights(Color.RED, 3000, 3000);

// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, LoginActivity.class);
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LoginActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("In Time Not Recorded, Submit In Time Manually"));
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        int mId = 0;
        mNotificationManager.notify(mId, mBuilder.build());
    }//NotificationCallinError


    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView = toast.getView();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }

}//BackgroundService
