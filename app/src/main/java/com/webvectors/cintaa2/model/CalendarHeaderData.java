package com.webvectors.cintaa2.model;

/**
 * Created by ASUS on 02/08/2016.
 */
public class CalendarHeaderData {
    public String startTime;
    public String filmName;
    public String shiftTime;
    public String Location;

    public CalendarHeaderData(String startTime, String filmName, String shiftTime, String location) {
        this.startTime = startTime;
        this.filmName = filmName;
        this.shiftTime = shiftTime;
        Location = location;
    }

    public CalendarHeaderData(String startTime, String filmName) {
        this.startTime = startTime;
        this.filmName = filmName;
    }
}
