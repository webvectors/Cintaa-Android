package com.webvectors.cintaa2.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ScheduleEntryEditData {
    String entryId,filmName,productionHouse,producerName,scheduleStatus,dateOfShoot;
    Boolean callStatus;
    Date shootDateFormatted;

    public ScheduleEntryEditData(String entryId,String dateOfShoot, String filmName, String productionHouse, String producerName, Boolean callStatus) {
        this.entryId = entryId;
        this.dateOfShoot = dateOfShoot;
        this.filmName = filmName;
        this.productionHouse = productionHouse;
        this.producerName = producerName;
        this.callStatus = callStatus;
    }

    public ScheduleEntryEditData(String entryId,String dateOfShoot, String filmName, String productionHouse, String producerName, Boolean callStatus, String scheduleStatus) {
        this.entryId = entryId;
        this.dateOfShoot = dateOfShoot;
        this.filmName = filmName;
        this.productionHouse = productionHouse;
        this.producerName = producerName;
        this.callStatus = callStatus;
        this.scheduleStatus = scheduleStatus;
    }

    public String getEntryId() {
        return entryId;
    }

    public String getDateOfShoot() {
        return dateOfShoot;
    }

    public String getFilmName() {
        return filmName;
    }

    public String getProductionHouse() {
        return productionHouse;
    }

    public String getProducerName() {
        return producerName;
    }

    public Boolean getCallStatus()
    {
        return callStatus;
    }

    public String getScheduleStatus(){
        return scheduleStatus;
    }


    private List<ScheduleEntryEditData> getDateFormat(List<ScheduleEntryEditData> scheduleData){
        for(int i=0; i<scheduleData.size();i++){

        }
        return scheduleData;
    }
    private List<ScheduleEntryEditData> getStringFormat(List<ScheduleEntryEditData> scheduleData){
        return scheduleData;
    }


    public List<ScheduleEntryEditData> orderListByShootDate(List<ScheduleEntryEditData> scheduleData){

        Collections.sort(scheduleData, new Comparator<ScheduleEntryEditData>() {
			@Override
			public int compare(ScheduleEntryEditData t1, ScheduleEntryEditData t2) {

                return t1.getDateOfShoot().compareTo(t2.getDateOfShoot());
			}
		});
		Collections.sort(scheduleData,Collections.reverseOrder());
        return scheduleData;
    }//orderByShootDate
}
