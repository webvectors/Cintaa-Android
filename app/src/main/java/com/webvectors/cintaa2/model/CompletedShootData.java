package com.webvectors.cintaa2.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class CompletedShootData {


    private final String house_name, mRemark, film_tv_name, producer_name, shoot_date, shoot_location, played_character, shift_time, call_time, rate, due_days, callin_time;
    private final String packup_time,authority_nm,authority_number,authority_sign,fees,house_email,rater,authority_sign_date;

    public CompletedShootData(String house_name, String mRemark, String film_tv_name,
                              String producer_name, String shoot_date, String shoot_location,
                              String played_character, String shift_time, String call_time,
                              String rate, String due_days, String callin_time, String packup_time,
                              String authority_nm, String authority_number, String authority_sign,
                              String fees, String house_email, String rater, String authority_sign_date) {

        this.house_name = house_name;
        this.mRemark = mRemark;
        this.film_tv_name = film_tv_name;
        this.producer_name = producer_name;
        this.shoot_date = shoot_date;
        this.shoot_location = shoot_location;
        this.played_character = played_character;
        this.shift_time = shift_time;
        this.call_time = call_time;
        this.rate = rate;
        this.due_days = due_days;
        this.callin_time = callin_time;
        this.packup_time = packup_time;
        this.authority_nm = authority_nm;
        this.authority_number = authority_number;
        this.authority_sign = authority_sign;
        this.fees = fees;
        this.house_email = house_email;
        this.rater = rater;
        this.authority_sign_date = authority_sign_date;

    }

}
