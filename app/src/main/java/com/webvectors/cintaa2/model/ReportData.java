package com.webvectors.cintaa2.model;

/**
 * @author The Brown Arrow.
 */

public class ReportData {

    private String house_name, mRemark, film_tv_name, producer_name, shoot_date, shoot_location, played_character, shift_time, call_time, rate, due_days, callin_time, packup_time, authority_nm, authority_number, authority_sign, fees, house_email, rater, authority_sign_date;

    public String getHouse_name() {
        return house_name;
    }

    public void setHouse_name(String house_name) {
        this.house_name = house_name;
    }

    public String getmRemark() {
        return mRemark;
    }

    public void setmRemark(String mRemark) {
        this.mRemark = mRemark;
    }

    public String getFilm_tv_name() {
        return film_tv_name;
    }

    public void setFilm_tv_name(String film_tv_name) {
        this.film_tv_name = film_tv_name;
    }

    public String getProducer_name() {
        return producer_name;
    }

    public void setProducer_name(String producer_name) {
        this.producer_name = producer_name;
    }

    public String getShoot_date() {
        return shoot_date;
    }

    public void setShoot_date(String shoot_date) {
        this.shoot_date = shoot_date;
    }

    public String getShoot_location() {
        return shoot_location;
    }

    public void setShoot_location(String shoot_location) {
        this.shoot_location = shoot_location;
    }

    public String getPlayed_character() {
        return played_character;
    }

    public void setPlayed_character(String played_character) {
        this.played_character = played_character;
    }

    public String getShift_time() {
        return shift_time;
    }

    public void setShift_time(String shift_time) {
        this.shift_time = shift_time;
    }

    public String getCall_time() {
        return call_time;
    }

    public void setCall_time(String call_time) {
        this.call_time = call_time;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDue_days() {
        return due_days;
    }

    public void setDue_days(String due_days) {
        this.due_days = due_days;
    }

    public String getCallin_time() {
        return callin_time;
    }

    public void setCallin_time(String callin_time) {
        this.callin_time = callin_time;
    }

    public String getPackup_time() {
        return packup_time;
    }

    public void setPackup_time(String packup_time) {
        this.packup_time = packup_time;
    }

    public String getAuthority_nm() {
        return authority_nm;
    }

    public void setAuthority_nm(String authority_nm) {
        this.authority_nm = authority_nm;
    }

    public String getAuthority_number() {
        return authority_number;
    }

    public void setAuthority_number(String authority_number) {
        this.authority_number = authority_number;
    }

    public String getAuthority_sign() {
        return authority_sign;
    }

    public void setAuthority_sign(String authority_sign) {
        this.authority_sign = authority_sign;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getHouse_email() {
        return house_email;
    }

    public void setHouse_email(String house_email) {
        this.house_email = house_email;
    }

    public String getRater() {
        return rater;
    }

    public void setRater(String rater) {
        this.rater = rater;
    }

    public String getAuthority_sign_date() {
        return authority_sign_date;
    }

    public void setAuthority_sign_date(String authority_sign_date) {
        this.authority_sign_date = authority_sign_date;
    }
}
