package com.webvectors.cintaa2.model;

/**
 * Created by ASUS on 19/07/2016.
 */
public class DiarySnapshotListSorting {

    public String date;
    public String productionHouse;
    public String location;
    public String shiftTime;
    public String entry_id;

    public DiarySnapshotListSorting(String date, String productionHouse, String location, String shiftTime, String entry_id) {
        this.date = date;
        this.productionHouse = productionHouse;
        this.location = location;
        this.shiftTime = shiftTime;
        this.entry_id = entry_id;
    }
}
