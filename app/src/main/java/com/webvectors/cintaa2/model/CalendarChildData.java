package com.webvectors.cintaa2.model;

/**
 * Created by ASUS on 02/08/2016.
 */
public class CalendarChildData {
    public String shiftTime;
    public String Location;
    public String entry_id;
    String house_name;

    public CalendarChildData(String shiftTime, String location, String entry_id,String house_name) {
        this.shiftTime = shiftTime;
        Location = location;
        this.entry_id = entry_id;
        this.house_name = house_name;
    }

}
