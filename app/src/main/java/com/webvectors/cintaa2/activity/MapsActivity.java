package com.webvectors.cintaa2.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.webvectors.cintaa2.adapter.PlaceArrayAdapter;
import com.webvectors.cintaa2.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    Context mContext;
    TextView tvAddress;
    Button btok;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient = null;
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(1000)
            .setFastestInterval(16)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    static boolean counter=true;
    AutoCompleteTextView mAutocompleteTextView_to_loc;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    public static String curlocation;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private Animation animShow, animHide;
    String latitude,longitude;
    Geocoder geocoder;
    // EditText et_flat,et_address,et_landmark,et_city,et_zip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mContext = this;

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Initialise();
        mapFragment.getMapAsync(this);
        if(mGoogleApiClient==null){
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .addApi(Places.GEO_DATA_API)
                    .build();
        }

        mAutocompleteTextView_to_loc = (AutoCompleteTextView)  findViewById(R.id.autoCompleteTextView_to_loc);
        mAutocompleteTextView_to_loc.setThreshold(3);
        mAutocompleteTextView_to_loc.setOnItemClickListener(mAutocompleteClickListenerDestination);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView_to_loc.setAdapter(mPlaceArrayAdapter);
        geocoder = new Geocoder(mContext, Locale.getDefault());
        /*et_flat=(EditText)findViewById(R.id.et_flat);
        et_address=(EditText)findViewById(R.id.et_address);
        et_landmark=(EditText)findViewById(R.id.et_landmark);
        et_city=(EditText)findViewById(R.id.et_city);
        et_zip=(EditText)findViewById(R.id.et_zip);*/
        //initPopup();

        btok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }//OnCreate

    private void Initialise(){
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");
        tvAddress = (TextView) findViewById(R.id.uber_tvAddress);
        tvAddress.setTypeface(custom_font);
        btok = (Button) findViewById(R.id.btok);
        btok.setTypeface(custom_font);

    }//Initialise

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestination = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            curlocation = String.valueOf(item.description);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackDestination);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);


            /*Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("DEST",dest);*/
        }

    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackDestination = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            LatLng mlatlng = place.getLatLng();

            String[] parts;
            String part1, part2;
            parts = mlatlng.toString().split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            String[] s3 = s1.split("\\(");
            String[] s4 = s2.split("\\)");

            LatLng India = new LatLng(Double.parseDouble(s3[1]), Double.parseDouble(s4[0]));
            // mMap.addMarker(new MarkerOptions().position(India).title("Marker in " + curlocation));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(India));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
            mMap.getMaxZoomLevel();

        }

    };
    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(this);
        }
        super.onStop();
    }
    private void stopAutoManage() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.stopAutoManage(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            checkGps();
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //googleMap.getUiSettings().setRotateGesturesEnabled(false);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition position) {
                // TODO Auto-generated method stub

                Log.w(""+position.target.latitude, ""+position.target.longitude);

                new ReverseGeocodingTask().execute(position.target);


            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }


    }

    @Override
    public void onLocationChanged(Location loc) {
        // TODO Auto-generated method stub
        if (loc == null)
            return;


      /* if (markerCurre == null) {
            markerCurre = mMap.addMarker(new MarkerOptions()
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.droplocation_icon))
                    .title("Your Current Position")
                    .anchor(0.5f, 0.5f)
                    .position(new LatLng(loc.getLatitude(), loc.getLongitude())));*/
        if(counter)
        {
            counter=false;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 16.0f));
        }
    }
    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            checkGps();
            return;
        }
        Location mLastLocation;
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,REQUEST,this);  // LocationListener
            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        }else{
            checkGps();
        }

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        mPlaceArrayAdapter.setGoogleApiClient(null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }
    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String>{
        double _latitude,_longitude;

        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder = new Geocoder(mContext);
            _latitude = params[0].latitude;
            _longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText="";

            try {
                addresses = geocoder.getFromLocation(_latitude, _longitude,1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(addresses != null && addresses.size() > 0 ){
                Address returnedAddress = addresses.get(0);
                String country = returnedAddress.getCountryName();
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    if(returnedAddress.getMaxAddressLineIndex()==(i-1))
                    {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i));
                    }
                    else
                    {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                    }
                }
                addressText = strReturnedAddress.toString()+country;
                // Log.w("My Current loction address", "" + strReturnedAddress.toString());
            }

            return addressText+":"+_latitude+":"+_longitude;
        }

        @Override
        protected void onPostExecute(String addressText) {
            //final String result=addressText;
            String parts[];
            parts = addressText.split(":");
            final String result=parts[0];
            latitude = parts[1];
            longitude = parts[2];
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    if(result.equals(""))
                        tvAddress.setText("getting address...");
                    tvAddress.setText(result);
                }
            });
        }
    }//ReverseGeocodingTask class

    public void checkGps(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.gps_dialog);
        dialog.setTitle("GPS");
        dialog.setCancelable(false);


        Button gpsSettings = (Button) dialog.findViewById(R.id.gps_sett_button);
        gpsSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
            }
        });

        Button gpsCancel = (Button) dialog.findViewById(R.id.gps_cancel_button);
        gpsCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
    }//checkGps

}
