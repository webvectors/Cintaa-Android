package com.webvectors.cintaa2.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.utility.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class UpdateAppActivity extends AppCompatActivity {

    TextView tvupdateapp;
    Button btnupdate;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_app);

        sqLiteDB = new SQLiteDB(this);
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        tvupdateapp = (TextView)findViewById(R.id.tvupdateapp);
        tvupdateapp.setTypeface(font);
        btnupdate = (Button)findViewById(R.id.btnupdate);

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*
                PackageManager manager = getApplicationContext().getPackageManager();
                PackageInfo info = null;
                try {
                    info = manager.getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String versionName = info.versionName;
                String versionCode = String.valueOf(info.versionCode);
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_appversion set versionName='"+versionName+"', versionCode='"+versionCode+"';");
                int n = sqLiteStatement.executeUpdateDelete();
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                */
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.ic_youtube.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        checkProcess();
    }//onCreate

    private void checkProcess(){

        String versionName = "1.0";
        String versionCode = "1";
        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionName = info.versionName;
        versionCode = String.valueOf(info.versionCode);

        try{
            cursor=sqLiteDatabaseRead.rawQuery("select versionName, versionCode from sqtb_appversion",null);
            if(cursor.moveToFirst())
            {
                do {
                    String version_nm = cursor.getString(0);
                    String version_cod = cursor.getString(1);

                    if((version_nm.equals(versionName)) || (versionCode.equals(version_cod))){
                        startActivity(new Intent(UpdateAppActivity.this, LoginActivity.class));
                        finish();
                    }
                }while (cursor.moveToNext());
            }
        }finally {
            if (cursor!=null)
                cursor.close();
        }

        if(Config.isNetworkConnected(UpdateAppActivity.this)){
            CheckSession();
        }else {
            showToast("Enable Network Connection",Toast.LENGTH_LONG);
        }

    }//checkProcess

    private void CheckSession(){

        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(UpdateAppActivity.this);
        String member_id=  preferences.getString("member_id","abc");

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);

        client.post(Config.URLselectUserSession, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray= response.getJSONArray("Session_Check");
                    JSONArray jsonArray_appVersion= response.getJSONArray("App_version");

                    if(jsonArray.length()>0 && jsonArray_appVersion.length()>0) {

                        JSONObject jsonObject = jsonArray_appVersion.getJSONObject(0);
                        String app_ver_nm = jsonObject.getString("versionName");
                        String app_ver_code = jsonObject.getString("versionCode");

                        //versionName, versionCode from
                        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_appversion set versionName='"+app_ver_nm+"', versionCode='"+app_ver_code+"';");
                        int n = sqLiteStatement.executeUpdateDelete();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String Session_value = j1.getString("user_session");

                            if(Session_value.equals("OFF")){
                                SharedPreferences preferences;
                                preferences = PreferenceManager.getDefaultSharedPreferences(UpdateAppActivity.this);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.commit();

                                Intent intent = new Intent(UpdateAppActivity.this,LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                showToast("Network Error",Toast.LENGTH_LONG);
            }
        });


    }//CheckSession

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView  = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }
}
