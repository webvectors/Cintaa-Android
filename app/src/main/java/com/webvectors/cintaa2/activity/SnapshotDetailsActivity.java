package com.webvectors.cintaa2.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;

//import com.ic_youtube.android.gms.nearby.messages.Message;

public class SnapshotDetailsActivity extends BaseActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private AdView mAdView;
    SharedPreferences sharedPreferences;
    String encoded_string, img_name, member_id, entry_id, callin_time, packup_time;
    String submit_time, phouse_email, house_id;
    TextView etmaplocation, tvconveyance, tvflimnm, tvphouse, tvprodnm, tvshootdate, tvlocation, tvcharnm, tvshifttime, tvrate, tvduedays, tvremark, tvcallin;
    LinearLayout llconveyancedetail, llph_email;
    EditText etauthoritynm, etcallin, etpackup, etauthoritynumb, etauthorityemail;
    ImageView ivauthoritsign;
    Button btnpackup, btnpackupsubmit, btncallin, btncallinsubmit, btsubmit;
    ProgressDialog pd;
    int callin_flag = 0, packup_flag = 0, check = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    // private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    SignaturePad mSignaturePad;
    Button mClearButton;
    Button mSaveButton;
    Bitmap signatureBitmap;

    private static String username = "";
    private static String password = "";
    private static String name = "", email, subject;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;
    Geocoder geocoder;
    List<Address> addresses;

    String address1, city, country, state, postalCode, knownName, StateName, CityName, CountryName;
    double MyLat = 0;
    double MyLong = 0;
    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    String sublat, sublon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapshot_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        member_id = sharedPreferences.getString("member_id", "abc");
        entry_id = sharedPreferences.getString("Snapshot_list_item", "xcz");
        initializeControls();
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        if (savedInstanceState != null) {
            signatureBitmap = savedInstanceState.getParcelable("Auth_Sign_img");
            if (signatureBitmap != null) {
                ivauthoritsign.setImageBitmap(signatureBitmap);
            }
        }

        sqLiteDB = new SQLiteDB(getApplicationContext());
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        buildGoogleApiClient();


        selectSnapshotDeatils();

        btncallin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shoot_date = tvshootdate.getText().toString().trim();
                SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

                Calendar cal = Calendar.getInstance();
                String currentDate = sdfDate.format(cal.getTime());
                Date currDate = new Date();
                Date end_date = new Date();

                try {

                    currDate = sdfDate.parse(currentDate);
                    end_date = sdfDate.parse(shoot_date);

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

                long diff = end_date.getTime() - currDate.getTime();
                int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                if (DaysFromShoot <= 0) {
                    callin_time = getCurrentTime();
                    etcallin.setText(callin_time);
                } else {
                    showToast("In Time Cannot be Recorded before Date of Shoot", Toast.LENGTH_LONG);
                }

            }
        });
        btncallinsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etcallin.getText().toString().length() == 0) {
                    etcallin.setError("In Time is Not Set");
                    showToast("In Time is Not Set", Toast.LENGTH_LONG);

                } else {
                    if (isNetworkConnected()) {
                        //////////////////////////////////////////////////////////////
                        try {
                            // Getting address from found locations.
                            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
                            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            knownName = addresses.get(0).getFeatureName();
                            // you can get more details other than this . like country code, state code, etc.

                            String address = address1 + "; " + city + "; " + state + "; " + country;
                            ;
                            storeCallinToDB(address, MyLat, MyLong);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        //Insert Callin Details To SQLite
                        InsertCallinSQLiteOFFLine(entry_id, String.valueOf(MyLat), String.valueOf(MyLong));

                        //Toast.makeText(getApplicationContext(), "\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG).show();
                    }


                }
                return;

            }
        });

        btnpackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etcallin.getText().toString().length() == 0) {
                    etcallin.setError("In Time is Not Set");
                    showToast("In Time is Not Set", Toast.LENGTH_LONG);

                } else {
                    String shoot_date = tvshootdate.getText().toString().trim();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

                    Calendar cal = Calendar.getInstance();
                    String currentDate = sdfDate.format(cal.getTime());
                    Date currDate = new Date();
                    Date end_date = new Date();

                    try {

                        currDate = sdfDate.parse(currentDate);
                        end_date = sdfDate.parse(shoot_date);

                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }

                    long diff = end_date.getTime() - currDate.getTime();
                    int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                    if (DaysFromShoot <= 0) {
                        packup_time = getCurrentTime();
                        etpackup.setText(packup_time);
                    } else {
                        showToast("Packup Time Cannot be Recorded before Date of Shoot", Toast.LENGTH_LONG);
                    }
                }
                return;
            }
        });
        btnpackupsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etpackup.getText().toString().length() == 0) {
                    etpackup.setError("Packup Time is Not Set");
                    showToast("Packup Time is Not Set", Toast.LENGTH_LONG);
                } else {
                    if (isNetworkConnected()) {
                        //////////////////////////////////////////////////////////////
                        try {
                            // Getting address from found locations.
                            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
                            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            knownName = addresses.get(0).getFeatureName();
                            // you can get more details other than this . like country code, state code, etc.

                            String address = address1 + "; " + city + "; " + state + "; " + country;
                            storePackupToDB(address, MyLat, MyLong);
                            /*if (!postalCode.equals("null")) {
                                address = address1 + "; " + city + "; " + state + "; " + country + "- " + postalCode;
                            } else {

                            }*/


                            //Toast.makeText(getApplicationContext(),address1+"; "+city+"; "+state+"; "+country+"- "+postalCode,Toast.LENGTH_LONG).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        //Insert Packup Details To SQLite
                        InsertPackupSQLiteOFFLine(entry_id, String.valueOf(MyLat), String.valueOf(MyLong));

                        showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
                    }


                }
                return;
            }
        });

        btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((etauthoritynumb.getText().toString().length() == 0) && (etauthoritynm.getText().toString().length() == 0)) {
                    etauthoritynm.setError("Name of Signing Authority Cannot be Empty");
                    etauthoritynumb.setError("Number of Authority is Required");
                    showToast("Authority Name or Number is Missing", Toast.LENGTH_LONG);
                } else {
                    if (ivauthoritsign.getDrawable() == null) {
                        showToast("Authority Sign is Missing", Toast.LENGTH_LONG);
                    } else {
                        if (etauthoritynumb.getText().toString().trim().length() == 10) {
                            if (etauthorityemail.getText().toString().trim().length() > 0 && isValidEmaillId(etauthorityemail.getText().toString().trim())) {
                                String shoot_date = tvshootdate.getText().toString().trim();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

                                Calendar cal = Calendar.getInstance();
                                String currentDate = sdfDate.format(cal.getTime());
                                Date currDate = new Date();
                                Date end_date = new Date();

                                try {

                                    currDate = sdfDate.parse(currentDate);
                                    end_date = sdfDate.parse(shoot_date);

                                } catch (java.text.ParseException e) {
                                    e.printStackTrace();
                                }

                                long diff = end_date.getTime() - currDate.getTime();
                                int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                                if (DaysFromShoot <= 0) {
                                    if (isNetworkConnected()) {
                                        submit_time = getCurrentTime();
                                        //SubmitEvent();
                                        /////////////////////////////////////////
                                        try {
                                            new Encode_image().execute();

                                        } catch (Exception e1) {
                                            showToast("Error in Uploading Image", Toast.LENGTH_SHORT);
                                        }
                                        ////////////////////////////////////////

                                    } else {
                                        showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
                                    }
                                } else {
                                    showToast("Event Cannot be Submited before Date of Shoot", Toast.LENGTH_LONG);
                                }
                            } else {
                                etauthorityemail.setError("Input Valid Email");
                                showToast("Input Valid Email", Toast.LENGTH_LONG);
                            }
                        } else {
                            etauthoritynumb.setError("Input Valid Number");
                            showToast("Input Valid Number", Toast.LENGTH_LONG);
                        }

                    }


                }
                return;
            }
        });
        ivauthoritsign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showsignaturedialog();
            }
        });
    }//onCreate

    private void InsertCallinSQLiteOFFLine(String entry_id, String MyLat, String MyLong) {

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
        //entry_id, callin_time, callin_lat, callin_lng, Status
        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, etcallin.getText().toString().trim());
        sqLiteStatement.bindString(3, MyLat);
        sqLiteStatement.bindString(4, MyLong);
        sqLiteStatement.bindString(5, "Pending");
        long res = sqLiteStatement.executeInsert();

        //Invokes Background Service to in Time Update to Server
        //startService(new Intent(SnapshotDetailsActivity.this, BackgroundService_OffLineCallinPackup.class));


        //Functions Performed in OnSuccess of Callin Submit
        showToast("In Time Recorded Successfully in OffLine Mode", Toast.LENGTH_LONG);
        btncallinsubmit.setEnabled(false);
        btncallin.setEnabled(false);
        btncallin.setAlpha((float) 0.5);
        btncallinsubmit.setAlpha((float) 0.5);

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set Task_callin = 'done' where entry_id = '" + entry_id + "'");
        long result = sqLiteStatement.executeUpdateDelete();

    }//InsertCallinSQLiteOFFLine

    private void InsertPackupSQLiteOFFLine(String entry_id, String MyLat, String MyLong) {

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Packup values(?,?,?,?,?)");
        //entry_id, packup_time, packup_lat, packup_lng, Status
        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, etpackup.getText().toString().trim());
        sqLiteStatement.bindString(3, MyLat);
        sqLiteStatement.bindString(4, MyLong);
        sqLiteStatement.bindString(5, "Pending");
        long res = sqLiteStatement.executeInsert();

        //Invokes Background Service to Packup Time Update to Server
        //startService(new Intent(SnapshotDetailsActivity.this, BackgroundService_OffLineCallinPackup.class));

        //Functions Performed in OnSuccess of Packup Submit
        showToast("Packup Recorded Successfully in OffLine Mode", Toast.LENGTH_LONG);
        btnpackupsubmit.setEnabled(false);
        btnpackup.setEnabled(false);
        btnpackup.setAlpha((float) 0.5);
        btnpackupsubmit.setAlpha((float) 0.5);

    }//InsertPackupSQLiteOFFLine

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //outState.putExtra("BitmapImage", signatureBitmap);
        if (signatureBitmap != null) {
            outState.putParcelable("Auth_Sign_img", signatureBitmap);
        }
    }

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public void checkGps() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.gps_dialog);
        dialog.setTitle("GPS");
        dialog.setCancelable(false);


        Button gpsSettings = (Button) dialog.findViewById(R.id.gps_sett_button);
        gpsSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
            }
        });

        Button gpsCancel = (Button) dialog.findViewById(R.id.gps_cancel_button);
        gpsCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
    }//checkGps

    private void SubmitEvent() {


        /////////////////////////////////////////
        try {
            new Encode_image().execute();

        } catch (Exception e1) {
            showToast("Error in Uploading Image", Toast.LENGTH_SHORT);
        }
        ////////////////////////////////////////


    }//SubmitEvent

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    private void storePackupToDB(String address, final double Lat, final double Lng) {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("packup_time", etpackup.getText().toString().trim());
        params.put("packup_location", address);
        params.put("packup_lat", Lat);
        params.put("packup_lng", Lng);
        params.put("packup_entry_type", "ManualOnline");

        client.post(Config.URLupdatePackupTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast("Packup Recorded Successfully", Toast.LENGTH_LONG);
                    btnpackupsubmit.setEnabled(false);
                    btnpackup.setEnabled(false);
                    btnpackup.setAlpha((float) 0.5);
                    btnpackupsubmit.setAlpha((float) 0.5);

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Packup values(?,?,?,?,?)");
                    //entry_id, packup_time, packup_lat, packup_lng, Status
                    sqLiteStatement.bindString(1, entry_id);
                    sqLiteStatement.bindString(2, etpackup.getText().toString().trim());
                    sqLiteStatement.bindString(3, String.valueOf(Lat));
                    sqLiteStatement.bindString(4, String.valueOf(Lng));
                    sqLiteStatement.bindString(5, "done");
                    long res = sqLiteStatement.executeInsert();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Could Not Connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });


    }//storePackupToDB

    private void storeCallinToDB(String address, final double Lat, final double Lng) {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("callin_time", etcallin.getText().toString().trim());
        params.put("callin_location", address);
        params.put("callin_lat", Lat);
        params.put("callin_lng", Lng);
        params.put("callin_entry_type", "ManualOnline");

        client.post(Config.URLupdateCallinTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast("In Time Recorded Successfully", Toast.LENGTH_LONG);
                    btncallinsubmit.setEnabled(false);
                    btncallin.setEnabled(false);
                    btncallin.setAlpha((float) 0.5);
                    btncallinsubmit.setAlpha((float) 0.5);

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                    sqLiteStatement.bindString(1, entry_id);
                    sqLiteStatement.bindString(2, etcallin.getText().toString().trim());
                    sqLiteStatement.bindString(3, String.valueOf(Lat));
                    sqLiteStatement.bindString(4, String.valueOf(Lng));
                    sqLiteStatement.bindString(5, "done");
                    long res = sqLiteStatement.executeInsert();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set Task_callin = 'done' where entry_id = '" + entry_id + "'");
                    long result = sqLiteStatement.executeUpdateDelete();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Could Not Connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });


    }//storeCallinToDB

    private void showprogressdialog() {
        pd = new ProgressDialog(SnapshotDetailsActivity.this);
        pd.setTitle("Please Wait.....");
        pd.setMessage("Loading data.....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }
    //Code to Get LAT and LNG of user
    @Override
    public void onConnected(Bundle bundle) {


        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            sublat = String.valueOf(mLastLocation.getLatitude());
            sublon = String.valueOf(mLastLocation.getLongitude());
            updateUI();
        } else {
            checkGps();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        sublat = String.valueOf(location.getLatitude());
        sublon = String.valueOf(location.getLongitude());


        updateUI();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    void updateUI() {
        MyLat = Double.parseDouble(sublat);
        MyLong = Double.parseDouble(sublon);
/*
        try {
            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
            address1 = addresses.get(0).getAddressLine(0);
            Toast.makeText(this,"LAT: "+MyLat+"\n LNG: "+MyLong,Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(this,"Address: "+address1.toString(),Toast.LENGTH_SHORT).show();*/
    }//updateUI

    //Code of Touch Signature
    public void showsignaturedialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(SnapshotDetailsActivity.this);

        final Dialog dialog = new Dialog(SnapshotDetailsActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 1000f;
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setContentView(li.inflate(R.layout.signature, null));
        mClearButton = (Button) dialog.findViewById(R.id.clear_button);
        mSaveButton = (Button) dialog.findViewById(R.id.save_button);
        mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                // Toast.makeText(dialog.getContext(), "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureBitmap = mSignaturePad.getSignatureBitmap();
                ivauthoritsign.setImageBitmap(signatureBitmap);

                dialog.dismiss();
            }
        });
        dialog.show();

    }//showsignaturedialog

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("Tag", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateAuthoritySign();
        }
    }//Encode_image


    //Rename image name
    private String createImgName() {
        String imgName = "";

        SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyyHHmmssSSS", Locale.ENGLISH);//yyyy-MM-dd HH:mm:ss.SSS
        Date now = new Date();
        String strDate = sdfDate.format(now);
        imgName = member_id + strDate;

        return imgName;
    }

    private void updateAuthoritySign() {

        String sub = createImgName();
        String img_name = etauthoritynm.getText().toString().trim() + "_" + sub;

        //img_name = etauthoritynm.getText().toString().trim()+"_"+submit_time;


        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("encoded_string", encoded_string);
        params.put("img_name", img_name);
        params.put("entry_id", entry_id);
        params.put("authority_sign_date", submit_time);
        params.put("authority_nm", etauthoritynm.getText().toString().trim());
        params.put("authority_number", etauthoritynumb.getText().toString().trim());
        params.put("authority_email", etauthorityemail.getText().toString().trim());
        params.put("house_id", house_id);
        params.put("member_id", member_id);

        client.post(Config.URLupdateauthoritysign, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast(getString(R.string.event_complete), Toast.LENGTH_LONG);
                    //Send Email to Production
                    name = "CINTAA App";
                    username = "cintaaforad@gmail.com";
                    password = "abcd##1234";
                    email = etauthorityemail.getText().toString();
                    subject = "Artist Payment Confirmation from CINTAA.";
                    sendMail();

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//updateAuthoritySign


    private void initializeControls() {
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");

        tvflimnm = (TextView) findViewById(R.id.etfilmname);
        tvflimnm.setTypeface(custom_font);
        tvflimnm.setEnabled(false);
        tvphouse = (TextView) findViewById(R.id.etprodhouname);
        tvphouse.setTypeface(custom_font);
        tvphouse.setEnabled(false);
        tvphouse.setMovementMethod(new ScrollingMovementMethod());
        tvphouse.setEnabled(false);
        tvprodnm = (TextView) findViewById(R.id.etprodname);
        tvprodnm.setTypeface(custom_font);
        tvprodnm.setEnabled(false);
        ivauthoritsign = (ImageView) findViewById(R.id.ivauthoritysign);
        tvshootdate = (TextView) findViewById(R.id.etdtshoot);
        tvshootdate.setTypeface(custom_font);
        tvshootdate.setEnabled(false);
        tvlocation = (TextView) findViewById(R.id.etshootloc);
        tvlocation.setTypeface(custom_font);
        tvlocation.setEnabled(false);
        etmaplocation = (TextView) findViewById(R.id.etmaplocation);
        etmaplocation.setTypeface(custom_font);
        etmaplocation.setEnabled(false);
        tvcharnm = (TextView) findViewById(R.id.etcharname);
        tvcharnm.setTypeface(custom_font);
        tvcharnm.setEnabled(false);
        tvshifttime = (TextView) findViewById(R.id.etshifttime);
        tvshifttime.setTypeface(custom_font);
        tvshifttime.setEnabled(false);
        tvrate = (TextView) findViewById(R.id.etrate);
        tvrate.setTypeface(custom_font);
        tvrate.setEnabled(false);
        tvcallin = (TextView) findViewById(R.id.etcallintime);
        tvcallin.setTypeface(custom_font);
        tvcallin.setEnabled(false);
        //tvratetype = (TextView) findViewById(R.id.etRateType);
        //tvratetype.setTypeface(custom_font);
        tvduedays = (TextView) findViewById(R.id.etdueday);
        tvduedays.setTypeface(custom_font);
        tvduedays.setEnabled(false);
        tvremark = (TextView) findViewById(R.id.etremark);
        tvremark.setTypeface(custom_font);
        tvremark.setEnabled(false);
        etcallin = (EditText) findViewById(R.id.tvcallin);
        etcallin.setTypeface(custom_font);
        etpackup = (EditText) findViewById(R.id.tvpackup);
        etpackup.setTypeface(custom_font);
        btnpackup = (Button) findViewById(R.id.btnpackup);
        btnpackup.setTypeface(custom_font);
        btnpackupsubmit = (Button) findViewById(R.id.btnpackupsubmit);
        btnpackupsubmit.setTypeface(custom_font);
        btncallin = (Button) findViewById(R.id.btncallin);
        btncallin.setTypeface(custom_font);
        btncallinsubmit = (Button) findViewById(R.id.btncallinsubmit);
        btncallinsubmit.setTypeface(custom_font);
        btsubmit = (Button) findViewById(R.id.btsubmit);
        btsubmit.setTypeface(custom_font);
        //btsubmit.setEnabled(false);
        etauthoritynm = (EditText) findViewById(R.id.etauthoritynm);
        etauthoritynm.setTypeface(custom_font);

        etauthoritynumb = (EditText) findViewById(R.id.etauthoritynumb);
        etauthoritynumb.setTypeface(custom_font);

        tvconveyance = (TextView) findViewById(R.id.tvconveyance);
        llconveyancedetail = (LinearLayout) findViewById(R.id.llconveyancedetail);
        llconveyancedetail.setVisibility(View.GONE);

        llph_email = (LinearLayout) findViewById(R.id.llph_email);
        etauthorityemail = (EditText) findViewById(R.id.etauthorityemail);
        etauthorityemail.setTypeface(custom_font);

        llph_email.setVisibility(View.GONE);

        //  etauthoritysign = (EditText) findViewById(R.id.etauthoritysign);
    }//initializeControls

    private void CheckCallinPackupSQLite(String entry_id) {

        //NOT IMPLEMENTED YET IN INPROGRESS-EDIT MODE...TO IMPLEMENT THE CONSTRAINTS !!

        //Check Callin Entry
        cursor = sqLiteDatabaseRead.rawQuery("select callin_time from sqtb_diary_Callin where entry_id='" + entry_id + "'", null);
        if (cursor.moveToFirst()) {
            do {
                //alert_flag = cursor.getString(0);
                etcallin.setText(cursor.getString(0));
                btncallin.setEnabled(false);
                btncallin.setAlpha((float) 0.5);
                btncallinsubmit.setEnabled(false);
                btncallinsubmit.setAlpha((float) 0.5);

            } while (cursor.moveToNext());
        }

        //Check Packup Entry
        cursor = sqLiteDatabaseRead.rawQuery("select packup_time from sqtb_diary_Packup where entry_id='" + entry_id + "'", null);
        if (cursor.moveToFirst()) {
            do {
                //alert_flag = cursor.getString(0);
                etpackup.setText(cursor.getString(0));
                btnpackup.setEnabled(false);
                btnpackupsubmit.setEnabled(false);
                btnpackup.setAlpha((float) 0.5);
                btnpackupsubmit.setAlpha((float) 0.5);

            } while (cursor.moveToNext());
        }

    }//CheckCallinPackupSQLite


    private void selectSnapshotDeatils() {
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", entry_id);

        CheckCallinPackupSQLite(entry_id);

        client.post(Config.URLselectSnapshotDeatils, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("SnapshotDetails");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
                            tvflimnm.setText(j1.getString("film_tv_name"));
                            tvphouse.setText(house_name);
                            tvprodnm.setText(j1.getString("producer_name"));
                            tvshootdate.setText(j1.getString("shoot_date"));
                            tvlocation.setText(j1.getString("shoot_location"));

                            String map = j1.getString("shoot_map_location");
                            if (!map.equals("null")) {
                                etmaplocation.setText(j1.getString("shoot_map_location"));
                            }


                            tvcharnm.setText(j1.getString("played_character"));
                            tvshifttime.setText(j1.getString("shift_time"));
                            tvcallin.setText(j1.getString("call_time"));
                            // tvratetype.setText(j1.getString("rate_type"));
                            tvrate.setText(j1.getString("rate") + " " + j1.getString("rate_type"));
                            tvduedays.setText(j1.getString("due_days"));
                            tvremark.setText(j1.getString("remark"));

                            phouse_email = j1.getString("house_email");
                            if (phouse_email.equals("NA")) {
                                etauthorityemail.setText("");
                                llph_email.setVisibility(View.VISIBLE);
                            } else {
                                etauthorityemail.setText(j1.getString("house_email"));
                                llph_email.setVisibility(View.GONE);
                            }
                            house_id = j1.getString("house_id");
                            String rate = j1.getString("rate");
                            int conv_rate = Integer.parseInt(rate);

                            if (conv_rate <= 5000) {
                                llconveyancedetail.setVisibility(View.VISIBLE);
                                tvconveyance.setText("Conveyance fees: " + j1.getString("fees") + " INR");
                            }


                            if (!j1.getString("callin_time").equals("null")) {
                                etcallin.setText(j1.getString("callin_time"));
                                btncallin.setEnabled(false);
                                btncallin.setAlpha((float) 0.5);
                                btncallinsubmit.setEnabled(false);
                                btncallinsubmit.setAlpha((float) 0.5);

                            }
                            if (!j1.getString("packup_time").equals("null")) {
                                etpackup.setText(j1.getString("packup_time"));
                                btnpackup.setEnabled(false);
                                btnpackupsubmit.setEnabled(false);
                                btnpackup.setAlpha((float) 0.5);
                                btnpackupsubmit.setAlpha((float) 0.5);

                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//selectSnapshotDeatils

    private String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentTime

    private void sendMail() {

        getSignIn();

    }

    private void getSignIn() {
        if (!(isValidEmaillId(email))) {
            etauthorityemail.setText("Please Enter Valid EmailId *");
        } else {
            if (!isNetworkConnected())  //if connection available
            {
                showToast("Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            } else {

                String message = "Payment for Memeber ID: " + member_id + " is Rupees:" + tvrate.getText().toString().trim() + "\n Payment Details: \n Film Name: " + tvflimnm.getText().toString().trim() + "\n Signing Authority Name: " + etauthoritynm.getText().toString().trim() + "\n Contact: " + etauthoritynumb.getText().toString().trim() + "\nSign Date: " + submit_time + "";
                sendMail(email, subject, message);

            }
        }

    }

    private void sendMail(String email, String subject, String messageBody) {
        Session session = createSessionObject();

        try {
            Message message = createMessage(email, subject, messageBody,
                    session);
            new SendMailTask().execute(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject,
                                  String messageBody, Session session) throws MessagingException,
            UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", Config.smtpHost);
        properties.put("mail.smtp.port", Config.smtpPort);

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }

    public class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(SnapshotDetailsActivity.this,
                    "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        /*Toast.makeText(SnapshotDetailsActivity.this,
                                "Passwod Send To Your Mail Successfully", Toast.LENGTH_LONG)
                                .show();*/
                    }
                });
            } catch (final MessagingException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        ///https://www.google.com/settings/security/lesssecureapps
                        //go throught that link and select less security
                              /*  Toast.makeText(ForgotPasswordActivity.this,
                                        e.getClass() + " : " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();*/
                    }
                });
            }
            return null;
        }
    }

}










