package com.webvectors.cintaa2.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

import static com.webvectors.cintaa2.utility.Constants.house_name;
import static com.webvectors.cintaa2.utility.Constants.shift_time;
import static com.webvectors.cintaa2.utility.Constants.shoot_date;
import static com.webvectors.cintaa2.utility.Constants.shoot_location;

public class CreatePDFofTemplate extends AppCompatActivity {
    String filepath = "Cintaa";
    //    String filename = "Progress-Graphs_" + System.currentTimeMillis() + ".pdf";
    String filename = "CompletedShoots.pdf";
    private File pdfFile;
    private ProgressDialog dialog;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pdfof_template);

    }

    List<String> list_entry_id = new ArrayList<>();
    List<String> film_nm = new ArrayList<>();
    private ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();

    void selectdata() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CreatePDFofTemplate.this);
        String member_id = preferences.getString("member_id", "abc");

        GlobalClass.printLog(CreatePDFofTemplate.this, "-----------member_id:-------" + member_id);

        pd = new ProgressDialog(CreatePDFofTemplate.this);
        pd.setCancelable(false);
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLcompletedscheduleentry, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("CompletedEntryTable");
                    if (jsonArray.length() > 0) {
                        list_entry_id.clear();
                        film_nm.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String shoot_date = j1.getString("shoot_date");
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            String entry_id = j1.getString("entry_id");
                            String film_name = j1.getString("film_tv_name");
                            String shoot_location = j1.getString("shoot_location");
                            String shift_time = j1.getString("shift_time");

                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
                            HashMap<String, String> temp1 = new HashMap<String, String>();
                            temp1.put("shoot_date", shoot_date);
                            temp1.put("house_name", house_name);
                            temp1.put("shoot_location", shoot_location);
                            temp1.put("shift_time", shift_time);


                            String[] parts = entry_id.split("_");
                            String part2 = parts[1];

                            temp1.put("part2", part2);

                            hashMapArrayList.add(temp1);
                            list_entry_id.add(entry_id);
                            film_nm.add(film_name);
                        }
//.. to sort collection date wise..
                        Collections.sort(hashMapArrayList, new MapComparator("part2"));
                        Collections.reverse(hashMapArrayList);

                        GlobalClass.printLog(CreatePDFofTemplate.this, "---------" + hashMapArrayList.size());
//
//                        for (int i = 0; i < hashMapArrayList.size(); i++) {
//                            HashMap<String, String> map = hashMapArrayList.get(i);
//                            String[] parts = map.get(shift_time).split("to");
//                            String part1 = parts[0];
//                            String shoot_date1 = map.get(shoot_date);
//                            String house_name1 = map.get(house_name);
//                            String shoot_location1 = map.get(shoot_location);
//                            String shift_time1 = map.get(part1);
//
//                        }
                        new PdfGenerationTask().execute();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
//                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }

    private class MapComparator implements Comparator<Map<String, String>> {
        private final String key;

        MapComparator(String key) {
            this.key = key;
        }

        public int compare(Map<String, String> first,
                           Map<String, String> second) {
            // TODO: Null checking, both for maps and values
            String firstValue = first.get(key);
            String secondValue = second.get(key);
            return firstValue.compareTo(secondValue);
        }
    }

    private class PdfGenerationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(CreatePDFofTemplate.this);
            dialog.setMessage(getString(R.string.please_wait));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {

//            left,Right,top,Bottom
            Document document = new Document(PageSize.A4, 20, 20, 20, 50);
            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
//                set Background in PDF
                writer.setPageEvent(new PDFBackground());

                // For set title of PDF
                Font headingFont = new Font(Font.FontFamily.HELVETICA, 22, Font.BOLD, BaseColor.BLACK);
                Paragraph TitleName = new Paragraph(22);
                TitleName.setSpacingAfter(5);
                TitleName.setAlignment(Element.ALIGN_CENTER);
                TitleName.setFont(headingFont);
//                TitleName.setIndentationLeft(50);
//                TitleName.setIndentationRight(50);
                TitleName.add("COMPLETED SHOOT");

                //Set screen-short on top of pdf
//                InputStream inputStream = new FileInputStream(MyScreenShotFilePath);
//                Log.e("ScreenShort", " Path in PDF ------------------" + MyScreenShotFilePath);
//                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
//
//                int width_ = bmp.getWidth();
//                int height_ = bmp.getHeight();
//
//                Log.e("height_", "" + height_);
//                Log.e("width_", "" + width_);
//
//                Log.e("document.right()", "" + document.right());
//                Log.e("document.left()", "" + document.left());
//
//                Log.e("document.getPageWidth()", "" + document.getPageSize().getWidth());
//                Log.e("document.getHeight()", "" + document.getPageSize().getHeight());
//
//                int total_w = (int) (document.getPageSize().getWidth() - document.right() - document.left());
//                int total_h = (int) (document.getPageSize().getHeight() - document.bottom() - 20);
//
//                Log.e("total width==", "" + total_w);
//                Log.e("total height==", "" + total_h);
//
//                ByteArrayOutputStream stream_ss = new ByteArrayOutputStream();
//                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream_ss);
//                Image mainImage = Image.getInstance(stream_ss.toByteArray());
//                mainImage.setAlignment(Element.ALIGN_CENTER | Element.ALIGN_TOP);
//                mainImage.scalePercent(50f);

//                if (width_ >= document.getPageSize().getWidth() - document.right() - document.left() - 50) {
//                    mainImage.scaleAbsoluteWidth(document.right() - document.left());
//                    if (height_ >= document.getPageSize().getHeight() - document.bottom() - 20) {
//                        if (width_ > height_) {
//                            mainImage.scaleAbsoluteHeight((document.getPageSize().getHeight() - document.bottom() - 100) / 2);
//                        }else
//                            mainImage.scaleAbsoluteHeight(document.getPageSize().getHeight() - document.bottom() - 100);
//                    }
//                } else if (height_ >= document.getPageSize().getHeight() - document.bottom() - 20)
//                    mainImage.scaleAbsoluteHeight((document.getPageSize().getHeight() - document.bottom() - 100));


//      mainImage.scaleAbsolute(document.getPageSize().getWidth() - 36f - 72f,
//                        document.getPageSize().getHeight() - 36f - 216.6f);

                document.open();
                // set Image in background
//                PdfContentByte canvas = writer.getDirectContentUnder();
//                Drawable drawableBackground = getResources().getDrawable(R.drawable.background_image);
//                BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawableBackground);
//                Bitmap bitmapBackground = bitmapDrawable.getBitmap();
//                ByteArrayOutputStream streamBackground = new ByteArrayOutputStream();
//                bitmapBackground.compress(Bitmap.CompressFormat.PNG, 100, streamBackground);
//                Image backgroundImage = Image.getInstance(streamBackground.toByteArray());
//                backgroundImage.scaleAbsolute(PageSize.A4);
//                backgroundImage.setAbsolutePosition(0, 0);
//                canvas.addImage(backgroundImage);

//                document.add(new Paragraph("\n"));
                document.add(TitleName);
//                document.add(mainImage);

//                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//                        - document.rightMargin()) / mainImage.getWidth()) * 100;
//                mainImage.scalePercent(scaler);
//---------------------------------------------
                // add a blank lines
//                document.add(Chunk.NEWLINE);
//                document.add(new Paragraph(""));

                //Chunk will add black line seperator
//                document.add(new Phrase("\n"));

                LineSeparator ls = new LineSeparator();
                ls.setLineColor(BaseColor.BLACK);
                document.add(new Chunk(ls));
                document.add(Chunk.NEWLINE);

//                Log.e("Document Height", "" + document.getPageSize().getHeight());
//                Log.e("Document Width", "" + document.getPageSize().getWidth());
                Font titleFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
//                titleFont.setColor(BaseColor.BLACK);
                Font dateFont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, BaseColor.BLACK);
//                Log.e("array size in pdf", "" + DisplayEventListActivity.array_eventMaster.size());
                float[] column = {1f, 10f, 0.1f, 4.5f};
                //create PDF table with the given widths
                PdfPTable table = new PdfPTable(4);
                table.setWidths(new int[]{1, 2, 2, 1});
                table.addCell(createCell("Shoot Date", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Production House Name", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Location", 2, 1, Element.ALIGN_CENTER, titleFont));
                table.addCell(createCell("Shift Time", 2, 1, Element.ALIGN_CENTER, titleFont));


                table.setSplitLate(false);
                table.setSplitRows(false);
                table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                table.getDefaultCell().setPadding(56);
                table.setTotalWidth(document.right() - document.left());
                table.setWidthPercentage(80);
                table.setLockedWidth(true);
//                table.setHeaderRows(1);
                table.setWidthPercentage(86f);

                for (int i = 0; i < hashMapArrayList.size(); i++) {
                    HashMap<String, String> map = hashMapArrayList.get(i);
                    String[] parts = map.get(shift_time).split("to");
                    String part1 = parts[0];

                    String shoot_date1 = map.get(shoot_date);
                    String house_name1 = map.get(house_name);
                    String shoot_location1 = map.get(shoot_location);
                    table.addCell(createCell(shoot_date1, 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(house_name1, 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(shoot_location1, 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(part1, 2, 1, Element.ALIGN_CENTER, dateFont));
//                    insertTextCell(table, shoot_date1, Element.ALIGN_LEFT, 4, titleFont);
//                    insertTextCell(table, house_name1, Element.ALIGN_LEFT, 4, dateFont);
//                    insertTextCell(table, shoot_location1, Element.ALIGN_LEFT, 4, dateFont);
//                    insertTextCell(table, part1, Element.ALIGN_LEFT, 4, dateFont);

//                    if (hashMapArrayList == null) {
//                        if (writer.getVerticalPosition(true) - table.getTotalHeight() - document.bottom() - 90 <= document.bottom()) {
//                            document.newPage();
//                        }
//
//                    } else {
//                        if (writer.getVerticalPosition(true) - table.getTotalHeight() - document.bottom() - 60 <= document.bottom()) {
//                            document.newPage();
//                        }
//                    }

//                    document.add(new Chunk(ls));
//                    document.add(Chunk.NEWLINE);
                }
                document.add(table);
//                if (writer.getVerticalPosition(true) - 15 <= document.bottom()) {
//                    document.newPage();
//                }
                //add footer on the Last page of pdf
                PdfContentByte cb = writer.getDirectContent();
                Font footerFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
                //footer content
                String footerContent = "Copyright 2017 WebVectors Inc. All Rights Reserved.";
                String disclaimer = "Disclaimer: This PDF is generated using Cintaa mobile application.";

            /*
             * Footer
             */
                // copyRight String
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(footerContent, footerFont),
                        (document.left() + document.right()) / 2, 40, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer, footerFont),
                        (document.left() + document.right()) / 2, 25, 0);
//                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer_two, footerFont),
//                        (document.left() + document.right()) / 2, 10, 0);

                document.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (dialog.isShowing())
                dialog.dismiss();

            Toast.makeText(CreatePDFofTemplate.this, "Report Gnerated", Toast.LENGTH_SHORT).show();
            finish();
            mailPDF();
        }
    }

    public PdfPCell createCell(String content, float borderWidth, int colspan, int alignment, Font fontStyle) {
        PdfPCell cell = new PdfPCell(new Phrase(content, fontStyle));
        cell.setBorderWidth(1);
        cell.setColspan(colspan);
        cell.setBorder(Rectangle.BOX);
        cell.setUseBorderPadding(true);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }


    private void insertTextCell(PdfPTable table, String text, int align, int colSpan, Font fontStyle) {
        //create a new cell with the specified Text and Font
//        Log.e("&&&&&&&&&&    5 ******", "5");
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), fontStyle));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setUseBorderPadding(true);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colSpan);
        //in case there is no text and you wan to create an empty row
//        if (text.trim().equalsIgnoreCase("") || text.trim().equalsIgnoreCase("null")) {
////            Log.e("&&&&&&&&&&    6 *******", "6");
//            cell.setMinimumHeight(2f);
//        }
        //add the call to the table
        table.addCell(cell);
    }

    private void mailPDF() {
        //file should contain path of pdf file
        Uri path = FileProvider.getUriForFile(CreatePDFofTemplate.this,
                getApplicationContext().getPackageName() + ".my.package.name.provider", pdfFile);

//        Log.e("create pdf uri path==>", "" + path);
//        try {
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(path, "application/pdf");
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//            finish();
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(getApplicationContext(),
//                    "There is no any PDF Viewer",
//                    Toast.LENGTH_SHORT).show();
//            finish();
//        }

//        File imageFilePath = new File(MyScreenShotFilePath);
//        if (imageFilePath.exists()) {
//            imageFilePath.delete();
//            Log.e("screen shot deleted!!", "-- :) --");
//        }
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "My Report");
        intent.putExtra(Intent.EXTRA_TEXT, "This is pdf file");
        intent.putExtra(Intent.EXTRA_STREAM, path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    public class PDFBackground extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {

            PdfContentByte canvas = writer.getDirectContentUnder();
            Drawable drawable = getResources().getDrawable(R.drawable.app_icon);
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
            Bitmap bitmap = bitmapDrawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image backgroundImage;

            try {
                backgroundImage = Image.getInstance(stream.toByteArray());
                float width = document.getPageSize().getWidth();
                float height = document.getPageSize().getHeight();

                writer.getDirectContentUnder()
                        .addImage(backgroundImage, width, 0, 0, height, 0, 0);
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
        }

    }
}
