package com.webvectors.cintaa2.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.CommonHelper;

public class BaseActivity extends AppCompatActivity {
    public void showToast(String message, int time) {
        Toast toast = Toast.makeText(this, message, time);
        View toastView = toast.getView();
        //toastView.setBackgroundColor(Color.YELLOW);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable(getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground(getResources().getDrawable(R.drawable.edshape_toast));
        }
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, this), CommonHelper.convertDpToPx(5, this), CommonHelper.convertDpToPx(10, this), CommonHelper.convertDpToPx(5, this));
        toast.show();
    }

}
