package com.webvectors.cintaa2.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends BaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener
{
	TextInputLayout TILmemberid, TILname, TILstate, TILzip, TILcountry, TILaddress, TILspassword, TILscpassword, TILemail, TILwebsite, TILmobileno, TILcity;
	EditText ETname, ETspassword, ETscpassword, ETemail, ETmemberid, ETstate, ETzip, ETaddress, ETwebsite, ETmobileno, ETcity;
	TextView tvgender, tvregister, tvlogin, tvhelp;
	ProgressDialog pd;
	AutoCompleteTextView ETcountry;
	AlertDialog.Builder builder1;
	LinearLayout llcountry;
	Button createaccount;
	String strcountry[] = {"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Yemen", "Zambia", "Zimbabwe"};
	RadioButton radiomale, radiofemale;
	RadioGroup radioSexGroup;
	String gender;
	int check = 0;
	private ArrayAdapter<String> adapter;

	public static String getCurrentTimeStamp()
	{
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		pd = new ProgressDialog(getApplicationContext());
		initialise();
		builder1 = new AlertDialog.Builder(getApplicationContext());

		createaccount.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				validation();
				if (check == 11)
				{
					if (isNetworkConnected())
					{
						selectgender();
						if (ETwebsite.getText().toString().trim().length() == 0)
						{
							insert();
						} else if (Patterns.WEB_URL.matcher(ETwebsite.getText().toString().trim()).matches())
						{
							insert();
						} else
						{
							ETwebsite.setError("Enter Valid Website URL");
							showToast("Enter Valid Website URL", Toast.LENGTH_LONG);
						}
					} else
					{
						showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
					}

				} else
				{
					showToast("Required fields are missing", Toast.LENGTH_LONG);
				}
			}
		});

		tvlogin.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});

		adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, strcountry);
		ETcountry.setDropDownBackgroundDrawable(new ColorDrawable(getApplicationContext().getResources().getColor(R.color.bgTitleTab)));
		ETcountry.setThreshold(1);

		//Set adapter to AutoCompleteTextView
		ETcountry.setAdapter(adapter);
		ETcountry.setOnItemSelectedListener(this);
		ETcountry.setOnItemClickListener(this);
	}//oncreate

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
							   long arg3)
	{
		InputMethodManager imm = (InputMethodManager) this.getSystemService(
				getApplicationContext().INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0)
	{
		InputMethodManager imm = (InputMethodManager) this.getSystemService(
				getApplicationContext().INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		//  Toast.makeText(getActivity(), "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2),
		//  Toast.LENGTH_LONG).show();
		InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		// Log.d("AutocompleteContacts", "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2));

	}

	private void showprogressdialog()
	{
		pd = new ProgressDialog(RegisterActivity.this);
		pd.setTitle("Please Wait.....");
		pd.setMessage("Loading data.....");
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setCancelable(false);
		pd.show();
		//pd.dismiss();
	}

	private void initialise()
	{
		Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");

		llcountry = (LinearLayout) findViewById(R.id.llcountry);
		ETname = (EditText) findViewById(R.id.ETname);
		ETname.setTypeface(custom_font);
		ETspassword = (EditText) findViewById(R.id.ETspassword);
		ETspassword.setTypeface(custom_font);
		ETscpassword = (EditText) findViewById(R.id.ETscpassword);
		ETscpassword.setTypeface(custom_font);
		ETemail = (EditText) findViewById(R.id.ETemail);
		ETemail.setTypeface(custom_font);
		ETmemberid = (EditText) findViewById(R.id.ETmemberID);
		ETmemberid.setTypeface(custom_font);
		ETcity = (EditText) findViewById(R.id.ETcity);
		ETcity.setTypeface(custom_font);
		ETzip = (EditText) findViewById(R.id.ETzip);
		ETzip.setTypeface(custom_font);
		ETcountry = (AutoCompleteTextView) findViewById(R.id.ETcountry);
		ETcountry.setTypeface(custom_font);
		ETwebsite = (EditText) findViewById(R.id.ETwebsite);
		ETwebsite.setTypeface(custom_font);
		ETstate = (EditText) findViewById(R.id.ETstate);
		ETstate.setTypeface(custom_font);
		ETmobileno = (EditText) findViewById(R.id.ETmobileno);
		ETmobileno.setTypeface(custom_font);
		ETaddress = (EditText) findViewById(R.id.ETaddress);
		ETaddress.setTypeface(custom_font);
		tvgender = (TextView) findViewById(R.id.tvgender);
		tvgender.setTypeface(custom_font);
		tvhelp = (TextView) findViewById(R.id.tvhelp);
		tvhelp.setTypeface(custom_font);
		tvlogin = (TextView) findViewById(R.id.tvlogin);
		tvlogin.setTypeface(custom_font);
		tvregister = (TextView) findViewById(R.id.tvregister);
		tvregister.setTypeface(custom_font);
		radiomale = (RadioButton) findViewById(R.id.radioMale);
		radiomale.setTypeface(custom_font);
		radiofemale = (RadioButton) findViewById(R.id.radioFemale);
		radiofemale.setTypeface(custom_font);
		radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
		createaccount = (Button) findViewById(R.id.createaccount);
		createaccount.setTypeface(custom_font);
	}

	public void selectgender()
	{
		int id = radioSexGroup.getCheckedRadioButtonId();
		RadioButton rbgender = (RadioButton) findViewById(id);
		gender = rbgender.getText().toString().trim();
	}

	private void showdialog1()
	{
		try
		{
			View customTitle = View.inflate(this, R.layout.titlecountry, null);
			builder1.setCustomTitle(customTitle);
			//  builder1.setTitle("Select Country");
			builder1.setSingleChoiceItems(strcountry, -1, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					ETcountry.setText(strcountry[i]);
					dialogInterface.cancel();
				}
			});
			builder1.show();
		} catch (Exception e)
		{
			Log.v("tag", "dialg error desc " + e.toString());
		}
	}//showdialog

	private boolean isValidEmaillId(String email)
	{

		return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
	}

	public void insert()
	{

		String Rdate = getCurrentTimeStamp();

		showprogressdialog();
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("member_id", ETmemberid.getText().toString().trim());
		params.put("name", ETname.getText().toString().trim());
		params.put("gender", gender);
		params.put("address", ETaddress.getText().toString().trim());
		params.put("country", ETcountry.getText().toString().trim());
		params.put("state", ETstate.getText().toString().trim());
		params.put("city", ETcity.getText().toString().trim());
		params.put("zip", ETzip.getText().toString().trim());
		params.put("mobile", ETmobileno.getText().toString().trim());
		params.put("website", ETwebsite.getText().toString().trim());
		params.put("email", ETemail.getText().toString().trim());
		params.put("date_registered", Rdate);
		params.put("password", ETspassword.getText().toString().trim());
		//  params.put("profile_imgurl", "http://shooting.pinnaculuminfotech.com/shoot/default.jpg");
		params.put("account_status", "active");
		params.put("member_type", "user");


		client.post(Config.urlRegisterUserDetailInsert, params, new JsonHttpResponseHandler()
		{
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response)
			{
				try
				{

					String s1 = response.getString("success");
					if (s1.equals("1"))
					{
						showToast("User Registered successfully", Toast.LENGTH_LONG);
						Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
						startActivity(intent);
					} else
					{
						showToast("User Already Exists", Toast.LENGTH_LONG);
					}
					//   Toast.makeText(getActivity(), s1, Toast.LENGTH_SHORT).show();
					ETmemberid.setText("");
					ETwebsite.setText("");
					ETzip.setText("");
					ETmobileno.setText("");
					ETstate.setText("");
					ETcity.setText("");
					ETaddress.setText("");
					ETcountry.setText("");
					ETname.setText("");
					ETemail.setText("");
					ETspassword.setText("");
					ETscpassword.setText("");
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				if (pd != null)
				{
					if (pd.isShowing())
					{
						pd.dismiss();
					}
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String res, Throwable t)
			{
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
				showToast("could not connect", Toast.LENGTH_LONG);
				if (pd != null)
				{
					if (pd.isShowing())
					{
						pd.dismiss();
					}
				}
			}
		});
	}


	private boolean isNetworkConnected()
	{
		ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		return cm.getActiveNetworkInfo() != null;
	}//isNetworkConnected()


	public void validation()
	{
		check = 0;
		{
			if (ETmemberid.getText().toString().length() == 0)
			{
				ETmemberid.setError("Member cannot be blank!");

			} else
			{
				check++;
			}
			if (ETname.getText().toString().length() == 0)
			{
				ETname.setError("Name cannot be blank!");

			} else
			{
				check++;
			}
			if (ETaddress.getText().toString().length() == 0)
			{
				ETaddress.setError("Address cannot be blank!");

			} else
			{
				check++;
			}
			if (ETcountry.getText().toString().length() == 0)
			{
				ETcountry.setError("Select Country!");

			} else
			{
				check++;
			}
			if (ETstate.getText().toString().length() == 0)
			{
				ETstate.setError("Enter State!");

			} else
			{
				check++;
			}
			if (ETcity.getText().toString().length() == 0)
			{
				ETcity.setError("Enter City!");

			} else
			{
				check++;
			}
			if (ETzip.getText().toString().length() == 0)
			{
				ETzip.setError("Name cannot be blank!");

			} else
			{
				check++;
			}
			if (ETmobileno.getText().toString().length() != 10)
			{
				ETmobileno.setError("Check Mobile no.!");

			} else
			{
				check++;
			}
			if (!isValidEmaillId(ETemail.getText().toString().trim()))
			{
				ETemail.setError("Please Enter Valid Email Address");

			} else
			{
				check++;
			}

			if (ETspassword.getText().toString().length() == 0)
			{
				ETspassword.setError("Password is required!");

			} else
			{
				check++;
			}
			if (!ETspassword.getText().toString().equals(ETscpassword.getText().toString()))
			{
				ETscpassword.setError("password and confirm password does not match");
			} else
			{
				check++;
			}
			return;
		}
	}

	private class MyTextWatcher implements TextWatcher
	{

		private View view;

		private MyTextWatcher(View view)
		{
			this.view = view;
		}

		public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
		{
		}

		public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
		{
		}

		public void afterTextChanged(Editable editable)
		{
			switch (view.getId())
			{
				case R.id.ETname:
					break;
				case R.id.ETspassword:
					break;
				case R.id.ETscpassword:
					break;
				case R.id.ETemail:
					break;
				case R.id.ETmemberID:
					break;
				case R.id.ETcity:
					break;
				case R.id.ETstate:
					break;
				case R.id.ETcountry:
					break;
				case R.id.ETzip:
					break;
				case R.id.ETmobileno:
					break;
				case R.id.ETwebsite:
					break;
				case R.id.ETaddress:
					break;
			}
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (pd != null) {
			if (pd.isShowing()) {
				pd.dismiss();
			}
		}
	}
}



