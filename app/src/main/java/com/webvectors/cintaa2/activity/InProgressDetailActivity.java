package com.webvectors.cintaa2.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.webvectors.cintaa2.adapter.PlaceArrayAdapter;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;

public class InProgressDetailActivity extends BaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private AdView mAdView;
    Button btnsave, btncancel;
    SharedPreferences preference;
    String InProgress_list_item, member_id, StHouse_id = "0", Prevhouse_name;
    ProgressDialog pd;
    LinearLayout llshootlocation, llprodhouse, lldtshoot, llshifttime, llinprogresscalltime;
    TextView tvRdays, tvdays, etmaplocation;
    EditText etflimnm, etphouse, etprodnm, etshootdate, etlocation, etcharnm, etshifttime, etrate, etduedays, etremark, etcalltimeinprogress;
    List<String> house_id, stprodhouname, stproducer_name, stalies_name, ph_alies;
    String[] AryStprodhouname, Arystalies_name, Aryph_alies, stratetype, stshifttime;
    AutoCompleteTextView etprodhouname;
    private ArrayAdapter<String> adapter;
    int size = 0, check = 0;
    TextView tvWelcome;

    private int CalendarHour, CalendarMinute;
    String format;
    Calendar calendar;
    TimePickerDialog timepickerdialog;

    AlertDialog.Builder builder, builder1, builder2;

    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient = null;
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)
            .setFastestInterval(16)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    AutoCompleteTextView mAutocompleteTextView_to_loc;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    public static String curlocation;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    String[] lat;
    String[] lng;
    double latdb = 0, lngdb = 0;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_progress_detail);
        Typeface customfont = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        String actionBarTitle = "In Progress Entry";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(ssb);
        preference = PreferenceManager.getDefaultSharedPreferences(this);
        InProgress_list_item = preference.getString("InProgress_list_item", "none");
        member_id = preference.getString("member_id", "abc");

        sqLiteDB = new SQLiteDB(getApplicationContext());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        Initialise();

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("13BF45B66CBEA5DD87E3ADF5F941FFE0") // Moto E Ankita
                .build();
        mAdView.loadAd(adRequest);

        house_id = new ArrayList<>();
        stprodhouname = new ArrayList<>();
        stproducer_name = new ArrayList<>();
        stalies_name = new ArrayList<>();
        ph_alies = new ArrayList<>();

        AryStprodhouname = new String[]{};
        Arystalies_name = new String[]{};
        Aryph_alies = new String[]{};
        stratetype = new String[]{};
        stshifttime = new String[]{};

        if (getIntent().getBooleanExtra("FromEditMapLocation", false)) {
            GetPreferences();
            btnsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    validation();
                    if (check == 9) {
                        if (isNetworkConnected()) {
                            if (validateTime()) {
                                updateRecords();
                            } else {
                                showToast("Invalid Call in time", Toast.LENGTH_LONG);
                                etcalltimeinprogress.setError("Call time must be in between given shift time");
                            }
                        } else {
                            showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
                        }
                    } else {
                        showToast("Required Fields are Empty !!", Toast.LENGTH_LONG);
                    }
                }
            });


            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                }
            });
            //SelectDataprodhous();
        } else {
            selectData();
        }

        selectShiftTime();

       /* mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mAutocompleteTextView_to_loc = (AutoCompleteTextView) findViewById(R.id.etshootloc);
        mAutocompleteTextView_to_loc.setThreshold(1);
        mAutocompleteTextView_to_loc.setOnItemClickListener(mAutocompleteClickListenerDestination);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView_to_loc.setAdapter(mPlaceArrayAdapter);
*/

        llshifttime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (hasFocus)
                    showdialogShift();
            }
        });
        llshifttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                showdialogShift();
            }
        });

        lldtshoot.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (hasFocus)
                    showcal();
            }
        });
        lldtshoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                showcal();
            }
        });

     /*   llratetype.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    showdialogratetype();
            }
        });
        llratetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogratetype();
            }
        });*/


        llinprogresscalltime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (hasFocus)
                    showcalltime();
            }
        });
        llinprogresscalltime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                showcalltime();
            }
        });

    /*    etmaplocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                //ShowdialogMapLoc();
                StorePreferences();
                Intent intent = new Intent(InProgressDetailActivity.this,MapLocationEditActivity.class);
                startActivity(intent);
                finish();
                //getFragmentManager().beginTransaction().replace(R.id.content_frame,new MapLocationFragment()).addToBackStack("Tag").commit();
            }
        });

        etmaplocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                StorePreferences();
                Intent intent = new Intent(InProgressDetailActivity.this,MapLocationEditActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        llshootlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                StorePreferences();
                Intent intent = new Intent(InProgressDetailActivity.this, MapLocationEditActivity.class);
                startActivity(intent);
                finish();
                // getFragmentManager().beginTransaction().replace(R.id.content_frame,new MapLocationFragment()).addToBackStack("Tag").commit();
            }
        });

        llshootlocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                StorePreferences();
                Intent intent = new Intent(InProgressDetailActivity.this, MapLocationEditActivity.class);
                startActivity(intent);
                finish();
            }
        });

        etprodnm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    showcal();

                    handled = true;
                }
                return handled;
            }
        });

        etlocation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    StorePreferences();
                    Intent intent = new Intent(InProgressDetailActivity.this, MapLocationEditActivity.class);
                    startActivity(intent);
                    finish();

                    handled = true;
                }
                return handled;
            }
        });

        etcharnm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    showdialogShift();

                    handled = true;
                }
                return handled;
            }
        });

    }//onCreate

    private void StorePreferences() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Edit_diary_txt_filmnm", etflimnm.getText().toString().trim());
        editor.putString("Edit_diary_txt_phouse", etprodhouname.getText().toString().trim());
        editor.putString("Edit_diary_txt_prodnm", etprodnm.getText().toString().trim());
        editor.putString("Edit_diary_txt_shootdate", etshootdate.getText().toString().trim());
        editor.putString("Edit_diary_txt_shootaddress", etlocation.getText().toString().trim());
        editor.putString("Edit_diary_txt_shootloc", etmaplocation.getText().toString().trim());
        editor.putString("Edit_diary_txt_charcnm", etcharnm.getText().toString().trim());
        editor.putString("Edit_diary_txt_shifttime", etshifttime.getText().toString().trim());
        editor.putString("Edit_diary_txt_calltime", etcalltimeinprogress.getText().toString().trim());
        editor.putString("Edit_diary_txt_rateper", etrate.getText().toString().trim());
        editor.putString("Edit_diary_txt_duetime", etduedays.getText().toString().trim());
        editor.putString("Edit_diary_txt_remark", etremark.getText().toString().trim());
        editor.putString("Edit_diary_txt_lat", String.valueOf(latdb));
        editor.putString("Edit_diary_txt_lon", String.valueOf(lngdb));
        editor.putString("Edit_diary_StHouse_id", StHouse_id);

        editor.commit();

    }//StorePreferences

    private void GetPreferences() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        etflimnm.setText(preferences.getString("Edit_diary_txt_filmnm", ""));
        etprodhouname.setText(preferences.getString("Edit_diary_txt_phouse", ""));
        etprodnm.setText(preferences.getString("Edit_diary_txt_prodnm", ""));
        etshootdate.setText(preferences.getString("Edit_diary_txt_shootdate", ""));
        etlocation.setText(preferences.getString("Edit_diary_txt_shootaddress", ""));
        //etmaplocation.setText();
        String adrs = preferences.getString("Edit_diary_txt_shootloc", "");
        latdb = Double.parseDouble(preferences.getString("Edit_diary_txt_lat", "0"));
        lngdb = Double.parseDouble(preferences.getString("Edit_diary_txt_lon", "0"));

        if (adrs.equals("getting address...") || adrs.equals("")) {
            etmaplocation.setText("");
            latdb = 0;
            lngdb = 0;
        } else {
            etmaplocation.setText(adrs);
            etcharnm.requestFocus();
        }
        etcharnm.setText(preferences.getString("Edit_diary_txt_charcnm", ""));
        etshifttime.setText(preferences.getString("Edit_diary_txt_shifttime", ""));
        etcalltimeinprogress.setText(preferences.getString("Edit_diary_txt_calltime", ""));
        etrate.setText(preferences.getString("Edit_diary_txt_rateper", ""));
        etduedays.setText(preferences.getString("Edit_diary_txt_duetime", ""));
        etremark.setText(preferences.getString("Edit_diary_txt_remark", ""));

        StHouse_id = preferences.getString("Edit_diary_StHouse_id", "0");


        //  Toast.makeText(getActivity(),"LAT: "+latdb+"\n LON: "+lngdb,Toast.LENGTH_LONG).show();

    }//GetPreferences

    private void DestroyPreferences() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("Edit_diary_txt_filmnm");
        editor.remove("Edit_diary_txt_phouse");
        editor.remove("Edit_diary_txt_prodnm");
        editor.remove("Edit_diary_txt_shootdate");
        editor.remove("Edit_diary_txt_shootaddress");
        editor.remove("Edit_diary_txt_shootloc");
        editor.remove("Edit_diary_txt_charcnm");
        editor.remove("Edit_diary_txt_shifttime");
        editor.remove("Edit_diary_txt_calltime");
        editor.remove("Edit_diary_txt_rateper");
        editor.remove("Edit_diary_txt_duetime");
        editor.remove("Edit_diary_txt_remark");
        editor.remove("Edit_Edit_diary_txt_lat");
        editor.remove("Edit_diary_txt_lon");
        // editor.remove("Edit_diary_StayHere");
        editor.remove("Edit_diary_StHouse_id");

        editor.commit();

    }//DestroyPreferences

    private void Initialise() {
        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        etflimnm = (EditText) findViewById(R.id.etfilmname);
        etflimnm.setTypeface(font);
        tvdays = (TextView) findViewById(R.id.tvdays);
        tvdays.setTypeface(font);
        tvRdays = (TextView) findViewById(R.id.tvRdays);
        tvRdays.setTypeface(font);
        etprodhouname = (AutoCompleteTextView) findViewById(R.id.etprodhouname);
        etprodhouname.setTypeface(font);
        etprodnm = (EditText) findViewById(R.id.etprodname);
        etprodnm.setTypeface(font);
        etshootdate = (EditText) findViewById(R.id.etdtshoot);
        etshootdate.setTypeface(font);
        etlocation = (EditText) findViewById(R.id.etshootloc);
        etlocation.setTypeface(font);
        etcharnm = (EditText) findViewById(R.id.etcharname);
        etcharnm.setTypeface(font);
        etshifttime = (EditText) findViewById(R.id.etshifttime);
        etshifttime.setTypeface(font);
        etcalltimeinprogress = (EditText) findViewById(R.id.etcalltimeinprogress);
        etcalltimeinprogress.setTypeface(font);
        etrate = (EditText) findViewById(R.id.etrate);
        etrate.setTypeface(font);

        etduedays = (EditText) findViewById(R.id.etdueday);
        etduedays.setTypeface(font);
        etremark = (EditText) findViewById(R.id.etremark);
        etremark.setTypeface(font);
        //llratetype=(LinearLayout)findViewById(R.id.llinprogressratetype);
        lldtshoot = (LinearLayout) findViewById(R.id.llinprogrssdtshoot);
        llprodhouse = (LinearLayout) findViewById(R.id.llinprogessprodhouse);
        llshifttime = (LinearLayout) findViewById(R.id.llinprogressshifttime);

        llinprogresscalltime = (LinearLayout) findViewById(R.id.llinprogresscalltime);

        btnsave = (Button) findViewById(R.id.btsave);
        btnsave.setTypeface(font);
        btncancel = (Button) findViewById(R.id.btcancel);
        btncancel.setTypeface(font);
        tvWelcome = (TextView) findViewById(R.id.tvWelcome);
        tvWelcome.setTypeface(font);

        etmaplocation = (TextView) findViewById(R.id.etmaplocation);
        etmaplocation.setTypeface(font);
        //etmaplocation.setEnabled(false);


        llshootlocation = (LinearLayout) findViewById(R.id.llshootlocation);


    }//Initialise

    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestination = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            curlocation = String.valueOf(item.description);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            etcharnm.requestFocus();
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackDestination);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            /*Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("DEST",dest);*/
        }

    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackDestination = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            LatLng mlatlng = place.getLatLng();

            String[] parts;
            String part1, part2;
            parts = mlatlng.toString().split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            lat = s1.split("\\(");
            lng = s2.split("\\)");

            latdb = Double.parseDouble(lat[1]);
            lngdb = Double.parseDouble(lng[0]);
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(this);
        }
        super.onStop();
    }

    private void stopAutoManage() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.stopAutoManage(this);
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,REQUEST,this);  // LocationListener
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        mPlaceArrayAdapter.setGoogleApiClient(null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }


    private void showprogressdialog() {
        pd = new ProgressDialog(InProgressDetailActivity.this);
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private boolean validateTime() {

        //Check if Call time is between Given Shift Time
        SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.ENGLISH);

        String startTime, endTime, callTime, timeType = null;
        Date startTimeDate, endTimeDate, callTimeDate;
        startTimeDate = new Date();
        endTimeDate = new Date();
        callTimeDate = new Date();

        callTime = etcalltimeinprogress.getText().toString();

        if (etshifttime.getText().toString().length() == 18) {

            startTime = etshifttime.getText().toString().substring(0, 7);
            endTime = etshifttime.getText().toString().substring(11);
            timeType = etshifttime.getText().toString().substring(5, 7);

            try {
                startTimeDate = sdfTime.parse(startTime);
                endTimeDate = sdfTime.parse(endTime);
                callTimeDate = sdfTime.parse(callTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

        } else if (etshifttime.getText().toString().length() == 20) {

            startTime = etshifttime.getText().toString().substring(0, 8);
            endTime = etshifttime.getText().toString().substring(12);
            timeType = etshifttime.getText().toString().substring(6, 8);


            try {
                startTimeDate = sdfTime.parse(startTime);
                endTimeDate = sdfTime.parse(endTime);
                callTimeDate = sdfTime.parse(callTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

        }


        boolean result = false;
        if (timeType.equals("AM")) {
            if ((startTimeDate.equals(callTimeDate) || startTimeDate.before(callTimeDate)) && endTimeDate.after(callTimeDate)) {
                result = true;
            } else {
                result = false;

            }
        } else if (timeType.equals("PM")) {
            if ((endTimeDate.equals(callTimeDate) || endTimeDate.before(callTimeDate)) && startTimeDate.after(callTimeDate)) {
                result = false;
            } else {
                result = true;
            }
        }

        return result;
    }//ValidateTime

    private void selectproductionhouse() {

        for (int m = 0; m < size; m++) {
            if (!Arystalies_name[m].equals("null")) {
                ph_alies.add(AryStprodhouname[m] + "/" + Arystalies_name[m]);
            } else {
                ph_alies.add(AryStprodhouname[m]);
            }
        }
        Aryph_alies = new String[ph_alies.size()];
        ph_alies.toArray(Aryph_alies);

        for (int z = 0; z < house_id.size(); z++) {
            if (Prevhouse_name.equals(Aryph_alies[z])) {
                StHouse_id = house_id.get(z);
            }
        }

        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, Aryph_alies);
        etprodhouname.setDropDownBackgroundDrawable(new ColorDrawable(getApplicationContext().getResources().getColor(R.color.bgTitleTab)));
        etprodhouname.setThreshold(1);

        //Set adapter to AutoCompleteTextView
        etprodhouname.setAdapter(adapter);
        etprodhouname.setOnItemSelectedListener(this);
        etprodhouname.setOnItemClickListener(this);


    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(
                getApplicationContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        // Toast.makeText(this,"Position: "+position,Toast.LENGTH_LONG);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(
                this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // Toast.makeText(getActivity(), "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2),
        //   Toast.LENGTH_LONG).show();
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        for (int i = 0; i < Aryph_alies.length; i++) {
            if (Aryph_alies[i].equals(arg0.getItemAtPosition(arg2))) {
                etprodnm.setText(stproducer_name.get(i));
                StHouse_id = house_id.get(i);
            }
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }


    private void selectData() {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", InProgress_list_item);
        client.post(Config.URLselectInProgressDetails, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("InProgressDetails");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
                            etflimnm.setText(j1.getString("film_tv_name"));
                            etprodhouname.setText(house_name);
                            etprodnm.setText(j1.getString("producer_name"));
                            etshootdate.setText(j1.getString("shoot_date"));
                            etlocation.setText(j1.getString("shoot_location"));
                            etcharnm.setText(j1.getString("played_character"));
                            etshifttime.setText(j1.getString("shift_time"));
                            etcalltimeinprogress.setText(j1.getString("call_time"));
                            etrate.setText(j1.getString("rate"));
                            etduedays.setText(j1.getString("due_days"));
                            etremark.setText(j1.getString("remark"));
                            Prevhouse_name = house_name;
                            latdb = Double.parseDouble(j1.getString("lat"));
                            lngdb = Double.parseDouble(j1.getString("lng"));
                            String callin_time = j1.getString("callin_time");
                            String map = j1.getString("shoot_map_location");
                            if (!map.equals("null")) {
                                etmaplocation.setText(j1.getString("shoot_map_location"));
                            }


                            if (!callin_time.equals("null")) {
                                lldtshoot.setEnabled(false);
                                llinprogresscalltime.setEnabled(false);
                                llshifttime.setEnabled(false);
                                etlocation.setEnabled(false);
                                etmaplocation.setEnabled(false);
                                llshootlocation.setEnabled(false);
                            }

                        }

                        // SelectDropDownData();

                        btnsave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                validation();
                                if (check == 9) {
                                    if (isNetworkConnected()) {
                                        if (validateTime()) {
                                            updateRecords();
                                        } else {
                                            showToast("Invalid Call in time", Toast.LENGTH_LONG);
                                            etcalltimeinprogress.setError("Call time must be in between given shift time");
                                        }
                                    } else {
                                        showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
                                    }
                                } else {
                                    showToast("Required Fields are Empty !!", Toast.LENGTH_LONG);
                                }
                            }
                        });


                        btncancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            }
                        });
                        SelectDataprodhous();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//selectData

    public void validation() {
        check = 0;
        {
            if (etflimnm.getText().toString().length() == 0) {
                etflimnm.setError("Enter the film or serial name!");

            } else {
                check++;
            }
            if (etprodhouname.getText().toString().length() == 0) {
                etprodhouname.setError("Select production house!");

            } else {
                check++;
            }
            if (etshootdate.getText().toString().length() == 0) {
                etshootdate.setError("Select date of shoot!");

            } else {
                check++;
            }
            if (etlocation.getText().toString().length() == 0) {
                etlocation.setError("Enter shoot location!");

            } else {
                check++;
            }

            if (etcharnm.getText().toString().length() == 0) {
                etcharnm.setError("Enter charactor name!");

            } else {
                check++;
            }
            if (etshifttime.getText().toString().length() == 0) {
                etshifttime.setError("Enter shoot location!");

            } else {
                check++;
            }
            if (etcalltimeinprogress.getText().toString().length() == 0) {
                etcalltimeinprogress.setError("Select callin time!");

            } else {
                check++;
            }
          /*  if (etratetype.getText().toString().length() == 0) {
                etratetype.setError("Select rate type!");

            } else {
                check++;
            }*/
            if (etrate.getText().toString().length() == 0) {
                etrate.setError("Enter rate!");

            } else {
                check++;
            }

            if (etduedays.getText().toString().length() == 0) {
                etduedays.setError("Enter number of due days!");

            } else {
                check++;
            }
            return;
        }
    }//validation

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    public void SelectDataprodhous() {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        client.post(Config.UrlselectProductionHouseProducer, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("ProductionHouseData");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j1 = jsonArray.getJSONObject(i);

                        house_id.add(j1.getString("house_id"));
                        stprodhouname.add(j1.getString("house_name"));
                        stproducer_name.add(j1.getString("producer_name"));
                        stalies_name.add(j1.getString("alies_name"));
                    }

                    AryStprodhouname = new String[stprodhouname.size()];
                    stprodhouname.toArray(AryStprodhouname);
                    Arystalies_name = new String[stalies_name.size()];
                    stalies_name.toArray(Arystalies_name);
                    size = AryStprodhouname.length;
                    selectproductionhouse();

                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//SelectDataprodhous

    private void updateRecords() {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", InProgress_list_item);
        params.put("film_tv_name", etflimnm.getText().toString().trim());
        params.put("house_id", StHouse_id);
        params.put("producer_name", etprodnm.getText().toString().trim());
        params.put("shoot_date", etshootdate.getText().toString().trim());
        params.put("shoot_location", etlocation.getText().toString().trim());
        params.put("played_character", etcharnm.getText().toString().trim());
        params.put("shift_time", etshifttime.getText().toString().trim());
        params.put("call_time", etcalltimeinprogress.getText().toString().trim());
        // params.put("rate_type", etratetype.getText().toString().trim());
        params.put("rate_type", "per day");
        params.put("rate", etrate.getText().toString().trim());
        params.put("due_days", etduedays.getText().toString().trim());
        params.put("remark", etremark.getText().toString().trim());
        params.put("lat", latdb);
        params.put("lng", lngdb);
        params.put("shoot_map_location", etmaplocation.getText().toString().trim());

        params.put("new_house_name", etprodhouname.getText().toString().trim());
        params.put("new_producer_name", etprodnm.getText().toString().trim());

        String date = etshootdate.getText().toString().trim();
        String hours = etcalltimeinprogress.getText().toString().trim();

        final String dateHours = date + " " + hours;

        client.post(Config.URLupdateInProgressDetails, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    showToast("Entry Updated", Toast.LENGTH_LONG);
                    //   Intent intent=new Intent(getActivity(),RegisterLoginActivity.class);
                    // startActivity(intent);
                    ////////////////////////
                    long seconds = CalculateSecondsFromCurrentDate(dateHours);

                    int requestCode = startAlert(seconds);
                    SetAllAlertOFF();
                    UpdateSQLite(InProgress_list_item, requestCode);
                    ////////////////////////

                    etflimnm.setText("");
                    etprodhouname.setText("");
                    etprodnm.setText("");
                    etshootdate.setText("");
                    etlocation.setText("");
                    etcharnm.setText("");
                    etshifttime.setText("");

                    etrate.setText("");
                    etduedays.setText("");
                    etremark.setText("");


                    String s1 = response.getString("success");
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    //    Toast.makeText(getActivity(), s1, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Could Not Connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });


    }//updateRecords

    private void SetAllAlertOFF() {

        int requestCode;

        cursor = sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where entry_id=?", new String[]{InProgress_list_item});
        if (cursor.moveToFirst()) {
            do {
                requestCode = cursor.getInt(0);
                Intent intent = new Intent(this, AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();
            } while (cursor.moveToNext());
        }
    }//SetAllAlertOFF

    //Calculate Time in seconds from Current Date to Given Date
    private long CalculateSecondsFromCurrentDate(String dateHours) {

        long seconds = 0;
        Date now, eventDate, currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
            eventDate = sdfformat.parse(dateHours);

            //in milliseconds
            long diff = eventDate.getTime() - currentDate.getTime();

            long diffSeconds = diff / 1000;
            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);

            long on_head = diffSeconds + diffMinutes + diffHours + diffDays;
            String alert = "00:00";
            cursor = sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    alert = cursor.getString(0);
                } while (cursor.moveToNext());
            }

           /*String time = "02:30"; //mm:ss
String[] units = time.split(":"); //will break the string up into an array
int minutes = Integer.parseInt(units[0]); //first element
int seconds = Integer.parseInt(units[1]); //second element
int duration = 60 * minutes + seconds; //add up our values
*/
            String[] units = alert.split(":"); //will break the string up into an array
            long hours = Integer.parseInt(units[0]); //hours element
            long minutes = Integer.parseInt(units[1]); //minutes element
            long before_head = 60 * 60 * hours + minutes * 60; //add up our values

            before_head = on_head - before_head;

            if (before_head > 0) {
                seconds = before_head;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate

    public int startAlert(long seconds) {

        String check_alert_setting = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                check_alert_setting = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        int requestCode = (int) (new Date().getTime() / 1000);
        // If alerts are ON in setting menu then only set Alarm for the Event
        if (check_alert_setting.equals("ON") && seconds > 0) {
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(this, AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            // alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds * 1000), pendingIntent);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (seconds * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }

        return requestCode;
    }//startAlert

    private void UpdateSQLite(String entry_id, int requestCode) {
        String alert_flag = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                alert_flag = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set shoot_date =?, film_tv_name = ?, shift_time = ?,call_time = ?,shoot_location = ?, lat = ?, lng = ?,requestCode_Alarm = ? where entry_id='" + entry_id + "';");

        sqLiteStatement.bindString(1, etshootdate.getText().toString().trim());
        sqLiteStatement.bindString(2, member_id);
        sqLiteStatement.bindString(3, etshifttime.getText().toString().trim());
        sqLiteStatement.bindString(4, etcalltimeinprogress.getText().toString().trim());
        sqLiteStatement.bindString(5, etlocation.getText().toString().trim());
        sqLiteStatement.bindString(6, String.valueOf(latdb));
        sqLiteStatement.bindString(7, String.valueOf(lngdb));
        sqLiteStatement.bindLong(8, requestCode);


        int n = sqLiteStatement.executeUpdateDelete();

        // Toast.makeText(getActivity(),"SQLite Success"+result,Toast.LENGTH_LONG).show();

    }//UpdateSQLite

    //..select's proucer name and production house name from database
    public void SelectDropDownData() {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        client.post(Config.UrlselectProductionHouseProducer, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("ProductionHouseData");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j1 = jsonArray.getJSONObject(i);

                        house_id.add(j1.getString("house_id"));
                        stprodhouname.add(j1.getString("house_name"));
                        stproducer_name.add(j1.getString("producer_name"));
                        stalies_name.add(j1.getString("alies_name"));
                    }


                    AryStprodhouname = new String[stprodhouname.size()];
                    stprodhouname.toArray(AryStprodhouname);
                    Arystalies_name = new String[stalies_name.size()];
                    stalies_name.toArray(Arystalies_name);
                    size = AryStprodhouname.length;


                    ph_alies.clear();
                    for (int m = 0; m < size; m++) {
                        if (!Arystalies_name[m].equals("null")) {
                            ph_alies.add(AryStprodhouname[m] + "/" + Arystalies_name[m]);
                        } else {
                            ph_alies.add(AryStprodhouname[m]);
                        }
                    }
                    Aryph_alies = new String[ph_alies.size()];
                    ph_alies.toArray(Aryph_alies);


                    for (int z = 0; z < house_id.size(); z++) {
                        if (Prevhouse_name.equals(Aryph_alies)) {
                            StHouse_id = house_id.get(z);
                        }
                    }


                    ///////////////////////////////////////////////

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//SelectDropDownData

    private void showdialogHouse() {

        //Code for popup dialog.
        try {

            builder = new AlertDialog.Builder(this);
            View customTitle = View.inflate(this, R.layout.titleproductionhouse, null);
            builder.setCustomTitle(customTitle);
            //  builder.setTitle("Select Production House");
            builder.setSingleChoiceItems(Aryph_alies, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etphouse.setText(Aryph_alies[i]);
                    etprodnm.setText(stproducer_name.get(i));
                    StHouse_id = house_id.get(i);
                    dialogInterface.cancel();
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialog

    private void selectShiftTime() {

        List<String> shift_start = new ArrayList<String>();

        cursor = sqLiteDatabaseRead.rawQuery("select shift_start from sqtb_shift_time", null);
        if (cursor.moveToFirst()) {
            do {
                shift_start.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        stshifttime = new String[shift_start.size()];
        shift_start.toArray(stshifttime);

    }//selectShiftTime

    private void showdialogShift() {
        try {
            builder1 = new AlertDialog.Builder(this);
            View customTitle = View.inflate(this, R.layout.titleshifttime, null);
            builder1.setCustomTitle(customTitle);
            String currentValue = etshifttime.getText().toString().trim();
            int selected = -1;
            for (int i = 0; i < stshifttime.length; i++) {
                if (currentValue.equals(stshifttime[i])) {
                    selected = i;
                    break;
                }
            }
            //stshifttime=new String[]{"7am to 7pm","9am to 9pm","7pm to 5am","9pm to 7am"};
            //   builder1.setTitle("Select Shift Time");
            builder1.setSingleChoiceItems(stshifttime, selected, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etshifttime.setText(stshifttime[i]);
                    showcalltime();
                    dialogInterface.cancel();
                }
            });
            builder1.show();


        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialog1

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerd = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            String date = datee + "/" + month + "/" + year;
            if (IsDateValid(date)) {
                etshootdate.setText(date);
            } else {
                showToast("Date of Shoot cannot be before Current Date", Toast.LENGTH_LONG);
            }

            etlocation.requestFocus();
        }
    };

    private void showcal() {
       /* Calendar cl = Calendar.getInstance();
        int year = cl.get(Calendar.YEAR);
        int month = cl.get(Calendar.MONTH);
        int day = cl.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dg = new DatePickerDialog(this, datelistener, year, month, day);
        View customTitle = View.inflate(this, R.layout.titledateshoot, null);
        dg.setCustomTitle(customTitle);
        dg.show();*/

        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerd,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgColor));


        dpd.vibrate(true);

        dpd.show(getFragmentManager(), "Datepickerdialog");
    }//showcal

    private Boolean IsDateValid(String date) {

        Boolean res = false;
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

        Date shootDate = new Date();
        Calendar cal = Calendar.getInstance();
        String currentDate = sdfDate.format(cal.getTime());
        Date currDate = new Date();


        try {

            currDate = sdfDate.parse(currentDate);
            shootDate = sdfDate.parse(date);

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long diff = shootDate.getTime() - currDate.getTime();
        int daysDifference = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        if (daysDifference > 0) {
            res = true;
        } else {
            res = false;
        }
        return res;
    }//IsDateValid

    DatePickerDialog.OnDateSetListener datelistener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            int month = monthOfYear + 1;
            String date;
            if (month > 9) {
                date = dayOfMonth + "/" + month + "/" + year;
            } else {
                date = dayOfMonth + "/0" + month + "/" + year;
            }
            etshootdate.setText(date);
            etlocation.requestFocus();

        }
    };


    private void showdialogratetype() {
        try {
            stratetype = new String[]{"per hour", "per day"};
            builder2 = new AlertDialog.Builder(this);
            View customTitle = View.inflate(this, R.layout.titlecountry, null);
            builder2.setCustomTitle(customTitle);
            // builder2.setTitle("Select Rate Type");
            builder2.setSingleChoiceItems(stratetype, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // etratetype.setText(stratetype[i]);
                    dialogInterface.cancel();
                }
            });
            builder2.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialogratetype

    com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener timeSet = new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
            String minuteString = minute < 10 ? "0" + minute : "" + minute;
            String secondString = second < 10 ? "0" + second : "" + second;
            String day_night = "AM";
            int hours = hourOfDay;
            if (hourOfDay >= 12) {
                day_night = "PM";

            }

            if (hourOfDay > 12) {

                hours = hourOfDay - 12;
            }

            String time = hours + ":" + minuteString + " " + day_night;
            etcalltimeinprogress.setText(time);
            etrate.requestFocus();
        }
    };

    public void showcalltime() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                timeSet,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(true);
        tpd.enableMinutes(true);
        tpd.setAccentColor(getResources().getColor(R.color.bgColor));
        tpd.show(getFragmentManager(), "Timepickerdialog");

      /*  calendar = Calendar.getInstance();
        CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(InProgressDetailActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = " AM";
                        }
                        else if (hourOfDay == 12) {

                            format = " PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = " PM";

                        }
                        else {

                            format = " AM";
                        }
                        String min = String.valueOf(minute);
                        if(minute<=9){
                            min="0"+min;
                        }

                        etcalltimeinprogress.setText(hourOfDay + ":" + min + format);

                    }
                }, CalendarHour, CalendarMinute, false);
        View customTitle = View.inflate(InProgressDetailActivity.this, R.layout.titlecallintime, null);
        timepickerdialog.setCustomTitle(customTitle);
        timepickerdialog.show();
*/
    }
}
