package com.webvectors.cintaa2.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.webvectors.cintaa2.Dialog_Classes.ExitApp_Dialog_Class;
import com.webvectors.cintaa2.Dialog_Classes.HelpCenterDialog;
import com.webvectors.cintaa2.Dialog_Classes.LogoutApp_Dialog_Class;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.fragment.CalendarFragment;
import com.webvectors.cintaa2.fragment.CompletedEventsFragment;
import com.webvectors.cintaa2.fragment.ContentFragment;
import com.webvectors.cintaa2.fragment.DiaryentryFragment;
import com.webvectors.cintaa2.fragment.HomeFragment;
import com.webvectors.cintaa2.fragment.MyProfileFragment;
import com.webvectors.cintaa2.fragment.SettingFragment;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.utility.PermissionModule;

import java.util.ArrayList;
import java.util.List;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import me.toptas.fancyshowcase.FancyShowCaseView;
import yalantis.com.sidemenu.interfaces.Resourceble;
import yalantis.com.sidemenu.interfaces.ScreenShotable;
import yalantis.com.sidemenu.model.SlideMenuItem;
import yalantis.com.sidemenu.util.ViewAnimator;


public class HomeActivity extends BaseActivity implements ViewAnimator.ViewAnimatorListener {

    private AdView mAdView;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private List<SlideMenuItem> list = new ArrayList<>();
    List<String> listLable = new ArrayList<>();
    private ContentFragment contentFragment;
    private ViewAnimator viewAnimator;
    private int res = R.drawable.content_music;
    private LinearLayout linearLayout;
    public static AppCompatActivity appCompatActivity;
    private HomeFragment homeFragment;
    Typeface font;
    ProgressDialog pd;
    String member_id;
    SharedPreferences preferences;
    public static ImageView ivSettingMenu, ivMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setContents();
        setAdMob();
        setActionBar();

        if (savedInstanceState == null) {
            homeFragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, homeFragment)
                    .commit();
        }

    }


    private void setAdMob() {
        MobileAds.initialize(this, getResources().getString(R.string.app_id));

        // Create the InterstitialAd and set the adUnitId.
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("13BF45B66CBEA5DD87E3ADF5F941FFE0") // Moto E Ankita
                .build();
        mAdView.loadAd(adRequest);
    }

    private void setContents() {
        //apply permissions for all the devices
        PermissionModule permissionModule = new PermissionModule(HomeActivity.this);
        permissionModule.checkPermissions();

        if (getIntent().getBooleanExtra("EXIT_APP", false)) {
            finish();
            return;
        }

        appCompatActivity = this;

        pd = new ProgressDialog(getApplicationContext());
        pd.setCancelable(false);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        member_id = preferences.getString("member_id", "abc");

        contentFragment = ContentFragment.newInstance(R.drawable.content_music);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        linearLayout = (LinearLayout) findViewById(R.id.left_drawer);
        ivSettingMenu = (ImageView) findViewById(R.id.ivSettingMenu);
        ivMenu = (ImageView) findViewById(R.id.ivMenu);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        createMenuList();
        viewAnimator = new ViewAnimator<>(this, list, listLable, contentFragment, drawerLayout, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }//onCreateOptionsMenu


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        String servername, url;
        Intent i;
        switch (item.getItemId()) {
            case R.id.action_instruction:
                i = new Intent(this, WebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                servername = Config.URLInstructionDocx;
                url = servername.trim();
                i.putExtra("url", url);
                startActivity(i);
                finish();
                return true;
            case R.id.action_helpCenter:
                /*i = new Intent(this,SitesWebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                servername=Config.URLhelpCenter;
                url=servername.trim();
                i.putExtra("wvSite",url);
                startActivity(i);
                finish();*/
                showDialogContactus();
                return true;
            case R.id.action_privacyPolicy:
                i = new Intent(this, SitesWebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                servername = Config.URLprivacyPolicy;
                url = servername.trim();
                i.putExtra("wvSite", url);
                startActivity(i);
//                finish();
                return true;
            case R.id.action_rateus:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    finish();
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.ic_youtube.com/store/apps/details?id=" + appPackageName)));
                    finish();
                }
                return true;
            case R.id.action_share:
                share_via_app();
                return true;
//            case R.id.action_appTour:
//                showTip1();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showDialogContactus() {
        //new Change_pwd_Class().show(getSupportFragmentManager(), "tag");
        new HelpCenterDialog().show(getSupportFragmentManager(), "tag");
    }//showDialogContactus

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void createMenuList() {
        SlideMenuItem menuItem0 = new SlideMenuItem(HomeFragment.CLOSE, R.drawable.menu_close);
        list.add(menuItem0);
        listLable.add("Close");

        SlideMenuItem menuItem = new SlideMenuItem(HomeFragment.varhome, R.drawable.menu_home);
        list.add(menuItem);
        listLable.add("Dashboard");

        SlideMenuItem menuItem4 = new SlideMenuItem(HomeFragment.varcalenderdash, R.drawable.menu_calenderdash);
        list.add(menuItem4);
        listLable.add("Schedule Entry");

        SlideMenuItem menuItem2 = new SlideMenuItem(HomeFragment.vardiaryenrtydash, R.drawable.menu_diaryenrtydash);
        list.add(menuItem2);
        listLable.add("Diary Entry");

        SlideMenuItem menuItem5 = new SlideMenuItem(HomeFragment.varscheduledash, R.drawable.menu_scheduledash);
        list.add(menuItem5);
        listLable.add("Completed Shoot");

        SlideMenuItem menuItem3 = new SlideMenuItem(HomeFragment.varprofiledash, R.drawable.menu_profiledash);
        list.add(menuItem3);
        listLable.add("My Profile");
        /*
        SlideMenuItem menuItem6 = new SlideMenuItem(HomeFragment.varwebsitedash, R.drawable.menu_websitedash);
		list.add(menuItem6);
		SlideMenuItem menuItem7 = new SlideMenuItem(HomeFragment.varcontactus, R.drawable.menu_contactus);
		list.add(menuItem7);
		SlideMenuItem menuItem8 = new SlideMenuItem(HomeFragment.varcomplaindash, R.drawable.menu_complaindash);
		list.add(menuItem8);
		*/
        SlideMenuItem menuItem9 = new SlideMenuItem(HomeFragment.varmoudash, R.drawable.menu_moudash);
        list.add(menuItem9);
        listLable.add("MOU");

//        SlideMenuItem menuItem10 = new SlideMenuItem(HomeFragment.varshareappdash, R.drawable.menu_shareappdash);
//        list.add(menuItem10);
//        listLable.add("Share");
        /*
        SlideMenuItem menuItem11 = new SlideMenuItem(HomeFragment.varleavefeedbackdash, R.drawable.menu_leavefeedbackdash);
		list.add(menuItem11);
		SlideMenuItem menuItem12 = new SlideMenuItem(HomeFragment.varrateappdash, R.drawable.menu_rateappdash);
		list.add(menuItem12);
		*/
        SlideMenuItem menuItem13 = new SlideMenuItem(HomeFragment.varsettingdash, R.drawable.menu_settingdash);
        list.add(menuItem13);
        listLable.add("Setting");

        SlideMenuItem menuItem14 = new SlideMenuItem(HomeFragment.varexit, R.drawable.menu_exit);
        list.add(menuItem14);
        listLable.add("Exit");

        SlideMenuItem menuItem15 = new SlideMenuItem(HomeFragment.varlogout, R.drawable.menu_logout);
        list.add(menuItem15);
        listLable.add("Logout");
    }//createMenuList

    private void setActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar1);
        setSupportActionBar(toolbar);

        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        String actionBarTitle = "Dashboard";

        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(ssb);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                linearLayout.removeAllViews();
                linearLayout.invalidate();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset > 0.6 && linearLayout.getChildCount() == 0)
                    viewAnimator.showMenuContent();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }//setActionBar

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
        if (DiaryentryFragment.getFragment() != null)
            DiaryentryFragment.getFragment().hideTip();

        if (CalendarFragment.getFragment() != null)
            CalendarFragment.getFragment().hideTip();

        if (HomeFragment.getFragment() != null)
            HomeFragment.getFragment().hideTip();


        // Checks the orientation of the screen
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
//        }
    }

    private ScreenShotable replaceFragment(ScreenShotable screenShotable, int topPosition, String menuName) {
        this.res = this.res == R.drawable.content_music ? R.drawable.content_films : R.drawable.content_music;
        View view = findViewById(R.id.content_frame);
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");
        int finalRadius = Math.max(view.getWidth(), view.getHeight());
        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(view, 0, topPosition, 0, finalRadius);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setDuration(ViewAnimator.CIRCULAR_REVEAL_ANIMATION_DURATION);

        findViewById(R.id.content_overlay).setBackgroundDrawable(new BitmapDrawable(getResources(), screenShotable.getBitmap()));
        animator.start();
        //ContentFragment contentFragment = ContentFragment.newInstance(this.res);
        Fragment fragment = new HomeFragment();
        getSupportActionBar().setTitle("Dashboard");
        if (menuName.equals("varhome")) {
            fragment = new HomeFragment();

            String actionBarTitle = "Dashboard";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();

        } else if (menuName.equals("vardiaryenrtydash")) {
            fragment = new DiaryentryFragment();
            String actionBarTitle = "Diary Entry";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();

        } else if (menuName.equals("varprofiledash")) {
            fragment = new MyProfileFragment();

            String actionBarTitle = "My Profile";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varcontactus")) {
            Intent i = new Intent(getApplicationContext(), SitesWebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // String filepath=((result.get(position).getFilePath()));
            String servername = Config.URLContactUs;
            String url = servername.trim();
            i.putExtra("wvSite", url);
            startActivity(i);

            //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varcalenderdash")) {
            fragment = new CalendarFragment();

//            String actionBarTitle = "Calendar";
            String actionBarTitle =getString(R.string.schedule_entry);
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varscheduledash")) {
            //fragment = new ScheduleFragment();
            fragment = new CompletedEventsFragment();
            String actionBarTitle = "My Schedule";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varwebsitedash")) {
            Intent i = new Intent(getApplicationContext(), SitesWebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // String filepath=((result.get(position).getFilePath()));
            String servername = Config.URLwebite;
            String url = servername.trim();
            i.putExtra("wvSite", url);
            startActivity(i);
        } else if (menuName.equals("varcomplaindash")) {
            Intent i = new Intent(getApplicationContext(), SitesWebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // String filepath=((result.get(position).getFilePath()));
            String servername = Config.URLcomplains;
            String url = servername.trim();
            i.putExtra("wvSite", url);
            startActivity(i);
        } else if (menuName.equals("varmoudash")) {
            Intent i = new Intent(getApplicationContext(), WebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            String servername = Config.URLMouDocx;
            String url = servername.trim();
            i.putExtra("url", url);
            startActivity(i);
        } else if (menuName.equals("varshareappdash")) {
            // fragment = new DiarySnapshotFragment();
            String actionBarTitle = "Dashboard";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            share_via_app();
//            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varleavefeedbackdash")) {
            showToast("Leave your Feedback on Play Store", Toast.LENGTH_SHORT);
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.ic_youtube.com/store/apps/details?id=" + appPackageName)));
            }
            /*fragment = new HomeFragment();

			SharedPreferences preferences;
			SharedPreferences.Editor editor;
			preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			editor = preferences.edit();
			editor.putInt("FeedBack_flag", 1);
			editor.commit();

			String actionBarTitle = "Dashboard";
			SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
			ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
			HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();*/
        } else if (menuName.equals("varrateappdash")) {
            showToast("Rate us on Play Store", Toast.LENGTH_SHORT);
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.ic_youtube.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (menuName.equals("varsettingdash")) {
            fragment = new SettingFragment();
            String actionBarTitle = "Setting";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Tag").commit();
        } else if (menuName.equals("varexit")) {
            exitapp();
        } else if (menuName.equals("varlogout")) {
            logoutapp();
        }

        //  getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        return contentFragment;
        //return (ScreenShotable) homeFragment2;
    }//replaceFragment

    public void share_via_app() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("vnd.android-dir/mms-sms");
        //sendIntent.putExtra("address", phoneNumber);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "CINTAA APP Download at http://www.ic_youtube.com");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void exitapp() {
        drawerLayout.closeDrawers();
        new ExitApp_Dialog_Class().show(getSupportFragmentManager(), "tag");
    }//exitapp

    private void logoutapp() {
        drawerLayout.closeDrawers();
        new LogoutApp_Dialog_Class().show(getSupportFragmentManager(), "tag");
    }//logoutapp


    @Override
    public ScreenShotable onSwitch(Resourceble slideMenuItem, ScreenShotable screenShotable, int position) {
        String menuName = slideMenuItem.getName();
        switch (slideMenuItem.getName()) {
            case ContentFragment.CLOSE:
                return screenShotable;
            default:
                return replaceFragment(screenShotable, position, menuName);
        }
    }

    @Override
    public void disableHomeButton() {
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    @Override
    public void enableHomeButton() {
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerLayout.closeDrawers();

    }

    @Override
    public void addViewToContainer(View view) {
        linearLayout.addView(view);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}