package com.webvectors.cintaa2.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;

public class SitesWebViewActivity extends BaseActivity {

    WebView wvSite;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sites_web_view);

        String web = getIntent().getExtras().getString("wvSite");

        String actionBarTitle = "CINTAA";
        if(web.equals(Config.URLContactUs)){
            actionBarTitle = "Contact Us";
        }else if(web.equals(Config.URLwebite)){
            actionBarTitle = "CINTAA";
        }else if(web.equals(Config.URLcomplains)){
            actionBarTitle = "Complains";
        }else if(web.equals(Config.URLhelpCenter)){
            actionBarTitle = "Help Center";
        }else if(web.equals(Config.URLprivacyPolicy)){
            actionBarTitle = "Privacy Policy";
        }

        Typeface customfont=Typeface.createFromAsset(this.getAssets(),"lane.ttf");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");

        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(ssb);

        if(isNetworkConnected()){

        showprogressdialog();

        wvSite = (WebView)findViewById(R.id.wvSite);
        wvSite.getSettings().setJavaScriptEnabled(true);
        wvSite.setVerticalScrollBarEnabled(true);
        wvSite.setWebViewClient(new WebViewClient());
//        wvSite.getSettings().setUseWideViewPort(true);
        wvSite.setHorizontalScrollBarEnabled(true);
//        wvSite.setInitialScale(1);
        wvSite.getSettings().setBuiltInZoomControls(true);
//        wvSite.getSettings().setUseWideViewPort(true);
        wvSite.getSettings().setLoadsImagesAutomatically(true);
        wvSite.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wvSite.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((i == KeyEvent.KEYCODE_BACK) && wvSite.canGoBack()) {
                    wvSite.goBack();
                    return true;
                }
                return false;
            }
        });

        wvSite.setWebChromeClient(new WebChromeClient()
        {
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });

        wvSite.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

        wvSite.loadUrl(web);
        }else{
			showToast("Please check Your Internet Connection and try again", Toast.LENGTH_LONG);
        }
    }//OnCreate

    private void showprogressdialog()
    {
        pd= new ProgressDialog(SitesWebViewActivity.this);
        pd.setTitle("Please Wait.....");
        pd.setMessage("Loading data.....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
