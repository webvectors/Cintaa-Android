package com.webvectors.cintaa2.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;

public class WebViewActivity extends BaseActivity {

    WebView webView;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);



        Typeface customfont=Typeface.createFromAsset(this.getAssets(),"lane.ttf");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        String actionBarTitle = "Web View";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(ssb);

        if(isNetworkConnected()){
        showprogressdialog();
        Intent docxUrl = getIntent();
        String url = docxUrl.getStringExtra("url");

        if(url.equals(Config.URLMouDocx)){
            actionBarTitle = "MoU";
            ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getSupportActionBar().setTitle(ssb);
        }else if(url.equals(Config.URLInstructionDocx)){
            actionBarTitle = "Instructions";
            ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            getSupportActionBar().setTitle(ssb);
        }

        String doc="<iframe src='http://docs.ic_youtube.com/gview?embedded=true&url="+url+"' width='100%' height='100%' style='border: none;'></iframe>";
        webView = (WebView) findViewById(R.id.webViewDOCX);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });
        webView.loadData( doc , "text/html", "UTF-8");

        }else {
			showToast("Please check Your Internet Connection and try again",Toast.LENGTH_LONG);
        }
    }//onCreate

    private void showprogressdialog()
    {
        pd= new ProgressDialog(WebViewActivity.this);
        pd.setTitle("Please Wait.....");
        pd.setMessage("Loading data.....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
