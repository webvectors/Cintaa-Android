package com.webvectors.cintaa2.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SplashActivity extends Activity {
    Context context;
    ImageView logo;
    Animation animation;
    TextView tvpower, tvwebvector;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);


        if (isNetworkConnected()) {
            callUpdationMethod();
        } else {
            showToast("Network not available",Toast.LENGTH_LONG);
        }

        Typeface font = Typeface.createFromAsset(this.getAssets(), "lane.ttf");
        tvpower = (TextView) findViewById(R.id.tvpowrd);
        tvpower.setTypeface(font);
        tvwebvector = (TextView) findViewById(R.id.tvwebvector);
        tvwebvector.setTypeface(font);
    }//onCreate

    public void showToast(String message, int time)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, time);
        View toastView  = toast.getView();
        toastView.setBackgroundColor(Color.YELLOW);
        TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
        v.setPadding(CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()), CommonHelper.convertDpToPx(10, getApplicationContext()), CommonHelper.convertDpToPx(5, getApplicationContext()));
        toast.show();
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    private void callUpdationMethod() {

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(Config.VersionCheck, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    GlobalClass.printLog(SplashActivity.this, "----response-------------------------------" + response.getString("flag"));

                    if (!response.getString("flag").equals("false")) {
                        startActivity(new Intent(SplashActivity.this, AppUnderUpdationActivity.class));
                    } else {
                        startAnimation();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
            }
        });
    }

    public void startAnimation() {
        // logo=(ImageView)findViewById(R.id.logo);
        //tvcintaa=(TextView)findViewById(R.id.Tvcintaa);
        //tvwebvector=(TextView)findViewById(R.id.Tvwebvector);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.custom);
        // tvcintaa.startAnimation(animation);
        //tvwebvector.startAnimation(animation);
        //logo.startAnimation(animation);
        new Thread() {
            public void run() {
                try {

                    sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(new Intent(context, LoginActivity.class));
                }
            }
        }.start();

    }//startAnimation


}