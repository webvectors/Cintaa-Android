package com.webvectors.cintaa2.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

public class AlertStopActivity extends BaseActivity {

    TextView tvcallin,tvshootLocation,tvshootAt,tvfilmnm,tvtitle;
    Button btnstop;
    MediaPlayer mp;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    String call_time, entryID, filmName, productionHouse, date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_stop);

        Intent intent = getIntent();
        String requestCode = intent.getStringExtra("Alert_requestCode_ShowDetails");
//        String Alert_entry_ids = intent.getStringExtra("Alert_entry_ids");

        sqLiteDB = new SQLiteDB(getApplicationContext());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        Initialize();

        try{
            cursor=sqLiteDatabaseRead.rawQuery("select entry_id,call_time from sqtb_diary_entry where requestCode_Alarm=? ",new  String[]{requestCode});
            if(cursor.moveToFirst())
            {
                    entryID = cursor.getString(0);
                    call_time = cursor.getString(1);
            }
            cursor=sqLiteDatabaseRead.rawQuery("select scheduleDate, filmName, productionHouse from tbScheduleDetails where entryId=? ",new String[]{entryID});
            if(cursor.moveToFirst())
            {
                    date = cursor.getString(0);
                    filmName = cursor.getString(1);
                    productionHouse = cursor.getString(2);
            }
        }finally {
            if(cursor!=null)
                cursor.close();
        }
        tvfilmnm.setText(filmName);
        tvshootAt.setText("Shoot Date: " + date);
        tvshootLocation.setText("Location: "+ productionHouse);
        tvcallin.setText("Call Time: "+ call_time);

        mp= MediaPlayer.create(getApplicationContext(), R.raw.alert	);
        mp.start();

        btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.stop();
                finish();
            }
        });
    }//Oncreate

    @Override
    public void onBackPressed() {
        mp.stop();
        finish();
    }

    private void Initialize(){
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");
        tvtitle = (TextView)findViewById(R.id.tvtitle);
        tvtitle.setTypeface(font);
        btnstop = (Button) findViewById(R.id.btnOk);
        btnstop.setTypeface(font);
        tvfilmnm = (TextView)findViewById(R.id.tvfilmnm);
        tvfilmnm.setTypeface(font);
        tvshootAt = (TextView)findViewById(R.id.tvshootAt);
        tvshootAt.setTypeface(font);
        tvshootLocation = (TextView)findViewById(R.id.tvshootLocation);
        tvshootLocation.setTypeface(font);
        tvcallin = (TextView)findViewById(R.id.tvcallin);
        tvcallin.setTypeface(font);
    }//Initialize
}
