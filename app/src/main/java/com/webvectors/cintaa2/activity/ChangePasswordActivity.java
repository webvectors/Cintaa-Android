package com.webvectors.cintaa2.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.Config;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ChangePasswordActivity extends BaseActivity {

    EditText etpassowrd,etconfirmpwd,etusername;
    Button btnSave;
    String memberID = null;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initialise();
        pd=new ProgressDialog(getApplicationContext());
        Bundle extra = getIntent().getExtras();
        if (extra!=null){
            memberID = extra.getString("memberID");
            etusername.setText(memberID);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    updatePassword();
                }
            }
        });
    }//onCreate

    private void initialise(){
        etpassowrd = (EditText)findViewById(R.id.etpassowrd);
        etconfirmpwd = (EditText)findViewById(R.id.etconfirmpwd);
        etusername = (EditText)findViewById(R.id.etusername);
        btnSave = (Button)findViewById(R.id.btnSave);
    }//initialise

    private Boolean validate(){
        if (etpassowrd.getText().toString().trim().length()==0){
            etpassowrd.setError("Input Password");
            return false;
        }

        if(etpassowrd.getText().toString().trim().equals(etconfirmpwd.getText().toString().trim())){
            return true;
        } else{
            etconfirmpwd.setError("Password and Confirm Password Must Match");
            return false;
        }

    }//validate

    private void showprogressdialog()
    {
        pd= new ProgressDialog(ChangePasswordActivity.this);
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.show();
        //pd.dismiss();
    }

    private void updatePassword(){

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", memberID);
        params.put("password", etconfirmpwd.getText().toString().trim());


        client.post(Config.URLchangepassword, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    String s1 = response.getString("success");

                    Intent intent=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                    intent.putExtra("isFirst",false);
                    intent.putExtra("M_ID",memberID);
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);

            }
        });
        if (pd!=null && pd.isShowing())
        pd.dismiss();

    }//updatePassword
}
