package com.webvectors.cintaa2.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends BaseActivity {

    EditText ETusername, ETpassword, etOtp;
    Button login, btnVerifyOtp;
    TextView tvSendOtp, tvlogintxt, tvforgtpass, tvcontctus, tvsignup, tvhelp, tvOtpNote;
    ProgressDialog pd;
    TextInputLayout TILusername, TILpassword;
    int check = 0;
    Intent intent;
    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;
    String notification = "ON";
    ImageView ivfb, ivtwitter, ivgoogle;
    Boolean isFirstTime = true;
    LinearLayout llpwd, llotp;
    Timer timer, timerResetOTP;

    String success1, member_id, profile_url, name;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    static int permissionCode;

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("Tag","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }*/
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
//                doTaskOnPermissionGranted();
            } else {

                Log.v("tag", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }

            if (this.checkSelfPermission(android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("tag", "Permission is granted");
//                doTaskOnPermissionGranted();
            } else {

                Log.v("tag", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 1);
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Tag", "Permission is granted");
//            doTaskOnPermissionGranted();
        }
    }//isStoragePermissionGranted

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

       /* permissionCode=1;
        requestPermission();*/

        initialise();

        sqLiteDB = new SQLiteDB(getApplicationContext());
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();

        try {
            cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    notification = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }


        pd = new ProgressDialog(getApplicationContext());
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        member_id = preferences.getString("member_id", "abc");
        editor = preferences.edit();
        GlobalClass.printLog(LoginActivity.this, "---------member_id" + member_id);

        //  Toast.makeText(getActivity(),"aaaaaaa",Toast.LENGTH_LONG).show();

        if (!member_id.equalsIgnoreCase("abc")) {
            GlobalClass.printLog(LoginActivity.this, "---------iffff" + member_id);

            if (notification.equals("ON")) {
                editor.putInt("admin_notificaton", 1);
            }

            editor.putInt("app_reminder", 1);
            editor.putInt("firstStart", 1);//new preference --28June
            editor.commit();
            GlobalClass.printLog(LoginActivity.this, "---------AAAAAAAAAA");
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            checkIfFirstLogin();
        }

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            isFirstTime = extra.getBoolean("isFirst");
            ETusername.setText(extra.getString("M_ID"));
            /*ETusername.setEnabled(false);*/
        }
        if (!isFirstTime) {
            login.setText("Sign In");
            llpwd.setVisibility(View.VISIBLE);
            llotp.setVisibility(View.GONE);
        } else {
            login.setText("Next");
            llpwd.setVisibility(View.GONE);
        }


        tvsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ivfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Config.urlFB));
                startActivity(i);
            }
        });
        ivtwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Config.urlTwitter));
                startActivity(i);
            }
        });
        ivgoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Config.urlYoutube));
                startActivity(i);
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                //  Toast.makeText(getActivity(),"fdfdfdf",Toast.LENGTH_LONG).show();
                if (isNetworkConnected()) {
                    if (isFirstTime) {
                        if (ETusername.getText().toString().trim().length() > 0) {
                            checkFirstTimeStatus(ETusername.getText().toString().trim());
                        } else {
                            ETusername.setError("Input CINTAA Member ID");
                            showToast("Input CINTAA Member ID", Toast.LENGTH_SHORT);
                        }
                    } else {
                        validations();
                        if (check == 2) {
                            checkLogin();
                        }
                    }
                } else {
                    showToast("Network Connection is Closed.", Toast.LENGTH_LONG);
                }
            }

        });

        tvforgtpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(ETusername.getText().toString().trim().length()>0){
                    fogotPasswordRoutine(ETusername.getText().toString().trim());
                }else {
                    ETusername.setError("Input CINTAA Member ID");
                    showToast("Input CINTAA Member ID",Toast.LENGTH_SHORT);
                }*/
                //showToast(randomAlphaNumeric(6),Toast.LENGTH_SHORT);
                if (Config.isNetworkConnected(LoginActivity.this)) {
                    if (ETusername.getText().toString().trim().length() > 0) {
                        fogotPasswordRoutine(ETusername.getText().toString().trim());
                    } else {
                        showToast("Enter Member ID", Toast.LENGTH_SHORT);
                        ETusername.setError("Enter Member ID");
                    }
                } else {
                    showToast("Check Network Conection", Toast.LENGTH_SHORT);
                }
            }
        });

        tvSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Config.isNetworkConnected(LoginActivity.this)) {
                    if (ETusername.getText().toString().trim().length() > 0) {
                        fogotPasswordRoutine(ETusername.getText().toString().trim());
                    } else {
                        showToast("Enter Member ID", Toast.LENGTH_SHORT);
                        ETusername.setError("Enter Member ID");
                    }
                } else {
                    showToast("Check Network Conection", Toast.LENGTH_SHORT);
                }
            }
        });

        btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Config.isNetworkConnected(LoginActivity.this)) {
                    if (ETusername.getText().toString().trim().length() > 0) {
                        if (etOtp.getText().toString().trim().length() == 6) {
                            verifyUserOTP(ETusername.getText().toString().trim());
                        } else {
                            etOtp.setError("Enter 6 Digits OTP");
                            showToast("Enter 6 Digits OTP", Toast.LENGTH_SHORT);
                        }
                    } else {
                        showToast("Enter Member ID", Toast.LENGTH_SHORT);
                        ETusername.setError("Enter Member ID");
                    }
                } else {
                    showToast("Check Network Conection", Toast.LENGTH_SHORT);
                }
            }
        });


    }//oncreate

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()


    private void showprogressdialog() {
        pd = new ProgressDialog(LoginActivity.this);
        pd.setTitle("Please Wait.....");
        pd.setMessage("Loading data.....");
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.show();
        //pd.dismiss();
    }

    private void initialise() {
        Typeface customfont = Typeface.createFromAsset(getApplicationContext().getAssets(), "lane.ttf");
        ETusername = (EditText) findViewById(R.id.ETusername);
        ETusername.setTypeface(customfont);
        ETpassword = (EditText) findViewById(R.id.ETpassword);
        ETpassword.setTypeface(customfont);
        ETusername.addTextChangedListener(new MyTextWatcher(ETusername));
        ETusername.setTypeface(customfont);
        ETpassword.addTextChangedListener(new MyTextWatcher(ETpassword));
        ETpassword.setTypeface(customfont);
        tvlogintxt = (TextView) findViewById(R.id.tvlogin);
        tvlogintxt.setTypeface(customfont);
        tvforgtpass = (TextView) findViewById(R.id.tvforgtpass);
        tvforgtpass.setTypeface(customfont);
        tvcontctus = (TextView) findViewById(R.id.tvcontactus);
        tvcontctus.setTypeface(customfont);
        tvsignup = (TextView) findViewById(R.id.tvsignup);
        tvsignup.setTypeface(customfont);
        tvhelp = (TextView) findViewById(R.id.tvhelp);
        tvhelp.setTypeface(customfont);
        login = (Button) findViewById(R.id.btnsignin);
        login.setTypeface(customfont);

        ivfb = (ImageView) findViewById(R.id.ivfb);
        ivtwitter = (ImageView) findViewById(R.id.ivtwitter);
        ivgoogle = (ImageView) findViewById(R.id.ivgoogle);

        llpwd = (LinearLayout) findViewById(R.id.llpwd);
        llotp = (LinearLayout) findViewById(R.id.llotp);
        etOtp = (EditText) findViewById(R.id.etOtp);
        tvSendOtp = (TextView) findViewById(R.id.tvSendOtp);
        tvSendOtp.setTypeface(customfont);
        tvSendOtp.setPaintFlags(tvSendOtp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnVerifyOtp = (Button) findViewById(R.id.btnVerifyOtp);
        tvOtpNote = (TextView) findViewById(R.id.tvOtpNote);
        tvOtpNote.setTypeface(customfont);

    }

    private void fogotPasswordRoutine(final String M_id) {
        llotp.setVisibility(View.VISIBLE);
        login.setVisibility(View.GONE);
        tvforgtpass.setVisibility(View.GONE);
        llpwd.setVisibility(View.GONE);
        tvSendOtp.setEnabled(false);
        tvSendOtp.setAlpha((float) 0.3);

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", M_id);
        client.post(Config.URLselectCheckFirstTimeLogin, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("firstCheck");
                    if (jsonArray.length() > 0) {
                        JSONObject j1 = jsonArray.getJSONObject(0);
                        String isFirstLogin = j1.getString("isFirstLogin");
                        String account_status = j1.getString("account_status");
                        String mobile = j1.getString("mobile");
                        String email = j1.getString("email");

                        if (account_status.equals("active")) {
                            String otp = randomAlphaNumeric(6);
                            //Send Otp by SMS
                            sendSMSOtp(M_id, otp, mobile);


                            //Send Otp by Email
                            if (email.length() > 0) {
                                if (isValidEmaillId(email)) {
                                    sendMailOtp(otp, email);
                                }
                            }

                            if (mobile.length() > 4) {
                                showToast("OTP sent to ******" + mobile.substring(mobile.length() - 4), Toast.LENGTH_SHORT);
                            } else {
                                showToast("OTP sent to ******" + mobile.substring(mobile.length()), Toast.LENGTH_SHORT);
                            }


                        }
                    } else {
                        showToast("Invalid CINTAA Member ID !!", Toast.LENGTH_SHORT);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//fogotPasswordRoutine

    private void checkFirstTimeStatus(final String M_id) {
        String id = null, firstLogin = null;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select * from tbFirstLogin", null);
            if (cursor.moveToFirst()) {
                id = cursor.getString(0);
                firstLogin = cursor.getString(1);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        if (M_id.equals(id)) {
            //Member ID Matched
            if (!firstLogin.equals("no")) {
                //Send to Otp Request
                fogotPasswordRoutine(M_id);
            } else {
                login.setText("Sign In");
                llpwd.setVisibility(View.VISIBLE);
                llotp.setVisibility(View.GONE);
                isFirstTime = false;
            }
        } else {
            //Member ID doesn't Matches, Check Online
            //selectCheckFirstTimeLogin
            showprogressdialog();
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("member_id", M_id);
            client.post(Config.URLselectCheckFirstTimeLogin, params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("firstCheck");
                        if (jsonArray.length() > 0) {
                            JSONObject j1 = jsonArray.getJSONObject(0);
                            String isFirstLogin = j1.getString("isFirstLogin");
                            String account_status = j1.getString("account_status");
                            String mobile = j1.getString("mobile");

                            if (account_status.equals("active")) {
                                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbFirstLogin set " +
                                        "member_id='" + M_id + "', isFirstLogin='" + isFirstLogin + "' ");
                                long result = sqLiteStatement.executeUpdateDelete();
                                if (!isFirstLogin.equals("no")) {
                                    //Send Otp
                                    fogotPasswordRoutine(M_id);
                                } else {
                                    login.setText("Sign In");
                                    llpwd.setVisibility(View.VISIBLE);
                                    llotp.setVisibility(View.GONE);
                                    isFirstTime = false;
                                }
                            } else {
                                showToast("Account is Deactivated. Contact CINTAA !!", Toast.LENGTH_SHORT);
                            }
                        } else {
                            showToast("Incorrect CINTAA Member ID", Toast.LENGTH_SHORT);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pd != null) {
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    showToast("could not connect", Toast.LENGTH_LONG);
                    if (pd != null) {
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }
            });
        }
    }//checkFirstTimeStatus

    public void checkLogin() {
        String id = null;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select * from tbFirstLogin", null);
            if (cursor.moveToFirst()) {
                id = cursor.getString(0);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        if (!ETusername.getText().toString().trim().equals(id)) {
            checkFirstTimeStatus(ETusername.getText().toString().trim());
        } else {
            showprogressdialog();
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("member_id", ETusername.getText().toString().trim());
            params.put("password", ETpassword.getText().toString().trim());
            client.post(Config.urlselectIdandPw, params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        success1 = response.getString("success");
                        GlobalClass.printLog(LoginActivity.this,"-----------success1==========="+success1);
                        if (success1.equals("1")) {
                            selectprofileinfo();
                        } else {
                            showToast("incorrect Login", Toast.LENGTH_LONG);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (pd != null) {
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    showToast("could not connect", Toast.LENGTH_LONG);
                    if (pd != null) {
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }
            });
        }

    }//checkLogin

    public void getMemberData() {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", ETusername.getText().toString().trim());
        client.post(Config.URLmyprofilegetdata, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("Myprofiledata");


                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String name = j1.getString("name");
                            String gender = j1.getString("gender");
                            String address = j1.getString("address");
                            String city = j1.getString("city");
                            String state = j1.getString("state");
                            String zip = j1.getString("zip");
                            String country = j1.getString("country");
                            String email = j1.getString("email");
                            String password = j1.getString("password");
                            String mobile = j1.getString("mobile");
                            String website = j1.getString("website");
                            String profile_imgurl = j1.getString("profile_imgurl");
                            String alt_mobile = j1.getString("alt_mobile");

                            //Delete Old Values from SQLite
                            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_UserProfile");
                            int res = sqLiteStatement.executeUpdateDelete();

                            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_UserProfile values(?,?,?,?,?,?,?,?,?,?,?,?)");
                            //name ,gender ,address ,city ,state ,country ,zip ,email ,password ,mobile ,alt_mobile ,website
                            sqLiteStatement.bindString(1, name);
                            sqLiteStatement.bindString(2, gender);
                            sqLiteStatement.bindString(3, address);
                            sqLiteStatement.bindString(4, city);
                            sqLiteStatement.bindString(5, state);
                            sqLiteStatement.bindString(6, country);
                            sqLiteStatement.bindString(7, zip);
                            sqLiteStatement.bindString(8, email);
                            sqLiteStatement.bindString(9, password);
                            sqLiteStatement.bindString(10, mobile);
                            sqLiteStatement.bindString(11, alt_mobile);
                            sqLiteStatement.bindString(12, website);
                            long result = sqLiteStatement.executeInsert();

                            ETusername.setText("");
                            ETpassword.setText("");

                            if (pd != null) {
                                if (pd.isShowing()) {
                                    pd.dismiss();
                                }
                            }
                            GlobalClass.printLog(LoginActivity.this, "---------BBBBBBBBBBB");

                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (pd != null) {
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//getMemberData()


    public void validations() {
        check = 0;
        if (ETusername.getText().toString().length() == 0) {
            ETusername.setError("Input CINTAA Member ID");
        } else {
            check++;
        }
        if (ETpassword.getText().toString().length() == 0) {
            ETpassword.setError("Input Password");
        } else {
            check++;
        }
        return;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.ETusername:
                    break;
                case R.id.ETpassword:
                    break;

            }
        }
    }

    void selectprofileinfo() {
        pd.setTitle("Login Success..");
        pd.setMessage("Downloading Data, Please Wait...");
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", ETusername.getText().toString().trim());
        client.post(Config.URLprofileimgandname, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("Nameandimage");

                    String email = "";

                    preRequest();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject j1 = jsonArray.getJSONObject(i);
                        profile_url = j1.getString("profile_imgurl");
                        email = j1.getString("email");
                        name = j1.getString("name");
                        //String a = j1.getString("name");
                        String gender = j1.getString("gender");
                        String address = j1.getString("address");
                        String city = j1.getString("city");
                        String state = j1.getString("state");
                        String zip = j1.getString("zip");
                        String country = j1.getString("country");
                        //String email = j1.getString("email");
                        String password = j1.getString("password");
                        String mobile = j1.getString("mobile");
                        String website = j1.getString("website");
                        String profile_imgurl = j1.getString("profile_imgurl");
                        String alt_mobile = j1.getString("alt_mobile");

                        //Delete Old Values from SQLite
                        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_UserProfile");
                        long res = sqLiteStatement.executeUpdateDelete();

                        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_UserProfile values(?,?,?,?,?,?,?,?,?,?,?,?)");
                        //name ,gender ,address ,city ,state ,country ,zip ,email ,password ,mobile ,alt_mobile ,website
                        sqLiteStatement.bindString(1, name);
                        sqLiteStatement.bindString(2, gender);
                        sqLiteStatement.bindString(3, address);
                        sqLiteStatement.bindString(4, city);
                        sqLiteStatement.bindString(5, state);
                        sqLiteStatement.bindString(6, country);
                        sqLiteStatement.bindString(7, zip);
                        sqLiteStatement.bindString(8, email);
                        sqLiteStatement.bindString(9, password);
                        sqLiteStatement.bindString(10, mobile);
                        sqLiteStatement.bindString(11, alt_mobile);
                        sqLiteStatement.bindString(12, website);
                        long result = sqLiteStatement.executeInsert();

                        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbFirstLogin set " +
                                "member_id='" + ETusername.getText().toString().trim() + "', isFirstLogin='no' ");
                        result = sqLiteStatement.executeUpdateDelete();

                    }

                    editor.putString("member_id", ETusername.getText().toString().trim());
                    editor.putString("password", ETpassword.getText().toString().trim());
                    editor.putString("profile_imgurl", profile_url);
                    if (notification.equals("ON")) {
                        editor.putInt("admin_notificaton", 1);
                    }
                    editor.putInt("app_reminder", 1);
                    editor.putString("name", name);
                    editor.putString("email", email);
                    editor.putInt("firstStart", 1);//new preference --28June
                    editor.commit();
                    String m_id = ETusername.getText().toString().trim();
                    // SetAlert(m_id);
                    //getMemberData();
                    GlobalClass.printLog(LoginActivity.this, "---------CCCCCCCCCCC");
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//selectprofileinfo

    private void InsertSQLite(String m_id, String entry_id, String shoot_date, String film_tv_name, String shift_time, String call_time, String shoot_location, String lat, String lng, String schedule_status) {

        //Insert New Values To SQLite
        String alert_flag = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                alert_flag = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        int requestCode = (int) (new Date().getTime() / 1000);

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry values(?,?,?,?,?,?,?,?,?,?,?,?,?)");

        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, m_id);
        sqLiteStatement.bindString(3, shoot_date);
        sqLiteStatement.bindString(4, film_tv_name);
        sqLiteStatement.bindString(5, shift_time);
        sqLiteStatement.bindString(6, call_time);
        sqLiteStatement.bindString(7, shoot_location);
        sqLiteStatement.bindString(8, lat);
        sqLiteStatement.bindString(9, lng);
        sqLiteStatement.bindString(10, schedule_status);
        sqLiteStatement.bindString(11, alert_flag);
        sqLiteStatement.bindLong(12, requestCode);
        sqLiteStatement.bindString(13, "done");

        long result = sqLiteStatement.executeInsert();

        // Toast.makeText(getActivity(),"SQLite Success"+result,Toast.LENGTH_LONG).show();

    }//InsertSQLite

   /* private void SetAlert(String m_id){
        String alert_flag = "ON";
        cursor=sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings",null);
        if(cursor.moveToFirst())
        {
            do {
                alert_flag = cursor.getString(0);
            }while (cursor.moveToNext());
        }
        if(alert_flag.equals("ON")){

            List<Integer> requestCode = new ArrayList<Integer>();
            List<String> dateHour = new ArrayList<String>();

            cursor=sqLiteDatabaseRead.rawQuery("select shoot_date,call_time,requestCode_Alarm from sqtb_diary_entry where alert_flag='ON' and member_id='"+m_id+"'",null);
            if(cursor.moveToFirst())
            {
                do {
                    String date = cursor.getString(0);
                    String hour = cursor.getString(1);
                    dateHour.add(date+" "+hour);
                    requestCode.add(cursor.getInt(2));

                }while (cursor.moveToNext());
            }
            if(requestCode.size()>0 && dateHour.size()>0){
                List<Long> seconds = CalculateSecondsFromCurrentDate(dateHour);

                //startAlert(requestCode,seconds);
            }

        }

    }//SetAlert*/

   /* //Calculate Time in seconds from Current Date to Given Date
    private List<Long> CalculateSecondsFromCurrentDate(List<String> dateHours)  {

        List<Long> seconds = new ArrayList<Long>();

        Date now,eventDate,currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        for(int i=0; i<dateHours.size();i++){

            try {
                eventDate = sdfformat.parse(dateHours.get(i));

                //in milliseconds
                long diff = eventDate.getTime() - currentDate.getTime();

                long diffSeconds = diff / 1000;
                long diffMinutes = diff / (60 * 1000);
                long diffHours = diff / (60 * 60 * 1000);
                long diffDays = diff / (24 * 60 * 60 * 1000);

                long on_head = diffSeconds+diffMinutes+diffHours+diffDays;
                String alert = "00:00";
                cursor=sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings",null);
                if(cursor.moveToFirst())
                {
                    do {
                        alert = cursor.getString(0);
                    }while (cursor.moveToNext());
                }

                String[] units = alert.split(":"); //will break the string up into an array
                long hours = Integer.parseInt(units[0]); //hours element
                long minutes = Integer.parseInt(units[1]); //minutes element
                long before_head = 60 * 60 * hours + minutes * 60; //add up our values

                before_head = on_head - before_head;

                if(before_head>0){
                    seconds.add(before_head);
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate
*/
   /* public void startAlert(List<Integer> requestCode, List<Long> seconds) {

        //int requestCode = (int) (new Date().getTime()/1000);

        for(int i=0; i<seconds.size();i++){
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(this, AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode",String.valueOf(requestCode.get(i)));
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), requestCode.get(i), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds.get(i) * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }


    }//startAlert*/


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    private void preRequest() {
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select entry_id from sqtb_diary_entry where ServerEntryStatus='done'", null);
            if (cursor.moveToFirst()) {
                do {
                    SetAlertOFF(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_entry where ServerEntryStatus='done'");
        int res = sqLiteStatement.executeUpdateDelete();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbScheduleDetails where ServerEntryStatus='done'");
        res = sqLiteStatement.executeUpdateDelete();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_Callin where Status='done'");
        res = sqLiteStatement.executeUpdateDelete();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_Packup where Status='done'");
        res = sqLiteStatement.executeUpdateDelete();


    }//preRequest

    private void SetAlertOFF(String entryID) {
        int requestCode;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where entry_id='" + entryID + "'", null);
            if (cursor.moveToFirst()) {
                requestCode = cursor.getInt(0);
                Intent intent = new Intent(LoginActivity.this, AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(LoginActivity.this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) LoginActivity.this.getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//SetAlertOFF

    private void checkIfFirstLogin() {
        String id, firstLogin = "default";
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select * from tbFirstLogin", null);
            if (cursor.moveToFirst()) {
                id = cursor.getString(0);
                firstLogin = cursor.getString(1);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        if (firstLogin.equalsIgnoreCase("default")) {
            login.setText("Next");
            llpwd.setVisibility(View.GONE);
            isFirstTime = true;
        } else if (firstLogin.equalsIgnoreCase("yes")) {
            login.setText("Next");
            llpwd.setVisibility(View.GONE);
            isFirstTime = true;
        } else {
            login.setText("Sign In");
            llpwd.setVisibility(View.VISIBLE);
            llotp.setVisibility(View.GONE);
            isFirstTime = false;
        }
    }//checkIfFirstLogin

    @NonNull
    private String randomAlphaNumeric(int count) {
        final String ALPHA_NUMERIC_STRING = "A1B2C3D4E5F6G7H8I9J0K1L2M3N4O5P6Q7R8S9T0U1V2W3X4Y5Z6";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }//randomAlphaNumeric

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }//isValidEmaillId

    private void sendSMSOtp(final String memberID, String otp, String userContact) {
        if (pd == null) {
            if (!pd.isShowing()) {
                showprogressdialog();
            }
        }
        pd.setMessage("Sending OTP...");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", memberID);
        params.put("userOTP", otp);
        params.put("phone", userContact);
        client.post(Config.URLcintaaSendSMS, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                //Set Resend Timer
                timer = new Timer();
                timer.schedule(new timerTask(), 1000, 1000);
                //timer.schedule(new timerTask(),60000,60000);
                //Expire OTP in 10 mins
                try {
                    timerResetOTP.cancel();
                } catch (Exception e) {
                }
                timerResetOTP = new Timer();
                timerResetOTP.schedule(new timerTaskResetOTP(memberID), 600000);//10 mins

                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                //showToast("Error in Sending Otp..", Toast.LENGTH_LONG);

                //Set Resend Timer
                timer = new Timer();
                timer.schedule(new timerTask(), 1000, 1000);
                //Expire OTP in 10 mins
                try {
                    timerResetOTP.cancel();
                } catch (Exception e) {
                }
                timerResetOTP = new Timer();
                timerResetOTP.schedule(new timerTaskResetOTP(memberID), 600000);//600000 10 mins

                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//sendSMSOtp

    class timerTask extends TimerTask {
        int countMinutes = 10, countSeconds = 60;

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvSendOtp.setText("Resend OTP ( After: \n " + countMinutes + ":" + countSeconds + " mins)");
                    countSeconds -= 1;
                    if (countMinutes == 0 && countSeconds == 0) {
                        timer.cancel();
                        tvSendOtp.setText("Resend OTP");
                        tvSendOtp.setEnabled(true);
                        tvSendOtp.setAlpha((float) 1.0);
                    }
                    if (countSeconds == 0 && countMinutes > 0) {
                        countMinutes -= 1;
                        countSeconds = 59;
                        tvSendOtp.setAlpha((float) 0.3);
                    }
                }
            });
        }

    }//timerTask

    class timerTaskResetOTP extends TimerTask {

        int count = 0;
        String memberID;

        public timerTaskResetOTP(String memberID) {
            if (count == 0) {
                this.memberID = memberID;
            }
        }

        @Override
        public void run() {
            if (count == 0) {
                new resetOTP(memberID).execute();
                count += 1;
            }
        }

    }//timerTaskResetOTP

    class resetOTP extends AsyncTask<String, String, String> {
        String memberId;

        public resetOTP(String memberId) {
            this.memberId = memberId;
            doInBackground();
        }

        @Override
        protected String doInBackground(String... params) {

            StringBuilder paramsString = new StringBuilder();
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(Config.URLresetOTP);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                paramsString.append(URLEncoder.encode("member_id", "UTF-8"));
                paramsString.append("=");
                paramsString.append(URLEncoder.encode(memberId, "UTF-8"));
                paramsString.append("&");

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String JsonResponse = null;
                OutputStream outputStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(paramsString.toString());
                writer.flush();
                writer.close();
                outputStream.close();

                //Input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                JsonResponse = reader.readLine();

                try {
                    //send to post execute
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }//doInBackground

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {

            }
        }

    } //resetOtp

    private void verifyUserOTP(String memberID) {
        showprogressdialog();
        pd.setMessage("Verifying OTP...");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", memberID);
        client.post(Config.URLverifyOTP, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("verifyOTP");
                    if (jsonArray.length() > 0) {
                        JSONObject j1 = jsonArray.getJSONObject(0);
                        String userOTP = j1.getString("userOTP");

                        //if (! userOTP.equals("no")){
                        if (etOtp.getText().toString().trim().equalsIgnoreCase(userOTP)) {
                            Intent intent = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                            intent.putExtra("memberID", ETusername.getText().toString());
                            startActivity(intent);
                            finish();
                        } else {
                            showToast("OTP is Incorrect", Toast.LENGTH_SHORT);
                            //showToast("Your current OTP is: "+userOTP,Toast.LENGTH_SHORT);
                        }

                        /*}else {
                            showToast("Error in OTP",Toast.LENGTH_SHORT);
                        }*/

                    } else {
                        showToast("Invalid CINTAA Member ID !!", Toast.LENGTH_SHORT);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//verifyUserOTP

    private void sendMailOtp(String otp, String memberEmail) {

        //artistName = ETname.getText().toString().trim();
        String name = "CINTAA Diary";
        String username = Config.cintaaMail;
        String password = Config.cintaaMailPwd;
        String email = memberEmail;
        String subject = otp + " is your OTP to your CINTAA Account";
        getSignIn(name, username, password, email, subject, otp);

    }//sendMailOtp


    private void getSignIn(String name, String username, String password, String email, String subject, String otp) {
        if (!(isValidEmaillId(email))) {
            //etauthorityemail.setText("Please Enter Valid EmailId *");
        } else {
            if (!isNetworkConnected())  //if connection available
            {
                showToast("Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            } else {
                String message;

                message = "Your one-time Verification code is: <b>" + otp + "</b>.<br/><br/>" +
                        "<b>Please do not share your OTP or password with anyone to avoid misuse of your account.</b>" +
                        "<br/><br/><br/><br/><br/><br/>" +
                        "This email message is intended only for CINTAA members and their employers and does not tantamount to spamming as it contains information that is confidential. If you are not the intended recipient, any disclosure, distribution or other use of this email message is prohibited. <br/><br/>" +
                        "<img src=\"" + Config.mailLogoUrl + "\" alt=\"Logo\" height=\"40\"> <br/><br/>" +
                        "<small>Cine & TV Artistes' Association (CINTAA)</small><br/>" +
                        "<small>221, Kartik Complex, 2nd Flr.,</small><br/>" +
                        "<small>Opp.Laxmi Ind. Estate, New Link Rd.,</small><br/>" +
                        "<small>Andheri (W), Mumbai-400 053.</small><br/>" +
                        "<small>Tel.# 26730511/10</small><br/>";

                sendMail(email, subject, message, name, username, password);

            }
        }

    }

    private void sendMail(String email, String subject, String messageBody, String name, String username, String password) {
        Session session = createSessionObject(username, password);

        try {
            Message message = createMessage(email, subject, messageBody,
                    session, username, name);
            new LoginActivity.SendMailTask(LoginActivity.this).execute(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject,
                                  String messageBody, Session session, String username, String name) throws MessagingException,
            UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                email, email));
        message.setSubject(subject);
        //message.setText(messageBody);
        message.setContent(messageBody, "text/html");
        return message;
    }

    private Session createSessionObject(final String username, final String password) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", Config.smtpHost);
        properties.put("mail.smtp.port", Config.smtpPort);

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }

    public class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        Activity ctx;

        public SendMailTask(Activity activity) {
            ctx = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ctx = LoginActivity.this;
            /*progressDialog = ProgressDialog.show(getActivity(),
                    "Please wait", "Sending mail", true, false);*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        /*Toast.makeText(SnapshotDetailsActivity.this,
                                "Passwod Send To Your Mail Successfully", Toast.LENGTH_LONG)
                                .show();*/
                    }
                });
            } catch (final MessagingException e) {
                e.printStackTrace();
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        ///https://www.google.com/settings/security/lesssecureapps
                        //go throught that link and select less security
                              /*  Toast.makeText(ForgotPasswordActivity.this,
                                        e.getClass() + " : " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();*/
                    }
                });
            }
            return null;
        }
    }
}





