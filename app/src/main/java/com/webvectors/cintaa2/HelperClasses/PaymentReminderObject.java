package com.webvectors.cintaa2.HelperClasses;

/**
 * Created by Admin on 11-Jun-17.
 */

public class PaymentReminderObject {
    String entry_id, member_id, film_tv_name, paymentAfter, shoot_date, rateType, rate,prodHouse, producerName;

    public String getProdHouse() {
        return prodHouse;
    }

    public void setProdHouse(String prodHouse) {
        this.prodHouse = prodHouse;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public PaymentReminderObject(String entry_id, String member_id, String film_tv_name, String paymentAfter, String shoot_date, String rateType, String rate, String prodHouse, String producerName){
        this.entry_id = entry_id;
        this.member_id = member_id;
        this.film_tv_name = film_tv_name;
        this.paymentAfter = paymentAfter;
        this.shoot_date = shoot_date;
        this.rateType = rateType;
        this.rate = rate;
        this.prodHouse=prodHouse;
        this.producerName=producerName;
    }

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getFilm_tv_name() {
        return film_tv_name;
    }

    public void setFilm_tv_name(String film_tv_name) {
        this.film_tv_name = film_tv_name;
    }

    public String getPaymentAfter() {
        return paymentAfter;
    }

    public void setPaymentAfter(String paymentAfter) {
        this.paymentAfter = paymentAfter;
    }

    public String getShoot_date() {
        return shoot_date;
    }

    public void setShoot_date(String shoot_date) {
        this.shoot_date = shoot_date;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
