package com.webvectors.cintaa2.HelperClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteDB extends SQLiteOpenHelper {

    /*While Updating the app version also increase the version below here */
    public static final int version=246;
    /*After increasing this version make changes at "LINE_1" and "LINE_2" in this file below:*/

    public static final String Database="Cintaa.db";

    public SQLiteDB(Context context) {
        super(context, Database, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table sqtb_appversion(versionName text, versionCode text)");
        /*LINE_1:
        here in insert query, change the parameters values with new versionName and versionCode respectively*/
        db.execSQL("insert into sqtb_appversion values('6.0.0.7','13')");

        db.execSQL("create table sqtb_settings(notification text, alert text, alert_before text)");
        db.execSQL("insert into sqtb_settings values('ON','ON','01:00')");

        db.execSQL("create table sqtb_ScheduleFilterData_inprogress(shoot_date text,phouse text,location text,shift_time text,film_nm text,entry_id text,schedule_status text)");
        db.execSQL("create table sqtb_ScheduleFilterData_completed(shoot_date text,phouse text,location text,shift_time text,film_nm text,entry_id text,schedule_status text)");

        db.execSQL("create table sqtb_UserProfile (name text,gender text,address text,city text,state text,country text,zip text,email text,password text,mobile text,alt_mobile text,website text)");

        db.execSQL("CREATE TABLE sqtb_shift_time (shift_start text)");

        db.execSQL("CREATE TABLE sqtb_diary_Callin (entry_id text, callin_time text, callin_lat text, callin_lng text,Status text)");
        db.execSQL("CREATE TABLE sqtb_diary_Packup (entry_id text, packup_time text, packup_lat text, packup_lng text,Status text)");

        /******New Update*******/
        //Changes in sqtb_diary_entry
        //In table sqtb_diary_entry, "Task_callin" is used to check if the Callin Time is inserted to Server or not
        db.execSQL("create table sqtb_diary_entry(entry_id text, member_i" +
                "d text, shoot_date text, film_tv_name text," +
                " shift_time text,call_time text,shoot_location text, lat text, lng text, schedule_status text,alert_flag text," +
                "requestCode_Alarm INTEGER, Task_callin text, characterName text, rate text, paymentAfter text," +
                " remark text, ServerEntryStatus text,rateType text, shoot_map_location text)");

        db.execSQL("CREATE TABLE tbScheduleDetails (memberId text, entryId text, scheduleDate text, filmName text, productionHouse text, producerName text, scheduleStatus text, ServerEntryStatus text)");
        db.execSQL("CREATE TABLE tbConveyance (conveyance text)");
        db.execSQL("CREATE TABLE tbProductionHouse (productionHouse text,producerName text, productionEmail text)");
        db.execSQL("CREATE TABLE tbAuthorise (entryId text,member_id text, name text, number text, sign text, encodedString text, signDate text, email text, prodHouse text, prodName text, severStatus text,author_remark text)");

        db.execSQL("CREATE TABLE tbFirstLogin (member_id text, isFirstLogin text)");
        db.execSQL("insert into tbFirstLogin values('0x0','default')");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("---------","ON UPGRADE:======="+newVersion +" old:  "+oldVersion);        /******New Update*******/

        /******New Update*******/
        //db.execSQL("DROP TABLE IF EXISTS sqtb_diary_entry");

        //In table sqtb_diary_entry, "Task_callin" is used to check if the Callin Time is inserted to Server or not
        /*db.execSQL("create table sqtb_diary_entry(entry_id text, member_id text, shoot_date text, film_tv_name text," +
                " shift_time text,call_time text,shoot_location text, lat text, lng text, schedule_status text,alert_flag text," +
                "requestCode_Alarm INTEGER, Task_callin text, characterName text, rate text, paymentAfter text," +
                " remark text, ServerEntryStatus text,rateType text)");
*/
        //db.execSQL("DROP TABLE IF EXISTS tbScheduleDetails");
        //db.execSQL("CREATE TABLE tbScheduleDetails (memberId text, entryId text, scheduleDate text, filmName text, productionHouse text, producerName text, scheduleStatus text, ServerEntryStatus text)");
        //db.execSQL("DROP TABLE IF EXISTS tbConveyance");
        //db.execSQL("CREATE TABLE tbConveyance (conveyance text)");
        //db.execSQL("DROP TABLE IF EXISTS tbProductionHouse");
        //db.execSQL("CREATE TABLE tbProductionHouse (productionHouse text,producerName text, productionEmail text)");
        //db.execSQL("DROP TABLE IF EXISTS tbAuthorise");
        //db.execSQL("CREATE TABLE tbAuthorise (entryId text,member_id text, name text, number text, sign text, encodedString text, signDate text, email text, prodHouse text, prodName text, severStatus text)");

        //db.execSQL("DROP TABLE IF EXISTS tbFirstLogin");
        //db.execSQL("CREATE TABLE tbFirstLogin (member_id text, isFirstLogin text)");
        //db.execSQL("insert into tbFirstLogin values('0x0','default')");

        try{
            //db.execSQL("  ALTER TABLE tbAuthorise ADD author_remark text default 'xRemarker'");
            db.execSQL("ALTER TABLE sqtb_diary_entry ADD shoot_map_location text");
        }catch (Exception e){}

        /*LINE_2: here also change the values with new versionName and VersionCode respectively*/
        db.execSQL("update sqtb_appversion set versionName='6.0.0.7' , versionCode='13'; ");


    }
}
