package com.webvectors.cintaa2.fragment;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment{

    public void showToast(String message, int time)
    {

        Toast toast = Toast.makeText(getContext(), message, time);
		View toastView  = toast.getView();
		//toastView.setBackgroundColor(Color.YELLOW);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toastView.setBackgroundDrawable( getResources().getDrawable(R.drawable.edshape_toast));
        } else {
            toastView.setBackground( getResources().getDrawable(R.drawable.edshape_toast));
        }
		TextView v = (TextView) toastView.findViewById(android.R.id.message);
        v.setTextColor(Color.BLACK);
		v.setPadding(CommonHelper.convertDpToPx(10, getContext()), CommonHelper.convertDpToPx(5, getContext()), CommonHelper.convertDpToPx(10, getContext()), CommonHelper.convertDpToPx(5, getContext()));
        toast.show();
    }
}
