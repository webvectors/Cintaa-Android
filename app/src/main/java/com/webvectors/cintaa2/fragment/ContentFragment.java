package com.webvectors.cintaa2.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webvectors.cintaa2.R;

import yalantis.com.sidemenu.interfaces.ScreenShotable;

/**
 * Created by Konstantin on 22.12.2014.
 */
public class ContentFragment extends Fragment implements ScreenShotable {
    public static final String CLOSE = "Close";
    public static final String BUILDING = "Building";
    public static final String BOOK = "Book";
    public static final String PAINT = "Paint";
    public static final String CASE = "Case";
    public static final String SHOP = "Shop";
    public static final String PARTY = "Party";
    public static final String MOVIE = "Movie";

    public static final String varprivacypolicydash = "varprivacypolicydash";
    public static final String varhelpcenterdash = "varhelpcenterdash";
    public static final String varsettingdash = "varsettingdash";
    public static final String varrateappdash = "varrateappdash";
    public static final String varleavefeedbackdash = "varleavefeedbackdash";
    public static final String varshareappdash = "varshareappdash";
    public static final String varmoudash = "varmoudash";
    public static final String varcomplaindash = "varcomplaindash";
    public static final String varwebsitedash = "varwebsitedash";
    public static final String varscheduledash = "varscheduledash";
    public static final String varcalenderdash = "varcalenderdash";
    public static final String vardiarysnapdash = "vardiarysnapdash";
    public static final String varprofiledash = "varprofiledash";
    public static final String vardiaryenrtydash = "vardiaryenrtydash";
    public static final String varinstructiondash = "varinstructiondash";

    private View containerView;
    protected ImageView mImageView;
    protected int res;
    private Bitmap bitmap;

    public static ContentFragment newInstance(int resId) {
        ContentFragment contentFragment = new ContentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Integer.class.getName(), resId);
        contentFragment.setArguments(bundle);
        return contentFragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.containerView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getArguments().getInt(Integer.class.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
       // mImageView = (ImageView) rootView.findViewById(R.id.image_content);
       // mImageView.setClickable(true);
      //  mImageView.setFocusable(true);
       // mImageView.setImageResource(res);
        return rootView;
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
             //   Bitmap bitmap = Bitmap.createBitmap(containerView.getWidth(),
               //         containerView.getHeight(), Bitmap.Config.ARGB_8888);
               // Canvas canvas = new Canvas(bitmap);
               // containerView.draw(canvas);
               // ContentFragment.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }
}

