package com.webvectors.cintaa2.fragment;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.activity.BaseActivity;
import com.webvectors.cintaa2.activity.InProgressDetailActivity;
import com.webvectors.cintaa2.activity.UpdateAppActivity;
import com.webvectors.cintaa2.activity.WebViewActivity;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.BackgroundService_OffLineCallinPackup;
import com.webvectors.cintaa2.utility.BackgroundService_OfflineTbDiaryEntry;
import com.webvectors.cintaa2.utility.BackgroundService_OfflineTbSchedule;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.Dialog_Classes.AdminNotification_Dialog_Class;
import com.webvectors.cintaa2.Dialog_Classes.ExitApp_Dialog_Class;
import com.webvectors.cintaa2.Dialog_Classes.PaymentAfterList_Dialog_Class;
import com.webvectors.cintaa2.Dialog_Classes.ReminderRenew_Dialog_Class;
import com.webvectors.cintaa2.HelperClasses.PaymentReminderObject;
import com.webvectors.cintaa2.model.ScheduleEntryEditData;
import com.webvectors.cintaa2.activity.HomeActivity;
import com.webvectors.cintaa2.activity.LoginActivity;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.OnViewInflateListener;


public class HomeFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    public static HomeFragment homeFragment;
    SharedPreferences preferences;
    Typeface font;
    int keycode;
    AutoCompleteTextView autotext;
    String member_id, profile_url, name;
    private ArrayAdapter<String> adapter;

    LinearLayout llDashCompleted, llDashDiaryEntry, lldashSchedule;
    ImageView ivDashCompleted, ivDashDiaryEntry, ivdashSchedule;
    TextView tvDashCompleted, tvDashDiaryEntry, tvdashSchedule;

    String item[] = {
            "Instructions", "Diary Entry", "Profile", "Calendar", "Schedule", "Contact Us", "Our Website", "Wanna Complain",
            "MoU", "Share App", "Leave Feedback", "Rate App", "Settings", "Help Center", "Privacy Policy"};
    public static final String CLOSE = "Close";
    public static final String varexit = "varexit";
    public static final String varlogout = "varlogout";
    public static final String varsettingdash = "varsettingdash";
    public static final String varrateappdash = "varrateappdash";
    public static final String varleavefeedbackdash = "varleavefeedbackdash";
    public static final String varshareappdash = "varshareappdash";
    public static final String varmoudash = "varmoudash";
    public static final String varcomplaindash = "varcomplaindash";
    public static final String varwebsitedash = "varwebsitedash";
    public static final String varscheduledash = "varscheduledash";
    public static final String varcalenderdash = "varcalenderdash";
    public static final String varcontactus = "varcontactus";
    public static final String varprofiledash = "varprofiledash";
    public static final String vardiaryenrtydash = "vardiaryenrtydash";
    public static final String varhome = "varhome";
    ProgressDialog pd;
    int FeedBack_flag, admin_notificaton, Reminder, firstStart;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor, subCursor;
    private FancyShowCaseView mTipFocusView;
    private String currentDate2;
    private Cursor cursor2;

    public static HomeFragment getFragment() {
        return homeFragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("Tag","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        if (DiaryentryFragment.getFragment() != null)
            DiaryentryFragment.getFragment().hideTip();

        pd = new ProgressDialog(getActivity());

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "Dashboard";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", fonts), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);


        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        profile_url = preferences.getString("profile_imgurl", "xy12z");
        FeedBack_flag = preferences.getInt("FeedBack_flag", 0);
        admin_notificaton = preferences.getInt("admin_notificaton", 0);
        //flag for Execute once after every launch --28June
        firstStart = preferences.getInt("firstStart", 0);   //editor.putInt("firstStart",1);
        Reminder = preferences.getInt("app_reminder", 0);
        name = preferences.getString("name", "xy32z");

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, 1);
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();


        /*************Check for Update Version***********************/
        String versionName = "1.0";
        String versionCode = "1";
        PackageManager manager = getActivity().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionName = info.versionName;
        versionCode = String.valueOf(info.versionCode);

        cursor = sqLiteDatabaseRead.rawQuery("select versionName, versionCode from sqtb_appversion", null);

//        ..opens UPDATE activity
        if (cursor.moveToFirst()) {
            GlobalClass.printLog(getActivity(), "cursor.getCount------ " + cursor.getCount());

            String version_nm = cursor.getString(0);
            String version_cod = cursor.getString(1);

            GlobalClass.printLog(getActivity(), "version_nm: databse " + version_nm + "version_cod: " + version_cod);
            GlobalClass.printLog(getActivity(), "versionName: app " + versionName + "versionCode: " + versionCode);

            if ((!version_nm.equals(versionName)) || (!versionCode.equals(version_cod))) {
                getActivity().startActivity(new Intent(getActivity(), UpdateAppActivity.class));
                getActivity().finish();
            }
        }
        /*************End Check for Update Version***********************/

        initialise(view);

        llDashDiaryEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new DiaryentryFragment()).addToBackStack("Tag").commit();
                String actionBarTitle = "Diary Entry";
                SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
                ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            }
        });

        llDashCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new CompletedEventsFragment()).addToBackStack("Tag").commit();
                String actionBarTitle = "My Schedule";
                SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
                ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            }
        });

        lldashSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new CalendarFragment()).addToBackStack("Tag").commit();
                String actionBarTitle = getString(R.string.schedule_entry);
                SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
                ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            }
        });


        try {

            if (Reminder == 1) {
                if (Config.isNetworkConnected(getActivity())) {
                    CheckSession();
                } else {
                    showToast("Network Connection is Close", Toast.LENGTH_LONG);
                }
                //Exicute only once after each app launch
                initSQLITE();

            }

            //.. Execute once after every launch --28June
            if (firstStart == 1) {
                preferences.edit().remove("firstStart").commit();
                showPaymentReminder();
            }

            /*String feed_back_reminder="";
            cursor=sqLiteDatabaseRead.rawQuery("select feed_date from sqtb_FeedbackReminder",null);
            if(cursor.moveToFirst())
            {
                do {
                    feed_back_reminder = cursor.getString(0);
                }while (cursor.moveToNext());
            }*/
        } finally {

            if (cursor != null)
                cursor.close();
        }

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, item);
        autotext.setThreshold(1);
        //Set adapter to AutoCompleteTextView
        autotext.setAdapter(adapter);
        autotext.setOnItemSelectedListener(this);
        autotext.setOnItemClickListener(this);


        view.setFocusableInTouchMode(true);
        view.requestFocus();

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        new ExitApp_Dialog_Class().show(getFragmentManager(), "tag");
                        //Toast.makeText(getActivity(), "Back Pressed", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });


        CheckOffLineCallinPackup();
        //Clean Unwanted Data
        DestroyPreferences();

        return view;
    }//OnCreate

    private void initSQLITE() {
        //entryId, scheduleDate, filmName, productionHouse, producerName
        cursor = sqLiteDatabaseRead.rawQuery("select entryId, scheduleDate, filmName, productionHouse, producerName from tbScheduleDetails where memberId = ? and ServerEntryStatus='done'", new String[]{member_id});
        if (cursor.moveToFirst()) {

        } else {
            if (Config.isNetworkConnected(getActivity())) {
                //get all stored event dates from database
                selectAllEvents();
            } else {
                showToast("Please Enable Data Connection !!", Toast.LENGTH_LONG);
            }
        }

        if (cursor != null)
            cursor.close();
    }//initSQLITE

    private void selectAllEvents() {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);

        client.post(Config.UrlSelectAllCalenderEvents, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {

                    List<ScheduleEntryEditData> scheduleData = new ArrayList<ScheduleEntryEditData>();
                    JSONArray jsonArray = response.getJSONArray("sht_date");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String entry_id, film_tv_name, house_name, producer_name, shoot_date, schedule_status;
                        Boolean callStatus;
                        JSONObject j1 = jsonArray.getJSONObject(i);
                        entry_id = j1.getString("entry_id");
                        film_tv_name = j1.getString("film_tv_name");
                        house_name = j1.getString("house_name");
                        producer_name = j1.getString("producer_name");
                        shoot_date = j1.getString("shoot_date");
                        schedule_status = j1.getString("schedule_status");

                        String shift_time, call_time, shoot_location, lat, lng, played_character, rate, due_days, remark, rate_type, callin_time, packup_time, shoot_map_location;
                        shift_time = j1.getString("shift_time");
                        call_time = j1.getString("call_time");
                        shoot_location = j1.getString("shoot_location");
                        lat = j1.getString("lat");
                        lng = j1.getString("lng");
                        played_character = j1.getString("played_character");
                        rate = j1.getString("rate");
                        due_days = j1.getString("due_days");
                        remark = j1.getString("remark");
                        rate_type = j1.getString("rate_type");
                        callin_time = j1.getString("callin_time");
                        packup_time = j1.getString("packup_time");
                        shoot_map_location = j1.getString(("shoot_map_location"));

                        if (call_time.length() > 4) {
                            insertAllSqtbDiaryEntry(entry_id, shoot_date, film_tv_name, schedule_status, shift_time, call_time, shoot_location, lat, lng, played_character, rate, due_days, remark, rate_type, callin_time, packup_time, shoot_map_location);
                        }

                        scheduleData.add(new ScheduleEntryEditData(entry_id, shoot_date, film_tv_name, house_name, producer_name, true, schedule_status));

                    }

                    insertCalendarDataSqlite(scheduleData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

//                //.. Execute once after every launch --28June
//                //... new added here
                if (firstStart == 1) {
                    preferences.edit().remove("firstStart").commit();
                    showPaymentReminder();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//selectAllEvents

    private void insertAllSqtbDiaryEntry(String entry_id, String shoot_date, String film_tv_name, String schedule_status, String shift_time, String call_time, String shoot_location, String lat, String lng, String played_character, String rate, String due_days, String remark, String rate_type, String callin_time, String packup_time, String shoot_map_location) {
        try {
            String alert_flag = "ON";
            cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    alert_flag = cursor.getString(0);
                } while (cursor.moveToNext());
            }

            String date = shoot_date;
            String hours = call_time;
            final String dateHours = date + " " + hours;
            long seconds = CalculateSecondsFromCurrentDate(dateHours);
            int requestCode = startAlert(seconds, entry_id);
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        /*
        entry_id , member_id , shoot_date , film_tv_name , shift_time ,call_time ,shoot_location , lat , lng , schedule_status ,
        alert_flag ,requestCode_Alarm INTEGER, Task_callin , characterName , rate , paymentAfter ,remark , ServerEntryStatus ,rateType
        */
            sqLiteStatement.bindString(1, entry_id);
            sqLiteStatement.bindString(2, member_id);
            sqLiteStatement.bindString(3, shoot_date);
            sqLiteStatement.bindString(4, film_tv_name);
            sqLiteStatement.bindString(5, shift_time);
            sqLiteStatement.bindString(6, call_time);
            sqLiteStatement.bindString(7, shoot_location);
            sqLiteStatement.bindString(8, lat);
            sqLiteStatement.bindString(9, lng);
            sqLiteStatement.bindString(10, schedule_status);
            sqLiteStatement.bindString(11, alert_flag);
            sqLiteStatement.bindLong(12, requestCode);
            if (callin_time.equals("NA") || callin_time.length() == 0) {
                sqLiteStatement.bindString(13, "undone");
            } else {
                sqLiteStatement.bindString(13, "done");
            }
            sqLiteStatement.bindString(14, played_character);
            sqLiteStatement.bindString(15, rate);
            sqLiteStatement.bindString(16, due_days);
            sqLiteStatement.bindString(17, remark);
            sqLiteStatement.bindString(18, "done");
            sqLiteStatement.bindString(19, rate_type);
            sqLiteStatement.bindString(20, shoot_map_location);
            long result = sqLiteStatement.executeInsert();

            if (!(callin_time.equals("NA") || callin_time.length() == 0)) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                // entry_id, callin_time, callin_lat, callin_lng, Status
                sqLiteStatement.bindString(1, entry_id);
                sqLiteStatement.bindString(2, callin_time);
                sqLiteStatement.bindString(3, "0");
                sqLiteStatement.bindString(4, "0");
                sqLiteStatement.bindString(5, "done");
                result = sqLiteStatement.executeInsert();
            }

            if (!(packup_time.equals("NA") || packup_time.length() == 0)) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Packup values(?,?,?,?,?)");
                sqLiteStatement.bindString(1, entry_id);
                sqLiteStatement.bindString(2, packup_time);
                sqLiteStatement.bindString(3, "0");
                sqLiteStatement.bindString(4, "0");
                sqLiteStatement.bindString(5, "done");
                result = sqLiteStatement.executeInsert();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

    }//insertAllSqtbDiaryEntry

    //..
    private void insertCalendarDataSqlite(List<ScheduleEntryEditData> scheduleData) {
        for (int k = 0; k < scheduleData.size(); k++) {
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbScheduleDetails values(?,?,?,?,?,?,?,?)");
            //memberId, entryId, scheduleDate, filmName, productionHouse, producerName from tbScheduleDetails
            sqLiteStatement.bindString(1, member_id);
            sqLiteStatement.bindString(2, scheduleData.get(k).getEntryId());
            sqLiteStatement.bindString(3, "" + scheduleData.get(k).getDateOfShoot());
            sqLiteStatement.bindString(4, scheduleData.get(k).getFilmName());
            sqLiteStatement.bindString(5, scheduleData.get(k).getProductionHouse());
            sqLiteStatement.bindString(6, scheduleData.get(k).getProducerName());
            sqLiteStatement.bindString(7, scheduleData.get(k).getScheduleStatus());
            sqLiteStatement.bindString(8, "done");
            long result = sqLiteStatement.executeInsert();


            /*if(scheduleData.get(k).getCallStatus()){
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                // entry_id, callin_time, callin_lat, callin_lng, Status
                sqLiteStatement.bindString(1, scheduleData.get(k).getEntryId());
                sqLiteStatement.bindString(2, "0");
                sqLiteStatement.bindString(3, "0");
                sqLiteStatement.bindString(4, "0");
                sqLiteStatement.bindString(5, "done");
            }*/
        }
    }//InsertCalendarData_Sqlite

    private void DestroyPreferences() {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();

        //New Entry
        editor.remove("diary_txt_filmnm");
        editor.remove("diary_txt_phouse");
        editor.remove("diary_txt_prodnm");
        editor.remove("diary_txt_shootdate");
        editor.remove("diary_txt_shootaddress");
        editor.remove("diary_txt_shootloc");
        editor.remove("diary_txt_charcnm");
        editor.remove("diary_txt_shifttime");
        editor.remove("diary_txt_calltime");
        editor.remove("diary_txt_rateper");
        editor.remove("diary_txt_duetime");
        editor.remove("diary_txt_remark");
        editor.remove("diary_txt_lat");
        editor.remove("diary_txt_lon");
        editor.remove("diary_StayHere");

        //Edit Mode
        editor.remove("Edit_diary_txt_filmnm");
        editor.remove("Edit_diary_txt_phouse");
        editor.remove("Edit_diary_txt_prodnm");
        editor.remove("Edit_diary_txt_shootdate");
        editor.remove("Edit_diary_txt_shootaddress");
        editor.remove("Edit_diary_txt_shootloc");
        editor.remove("Edit_diary_txt_charcnm");
        editor.remove("Edit_diary_txt_shifttime");
        editor.remove("Edit_diary_txt_calltime");
        editor.remove("Edit_diary_txt_rateper");
        editor.remove("Edit_diary_txt_duetime");
        editor.remove("Edit_diary_txt_remark");
        editor.remove("Edit_Edit_diary_txt_lat");
        editor.remove("Edit_diary_txt_lon");
        // editor.remove("Edit_diary_StayHere");
        editor.remove("Edit_diary_StHouse_id");

        editor.commit();

    }//DestroyPreferences

    private void showPaymentReminder() {
//...
        GlobalClass.printLog(getActivity(), "SHOW PAYMENT REMINDER");
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        Calendar cal = Calendar.getInstance();
        String currentDate = sdfDate.format(cal.getTime());
        Date currDate = new Date();
        Date end_date = new Date();
        List<PaymentReminderObject> paymentReminderList = new ArrayList<PaymentReminderObject>();
        List<PaymentReminderObject> paymentReminderListToShow = new ArrayList<PaymentReminderObject>();
        cursor = sqLiteDatabaseRead.rawQuery("select entry_id, member_id, film_tv_name, paymentAfter, shoot_date, rateType, rate from sqtb_diary_entry where member_id=?",
                new String[]{member_id});
        try {
            if (cursor.moveToFirst()) {
                do {
                    String entry_id, member_id, film_tv_name, paymentAfter, shoot_date, rateType, rate;
                    entry_id = cursor.getString(0);
                    member_id = cursor.getString(1);
                    film_tv_name = cursor.getString(2);
                    paymentAfter = cursor.getString(3);
                    shoot_date = cursor.getString(4);
                    rateType = cursor.getString(5);
                    rate = cursor.getString(6);
                    String prodHouse = "--", producerName = "--";
                    subCursor = sqLiteDatabaseRead.rawQuery("select productionHouse , producerName from tbScheduleDetails where entryId=?", new String[]{entry_id});
                    if (subCursor.moveToFirst()) {
                        prodHouse = subCursor.getString(0);
                        producerName = subCursor.getString(1);
                    }
                    if (subCursor != null)
                        subCursor.close();

                    if (null != shoot_date) {
                        paymentReminderList.add(new PaymentReminderObject(entry_id, member_id, film_tv_name, paymentAfter, shoot_date, rateType, rate, prodHouse, producerName));
                    }
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
            if (subCursor != null)
                subCursor.close();
        }
        for (int i = 0; i < paymentReminderList.size(); i++) {

            try {
                if (null == paymentReminderList.get(i).getShoot_date()) {
                    return;
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(sdfDate.parse(paymentReminderList.get(i).getShoot_date()));
                calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(paymentReminderList.get(i).getPaymentAfter()));
                currentDate2 = sdfDate.format(calendar.getTime());

                end_date = sdfDate.parse(currentDate2);
                currDate = sdfDate.parse(currentDate);

            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

//            long diff = end_date.getTime() - end_date2.getTime();
            long diff = end_date.getTime() - currDate.getTime();


            int paymentAfter = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            if (paymentAfter >= 0 && paymentAfter <= 2) {
                GlobalClass.printLog(getActivity(), "----if--paymentAfter: " + paymentAfter);
                //... commented to change due date of payment
//                paymentReminderList.get(i).setPaymentAfter(String.valueOf(paymentAfter + 1));
                paymentReminderList.get(i).setPaymentAfter(currentDate2);
                paymentReminderListToShow.add(paymentReminderList.get(i));
            } else {
//                If Payment Date is not with in the Range of two days then remove data from list
                paymentReminderList.remove(i);
            }
        }

        if (paymentReminderListToShow.size() == 0) {
            return;
        }

        ArrayList<String> entry_id = new ArrayList<String>(), member_id = new ArrayList<String>(), film_tv_name = new ArrayList<String>(), paymentAfter = new ArrayList<String>(), shoot_date = new ArrayList<String>(), rateType = new ArrayList<String>(), rate = new ArrayList<String>(), prodHouse = new ArrayList<>(), prodName = new ArrayList<>();

        GlobalClass.printLog(getActivity(), "paymentReminderListToShow.size : " + paymentReminderListToShow.size());

        for (PaymentReminderObject paymentReminderObject : paymentReminderListToShow) {
            entry_id.add(paymentReminderObject.getEntry_id());
            member_id.add(paymentReminderObject.getMember_id());
            film_tv_name.add(paymentReminderObject.getFilm_tv_name());
            paymentAfter.add(paymentReminderObject.getPaymentAfter());
            shoot_date.add(paymentReminderObject.getShoot_date());
            rateType.add(paymentReminderObject.getRateType());
            rate.add(paymentReminderObject.getRate());
            prodHouse.add(paymentReminderObject.getProdHouse());
            prodName.add(paymentReminderObject.getProducerName());
        }

        PaymentAfterList_Dialog_Class dialog = new PaymentAfterList_Dialog_Class();
        Bundle args = new Bundle();
        args.putStringArrayList("entry_id", entry_id);
        args.putStringArrayList("member_id", member_id);
        args.putStringArrayList("film_tv_name", film_tv_name);
        args.putStringArrayList("paymentAfter", paymentAfter);
        args.putStringArrayList("shoot_date", shoot_date);
        args.putStringArrayList("rateType", rateType);
        args.putStringArrayList("rate", rate);
        args.putStringArrayList("prodHouse", prodHouse);
        args.putStringArrayList("prodName", prodName);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), "tag");
    }//showPaymentReminder

    public void CheckOffLineCallinPackup() {
        Boolean upload = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Check 1
        cursor = sqLiteDatabaseRead.rawQuery("select * from tbScheduleDetails where ServerEntryStatus='no'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceTbSchedule", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OfflineTbSchedule.class));
            }
        }

        //Check 2
        cursor = sqLiteDatabaseRead.rawQuery("select * from tbScheduleDetails where ServerEntryStatus='update'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceTbSchedule", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OfflineTbSchedule.class));
            }
        }

        //Check 3
        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_diary_entry where ServerEntryStatus='no'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceTbDiaryEntry", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OfflineTbDiaryEntry.class));
            }
        }

        //Check 4
        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_diary_Callin where Status='Pending'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceCallinPackup", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OffLineCallinPackup.class));
            }
        }

        //Check 5
        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_diary_Packup where Status='Pending'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceCallinPackup", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OffLineCallinPackup.class));
            }
        }

        //check 6
        cursor = sqLiteDatabaseRead.rawQuery("select * from tbAuthorise where severStatus='no'", null);
        if (cursor.moveToFirst()) {
            if (!preferences.getString("ServiceTbDiaryEntry", "OFF").equals("ON")) {
                upload = true;
                getActivity().startService(new Intent(getActivity(), BackgroundService_OfflineTbDiaryEntry.class));
            }
        }

            /*if(upload){
                showToast("Uploading CINTAA Record to Server..", Toast.LENGTH_LONG);
            }*/


        //Check Send Mails
        cursor = sqLiteDatabaseRead.rawQuery("select entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName from tbAuthorise where severStatus='sendMail'", null);
        if (cursor.moveToFirst() && cursor != null) {
            do {
                try {
                    String entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName;
                    entryId = cursor.getString(0);
                    member_id = cursor.getString(1);
                    name = cursor.getString(2);
                    number = cursor.getString(3);
                    sign = cursor.getString(4);
                    encodedString = cursor.getString(5);
                    signDate = cursor.getString(6);
                    email = cursor.getString(7);
                    prodHouse = cursor.getString(8);
                    prodName = cursor.getString(9);
                    if (Config.isNetworkConnected(getActivity())) {
                        sendMailer(entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName);
                    }
                } catch (Exception e) {
                }
            } while (cursor.moveToNext() && cursor != null);
        }

        if (cursor != null)
            cursor.close();

    }//CheckOffLineCallinPackup

    public void share_via_app() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("vnd.android-dir/mms-sms");
        //sendIntent.putExtra("address", phoneNumber);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "CINTAA APP Download at http://www.ic_youtube.com");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void showprogressdialog() {
        pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.downloading_required_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private void showReminder() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);

        client.post(Config.URLselectRegisterDate, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("RegisterDate");
                    if (jsonArray.length() > 0) {
                        List<String> notes = new ArrayList<String>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String date_registered = j1.getString("date_registered");

                            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

                            Date start = new Date();
                            Calendar cal = Calendar.getInstance();
                            String currentDate = sdfDate.format(cal.getTime());
                            Date currDate = new Date();


                            try {

                                currDate = sdfDate.parse(currentDate);
                                start = sdfDate.parse(date_registered);

                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }

                            long diff = currDate.getTime() - start.getTime();
                            int UsedNumberOfDays = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);


                            if (UsedNumberOfDays <= 365 && UsedNumberOfDays >= 350) {
                                // notes.add(notification);
                                //Toast.makeText(getActivity(),"notification: "+i,Toast.LENGTH_LONG).show();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy
                                Calendar today = Calendar.getInstance();
                                today.setTime(new Date()); // Now use today date.
                                int differ = 365 - UsedNumberOfDays;
                                today.add(Calendar.DATE, differ); // Adding 5 days
                                String date = sdf.format(today.getTime());

                                showReminderDialog(date, differ);

                            }
                        }
                        // showNotificationDialog(notes);
                    }
                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//showReminder

    private void CheckSession() {
        GlobalClass.printLog(getActivity(),"-method called -CheckSession------");

        preferences.edit().remove("app_reminder").commit();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);

        client.post(Config.URLselectUserSession, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("Session_Check");
                    JSONArray jsonArray_appVersion = response.getJSONArray("App_version");

                    if (jsonArray.length() > 0 && jsonArray_appVersion.length() > 0) {

                        JSONObject jsonObject = jsonArray_appVersion.getJSONObject(0);
                        String app_ver_nm = jsonObject.getString("versionName");
                        String app_ver_code = jsonObject.getString("versionCode");

                        //versionName, versionCode from
                        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_appversion set versionName='" + app_ver_nm + "', versionCode='" + app_ver_code + "';");
                        int n = sqLiteStatement.executeUpdateDelete();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String Session_value = j1.getString("user_session");

                            if (Session_value.equals("OFF")) {
                                SharedPreferences preferences;
                                preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.commit();

                                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbFirstLogin set " +
                                        "member_id='x0x', isFirstLogin='default'");
                                long result = sqLiteStatement.executeUpdateDelete();

                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            } else {
                                GetShift_Time();
                                showReminder();
                                if (admin_notificaton == 1) {
                                    showNotifications();
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//CheckSession

    private void showReminderDialog(String date, int diff) {

        preferences.edit().remove("app_reminder").commit();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Reminder_date", date);
        editor.putInt("Reminder_diff", diff);
        editor.commit();
        new ReminderRenew_Dialog_Class().show(getFragmentManager(), "tag");
    }//showReminderDialog

    private void showNotifications() {
        GlobalClass.printLog(getActivity(),"-method called -showNotifications------");

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        GlobalClass.printLog(getActivity(),"-========-params------"+params);

        client.post(Config.URLselectNotifications,params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    GlobalClass.printLog(getActivity(),"-response-AAAAA------"+response);

                    JSONArray jsonArray = response.getJSONArray("AdminNotification");
//                    JSONArray jsonArray = new JSONArray(response);

                    GlobalClass.printLog(getActivity(),"-AdminNotification--------"+jsonArray);

                    if (jsonArray.length() > 0) {
                        List<String> notes = new ArrayList<String>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String notification = j1.getString("notification");
                            String start_date = j1.getString("start_date");
                            String end_date = j1.getString("end_date");

                            Date start = new Date();
                            Date end = new Date();


                            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy
                            Date now = new Date();
                            String curr = sdfDate.format(now);
                            try {
                                start = sdfDate.parse(start_date);
                                end = sdfDate.parse(end_date);
                                now = sdfDate.parse(curr);
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }


                            if ((start.equals(now) || start.before(now)) && (end.equals(now) || end.after(now))) {
                                notes.add(notification);
                                //Toast.makeText(getActivity(),"notification: "+i,Toast.LENGTH_LONG).show();
                            }
                        }
                        showNotificationDialog(notes);

                    }
                    //displaydata();
                } catch (JSONException e) {
                    GlobalClass.printLog(getActivity(),"-exception==in catch=======------"+e.toString());

                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {

                GlobalClass.printLog(getActivity(),"----exception in notification-----"+t.toString());
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//showNotifications

    private void GetShift_Time() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        client.post(Config.URLShiftTime, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    List<String> shift_start = new ArrayList<String>();
                    List<String> producerName = new ArrayList<String>();
                    List<String> productionHouseName = new ArrayList<String>();
                    List<String> houseEmail = new ArrayList<String>();
                    String fees = "300";
                    JSONArray jsonArray = response.getJSONArray("shift_time");


                    //.. this for loop will insert shift data to database from service side,
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject j1 = jsonArray.getJSONObject(i);
//                        String start = j1.getString("shift_Start");
//                        String end = j1.getString("shift_end");
//                        fees = j1.getString("fees");
//
//                        String shift = start + " to " + end;
//                        shift_start.add(shift);
//                    }
                    //.. This is static values for shit timing
                    shift_start.add("7:00 AM");
                    shift_start.add("9:00 AM");
                    shift_start.add("2:00 PM");
                    shift_start.add("7:00 PM");
                    shift_start.add("9:00 PM");
                    JSONArray jsonArray2 = response.getJSONArray("production_data");
                    for (int i = 0; i < jsonArray2.length(); i++) {
                        JSONObject j1 = jsonArray2.getJSONObject(i);
                        String house_name = j1.getString("house_name");
                        String producer_name = j1.getString("producer_name");
                        String house_email = j1.getString("house_email");
                        producerName.add(producer_name);
                        productionHouseName.add(house_name);
                        houseEmail.add(house_email);
                    }

                    InsertShift_SQLite(shift_start, fees, productionHouseName, producerName, houseEmail);


                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
            }
        });
    }//GetShift_Time

    private void InsertShift_SQLite(List<String> shift_start, String conveyance, List<String> productionHouseName, List<String> producerName, List<String> houseEmail) {
        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_shift_time");
        int res = sqLiteStatement.executeUpdateDelete();
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbConveyance");
        res = sqLiteStatement.executeUpdateDelete();
        //.. commented until api wont give me all production house details
//        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbProductionHouse");
//        res = sqLiteStatement.executeUpdateDelete();

        //Insert new Data
        //.. shift data inset into database
        for (int i = 0; i < shift_start.size(); i++) {
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_shift_time values(?)");
            sqLiteStatement.bindString(1, shift_start.get(i));
            long result = sqLiteStatement.executeInsert();
        }

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbConveyance values(?)");
        sqLiteStatement.bindString(1, conveyance);
        long result = sqLiteStatement.executeInsert();

        for (int i = 0; i < productionHouseName.size(); i++) {
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbProductionHouse values(?,?,?)");
            sqLiteStatement.bindString(1, productionHouseName.get(i));
            sqLiteStatement.bindString(2, producerName.get(i));
            sqLiteStatement.bindString(3, houseEmail.get(i));
            long r = sqLiteStatement.executeInsert();
        }

    }//InsertShift_SQLite

    private void showNotificationDialog(List<String> notes) {
        preferences.edit().remove("admin_notificaton").commit();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("Notification_Size", notes.size());
        for (int s = 0; s < notes.size(); s++) {
            editor.putString("AdminNotification_" + s, notes.get(s));
        }
        editor.commit();

        try {
            if (notes.size() > 0) {
                new AdminNotification_Dialog_Class().show(getFragmentManager(), "tag");
            }
        } catch (Exception e) {
            Log.e("Catch: ", e.toString());
        }

    }//showNotificationDialog

    private void initialise(View view) {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");

        autotext = (AutoCompleteTextView) view.findViewById(R.id.autosearch);
        autotext.setTypeface(font);
        tvDashCompleted = (TextView) view.findViewById(R.id.tvDashCompleted);
        tvDashCompleted.setTypeface(font);
        tvDashDiaryEntry = (TextView) view.findViewById(R.id.tvDashDiaryEntry);
        tvDashDiaryEntry.setTypeface(font);
        tvdashSchedule = (TextView) view.findViewById(R.id.tvdashSchedule);
        tvdashSchedule.setTypeface(font);
        llDashCompleted = (LinearLayout) view.findViewById(R.id.llDashCompleted);
        llDashDiaryEntry = (LinearLayout) view.findViewById(R.id.llDashDiaryEntry);
        lldashSchedule = (LinearLayout) view.findViewById(R.id.lldashSchedule);
        ivDashCompleted = (ImageView) view.findViewById(R.id.ivDashCompleted);
        ivDashDiaryEntry = (ImageView) view.findViewById(R.id.ivDashDiaryEntry);
        ivdashSchedule = (ImageView) view.findViewById(R.id.ivdashSchedule);
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        //  Toast.makeText(getActivity(), "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2),
        //  Toast.LENGTH_LONG).show();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        // Log.d("AutocompleteContacts", "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2));


        if (arg0.getItemAtPosition(arg2).equals("Instructions")) {
            Intent i = new Intent(getActivity(), WebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // String filepath=((result.get(position).getFilePath()));
            String servername = Config.URLInstructionDocx;
            String url = servername.trim();
            i.putExtra("url", url);
            getActivity().startActivity(i);
            getActivity().finish();
        } else if (arg0.getItemAtPosition(arg2).equals("Diary Entry")) {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new DiaryentryFragment()).addToBackStack("Tag").commit();
            //  HomeActivity.appCompatActivity.getSupportActionBar().setTitle("Home");
            String actionBarTitle = "Diary Entry";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
        } else if (arg0.getItemAtPosition(arg2).equals("Profile")) {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new MyProfileFragment()).addToBackStack("Tag").commit();
            //  HomeActivity.appCompatActivity.getSupportActionBar().setTitle("Home");
            String actionBarTitle = "My Profile";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
        } else if (arg0.getItemAtPosition(arg2).equals("Calendar")) {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new CalendarFragment()).addToBackStack("Tag").commit();
            //  HomeActivity.appCompatActivity.getSupportActionBar().setTitle("Home");
            String actionBarTitle = getString(R.string.schedule_entry);
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
        } else if (arg0.getItemAtPosition(arg2).equals("Schedule")) {
            //getFragmentManager().beginTransaction().replace(R.id.content_frame, new ScheduleFragment()).addToBackStack("Tag").commit();
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new CompletedEventsFragment()).addToBackStack("Tag").commit();
            //  HomeActivity.appCompatActivity.getSupportActionBar().setTitle("Home");
            String actionBarTitle = "Schedule";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
        } else if (arg0.getItemAtPosition(arg2).equals("MoU")) {
            Intent i = new Intent(getActivity(), WebViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // String filepath=((result.get(position).getFilePath()));
            String servername = Config.URLMouDocx;
            String url = servername.trim();
            i.putExtra("url", url);
            getActivity().startActivity(i);
            getActivity().finish();
        } else if (arg0.getItemAtPosition(arg2).equals("Share App")) {
            String actionBarTitle = "Dashboard";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
            share_via_app();
        } else if (arg0.getItemAtPosition(arg2).equals("Settings")) {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingFragment()).addToBackStack("Tag").commit();
            //  HomeActivity.appCompatActivity.getSupportActionBar().setTitle("Home");
            String actionBarTitle = "Setting";
            SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
            ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            HomeActivity.appCompatActivity.getSupportActionBar().setTitle(ssb);
        }

    }

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    private long CalculateSecondsFromCurrentDate(String dateHours) {

        long seconds = 0;
        Date now, eventDate, currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
            eventDate = sdfformat.parse(dateHours);

            //in milliseconds
            long diff = eventDate.getTime() - currentDate.getTime();

            long diffSeconds = diff / 1000;
            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);

            long on_head = diffSeconds + diffMinutes + diffHours + diffDays;
            String alert = "00:00";
            cursor2 = sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings", null);
            if (cursor2.moveToFirst()) {
                do {
                    alert = cursor2.getString(0);
                } while (cursor2.moveToNext());
            }

            String[] units = alert.split(":"); //will break the string up into an array
            long hours = Integer.parseInt(units[0]); //hours element
            long minutes = Integer.parseInt(units[1]); //minutes element
            long before_head = 60 * 60 * hours + minutes * 60; //add up our values

            before_head = on_head - before_head;

            if (before_head > 0) {
                seconds = before_head;
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            // this gets called even if there is an exception somewhere above
            if (cursor2 != null)
                cursor2.close();
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate

    public int startAlert(long seconds, String entry_id) {

        String check_alert_setting = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                check_alert_setting = cursor.getString(0);
            } while (cursor.moveToNext());
        }


        int requestCode = (int) (new Date().getTime() / 1000);
        // If alerts are ON in setting menu then only set Alarm for the Event
        if (check_alert_setting.equals("ON") && seconds > 0) {
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
//            intent.putExtra("entry_ids",entry_id);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            // alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds * 1000), pendingIntent);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (seconds * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }

        return requestCode;
    }//startAlert

    private void SetAlertOFF(String entryID) {
        int requestCode;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where entry_id=?", new String[]{entryID});
            if (cursor.moveToFirst()) {
                requestCode = cursor.getInt(0);
                Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//SetAlertOFF

    private String GetAddress(String callin_lat, String callin_lng) throws IOException {
        double MyLat = Double.parseDouble(callin_lat), MyLong = Double.parseDouble(callin_lng);
        String address = "GPS Fault";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        ;
        List<Address> addresses;
        String address1, city, country, state, postalCode, knownName;
        addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
        if (addresses.size() > 0) {
            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            address = address1 + "; " + city + "; " + state + "; " + country;
        }
        return address;
    }//GetAddress

    private void sendMailer(String entryId, String member_id, String name, String number, String sign, String encodedString, String signDate, String email, String prodHouse, String prodName) {
        //Send Mail
        Cursor cur = null;
        String memberEmail = null, ArtistName = null, subject;
        String film_tv_name = null, shoot_date = null, shoot_location = null, characterName = null, shift_time = null, call_time = null, rate = null, rateType = null, paymentAfter = null, remark = null;
        String callin = null, packup = null, conveyance = null, lat, lng;
        try {
            cur = sqLiteDatabaseRead.rawQuery("select film_tv_name,shoot_date,shoot_location,characterName,shift_time,call_time," +
                    "rate,rateType,paymentAfter,remark,lat,lng from sqtb_diary_entry where entry_id=?", new String[]{entryId});
            if (cur.moveToFirst()) {
                film_tv_name = cur.getString(0);
                shoot_date = cur.getString(1);
                shoot_location = cur.getString(2);
                characterName = cur.getString(3);
                shift_time = cur.getString(4);
                call_time = cur.getString(5);
                rate = cur.getString(6);
                rateType = cur.getString(7);
                paymentAfter = cur.getString(8);
                remark = cur.getString(9);
                lat = cur.getString(10);
                lng = cur.getString(11);

                if (shoot_location.trim().length() == 0) {
                    shoot_location = GetAddress(lat, lng);
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set shoot_location = '" + shoot_location + "' " +
                            "where entry_id='" + entryId + "'; ");
                    long result = sqLiteStatement.executeUpdateDelete();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (cur != null)
                cur.close();
        }
        try {
            cur = sqLiteDatabaseRead.rawQuery("select callin_time from sqtb_diary_Callin where entry_id=?", new String[]{entryId});
            if (cur.moveToFirst()) {
                callin = cur.getString(0);
            }
            cur = sqLiteDatabaseRead.rawQuery("select packup_time from sqtb_diary_Packup where entry_id=?", new String[]{entryId});
            if (cur.moveToFirst()) {
                packup = cur.getString(0);
            }
            cur = sqLiteDatabaseRead.rawQuery("select conveyance from tbConveyance", null);
            if (cur.moveToFirst()) {
                conveyance = cur.getString(0);
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        try {
            cur = sqLiteDatabaseRead.rawQuery("select email,name from sqtb_UserProfile", null);
            if (cur.moveToFirst()) {
                memberEmail = cur.getString(0);
                ArtistName = cur.getString(1);
            }
        } finally {
            if (cur != null)
                cur.close();
        }

        //Send Email to Production
        String signImageUrl = sign;
        String Mname = "CINTAA Diary";
        String username = Config.cintaaMail;
        String password = Config.cintaaMailPwd;

        subject = "Work Schedule Report - " + ArtistName;
        sendMail(Mname, username, password, email, subject, member_id, ArtistName, film_tv_name, prodHouse, prodName, shoot_date, shoot_location, characterName, shift_time, call_time, rate, rateType, paymentAfter, remark, callin, packup, name, number, sign, signDate, conveyance);

        if (memberEmail != null) {
            sendMail(Mname, username, password, memberEmail, subject, member_id, ArtistName, film_tv_name, prodHouse, prodName, shoot_date, shoot_location, characterName, shift_time, call_time, rate, rateType, paymentAfter, remark, callin, packup, name, number, sign, signDate, conveyance);
        }

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbAuthorise set severStatus='done' where entryId = '" + entryId + "'");
        long result = sqLiteStatement.executeUpdateDelete();
    }//sendMailer

    private void sendMail(String Mailname, String username, String password, String email, String subject, String member_id, String ArtistName, String film_tv_name, String prodHouse, String prodName, String shoot_date, String shoot_location, String characterName, String shift_time, String call_time, String rate, String rateType, String paymentAfter, String remark, String callin, String packup, String name, String number, String sign, String signDate, String conveyance) {
        getSignIn(Mailname, username, password, email, subject, member_id, ArtistName, film_tv_name, prodHouse, prodName, shoot_date, shoot_location, characterName, shift_time, call_time, rate, rateType, paymentAfter, remark, callin, packup, name, number, sign, signDate, conveyance);
    }

    private void getSignIn(String Mailname, String username, String password, String email, String subject, String member_id, String ArtistName, String film_tv_name, String prodHouse, String prodName, String shoot_date, String shoot_location, String characterName, String shift_time, String call_time, String rate, String rateType, String paymentAfter, String remark, String callin, String packup, String name, String number, String sign, String signDate, String conveyance) {

        if (!Config.isNetworkConnected(getActivity()))  //if connection available
        {
            //showToast("Please Check Your Internet Connection", Toast.LENGTH_SHORT);
        } else {
            String message;
            if (rate.length() > 0) {
                message = "" +
                        "<table  border=\"1\" cellpadding=\"5\">" +
                        "<tr><th colspan=\"2\" bgcolor=\"#8f8f8f\">Work Schedule Report</th></tr>" +
                        "<tr>" +
                        "<td>CINTAA Member ID</td>  <td>" + member_id + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Name</td>  <td>" + ArtistName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Film/TV Serial</td>  <td>" + film_tv_name + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Production House</td>  <td>" + prodHouse + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Producer Name</td>  <td>" + prodName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Date of Shoot</td>  <td>" + shoot_date + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Address of Shoot</td>  <td>" + shoot_location + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Character Name</td>  <td>" + characterName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Shift Time</td>  <td>" + shift_time + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Call Time</td>  <td>" + call_time + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Rate</td>  <td>" + rate + " " + rateType + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Due After</td>  <td>" + paymentAfter + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Remark</td>  <td>" + remark + "</td>" +
                        "</tr>";
                try {
                    if (Double.parseDouble(rate) <= Config.conveyanceLimit) {
                        message = message +
                                "<tr>" +
                                "<td>Conveyance Fees</td>  <td>" + conveyance + "</td>" +
                                "</tr>";
                    }
                } catch (NumberFormatException e) {
                }

                message = message +
                        "<tr>" +
                        "<td>In Time</td>  <td>" + callin + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Pack-up Time</td>  <td>" + packup + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Name</td>  <td>" + name + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Number</td>  <td>" + number + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Sign</td>  <td><img src=\"" + Config.imgPngUrl + sign + ".png\" alt=\"Sign\" height=\"80\"></td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Date of Sign</td>  <td>" + signDate + "</td>" +
                        "</tr>" +
                        "</table> <br/>" +
                        "This email message is intended only for CINTAA members and their employers and does not tantamount to spamming as it contains information that is confidential. If you are not the intended recipient, any disclosure, distribution or other use of this email message is prohibited. <br/><br/>" +
                        "<img src=\"" + Config.mailLogoUrl + "\" alt=\"Logo\" height=\"40\"> <br/><br/>" +
                        "<small>Cine & TV Artistes' Association (CINTAA)</small><br/>" +
                        "<small>221, Kartik Complex, 2nd Flr.,</small><br/>" +
                        "<small>Opp.Laxmi Ind. Estate, New Link Rd.,</small><br/>" +
                        "<small>Andheri (W), Mumbai-400 053.</small><br/>" +
                        "<small>Tel.# 26730511/10</small><br/>";
            } else {
                //<img src=""+Config.mailLogoUrl+"" alt="Logo" height="80"> <br/><br/>
                //"<b>Cine & TV Artistes' Association</b> <br/><br/>" +
                message = "" +
                        "<table  border=\"1\" cellpadding=\"5\">" +
                        "<tr><th colspan=\"2\" bgcolor=\"#8f8f8f\">Work Schedule Report</th></tr>" +
                        "<tr>" +
                        "<td>CINTAA Member ID</td>  <td>" + member_id + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Name</td>  <td>" + ArtistName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Film/TV Serial</td>  <td>" + film_tv_name + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Production House</td>  <td>" + prodHouse + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Producer Name</td>  <td>" + prodName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Date of Shoot</td>  <td>" + shoot_date + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Address of Shoot</td>  <td>" + shoot_location + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Character Name</td>  <td>" + characterName + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Shift Time</td>  <td>" + shift_time + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Call Time</td>  <td>" + call_time + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Rate</td>  <td> NA </td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Due After</td>  <td>" + paymentAfter + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Remark</td>  <td>" + remark + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>In Time</td>  <td>" + callin + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Pack-up Time</td>  <td>" + packup + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Name</td>  <td>" + name + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Number</td>  <td>" + number + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Authority Sign</td>  <td><img src=\"" + Config.imgPngUrl + sign + ".png\" alt=\"Sign\" height=\"80\"></td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Date of Sign</td>  <td>" + signDate + "</td>" +
                        "</tr>" +
                        "</table> <br/>" +
                        "This email message is intended only for CINTAA members and their employers and does not tantamount to spamming as it contains information that is confidential. If you are not the intended recipient, any disclosure, distribution or other use of this email message is prohibited. <br/><br/>" +
                        "<img src=\"" + Config.mailLogoUrl + "\" alt=\"Logo\" height=\"40\"> <br/><br/>" +
                        "<small>Cine & TV Artistes' Association (CINTAA)</small><br/>" +
                        "<small>221, Kartik Complex, 2nd Flr.,</small><br/>" +
                        "<small>Opp.Laxmi Ind. Estate, New Link Rd.,</small><br/>" +
                        "<small>Andheri (W), Mumbai-400 053.</small><br/>" +
                        "<small>Tel.# 26730511/13</small><br/>";
            }
            sendMail(email, subject, message, Mailname, username, password);

        }


    }

    private void sendMail(String email, String subject, String messageBody, String name, String username, String password) {
        Session session = createSessionObject(username, password);

        try {
            Message message = createMessage(email, subject, messageBody,
                    session, username, name);
            new HomeFragment.SendMailTask(getActivity()).execute(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject,
                                  String messageBody, Session session, String username, String name) throws MessagingException,
            UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                email, email));
        message.setSubject(subject);
        message.setContent(messageBody, "text/html");
        return message;
    }

    private Session createSessionObject(final String username, final String password) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", Config.smtpHost);
        properties.put("mail.smtp.port", Config.smtpPort);

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }

    public class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        Activity ctx;

        public SendMailTask(Activity activity) {
            ctx = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                    }
                });
            } catch (final MessagingException e) {
                e.printStackTrace();
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                    }
                });
            }
            return null;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        homeFragment = this;

        if (DiaryentryFragment.getFragment() != null)
            DiaryentryFragment.getFragment().hideTip();

        if (CalendarFragment.getFragment() != null)
            CalendarFragment.getFragment().hideTip();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:
                showTip1();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            hideTip();
        }
    };

    public void hideTip() {
        if (mTipFocusView != null && mTipFocusView.isShown())
            mTipFocusView.hide();
    }

    private void showTip1() {
        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(View view) {
                        view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTipFocusView.hide();
                                showTip2();
                            }
                        });
                        view.findViewById(R.id.tvWelcome).setVisibility(View.VISIBLE);

                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                        tvTip.setText(R.string.tip_msg_welcome);

                        view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                    }
                })
                .closeOnTouch(false)
                .build();
        mTipFocusView.show();
    }

    private void showTip2() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showMenuTip1();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_msg_settting_menu_tip);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showMenuTip1() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_menu_item1, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip3();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_settting_menu_item);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    } //Tip2

    private void showTip3() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    Log.e("--------Perform-----", "CLICK");
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip4();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

//                                    View view2 = (View) view.findViewById(R.id.view);
//                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_msg_drawer_menu);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(HomeActivity.ivMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    }

    private void showTip4() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    Log.e("--------Perform-----", "CLICK");
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_schedule_entry, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip5();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

//                                    View view2 = (View) view.findViewById(R.id.view);
//                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_schedule_entry_on_main_fragment);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(lldashSchedule)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    }

    private void showTip5() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_diary_entry, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip6();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

//                                    View view2 = (View) view.findViewById(R.id.view);
//                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_diary_entry_msg_at_main_fragment);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(lldashSchedule)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);

        HomeActivity.ivMenu.performClick();

    }

    private void showTip6() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_completed_shoot, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip4();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_completed_shoot_at_main_fragment);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(lldashSchedule)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    }


}