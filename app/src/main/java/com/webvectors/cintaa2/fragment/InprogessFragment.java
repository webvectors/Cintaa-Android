package com.webvectors.cintaa2.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.adapter.ListViewAdapters;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class InprogessFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private ArrayList<HashMap<String, String>> list;
    List<String> list_entry_id,film_nm;
    SharedPreferences preferences;
    ProgressDialog pd;


    String member_id;
    TextView tvdateheader,tvprodhouheader,tvlocheader,tvshiftheader,tveditheader;
    ListView listView;
    RadioGroup rg_searchCategory;
    RadioButton radioDate,radioPHouse,radiofilm;
    AutoCompleteTextView autosearch;
    ImageView ivsearch,ivsearchclear;
    ListViewAdapters adapter;
    View viewSearch;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    private ArrayAdapter<String> autoComplete_adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view= inflater.inflate(R.layout.fragment_inprogess, container, false);

        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id=  preferences.getString("member_id","abc");
        list=new ArrayList<HashMap<String,String>>();
        list_entry_id = new ArrayList<>();
        film_nm = new ArrayList<>();
        listView=(ListView)view.findViewById(R.id.listView);

       initialise(view);

        if(savedInstanceState==null){

            selectdata(view);
        }else {


            list = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("list");
            if(list.size()>0){
                list_entry_id = savedInstanceState.getStringArrayList("id");
                film_nm =  savedInstanceState.getStringArrayList("film_name");
                adapter = new ListViewAdapters(getActivity(), list,list_entry_id);
                listView.setAdapter(adapter);

                String date[] = new String[list.size()];
                String house[] = new String[list.size()];
                String film[] = new String[film_nm.size()];
                for(int i=0;i<list.size();i++){
                    HashMap<String, String> map=list.get(i);
                    String s_date = map.get("shoot_date");
                    date[i] = s_date;
                    String p_house = map.get("house_name");
                    house[i] = p_house;
                    film[i] = film_nm.get(i);
                }
                if(radioDate.isChecked())
                    AutoCompleteSearch(date);
                else if(radioPHouse.isChecked())
                    AutoCompleteSearch(house);
                else if(radiofilm.isChecked())
                    AutoCompleteSearch(film);

            }else {
                selectdata(view);
            }

        }


        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        rg_searchCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Check which radio button was clicked
                switch(checkedId) {
                    case R.id.radioDate:
                        //    radioDate.setChecked(true);
                        String date[] = new String[list.size()];
                        for(int i=0;i<list.size();i++){
                            HashMap<String, String> map=list.get(i);
                            String s_date = map.get("shoot_date");
                            date[i] = s_date;
                        }
                        AutoCompleteSearch(date);
                            autosearch.setHint("Search by Date");
                        break;
                    case R.id.radioPHouse:
                         //   radioPHouse.setChecked(true);
                        String house[] = new String[list.size()];
                        for(int i=0;i<list.size();i++){
                            HashMap<String, String> map=list.get(i);
                            String p_house = map.get("house_name");
                            house[i] = p_house;
                        }
                        AutoCompleteSearch(house);

                            autosearch.setHint("Search by Prod. House");
                        break;
                    case R.id.radiofilm:
                         //   radiofilm.setChecked(true);
                        String film[] = new String[film_nm.size()];
                        for(int i=0;i<film_nm.size();i++){
                            film[i] = film_nm.get(i);
                        }
                        AutoCompleteSearch(film);

                            autosearch.setHint("Search by Film/TV Serial");
                        break;
                }
            }
        });



            autosearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(radioDate.isChecked()){
                    ShowTimePicker();
                    }
                }
            });

           /* autosearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(radioDate.isChecked()) {
                        ShowTimePicker();
                    }
                }
            });*/
        ivsearchclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                autosearch.setText("");

                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_inprogress",null);
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    List<String> ll_entry_id = new ArrayList<>();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        ll_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapters adapter = new ListViewAdapters(getActivity(), ll,ll_entry_id);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
                    showToast("No Result Found",Toast.LENGTH_LONG);
                }

                ivsearchclear.setVisibility(View.GONE);


            }
        });



        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Search();
            }
        });


        return view;
    }//OnCreate

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerd= new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = ""+mnth;
            if(mnth<10){
                month = "0"+(mnth);
            }

            String datee = ""+dayOfMonth;
            if(dayOfMonth<10){
                datee="0"+dayOfMonth;
            }

            String date = datee+"/"+month+"/"+year;
            autosearch.setText(date);
            ivsearchclear.setVisibility(View.VISIBLE);
            viewSearch.setVisibility(View.VISIBLE);
        }
    };

    private void ShowTimePicker(){
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerd,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)

        );

        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgColor));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowTimePicker

    @Override
    public void onSaveInstanceState(Bundle outState) {


        outState.putSerializable("list", list);
        outState.putStringArrayList("id", (ArrayList<String>) list_entry_id);
        outState.putStringArrayList("film_name", (ArrayList<String>) film_nm);
        super.onSaveInstanceState(outState);

    }

    private void initialise(View view){
        Typeface customfont=Typeface.createFromAsset(getActivity().getAssets(),"lane.ttf");
        tvdateheader=(TextView)view.findViewById(R.id.tvDateheader);
        tvdateheader.setTypeface(customfont);
        tvprodhouheader=(TextView)view.findViewById(R.id.tvprodhouheader);
        tvprodhouheader.setTypeface(customfont);
        tvlocheader=(TextView)view.findViewById(R.id.tvlocheader);
        tvlocheader.setTypeface(customfont);
        tvshiftheader=(TextView)view.findViewById(R.id.tvshiftheader);
        tvshiftheader.setTypeface(customfont);
        tveditheader=(TextView)view.findViewById(R.id.tveditheader);
        tveditheader.setTypeface(customfont);
        radioDate = (RadioButton)view.findViewById(R.id.radioDate);
        radioDate.setTypeface(customfont);
        radioPHouse = (RadioButton)view.findViewById(R.id.radioPHouse);
        radioPHouse.setTypeface(customfont);
        radiofilm = (RadioButton)view.findViewById(R.id.radiofilm);
        radiofilm.setTypeface(customfont);
        autosearch = (AutoCompleteTextView) view.findViewById(R.id.autosearch);
        autosearch.setTypeface(customfont);
        ivsearch = (ImageView) view.findViewById(R.id.ivsearch);
        ivsearchclear = (ImageView) view.findViewById(R.id.ivsearchclear);
        ivsearchclear.setVisibility(View.GONE);
        viewSearch = (View)view.findViewById(R.id.viewSearch);
        viewSearch.setVisibility(View.GONE);
        rg_searchCategory = (RadioGroup) view.findViewById(R.id.rg_searchCategory);

        autosearch.requestFocus();


    }//initialise



    public void selectdata(View view)
    {

        pd = new ProgressDialog(getActivity());
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLinprogrescheduleentry, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray= response.getJSONArray("InProgressTable");
                    if(jsonArray.length()>0) {
                        String shootDate[] = new String[jsonArray.length()];
                        list.clear();
                        list_entry_id.clear();
                        film_nm.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String shoot_date = j1.getString("shoot_date");
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            String entry_id = j1.getString("entry_id");
                            String film_name = j1.getString("film_tv_name");
                            if(!alies_name.equals("null")){
                                house_name = house_name+"/"+alies_name;
                            }
                            String shoot_location = j1.getString("shoot_location");
                            String shift_time = j1.getString("shift_time");
                            HashMap<String, String> temp1 = new HashMap<String, String>();
                            temp1.put("shoot_date", shoot_date);
                            temp1.put("house_name", house_name);
                            temp1.put("shoot_location", shoot_location);
                            temp1.put("shift_time", shift_time);

                            shootDate[i] = shoot_date;
                            list.add(temp1);
                            list_entry_id.add(entry_id);
                            film_nm.add(film_name);
                        }
                        InsertSQLite(list,list_entry_id,film_nm);
                        adapter = new ListViewAdapters(getActivity(), list,list_entry_id);
                        listView.setAdapter(adapter);

                        AutoCompleteSearch(shootDate);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
            }
        });
        pd.dismiss();

    }//SelectData

    private void AutoCompleteSearch(String item[]){
        autoComplete_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, item);
        autosearch.setThreshold(1);
        //Set adapter to AutoCompleteTextView
        autosearch.setAdapter(autoComplete_adapter);
        autosearch.setOnItemSelectedListener(this);
        autosearch.setOnItemClickListener(this);
    }//AutoCompleteSearch

    private void InsertSQLite(ArrayList<HashMap<String, String>> list,List<String> list_entry_id,List<String> list_film_nm){
      /*  cursor=sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings",null);
        if(cursor.moveToFirst())
        {
            do {
               String alert_flag = cursor.getString(0);
            }while (cursor.moveToNext());
        }

       */
        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_ScheduleFilterData_inprogress");
        int res = sqLiteStatement.executeUpdateDelete();

        for(int i=0; i<list.size();i++){
            HashMap<String, String> map=list.get(i);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_ScheduleFilterData_inprogress values(?,?,?,?,?,?,?)");
            //shoot_date, phouse, location, shift_time, film_nm, entry_id, schedule_status
            sqLiteStatement.bindString(1,map.get("shoot_date"));
            sqLiteStatement.bindString(2,map.get("house_name"));
            sqLiteStatement.bindString(3,map.get("shoot_location"));
            sqLiteStatement.bindString(4,map.get("shift_time"));
            sqLiteStatement.bindString(5,list_film_nm.get(i));
            sqLiteStatement.bindString(6,list_entry_id.get(i));
            sqLiteStatement.bindString(7,"pending");
            long result=sqLiteStatement.executeInsert();
        }//end for loop

    }//InsertSQLite

    private void Search(){

        if(autosearch.getText().toString().length()>0){
            ivsearchclear.setVisibility(View.VISIBLE);
            viewSearch.setVisibility(View.VISIBLE);
            if(radioDate.isChecked()){              //Search By Date
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_inprogress WHERE shoot_date LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    List<String> ll_entry_id = new ArrayList<>();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        ll_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapters adapter = new ListViewAdapters(getActivity(), ll,ll_entry_id);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
                    showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }else if(radioPHouse.isChecked()){      //Search By Prod. House
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_inprogress WHERE phouse LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    List<String> ll_entry_id = new ArrayList<>();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        ll_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapters adapter = new ListViewAdapters(getActivity(), ll,ll_entry_id);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
                    showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }else if(radiofilm.isChecked()){        //Search Film
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_inprogress WHERE film_nm LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    List<String> ll_entry_id = new ArrayList<>();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        ll_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapters adapter = new ListViewAdapters(getActivity(), ll,ll_entry_id);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
                    showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }
        }else {
            showToast("Nothing to Search",Toast.LENGTH_LONG);
        }

    }//Search

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(parent.getApplicationWindowToken(), 0);
        Search();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}