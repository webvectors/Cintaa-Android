package com.webvectors.cintaa2.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.Dialog_Classes.CompleteEvents_Dialog_Class;
import com.webvectors.cintaa2.adapter.ListViewAdapterCmpld;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private ArrayList<HashMap<String, String>> list;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String member_id,entry_id,callin_time,packup_time,submit_time,tvcallininstr;
    TextView tvauthorityno,tvcompldateheader,tvcomplprodheader,tvcompllocheader,tvcomplshiftheader,tvflimnm,tvheader, tvphouse, tvprodnm, tvshootdate, tvlocation, tvcharnm, tvshifttime, tvrate, tvratetype, tvduedays, tvremark,tvauthoritynm,tvcallin,tvpackup,tvauthosigndate,tvcomfilmname,tvcomprodhouname,tvcomprodname,tvcomdateofshoot,tvcomloc,tvcomcharname,tvcomshifttime,tvcomrate,tvcomdueafter,tvcomremark,tvcomcallin,tvcompckup,tvcomautname,tvcomautsign,tvcomautdate;
    String tvflimnmstr,tvheaderstr,ivauthoritysignstr, tvphousestr, tvprodnmstr, tvshootdatestr, tvlocationstr, tvcharnmstr, tvshifttimestr, tvratestr, tvratetypestr, tvduedaysstr, tvremarkstr,tvauthoritynmstr,tvauthoritysignstr,tvcallinstr,tvpackupstr,tvauthosigndatestr,tvcomfilmnamestr,tvcomprodhounamestr,tvcomprodnamestr,tvcomdateofshootstr,tvcomlocstr,tvcomcharnamestr,tvcomshifttimestr,tvcomratestr,tvcomdueafterstr,tvcomremarkstr,tvcomcallinstr,tvcompckupstr,tvcomautnamestr,tvcomautsignstr,tvcomautdatestr,tvauthoritynostr,tvcallinin;
    EditText etauthoritynm,etauthoritysign,etcallin,etpackup;
    Button btnpackup,btnpackupsubmit,btncallin,btncallinsubmit,btsubmit;
    int callin_flag = 0, packup_flag = 0, check=0;
    ListView listView;
    ImageView ivauthoritysign;
    ImageView tvauthoritysign;
    AlertDialog.Builder builder;
    List<String> list_entry_id,film_nm;
    ProgressDialog pd;
    Typeface customfont;
    RadioGroup rg_searchCategory;
    RadioButton radioDate,radioPHouse,radiofilm;
    AutoCompleteTextView autosearch;
    ImageView ivsearch,ivsearchclear;
    ListViewAdapterCmpld adapter;
    String fees,rater;
    View viewSearch;


    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    private ArrayAdapter<String> autoComplete_adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        View view= inflater.inflate(R.layout.fragment_completed, container, false);
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id=  preferences.getString("member_id","abc");


        list=new ArrayList<HashMap<String,String>>();
        list_entry_id = new ArrayList<>();
        film_nm = new ArrayList<>();

        initialise(view);
        if(savedInstanceState==null){
            selectdata();
        }else {
            list = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("list");
            if(list.size()>0){
                list_entry_id = savedInstanceState.getStringArrayList("id");
                film_nm =  savedInstanceState.getStringArrayList("film_name");
                adapter = new ListViewAdapterCmpld(getActivity(), list);
                listView.setAdapter(adapter);

                String date[] = new String[list.size()];
                String house[] = new String[list.size()];
                String film[] = new String[film_nm.size()];
                for(int i=0;i<list.size();i++){
                    HashMap<String, String> map=list.get(i);
                    String s_date = map.get("shoot_date");
                    date[i] = s_date;
                    String p_house = map.get("house_name");
                    house[i] = p_house;
                    film[i] = film_nm.get(i);
                }
                if(radioDate.isChecked())
                    AutoCompleteSearch(date);
                else if(radioPHouse.isChecked())
                    AutoCompleteSearch(house);
                else if(radiofilm.isChecked())
                    AutoCompleteSearch(film);
            }else {
                selectdata();
            }
        }
        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        rg_searchCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Check which radio button was clicked
                switch(checkedId) {
                    case R.id.radioDate:
                        //    radioDate.setChecked(true);
                        String date[] = new String[list.size()];
                        for(int i=0;i<list.size();i++){
                            HashMap<String, String> map=list.get(i);
                            String s_date = map.get("shoot_date");
                            date[i] = s_date;
                        }
                        AutoCompleteSearch(date);
                        autosearch.setHint("Search by Date");
                        break;
                    case R.id.radioPHouse:
                        //   radioPHouse.setChecked(true);
                        String house[] = new String[list.size()];
                        for(int i=0;i<list.size();i++){
                            HashMap<String, String> map=list.get(i);
                            String p_house = map.get("house_name");
                            house[i] = p_house;
                        }
                        AutoCompleteSearch(house);
                        autosearch.setHint("Search by Prod. House");
                        break;
                    case R.id.radiofilm:
                        //   radiofilm.setChecked(true);
                        String film[] = new String[film_nm.size()];
                        for(int i=0;i<film_nm.size();i++){
                            film[i] = film_nm.get(i);
                        }
                        AutoCompleteSearch(film);
                        autosearch.setHint("Search by Film/TV Serial");
                        break;
                }
            }
        });
        autosearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radioDate.isChecked()){
                    ShowTimePicker();
                }
            }
        });
        /*autosearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(radioDate.isChecked()) {
                    ShowTimePicker();
                }
            }
        });*/
        ivsearchclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                autosearch.setText("");
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed",null);
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapterCmpld adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
					showToast("No Result Found",Toast.LENGTH_LONG);
                }
                ivsearchclear.setVisibility(View.GONE);
                viewSearch.setVisibility(View.GONE);
            }
        });

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Search();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
            {
                //Toast.makeText(getContext(), ""+list_entry_id.get(position), Toast.LENGTH_SHORT).show();
                String entry_id = list_entry_id.get(position);
                displaydetails(entry_id);
            }

        });

        return view;
    }//OnCreate

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerd= new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = ""+mnth;
            if(mnth<10){
                month = "0"+(mnth);
            }

            String datee = ""+dayOfMonth;
            if(dayOfMonth<10){
                datee="0"+dayOfMonth;
            }

            String date = datee+"/"+month+"/"+year;
            autosearch.setText(date);
            ivsearchclear.setVisibility(View.VISIBLE);
            viewSearch.setVisibility(View.VISIBLE);
        }
    };

    private void ShowTimePicker(){
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerd,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowTimePicker

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putSerializable("list", list);
        outState.putStringArrayList("id", (ArrayList<String>) list_entry_id);
        outState.putStringArrayList("film_name", (ArrayList<String>) film_nm);
        super.onSaveInstanceState(outState);
    }

    private void initialise(View view){
        Typeface customfont=Typeface.createFromAsset(getActivity().getAssets(),"lane.ttf");
        listView=(ListView)view.findViewById(R.id.listCompletedShoots);
        tvcompldateheader=(TextView)view.findViewById(R.id.tvcompldateheader);
        tvcompldateheader.setTypeface(customfont);
        tvcomplprodheader=(TextView)view.findViewById(R.id.tvcomplprodhouheader);
        tvcomplprodheader.setTypeface(customfont);
        tvcompllocheader=(TextView)view.findViewById(R.id.tvcompllocheader);
        tvcompllocheader.setTypeface(customfont);
        tvcomplshiftheader=(TextView)view.findViewById(R.id.tvcomplshiftheader);
        tvcomplshiftheader.setTypeface(customfont);
        radioDate = (RadioButton)view.findViewById(R.id.radioDate);
        radioDate.setTypeface(customfont);
        radioPHouse = (RadioButton)view.findViewById(R.id.radioPHouse);
        radioPHouse.setTypeface(customfont);
        radiofilm = (RadioButton)view.findViewById(R.id.radiofilm);
        radiofilm.setTypeface(customfont);
        autosearch = (AutoCompleteTextView) view.findViewById(R.id.autosearch);
        autosearch.setTypeface(customfont);
        ivsearch = (ImageView) view.findViewById(R.id.ivsearch);
        ivsearchclear = (ImageView) view.findViewById(R.id.ivsearchclear);
        ivsearchclear.setVisibility(View.GONE);
        viewSearch = (View)view.findViewById(R.id.viewSearch);
        viewSearch.setVisibility(View.GONE);
        rg_searchCategory = (RadioGroup) view.findViewById(R.id.rg_searchCategory);

        autosearch.requestFocus();
    }//initialise

    void selectdata()
    {
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLcompletedscheduleentry, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray= response.getJSONArray("CompletedEntryTable");
                    if(jsonArray.length()>0) {
                        String shootDate[] = new String[jsonArray.length()];
                        list_entry_id.clear();
                        film_nm.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String shoot_date = j1.getString("shoot_date");
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            String entry_id = j1.getString("entry_id");
                            String film_name = j1.getString("film_tv_name");
                            if(!alies_name.equals("null")){
                                house_name = house_name+"/"+alies_name;
                            }
                            String shoot_location = j1.getString("shoot_location");
                            String shift_time = j1.getString("shift_time");
                            HashMap<String, String> temp1 = new HashMap<String, String>();
                            temp1.put("shoot_date", shoot_date);
                            temp1.put("house_name", house_name);
                            temp1.put("shoot_location", shoot_location);
                            temp1.put("shift_time", shift_time);

                            shootDate[i] = shoot_date;
                            list.add(temp1);
                            list_entry_id.add(entry_id);
                            film_nm.add(film_name);
                        }
                        InsertSQLite(list,list_entry_id,film_nm);
                        adapter = new ListViewAdapterCmpld(getActivity(), list);
                        listView.setAdapter(adapter);

                        AutoCompleteSearch(shootDate);
                    }

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
                        {
                            //Toast.makeText(getContext(), ""+list_entry_id.get(position), Toast.LENGTH_SHORT).show();
                            String entry_id = list_entry_id.get(position);
                            displaydetails(entry_id);
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
				if (pd != null) {
					if (pd.isShowing()) {
						pd.dismiss();
					}
				}
			}
            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
				showToast("could not connect", Toast.LENGTH_LONG);
				if (pd != null) {
					if (pd.isShowing()) {
						pd.dismiss();
					}
				}
            }
        });
    }

    private void InsertSQLite(ArrayList<HashMap<String, String>> list,List<String> list_entry_id,List<String> list_film_nm){

        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_ScheduleFilterData_completed");
        int res = sqLiteStatement.executeUpdateDelete();

        for(int i=0; i<list.size();i++){
            HashMap<String, String> map=list.get(i);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_ScheduleFilterData_completed values(?,?,?,?,?,?,?)");
            //shoot_date, phouse, location, shift_time, film_nm, entry_id, schedule_status
            sqLiteStatement.bindString(1,map.get("shoot_date"));
            sqLiteStatement.bindString(2,map.get("house_name"));
            sqLiteStatement.bindString(3,map.get("shoot_location"));
            sqLiteStatement.bindString(4,map.get("shift_time"));
            sqLiteStatement.bindString(5,list_film_nm.get(i));
            sqLiteStatement.bindString(6,list_entry_id.get(i));
            sqLiteStatement.bindString(7,"completed");
            long result=sqLiteStatement.executeInsert();
        }//end for loop

    }//InsertSQLite

    private void AutoCompleteSearch(String item[]){
        autoComplete_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, item);
        autosearch.setThreshold(1);
        //Set adapter to AutoCompleteTextView
        autosearch.setAdapter(autoComplete_adapter);
        autosearch.setOnItemSelectedListener(this);
        autosearch.setOnItemClickListener(this);
    }//AutoCompleteSearch

    private void showcompletedscheduledialog() {

//tvcallininstr,tvflimnmstr,ivauthoritysignstr, tvphousestr, tvprodnmstr, tvshootdatestr, tvlocationstr, tvcharnmstr, tvshifttimestr, tvratestr, tvduedaysstr, tvremarkstr,tvauthoritynmstr,tvcallinstr,tvpackupstr,tvauthosigndatestr,tvauthoritynostr

        editor=preferences.edit();
        editor.putString("tvcallininstr_cmplt",tvcallininstr);
        editor.putString("tvflimnmstr_cmplt",tvflimnmstr);
        editor.putString("ivauthoritysignstr_cmplt",ivauthoritysignstr);
        editor.putString("tvphousestr_cmplt",tvphousestr);
        editor.putString("tvprodnmstr_cmplt",tvprodnmstr);
        editor.putString("tvshootdatestr_cmplt",tvshootdatestr);
        editor.putString("tvlocationstr_cmplt",tvlocationstr);
        editor.putString("tvcharnmstr_cmplt",tvcharnmstr);
        editor.putString("tvshifttimestr_cmplt",tvshifttimestr);
        editor.putString("tvratestr_cmplt",tvratestr);
        editor.putString("tvduedaysstr_cmplt",tvduedaysstr);
        editor.putString("tvremarkstr_cmplt",tvremarkstr);
        editor.putString("tvauthoritynmstr_cmplt",tvauthoritynmstr);
        editor.putString("tvcallinstr_cmplt",tvcallinstr);
        editor.putString("tvpackupstr_cmplt",tvpackupstr);
        editor.putString("tvauthosigndatestr_cmplt",tvauthosigndatestr);
        editor.putString("tvauthoritynostr_cmplt",tvauthoritynostr);
        editor.putString("fees_cmplt",fees);
        editor.putString("rater_cmplt",rater);

        editor.commit();


        new CompleteEvents_Dialog_Class().show(getFragmentManager(), "tag");
       /* LayoutInflater li = LayoutInflater.from(getActivity());
        // final View promptsView = li.inflate(R.layout.completescheduledetail, null);
        // entry_id= sharedPreferences.getString("Completed_list_item","xcz");
        //final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(li.inflate(R.layout.completescheduledetail
                , null));
        // set prompts.xml to alertdialog builder
        // alertDialogBuilder.setView(promptsView);
        customfont = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        TextView   tvcomfilmname = (TextView) dialog.findViewById(R.id.tvcomfilmname);
        tvcomfilmname.setTypeface(customfont);
        TextView  tvcomprodhouname = (TextView) dialog.findViewById(R.id.tvcomprodhouname);
        tvcomprodhouname.setTypeface(customfont);
        TextView tvcomprodname = (TextView) dialog.findViewById(R.id.tvcomprodname);
        tvcomprodname.setTypeface(customfont);
        TextView tvcomdateofshoot = (TextView) dialog.findViewById(R.id.tvcomdtshoot);
        tvcomdateofshoot.setTypeface(customfont);
        TextView  tvcomloc = (TextView) dialog.findViewById(R.id.tvcomshootloc);
        tvcomloc.setTypeface(customfont);
        TextView  tvcomcharname = (TextView) dialog.findViewById(R.id.tvcomcharname);
        tvcomcharname.setTypeface(customfont);
        TextView  tvcomshifttime = (TextView) dialog.findViewById(R.id.tvcomshifttime);
        tvcomshifttime.setTypeface(customfont);
        TextView tvcomdueafter = (TextView) dialog.findViewById(R.id.tvcomdueday);
        tvcomdueafter.setTypeface(customfont);
        TextView  tvcomremark = (TextView) dialog.findViewById(R.id.tvcomremark);
        tvcomremark.setTypeface(customfont);
        TextView  tvcomcallin = (TextView) dialog.findViewById(R.id.tvcomcallin);
        tvcomcallin.setTypeface(customfont);
        TextView  tvcompckup = (TextView) dialog.findViewById(R.id.tvcompackup);
        tvcompckup.setTypeface(customfont);
        TextView tvcomautname = (TextView) dialog.findViewById(R.id.tvcomauthoritynm);
        tvcomautname.setTypeface(customfont);
        tvauthorityno= (TextView) dialog.findViewById(R.id.tvauthorityno);
        tvauthorityno.setTypeface(customfont);
        TextView  tvcomautsign = (TextView) dialog.findViewById(R.id.tvcomauthoritysign);
        tvcomautsign.setTypeface(customfont);
        TextView  tvcomautdate = (TextView) dialog.findViewById(R.id.tvcomauthoritysigndate);
        tvcomautdate.setTypeface(customfont);
        TextView  tvcomrate=(TextView)dialog.findViewById(R.id.tvcomrate);
        tvcomrate.setTypeface(customfont);
        TextView   tvheader = (TextView) dialog.findViewById(R.id.TVWelcome);
        tvheader.setTypeface(customfont);

        TextView   tvflimnm = (TextView) dialog.findViewById(R.id.tvfilmname);
        tvflimnm.setTypeface(customfont);
        TextView  tvphouse = (TextView) dialog.findViewById(R.id.tvprodhouname);
        tvphouse.setTypeface(customfont);
        tvphouse.setMovementMethod(new ScrollingMovementMethod());

        TextView   tvprodnm = (TextView) dialog.findViewById(R.id.tvprodname);
        tvprodnm.setTypeface(customfont);
        TextView   tvshootdate = (TextView) dialog.findViewById(R.id.tvdtshoot);
        tvshootdate.setTypeface(customfont);
        TextView  tvlocation = (TextView) dialog.findViewById(R.id.tvshootloc);
        tvlocation.setTypeface(customfont);
        TextView     tvcharnm = (TextView) dialog.findViewById(R.id.tvcharname);
        tvcharnm.setTypeface(customfont);
        TextView   tvshifttime = (TextView) dialog.findViewById(R.id.tvshifttime);
        tvshifttime.setTypeface(customfont);
        TextView tvrate = (TextView) dialog.findViewById(R.id.tvrate);
       TextView tvcallinin = (TextView) dialog.findViewById(R.id.tvcallinin);
        tvcallinin.setTypeface(customfont);
        TextView tvcomcallinin= (TextView) dialog.findViewById(R.id.tvcomcallinin);
        tvcomcallinin.setTypeface(customfont);
        tvrate.setTypeface(customfont);
        TextView  tvduedays = (TextView) dialog.findViewById(R.id.tvdueday);
        tvduedays.setTypeface(customfont);
        TextView   tvremark = (TextView) dialog.findViewById(R.id.tvremark);
        tvremark.setTypeface(customfont);
        TextView  tvcallin = (TextView) dialog.findViewById(R.id.tvcallin);
        tvcallin.setTypeface(customfont);
        TextView   tvpackup = (TextView) dialog.findViewById(R.id.tvpackup);
        tvpackup.setTypeface(customfont);
        TextView   tvauthoritynm = (TextView) dialog.findViewById(R.id.tvauthoritynm);
        tvauthoritynm.setTypeface(customfont);
       TextView tvcomauthorityno= (TextView) dialog.findViewById(R.id.tvcomauthorityno);
        tvcomauthorityno.setTypeface(customfont);
        ivauthoritysign= (ImageView) dialog.findViewById(R.id.ivauthoritysign);
        TextView  tvauthosigndate = (TextView) dialog.findViewById(R.id.tvauthoritysigndate);
        tvauthosigndate.setTypeface(customfont);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        dialog.setCancelable(false);
        btnOk.setTypeface(customfont);



        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvflimnm.setText(tvflimnmstr);
        tvphouse.setText(tvphousestr);
        tvprodnm.setText(tvprodnmstr);
        tvshootdate.setText(tvshootdatestr);
        tvlocation.setText(tvlocationstr);
        tvcharnm.setText(tvcharnmstr);
        tvshifttime.setText(tvshifttimestr);
        tvrate.setText(tvratestr);
        tvduedays.setText(tvduedaysstr);
        tvremark.setText(tvremarkstr);
        tvcallinin.setText(tvcallininstr);
        tvpackup.setText(tvpackupstr);
        tvauthoritynm.setText(tvauthoritynmstr);
        tvauthorityno.setText(tvauthoritynostr);
        tvauthosigndate.setText(tvauthosigndatestr);
        tvcallin.setText(tvcallinstr);

        Picasso.with(getActivity())
                .load(ivauthoritysignstr)
                .resize(300,150).noFade().into(ivauthoritysign);

        // show it
        dialog.show();*/
    }
    public void displaydetails(String entry_id)
    {
        pd = new ProgressDialog(getActivity());
		pd.setCancelable(false);
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", entry_id);
        client.post(Config.URLcompletedscheduledetails, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("CompletedDetails");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }

                            tvflimnmstr = j1.getString("film_tv_name");
                            tvphousestr = house_name;
                            tvprodnmstr = j1.getString("producer_name");
                            tvshootdatestr = j1.getString("shoot_date");
                            tvlocationstr = j1.getString("shoot_location");
                            tvcharnmstr = j1.getString("played_character");
                            tvshifttimestr = j1.getString("shift_time");
                            tvcallininstr=j1.getString("call_time");
                            tvratestr = j1.getString("rate") + "/-  " + j1.getString("rate_type");
                            tvduedaysstr = j1.getString("due_days") + " days";
                            String remark = j1.getString("remark");
                            if (remark.equals("null")){
                                tvremarkstr = "No Remark";
                            }else{
                                tvremarkstr = remark;
                            }
                            tvcallinstr = j1.getString("callin_time");
                            tvpackupstr = j1.getString("packup_time");
                            tvauthoritynmstr = j1.getString("authority_nm");
                            tvauthoritynostr=j1.getString("authority_number");
                            ivauthoritysignstr = j1.getString("authority_sign");
                            fees = j1.getString("fees");
                            rater = j1.getString("rate");

                          tvauthosigndatestr = j1.getString("authority_sign_date");
                            /////////////////////////////////////////////////////////

                        }
                        showcompletedscheduledialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
				if (pd != null) {
					if (pd.isShowing()) {
						pd.dismiss();
					}
				}
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
				showToast("could not connect", Toast.LENGTH_LONG);
				if (pd != null) {
					if (pd.isShowing()) {
						pd.dismiss();
					}
				}
            }
        });
    }

    private void Search(){

        if(autosearch.getText().toString().length()>0){
            ivsearchclear.setVisibility(View.VISIBLE);
            viewSearch.setVisibility(View.VISIBLE);
            if(radioDate.isChecked()){              //Search By Date
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE shoot_date LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapterCmpld adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
					showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }else if(radioPHouse.isChecked()){      //Search By Prod. House
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE phouse LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapterCmpld adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
					showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }else if(radiofilm.isChecked()){        //Search Film
                cursor=sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE film_nm LIKE ?",
                        new String[]{"%"+autosearch.getText().toString().trim()+"%"});
                if(cursor.moveToFirst())
                {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String,String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    }while (cursor.moveToNext());
                    ListViewAdapterCmpld adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                }else{
                    //On Result Found
					showToast("No Result Found",Toast.LENGTH_LONG);
                }
            }
        }else {
			showToast("Nothing to Search",Toast.LENGTH_LONG);
        }

    }//Search

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(parent.getApplicationWindowToken(), 0);
        Search();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if (pd != null) {
			if (pd.isShowing()) {
				pd.dismiss();
			}
		}
	}
}
