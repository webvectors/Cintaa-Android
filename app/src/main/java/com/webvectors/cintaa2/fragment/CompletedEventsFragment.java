package com.webvectors.cintaa2.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.webvectors.cintaa2.model.ReportData;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.Dialog_Classes.CompleteEvents_Dialog_Class;
import com.webvectors.cintaa2.activity.HomeActivity;
import com.webvectors.cintaa2.adapter.ListViewAdapterCmpld;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.GlobalClass;
import com.webvectors.cintaa2.utility.getDisplayData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.OnViewInflateListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedEventsFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private ArrayList<HashMap<String, String>> list;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String member_id, tvcallininstr;
    TextView tvcompldateheader, tvcomplprodheader, tvcompllocheader, tvcomplshiftheader, tvflimnm, tvheader, tvphouse, tvprodnm, tvshootdate, tvlocation, tvcharnm, tvshifttime, tvrate, tvratetype, tvduedays, tvremark, tvauthoritynm, tvcallin, tvpackup, tvauthosigndate, tvcomfilmname, tvcomprodhouname, tvcomprodname, tvcomdateofshoot, tvcomloc, tvcomcharname, tvcomshifttime, tvcomrate, tvcomdueafter, tvcomremark, tvcomcallin, tvcompckup, tvcomautname, tvcomautsign, tvcomautdate;
    String tvflimnmstr, ivauthoritysignstr, tvphousestr, tvprodnmstr, tvshootdatestr, tvlocationstr, tvcharnmstr, tvshifttimestr, tvratestr, tvratetypestr, tvduedaysstr, tvremarkstr, tvauthoritynmstr, tvauthoritysignstr, tvcallinstr, tvpackupstr, tvauthosigndatestr, tvcomfilmnamestr, tvcomprodhounamestr, tvcomprodnamestr, tvcomdateofshootstr, tvcomlocstr, tvcomcharnamestr, tvcomshifttimestr, tvcomratestr, tvcomdueafterstr, tvcomremarkstr, tvcomcallinstr, tvcompckupstr, tvcomautnamestr, tvcomautsignstr, tvcomautdatestr, tvauthoritynostr, tvcallinin, tvauthorityemailstr;
    ListView listView;
    private FancyShowCaseView mTipFocusView;
    List<String> list_entry_id, film_nm;
    ProgressDialog pd;
    Typeface customfont;
    RadioGroup rg_searchCategory;
    RadioButton radioDate, radioPHouse, radiofilm;
    AutoCompleteTextView autosearch;
    ImageView ivsearch, ivsearchclear;
    ListViewAdapterCmpld adapter;
    String fees, rater;
    Button btnReport;
    static CompletedEventsFragment completedEventsFragment;

    private String mStartDate;
    private String mEndDate;
    public static ArrayList<ReportData> reportDataArrayList = new ArrayList<>();

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;
    String filepath = "Cintaa";
    String filename = "CompletedShoots.pdf";
    private File pdfFile;
    private ArrayAdapter<String> autoComplete_adapter;
    private TextView tvStartDate;
    private TextView tvLastDate;
    private LinearLayout llDateSearch, llsearch;
    private ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();
    private ArrayList<HashMap<String, String>> hashMaps1 = new ArrayList<>();
    private ArrayList<HashMap<String, String>> hashMaps2 = new ArrayList<>();
    private ProgressDialog progressDialog;
    private ArrayList<HashMap<String, String>> arrayToPrint = new ArrayList<>();
    private PdfPTable table;
    private boolean searchClicked = false;


    public static CompletedEventsFragment getCompletedEventsFragment() {
        return completedEventsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed_events, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        completedEventsFragment = this;

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "Completed Shoot";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);



        list = new ArrayList<HashMap<String, String>>();
        list_entry_id = new ArrayList<>();
        film_nm = new ArrayList<>();

        initialise(view);
        if (savedInstanceState == null) {
            if (Config.isNetworkConnected(getActivity())) {
                selectdata();
            } else {
                showToast("Network is Not Available", Toast.LENGTH_SHORT);
            }
        } else {
            list = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("list");
            if (list.size() > 0) {
                list_entry_id = savedInstanceState.getStringArrayList("id");
                film_nm = savedInstanceState.getStringArrayList("film_name");
                adapter = new ListViewAdapterCmpld(getActivity(), list);
                listView.setAdapter(adapter);

                String date[] = new String[list.size()];
                String house[] = new String[list.size()];
                String film[] = new String[film_nm.size()];
                for (int i = 0; i < list.size(); i++) {
                    HashMap<String, String> map = list.get(i);
                    String s_date = map.get("shoot_date");
                    date[i] = s_date;
                    String p_house = map.get("house_name");
                    house[i] = p_house;
                    film[i] = film_nm.get(i);
                }
                if (radioDate.isChecked())
                    AutoCompleteSearch(date);
                else if (radioPHouse.isChecked())
                    AutoCompleteSearch(house);
                else if (radiofilm.isChecked())
                    AutoCompleteSearch(film);
            } else {
                if (Config.isNetworkConnected(getActivity())) {
                    selectdata();
                } else {
                    showToast("Network is Not Available", Toast.LENGTH_SHORT);
                }
            }
        }
        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        rg_searchCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                autosearch.setText("");
                // Check which radio button was clicked
                switch (checkedId) {

                    case R.id.radioDate:
                        //    radioDate.setChecked(true);
                        llsearch.setVisibility(View.GONE);
                        llDateSearch.setVisibility(View.VISIBLE);
                        autosearch.clearFocus();
                        ivsearchclear.setVisibility(View.GONE);
                        tvStartDate.setText(R.string.start_date);
                        tvLastDate.setText(R.string.last_date);
                        ivsearch.setVisibility(View.VISIBLE);
                        String date[] = new String[list.size()];
                        for (int i = 0; i < list.size(); i++) {
                            HashMap<String, String> map = list.get(i);
                            String s_date = map.get("shoot_date");
                            date[i] = s_date;
                        }
                        AutoCompleteSearch(date);
                        autosearch.setHint("Search by Date");
                        break;
                    case R.id.radioPHouse:
                        //   radioPHouse.setChecked(true);
                        llDateSearch.setVisibility(View.GONE);
                        llsearch.setVisibility(View.VISIBLE);
                        autosearch.requestFocus();
                        ivsearchclear.setVisibility(View.GONE);
                        ivsearch.setVisibility(View.VISIBLE);
                        String house[] = new String[list.size()];
                        for (int i = 0; i < list.size(); i++) {
                            HashMap<String, String> map = list.get(i);
                            String p_house = map.get("house_name");
                            house[i] = p_house;
                        }
                        AutoCompleteSearch(house);
                        autosearch.setHint("Search by Prod. House");
                        break;
                    case R.id.radiofilm:
                        //   radiofilm.setChecked(true);
                        llDateSearch.setVisibility(View.GONE);
                        llsearch.setVisibility(View.VISIBLE);
                        autosearch.requestFocus();
                        ivsearchclear.setVisibility(View.GONE);
                        ivsearch.setVisibility(View.VISIBLE);

                        String film[] = new String[film_nm.size()];
                        for (int i = 0; i < film_nm.size(); i++) {
                            film[i] = film_nm.get(i);
                        }
                        AutoCompleteSearch(film);
                        autosearch.setHint("Search by Film/TV Serial");
                        break;
                }
            }
        });
        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioDate.isChecked()) {
//                    autosearch.clearFocus();
                    ShowStartTimePicker();
                }
            }
        });
        tvLastDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioDate.isChecked()) {
//                    autosearch.clearFocus();
                    ShowEndTimePicker();
                }
            }
        });
//        autosearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (radioDate.isChecked()) {
//                    autosearch.clearFocus();
//                    ShowStartTimePicker();
//                }
//            }
//        });
        ivsearchclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                autosearch.setText("");
                cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed", null);
                if (cursor.moveToFirst()) {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String, String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        temp1.put("entry_id", cursor.getString(4));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    } while (cursor.moveToNext());
                    adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                } else {
                    //On Result Found
                    showToast("No Result Found", Toast.LENGTH_LONG);
                }
                ivsearchclear.setVisibility(View.GONE);
//                viewSearch.setVisibility(View.GONE);
                searchClicked = false;


                if (radioDate.isChecked()) {
                    Log.e("-if-", "CLEAR");
                    mEndDate = null;
                    mStartDate = null;
                    tvStartDate.setText(R.string.start_date);
                    tvLastDate.setText(R.string.last_date);
                    ivsearch.setVisibility(View.VISIBLE);
                    llsearch.setVisibility(View.GONE);
                    llDateSearch.setVisibility(View.VISIBLE);
                } else {
                    Log.e("-else-", "CLEAR");
                    llDateSearch.setVisibility(View.GONE);
                    llsearch.setVisibility(View.VISIBLE);
                    ivsearch.setVisibility(View.VISIBLE);
                }
            }
        });
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");

        btnReport = (Button) view.findViewById(R.id.btnReport);
        btnReport.setTypeface(custom_font);
        btnReport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                new Generate_report_Class().show(getFragmentManager(), "tag");
                pdfFile = new File(getActivity().getExternalFilesDir(filepath), filename);
                try {
                    pdfFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                GlobalClass.printLog(getActivity(), "----AAAAAAA---adapter.getListArray();-----" + adapter.getListArray().size());
                arrayToPrint = adapter.getListArray();

                if (mStartDate != null && mEndDate != null) {
                    if (searchClicked) {
                        reportDataArrayList = new ArrayList<ReportData>();
                        new getDisplayData(getActivity(), arrayToPrint, member_id).execute();
                    } else {
                        showToast("Filter The List", Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast(getString(R.string.please_select_proper_date), Toast.LENGTH_SHORT);
                }

//                getFullArray(arrayToPrint);
            }
        });

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radioDate.isChecked()) {
                    if (mStartDate != null && mEndDate != null) {
                        Date startDate, endDate;
                        try {
                            GlobalClass.printLog(getActivity(), "-------mStartDate-----" + mStartDate);
                            GlobalClass.printLog(getActivity(), "-------mEndDate-----" + mEndDate);

                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            startDate = formatter.parse(mStartDate);
                            endDate = formatter.parse(mEndDate);

                            if (endDate.after(startDate) || endDate.equals(startDate)) {
                                ivsearchclear.setVisibility(View.VISIBLE);
                                ivsearch.setVisibility(View.GONE);

                                hashMaps2 = new ArrayList<>();
                                for (int i = 0; i < getDaysBetweenDates(startDate, endDate).size(); i++) {
                                    hashMaps1.clear();
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                                    SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    String dateStr = getDaysBetweenDates(startDate, endDate).get(i).toString().trim();
                                    Date date = inputFormat.parse(dateStr);
                                    String str = outputFormat.format(date);
                                    hashMaps1 = getSearchByDate(str);
//                                    hashMaps1.addAll(getSearchByDate(mEndDate));
                                    GlobalClass.printLog(getActivity(), "-------hashMaps1-----" + hashMaps1);

                                    hashMaps2.addAll(hashMaps1);
                                }

                                if (hashMaps2.size() > 0) {
                                    searchClicked = true;
                                    adapter = new ListViewAdapterCmpld(getActivity(), hashMaps2);
                                    listView.setAdapter(adapter);
                                } else {
                                    //On Result Found
                                    showToast(getString(R.string.no_result_found), Toast.LENGTH_LONG);
                                }
                            } else {
                                ivsearchclear.setVisibility(View.GONE);
                                ivsearch.setVisibility(View.VISIBLE);
                                showToast(getString(R.string.startdate_should_before_enddate), Toast.LENGTH_SHORT);
                            }

                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        ivsearchclear.setVisibility(View.GONE);
                        ivsearch.setVisibility(View.VISIBLE);
                        showToast(getString(R.string.please_select_proper_date), Toast.LENGTH_SHORT);
                    }


                } else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    Search();
                }

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                //Toast.makeText(getContext(), ""+list_entry_id.get(position), Toast.LENGTH_SHORT).show();
                String entry_id = list_entry_id.get(position);
                displaydetails(entry_id);
            }

        });

        return view;
    }//OnCreate

    public void checkSize() {
        if (reportDataArrayList.size() == arrayToPrint.size()) {
            new PdfGenerationTask().execute();
        }
    }


    private PdfPCell createCell(String content, float borderWidth, int colspan, int alignment, Font fontStyle) {
        PdfPCell cell = new PdfPCell(new Phrase(content, fontStyle));
        cell.setBorderWidth(1);
        cell.setColspan(colspan);
        cell.setBorder(Rectangle.BOX);
        cell.setUseBorderPadding(true);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }


    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate) || calendar.getTime().equals(enddate)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }


    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerStartDate = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            mStartDate = datee + "/" + month + "/" + year;
//            autosearch.setText(date);
            tvStartDate.setText(mStartDate);

            //....
//            Search();
//            SearchByDate(tvStartDate.getText().toString().trim());
//            ivsearchclear.setVisibility(View.VISIBLE);
//            ivsearch.setVisibility(View.VISIBLE);
//            llDateSearch.setVisibility(View.VISIBLE);
//            llsearch.setVisibility(View.GONE);
        }
    };
    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerLastDate = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            mEndDate = datee + "/" + month + "/" + year;
//            autosearch.setText(date);
            tvLastDate.setText(mEndDate);
            //....
//            Search();


//            llDateSearch.setVisibility(View.VISIBLE);
//            llsearch.setVisibility(View.GONE);

        }
    };

    private void ShowStartTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerStartDate,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowStartTimePicker

    private void ShowEndTimePicker() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerLastDate,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));

        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }//ShowStartTimePicker

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("list", list);
        outState.putStringArrayList("id", (ArrayList<String>) list_entry_id);
        outState.putStringArrayList("film_name", (ArrayList<String>) film_nm);
        super.onSaveInstanceState(outState);
    }

    private void initialise(View view) {
        Typeface customfont = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        listView = (ListView) view.findViewById(R.id.listCompletedShoots);
        tvcompldateheader = (TextView) view.findViewById(R.id.tvcompldateheader);
        tvcompldateheader.setTypeface(customfont);
        tvStartDate = (TextView) view.findViewById(R.id.tvStartDate);
        tvStartDate.setTypeface(customfont);
        TextView tvTo = (TextView) view.findViewById(R.id.tvTo);
        tvTo.setTypeface(customfont);
        tvLastDate = (TextView) view.findViewById(R.id.tvLastDate);
        tvLastDate.setTypeface(customfont);

        llDateSearch = (LinearLayout) view.findViewById(R.id.llDateSearch);
        llsearch = (LinearLayout) view.findViewById(R.id.llsearch);

        tvcomplprodheader = (TextView) view.findViewById(R.id.tvcomplprodhouheader);
        tvcomplprodheader.setTypeface(customfont);
        tvcompllocheader = (TextView) view.findViewById(R.id.tvcompllocheader);
        tvcompllocheader.setTypeface(customfont);
        tvcomplshiftheader = (TextView) view.findViewById(R.id.tvcomplshiftheader);
        tvcomplshiftheader.setTypeface(customfont);
        radioDate = (RadioButton) view.findViewById(R.id.radioDate);
        radioDate.setTypeface(customfont);
        radioPHouse = (RadioButton) view.findViewById(R.id.radioPHouse);
        radioPHouse.setTypeface(customfont);
        radiofilm = (RadioButton) view.findViewById(R.id.radiofilm);
        radiofilm.setTypeface(customfont);
        autosearch = (AutoCompleteTextView) view.findViewById(R.id.autosearch);
        autosearch.setTypeface(customfont);
        ivsearch = (ImageView) view.findViewById(R.id.ivsearch);
        ivsearchclear = (ImageView) view.findViewById(R.id.ivsearchclear);
        llsearch.setVisibility(View.GONE);
        llDateSearch.setVisibility(View.VISIBLE);
        ivsearchclear.setVisibility(View.GONE);
        ivsearch.setVisibility(View.VISIBLE);
//        viewSearch = (View)view.findViewById(R.id.viewSearch);
//        viewSearch.setVisibility(View.GONE);
        rg_searchCategory = (RadioGroup) view.findViewById(R.id.rg_searchCategory);

        autosearch.clearFocus();

    }//initialise

    void selectdata() {
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLcompletedscheduleentry, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("CompletedEntryTable");
                    if (jsonArray.length() > 0) {
                        String shootDate[] = new String[jsonArray.length()];
                        list_entry_id.clear();
                        film_nm.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String shoot_date = j1.getString("shoot_date");
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            String entry_id = j1.getString("entry_id");
                            String film_name = j1.getString("film_tv_name");
                            String shoot_location = j1.getString("shoot_location");
                            String shift_time = j1.getString("shift_time");

                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
                            HashMap<String, String> temp1 = new HashMap<String, String>();
                            temp1.put("shoot_date", shoot_date);
                            temp1.put("house_name", house_name);
                            temp1.put("shoot_location", shoot_location);
                            temp1.put("shift_time", shift_time);
                            temp1.put("entry_id", entry_id);


                            String[] parts = entry_id.split("_");
                            String part2 = parts[1];

                            temp1.put("part2", part2);

                            shootDate[i] = shoot_date;
                            list.add(temp1);
                            list_entry_id.add(entry_id);
                            film_nm.add(film_name);
                        }
//.. to sort collection date wise..
                        Collections.sort(list, new MapComparator("part2"));
                        Collections.reverse(list);
                        InsertSQLite(list, list_entry_id, film_nm);
                        adapter = new ListViewAdapterCmpld(getActivity(), list);
                        listView.setAdapter(adapter);

                        AutoCompleteSearch(shootDate);
                    }

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                            //Toast.makeText(getContext(), ""+list_entry_id.get(position), Toast.LENGTH_SHORT).show();
//                            String entry_id = list_entry_id.get(position);
                            String entry_id = adapter.getListArray().get(position).get("entry_id");
                            displaydetails(entry_id);
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }

    private void InsertSQLite(ArrayList<HashMap<String, String>> list, List<String> list_entry_id, List<String> list_film_nm) {

        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_ScheduleFilterData_completed");
        int res = sqLiteStatement.executeUpdateDelete();

        for (int i = 0; i < list.size(); i++) {
            HashMap<String, String> map = list.get(i);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_ScheduleFilterData_completed values(?,?,?,?,?,?,?)");
            //shoot_date, phouse, location, shift_time, film_nm, entry_id, schedule_status
            sqLiteStatement.bindString(1, map.get("shoot_date"));
            sqLiteStatement.bindString(2, map.get("house_name"));
            sqLiteStatement.bindString(3, map.get("shoot_location"));
            sqLiteStatement.bindString(4, map.get("shift_time"));
            sqLiteStatement.bindString(5, list_film_nm.get(i));
            sqLiteStatement.bindString(6, list_entry_id.get(i));
            sqLiteStatement.bindString(7, "completed");
            long result = sqLiteStatement.executeInsert();
        }//end for loop

    }//InsertSQLite

    private void AutoCompleteSearch(String item[]) {
        autoComplete_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, item);
        autosearch.setThreshold(1);
        //Set adapter to AutoCompleteTextView
        autosearch.setAdapter(autoComplete_adapter);
        autosearch.setOnItemSelectedListener(this);
        autosearch.setOnItemClickListener(this);
    }//AutoCompleteSearch

    private void showcompletedscheduledialog() {

//tvcallininstr,tvflimnmstr,ivauthoritysignstr, tvphousestr, tvprodnmstr, tvshootdatestr, tvlocationstr, tvcharnmstr, tvshifttimestr, tvratestr, tvduedaysstr, tvremarkstr,tvauthoritynmstr,tvcallinstr,tvpackupstr,tvauthosigndatestr,tvauthoritynostr

        editor = preferences.edit();
        editor.putString("tvcallininstr_cmplt", tvcallininstr);
        editor.putString("tvflimnmstr_cmplt", tvflimnmstr);
        editor.putString("ivauthoritysignstr_cmplt", ivauthoritysignstr);
        editor.putString("tvphousestr_cmplt", tvphousestr);
        editor.putString("tvprodnmstr_cmplt", tvprodnmstr);
        editor.putString("tvshootdatestr_cmplt", tvshootdatestr);
        editor.putString("tvlocationstr_cmplt", tvlocationstr);
        editor.putString("tvcharnmstr_cmplt", tvcharnmstr);
        editor.putString("tvshifttimestr_cmplt", tvshifttimestr);
        editor.putString("tvratestr_cmplt", tvratestr);
        editor.putString("tvduedaysstr_cmplt", tvduedaysstr);
        editor.putString("tvremarkstr_cmplt", tvremarkstr);
        editor.putString("tvauthoritynmstr_cmplt", tvauthoritynmstr);
        editor.putString("tvcallinstr_cmplt", tvcallinstr);
        editor.putString("tvpackupstr_cmplt", tvpackupstr);
        editor.putString("tvauthosigndatestr_cmplt", tvauthosigndatestr);
        editor.putString("tvauthoritynostr_cmplt", tvauthoritynostr);
        editor.putString("fees_cmplt", fees);
        editor.putString("rater_cmplt", rater);
        editor.putString("tvauthorityemailstr_cmplt", tvauthorityemailstr);

        editor.commit();


        new CompleteEvents_Dialog_Class().show(getFragmentManager(), "tag");
    }

    //... display details
    public void displaydetails(String entry_id) {
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("entry_id", entry_id);
        client.post(Config.URLcompletedscheduledetails, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("CompletedDetails");
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String house_name = j1.getString("house_name");
                            String alies_name = j1.getString("alies_name");
                            if (!alies_name.equals("null")) {
                                house_name = house_name + "/" + alies_name;
                            }
                            tvflimnmstr = j1.getString("film_tv_name");
                            tvphousestr = house_name;
                            tvprodnmstr = j1.getString("producer_name");
                            tvshootdatestr = j1.getString("shoot_date");
                            tvlocationstr = j1.getString("shoot_location");
                            tvcharnmstr = j1.getString("played_character");
                            tvshifttimestr = j1.getString("shift_time");
                            tvcallininstr = j1.getString("call_time");
                            tvratestr = j1.getString("rate") + " " + j1.getString("rate_type");
                            tvduedaysstr = j1.getString("due_days") + " days";
                            String remark = j1.getString("remark");
                            if (remark.equals("null")) {
                                tvremarkstr = "No Remark";
                            } else {
                                tvremarkstr = remark;
                            }
                            tvcallinstr = j1.getString("callin_time");
                            tvpackupstr = j1.getString("packup_time");
                            tvauthoritynmstr = j1.getString("authority_nm");
                            tvauthoritynostr = j1.getString("authority_number");
                            ivauthoritysignstr = j1.getString("authority_sign");
                            fees = j1.getString("fees");
                            tvauthorityemailstr = j1.getString("house_email");
                            rater = j1.getString("rate");

                            tvauthosigndatestr = j1.getString("authority_sign_date");
                            /////////////////////////////////////////////////////////

                        }
                        showcompletedscheduledialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }

    private ArrayList<HashMap<String, String>> getSearchByDate(String date) {              //..Search By Date
        cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE shoot_date LIKE ?",
                new String[]{"%" + date + "%"});
        if (cursor.moveToFirst()) {
            hashMaps = new ArrayList<HashMap<String, String>>();
            list_entry_id.clear();
            do {
                HashMap<String, String> temp1 = new HashMap<String, String>();
                temp1.put("shoot_date", cursor.getString(0));
                temp1.put("house_name", cursor.getString(1));
                temp1.put("shoot_location", cursor.getString(2));
                temp1.put("shift_time", cursor.getString(3));
                temp1.put("entry_id", cursor.getString(4));
                hashMaps.add(temp1);
                list_entry_id.add(cursor.getString(4));
            } while (cursor.moveToNext());

        }
        return hashMaps;
    }

    private void Search() {

        if (autosearch.getText().toString().length() > 0) {
            ivsearchclear.setVisibility(View.VISIBLE);
            ivsearch.setVisibility(View.GONE);
            llDateSearch.setVisibility(View.GONE);
            llsearch.setVisibility(View.VISIBLE);

//            viewSearch.setVisibility(View.VISIBLE);
            if (radioDate.isChecked()) {              //..Search By Date

                autosearch.clearFocus();
                cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE shoot_date LIKE ?",
                        new String[]{"%" + autosearch.getText().toString().trim() + "%"});
                if (cursor.moveToFirst()) {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String, String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        temp1.put("entry_id", cursor.getString(4));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    } while (cursor.moveToNext());
                    adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                } else {
                    //On Result Found
                    showToast("No Result Found", Toast.LENGTH_LONG);
                }
            } else if (radioPHouse.isChecked()) {      //Search By Prod. House
                cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE phouse LIKE ?",
                        new String[]{"%" + autosearch.getText().toString().trim() + "%"});
                if (cursor.moveToFirst()) {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String, String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        temp1.put("entry_id", cursor.getString(4));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    } while (cursor.moveToNext());
                    adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                } else {
                    //On Result Found
                    showToast("No Result Found", Toast.LENGTH_LONG);
                }
            } else if (radiofilm.isChecked()) {        //Search Film
                cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, phouse, location, shift_time,entry_id from sqtb_ScheduleFilterData_completed WHERE film_nm LIKE ?",
                        new String[]{"%" + autosearch.getText().toString().trim() + "%"});
                if (cursor.moveToFirst()) {
                    ArrayList<HashMap<String, String>> ll = new ArrayList<HashMap<String, String>>();
                    list_entry_id.clear();
                    do {
                        HashMap<String, String> temp1 = new HashMap<String, String>();
                        temp1.put("shoot_date", cursor.getString(0));
                        temp1.put("house_name", cursor.getString(1));
                        temp1.put("shoot_location", cursor.getString(2));
                        temp1.put("shift_time", cursor.getString(3));
                        temp1.put("entry_id", cursor.getString(4));
                        ll.add(temp1);
                        list_entry_id.add(cursor.getString(4));
                    } while (cursor.moveToNext());
                    adapter = new ListViewAdapterCmpld(getActivity(), ll);
                    listView.setAdapter(adapter);
                } else {
                    //On Result Found
                    showToast("No Result Found", Toast.LENGTH_LONG);
                }
            }
        } else {
            showToast("Nothing to Search", Toast.LENGTH_LONG);
        }

    }//Search

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(parent.getApplicationWindowToken(), 0);
        Search();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (DiaryentryFragment.getFragment() != null)
            DiaryentryFragment.getFragment().hideTip();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:
                showTip1();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mTipFocusView != null && mTipFocusView.isShown())
                mTipFocusView.hide();
        }
    };

    private void showTip1() {
        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(View view) {
                        view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTipFocusView.hide();
                                showTip2();
                            }
                        });
                        view.findViewById(R.id.tvWelcome).setVisibility(View.VISIBLE);

                        TextView tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);
                        tvWelcome.setText(R.string.completed_shoot);
                        tvWelcome.setVisibility(View.VISIBLE);

                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                        tvTip.setText(R.string.tip_list_of_completed_shoot);
                        view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                    }
                })
                .closeOnTouch(false)
                .build();
        mTipFocusView.show();
    }

    private void showTip2() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip3();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_filter_date_wise);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(radioDate)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    } //Tip2

    private void showTip3() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip4();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_filter_production_house);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(radioPHouse)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showTip4() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTipForList();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_filter_per_movie);
                                }
                            })
                            .closeOnTouch(false)
                            .focusOn(radiofilm)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showTipForList() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_shoot_list_item, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showViewDetailDialog();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_shoot_item);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showViewDetailDialog() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_detail_of_dialog, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip5();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_shoot_detail_msg);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showTip5() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

                                    View view2 = (View) view.findViewById(R.id.view);
                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_view_detail_of_completed_shoot);
                                }
                            })
                            .closeOnTouch(false)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    } // Tip3

    //
//    public void addCellInTable(String house_name, String mRemark, String film_tv_name, String producer_name,
//                               String shoot_date, String shoot_location, String played_character,
//                               String shift_time, String call_time, String rate, String due_days,
//                               String callin_time, String packup_time, String authority_nm,
//                               String authority_number, String authority_sign, String fees,
//                               String house_email, String rater, String authority_sign_date) {
    public void addCellInTable() {
        new PdfGenerationTask().execute();
    }


    private class MapComparator implements Comparator<Map<String, String>> {
        private final String key;

        MapComparator(String key) {
            this.key = key;
        }

        public int compare(Map<String, String> first,
                           Map<String, String> second) {
            // TODO: Null checking, both for maps and values
            String firstValue = first.get(key);
            String secondValue = second.get(key);
            return firstValue.compareTo(secondValue);
        }
    }

    private class PdfGenerationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Document document = new Document(PageSize.A4.rotate(), 20, 20, 20, 50);
            table = new PdfPTable(12);
            try {
                table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Font titleFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
            Font dateFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);

            table.addCell(createCell("Shoot Date", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Film/TV serial Name", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Prod.House Name", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Shoot Location", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Character", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Call time", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("In time", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Packup Time", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Rate", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Conv.", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Auth Name", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.addCell(createCell("Auth Sign Date&Time", 2, 1, Element.ALIGN_CENTER, titleFont));
            table.setTotalWidth(document.right() - document.left());

            table.setSplitLate(false);
            table.setSplitRows(false);
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.getDefaultCell().setPadding(56);
            table.setWidthPercentage(80);
            table.setLockedWidth(true);
//                table.setHeaderRows(1);
            table.setWidthPercentage(86f);
            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
//                set Background in PDF
                writer.setPageEvent(new PDFBackground());

                // For set title of PDF
                Font headingFont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);
                Font headingFont2 = new Font(Font.FontFamily.HELVETICA, 16, Font.NORMAL, BaseColor.DARK_GRAY);
                Paragraph TitleName = new Paragraph(22);

                TitleName.setSpacingAfter(5);
                TitleName.setAlignment(Element.ALIGN_CENTER);
                TitleName.setFont(headingFont2);
                TitleName.add("COMPLETED SHOOT");

                Paragraph DateLine = new Paragraph(22);
                DateLine.setSpacingAfter(5);
                DateLine.setAlignment(Element.ALIGN_CENTER);
                DateLine.setFont(headingFont2);
                DateLine.add("Report for the period " + mStartDate + " to " + mEndDate);

                Drawable drawable = getResources().getDrawable(R.drawable.app_icon);
                BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
                Bitmap bitmap = bitmapDrawable.getBitmap();
                Bitmap ResisedBitmap = Bitmap.createScaledBitmap(bitmap, 50, 50, false);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                ResisedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image logoImage = Image.getInstance(stream2.toByteArray());

                Paragraph paragraph1 = new Paragraph();
                Paragraph paragraph2 = new Paragraph();
                paragraph2.setFont(headingFont);
                paragraph1.add(logoImage);
                paragraph2.add("CINE AND TV ARTISTS ASSOCIATION (CINTAA)");
                paragraph1.setAlignment(Element.ALIGN_CENTER);
                paragraph2.setAlignment(Element.ALIGN_CENTER);

                document.open();
                document.add(paragraph1);
                document.add(paragraph2);
                document.add(TitleName);
                document.add(DateLine);

                LineSeparator ls = new LineSeparator();
                ls.setLineColor(BaseColor.BLACK);
                document.add(new Chunk(ls));
                document.add(Chunk.NEWLINE);

                for (int i = 0; i < reportDataArrayList.size(); i++) {
                    ReportData reportData = reportDataArrayList.get(i);
                    table.addCell(createCell(reportData.getShoot_date(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getFilm_tv_name(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getHouse_name(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getShoot_location(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getPlayed_character(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getShift_time(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getCallin_time(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getPackup_time(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getRater(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getFees(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getAuthority_nm(), 2, 1, Element.ALIGN_CENTER, dateFont));
                    table.addCell(createCell(reportData.getAuthority_sign_date(), 2, 1, Element.ALIGN_CENTER, dateFont));
                }
                document.add(table);

                PdfContentByte cb = writer.getDirectContent();
                Font footerFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
                //footer content
                String footerContent = "Copyright 2017 WebVectors Inc. All Rights Reserved.";
                String disclaimer = "Disclaimer: This PDF is generated using Cintaa mobile application.";

                /*
                 * Footer
                 */
                // copyRight String
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(footerContent, footerFont),
                        (document.left() + document.right()) / 2, 40, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer, footerFont),
                        (document.left() + document.right()) / 2, 25, 0);
//                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(disclaimer_two, footerFont),
//                        (document.left() + document.right()) / 2, 10, 0);
                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            showToast("Report Gnerated", Toast.LENGTH_SHORT);
            mailPDF();
        }
    }

    public class PDFBackground extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte canvas = writer.getDirectContentUnder();
            Drawable drawable = getResources().getDrawable(R.drawable.bg_plain);
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
            Bitmap bitmap = bitmapDrawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image backgroundImage;
            try {
                backgroundImage = Image.getInstance(stream.toByteArray());
                float width = document.getPageSize().getWidth();
                float height = document.getPageSize().getHeight();
                writer.getDirectContentUnder().addImage(backgroundImage, width, 0, 0, height, 0, 0);
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void mailPDF() {
        String mEmail = null;
        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_UserProfile", null);
        if (cursor.moveToFirst()) {
            do {
                mEmail = cursor.getString(7);
            } while (cursor.moveToNext());
        }
        //file should contain path of pdf file
        Uri path = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", pdfFile);

        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, mEmail);

        intent.putExtra(Intent.EXTRA_SUBJECT, "Completed Shoot Report");
        intent.putExtra(Intent.EXTRA_TEXT, mStartDate + " to " + mEndDate);
        intent.putExtra(Intent.EXTRA_STREAM, path);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
        if (progressDialog.isShowing() && progressDialog != null)
            progressDialog.dismiss();
    }
}