package com.webvectors.cintaa2.fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webvectors.cintaa2.activity.HomeActivity;
import com.webvectors.cintaa2.adapter.PagerAdapterSchedule;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;

import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.OnViewInflateListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends BaseFragment {

    ViewPager viewpager1;
    //PagerAdapter adapter1;
    TabLayout tabLayout;
    PagerAdapterSchedule adapter1;
    private FancyShowCaseView mTipFocusView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_schedule, container, false);

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "My Schedule";

        Log.e("-----","Schedule Freeagment");
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout1);
        tabLayout.addTab(tabLayout.newTab().setText("In Progress"));
        tabLayout.addTab(tabLayout.newTab().setText("Completed"));

            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager1);

            //adapter1 = new PagerAdapterSchedule(getFragmentManager(), tabLayout.getTabCount());
        adapter1 = new PagerAdapterSchedule(getChildFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter1);

            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
                        return true;
                    }
//               getFragmentManager().beginTransaction().replace(R.id.content_nav, new OrderFragment(),"ok").addToBackStack(null).commit();
                    return false;
                }
            });


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
/* public void setCurrentItem (int item, boolean smoothScroll) {
        viewpager1.setCurrentItem(item, smoothScroll);
    }*/


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:
           //     showTip1();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mTipFocusView != null && mTipFocusView.isShown())
                mTipFocusView.hide();
        }
    };

    private void showTip1() {
        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(View view) {
                        view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTipFocusView.hide();
                                showTip2();
                            }
                        });

                        TextView tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);
                        tvWelcome.setText(R.string.schedule_entry);
                        tvWelcome.setVisibility(View.VISIBLE);

                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                        tvTip.setText(R.string.tip_schedule_create_view_edit);

                        view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                    }
                })
                .closeOnTouch(false)
                .build();
        mTipFocusView.show();
    }

    private void showTip2() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip3();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_schedule_navigate_datewise);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    } //Tip2

    private void showTip3() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

                                    View view2 = (View) view.findViewById(R.id.view);
                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_create_new_or_edit_existing_schedule);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    } // Tip3
}
