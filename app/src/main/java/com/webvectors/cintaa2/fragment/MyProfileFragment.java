package com.webvectors.cintaa2.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.webvectors.cintaa2.Dialog_Classes.Generate_report_Class;
import com.webvectors.cintaa2.activity.CreatePDFofTemplate;
import com.webvectors.cintaa2.utility.CommonHelper;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.GeneratePdf;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    EditText ETname, ETaddress, ETcity, ETstate, ETzipcode, ETEmail, ETmyPassword, ETmobile, ETaltmobile, ETwebsite;
    RadioButton radiomale, radiofemale;
    RadioGroup radioSexGroup;
    ProgressDialog pd;
    String profile_url, name;
    ImageView imgchange;
    AutoCompleteTextView ETcountry;
    CircleImageView profileimg;
    TextView tvname, tvWel, tvgender;
    //TextView tvchangepwd;
    Button save, cancel, edit;
    SharedPreferences preferences;
    private ArrayAdapter<String> adapterstr;
    SharedPreferences.Editor editor;
    int check = 0, check1 = 0;
    String gender, member_id, preEmail, preMobile, preAltMobile, preWebsite, artistName;
    // EditText etpassowrd,etnewpwd,etconfirmpwd;
    String strcountry[] = {"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Yemen", "Zambia", "Zimbabwe"};
    AlertDialog.Builder builder;
    AlertDialog.Builder builder1;

    static String local = "192.168.1.6:8090/tejas/images/";
    static String server = "http://shooting.pinnaculuminfotech.com/shoot/Images/";
    public static final String IMAGE_DIRECTORY_NAME = local;

    public static String encoded_string, image_name;
    Bitmap bm;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static String tv_image;
    public static String set_image;
    static int permissionCode;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "My Profile";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);


        member_id = preferences.getString("member_id", "abc");
        profile_url = preferences.getString("profile_imgurl", "xy12z");
        name = preferences.getString("name", "xy32z");
        editor = preferences.edit();

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        if (savedInstanceState == null) {

        }

        initialise(view);

        File path = new File(Environment.getExternalStorageDirectory() + "/CINTAA/" + member_id + ".png");
        if (path.exists()) {
            Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            profileimg.setImageBitmap(mBitmap);
        } else {
            Picasso.with(getActivity()).load(profile_url).resize(70, 70).into(profileimg);

            try {
                // downloadImagesToSdCard(profile_url);
                new AsyncDownloadImageToSDCard().execute(profile_url);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        tvname.setText(name);





        /*if(path.exists()){
            Toast.makeText(getActivity(), "DONE", Toast.LENGTH_LONG).show();
            Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            profileimg.setImageBitmap(mBitmap);
        }else {
            Picasso.with(getActivity()).load(profile_url).resize(70,70).into(profileimg);
        }*/


        profileimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeImgDialog();

            }
        });

        //   tvchangepwd.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        builder = new AlertDialog.Builder(getActivity());
        pd = new ProgressDialog(getActivity());


        builder1 = new AlertDialog.Builder(getActivity());
        strcountry = new String[]{"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Yemen", "Zambia", "Zimbabwe"};


        SelectProfileData();

        /*if(isNetworkConnected()){
            edit.setEnabled(true);
            getdata();
        }else {
            edit.setEnabled(false);
            Toast.makeText(getActivity().getApplicationContext(),"\"Network connection is closed...Please enable data connection\"",Toast.LENGTH_LONG).show();
        }*/
        save.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                new Change_pwd_Class().show(getFragmentManager(), "tag"); // or getFragmentManager() in API 11+
                ETname.setEnabled(true);
                ETaddress.setEnabled(true);
                ETcity.setEnabled(true);
                ETstate.setEnabled(true);
                ETzipcode.setEnabled(true);
                ETcountry.setEnabled(true);
                ETEmail.setEnabled(true);
                ETmyPassword.setEnabled(false);
                ETmobile.setEnabled(true);
                ETaltmobile.setEnabled(true);
                ETwebsite.setEnabled(true);
                save.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.VISIBLE);
                radiomale.setClickable(true);
                radiofemale.setClickable(true);
                edit.setVisibility(View.GONE);

                ETname.setAlpha((float) 1.0);
                ETaddress.setAlpha((float) 1.0);
                ETcity.setAlpha((float) 1.0);
                ETcountry.setAlpha((float) 1.0);
                ETstate.setAlpha((float) 1.0);
                ETzipcode.setAlpha((float) 1.0);
                ETEmail.setAlpha((float) 1.0);
                ETmyPassword.setAlpha((float) 1.0);
                ETmobile.setAlpha((float) 1.0);
                ETaltmobile.setAlpha((float) 1.0);
                ETwebsite.setAlpha((float) 1.0);
                radiomale.setAlpha((float) 1.0);
                radiofemale.setAlpha((float) 1.0);
                tvgender.setAlpha((float) 1.0);

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                validation();
                if (check == 7) {
                    selectgender();
                    if (isNetworkConnected()) {
                        if (ETwebsite.getText().toString().trim().length() == 0) {
                            insert();
                        } else if (Patterns.WEB_URL.matcher(ETwebsite.getText().toString().trim()).matches()) {
                            insert();
                        } else {
                            ETwebsite.setError("Enter Valid Website URL");
                            showToast("Enter Valid Website URL", Toast.LENGTH_LONG);
                        }

                    } else {
                        edit.setEnabled(false);
                        showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
                    }
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Fragment fr = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
            }
        });
      /*  tvchangepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangePasswordDialog();
            }
        });*/
        adapterstr = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, strcountry);
        //    ETcountry.setDropDownBackgroundDrawable(new ColorDrawable(getActivity().getResources().getColor(R.color.bgTitleTab)));
        ETcountry.setThreshold(1);

        //Set adapter to AutoCompleteTextView
        ETcountry.setAdapter(adapterstr);
        ETcountry.setOnItemSelectedListener(this);
        ETcountry.setOnItemClickListener(this);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
                    return true;
                }
//               getFragmentManager().beginTransaction().replace(R.id.content_nav, new OrderFragment(),"ok").addToBackStack(null).commit();
                return false;
            }
        });

        ETwebsite.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    handled = true;
                }
                return handled;
            }
        });


        return view;
    }//onCreate

    private void initialise(View view) {
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        profileimg = (CircleImageView) view.findViewById(R.id.cirprofile_img);
        tvname = (TextView) view.findViewById(R.id.tvprofname);
        tvname.setTypeface(custom_font);
        tvWel = (TextView) view.findViewById(R.id.tvWel);
        tvgender = (TextView) view.findViewById(R.id.tvgender);
        tvgender.setTypeface(custom_font);
        tvWel.setTypeface(custom_font);
        // tvchangepwd = (TextView)view.findViewById(R.id.tvchangepwd);
        edit = (Button) view.findViewById(R.id.btnedit);
        edit.setTypeface(custom_font);


        // etpassowrd = (EditText)view.findViewById(R.id.etpassowrd);
        ETname = (EditText) view.findViewById(R.id.etname);
        ETname.setTypeface(custom_font);
        imgchange = (ImageView) view.findViewById(R.id.imgchange);
        ETaddress = (EditText) view.findViewById(R.id.etaddress);
        ETaddress.setTypeface(custom_font);
        ETcity = (EditText) view.findViewById(R.id.etcity);
        ETcity.setTypeface(custom_font);
        ETcountry = (AutoCompleteTextView) view.findViewById(R.id.etcountry);
        ETcountry.setTypeface(custom_font);
        ETstate = (EditText) view.findViewById(R.id.etstate);
        ETstate.setTypeface(custom_font);
        ETzipcode = (EditText) view.findViewById(R.id.etzipcode);
        ETzipcode.setTypeface(custom_font);
        ETEmail = (EditText) view.findViewById(R.id.etpemail);
        ETEmail.setTypeface(custom_font);
        ETmyPassword = (EditText) view.findViewById(R.id.etpassowrd);
        ETmyPassword.setTypeface(custom_font);
        ETmobile = (EditText) view.findViewById(R.id.etmobile);
        ETmobile.setTypeface(custom_font);
        ETaltmobile = (EditText) view.findViewById(R.id.etaltmobile);
        ETaltmobile.setTypeface(custom_font);
        ETwebsite = (EditText) view.findViewById(R.id.etwebsite);
        ETwebsite.setTypeface(custom_font);
        radiomale = (RadioButton) view.findViewById(R.id.radioMale);
        radiomale.setTypeface(custom_font);
        radiofemale = (RadioButton) view.findViewById(R.id.radioFemale);
        radiofemale.setTypeface(custom_font);
        radioSexGroup = (RadioGroup) view.findViewById(R.id.radioSex);
        save = (Button) view.findViewById(R.id.btsave);
        save.setTypeface(custom_font);
        cancel = (Button) view.findViewById(R.id.btcancel);
        cancel.setTypeface(custom_font);

        ETname.setAlpha((float) 0.5);
        ETaddress.setAlpha((float) 0.5);
        ETcity.setAlpha((float) 0.5);
        ETcountry.setAlpha((float) 0.5);
        ETstate.setAlpha((float) 0.5);
        ETzipcode.setAlpha((float) 0.5);
        ETEmail.setAlpha((float) 0.5);
        ETmyPassword.setAlpha((float) 0.5);
        ETmobile.setAlpha((float) 0.5);
        ETaltmobile.setAlpha((float) 0.5);
        ETwebsite.setAlpha((float) 0.5);
        radiomale.setAlpha((float) 0.5);
        radiofemale.setAlpha((float) 0.5);
        tvgender.setAlpha((float) 0.5);

        ETname.setEnabled(false);
        ETaddress.setEnabled(false);
        ETcity.setEnabled(false);
        ETstate.setEnabled(false);
        ETzipcode.setEnabled(false);
        ETcountry.setEnabled(false);
        ETEmail.setEnabled(false);
        ETmyPassword.setEnabled(false);
        ETmobile.setEnabled(false);
        ETaltmobile.setEnabled(false);
        ETwebsite.setEnabled(false);
        radiomale.setClickable(false);
        radiofemale.setClickable(false);

    }//initialise

    private void SelectProfileData() {
        edit.setEnabled(true);

        cursor = sqLiteDatabaseRead.rawQuery("select * from sqtb_UserProfile", null);
        if (cursor.moveToFirst()) {
            do {
                ETname.setText(cursor.getString(0));
                String genders = cursor.getString(1);
                if (genders.equals("Male")) {
                    radiomale.setChecked(true);
                } else {
                    radiofemale.setChecked(true);
                }
                ETaddress.setText(cursor.getString(2));
                ETcity.setText(cursor.getString(3));
                ETstate.setText(cursor.getString(4));
                ETzipcode.setText(cursor.getString(6));
                ETcountry.setText(cursor.getString(5));
                ETEmail.setText(cursor.getString(7));
                ETmyPassword.setText(cursor.getString(8));
                ETmobile.setText(cursor.getString(9));
                String alt_mobile = cursor.getString(10);
                if (!alt_mobile.equals("null")) {
                    ETaltmobile.setText(alt_mobile);
                }
                ETwebsite.setText(cursor.getString(11));
            } while (cursor.moveToNext());
        }

        preEmail = ETEmail.getText().toString().trim();
        preMobile = ETmobile.getText().toString().trim();
        preAltMobile = ETaltmobile.getText().toString().trim();
        preWebsite = ETwebsite.getText().toString().trim();

    }//SelectProfileData

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        //  Toast.makeText(getActivity(), "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2),
        //  Toast.LENGTH_LONG).show();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        // Log.d("AutocompleteContacts", "Position:"+arg2+" Month:"+arg0.getItemAtPosition(arg2));

    }


    private void downloadImagesToSdCard(String downloadUrl) throws IOException {

        URL url = new URL(downloadUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        //////////////////////////////////
        connection.setRequestMethod("GET");
        //////////////////////////////////
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        Bitmap mBitmap = BitmapFactory.decodeStream(input);

        //Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
        //Bitmap mBitmap = BitmapFactory.decodeFile(String.valueOf(input));
        profileimg.setImageBitmap(mBitmap);

        File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA");
        if (!direct.exists()) {
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/CINTAA/");
            wallpaperDirectory.mkdirs();
        }
        String image = "" + member_id + ".png";
        File outputFile = new File(new File(Environment.getExternalStorageDirectory() + "/CINTAA/"), image);
        if (outputFile.exists()) {
            outputFile.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

            //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

      /*  try{
            URL url = new URL(downloadUrl); //you can write here any link

            File myDir =  new File("/sdcard/CINTAA/");
            //Something like ("/sdcard/file.mp3")


            if(!myDir.exists()){
                myDir.mkdir();
                Log.v("", "inside mkdir");

            }

            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = imageName;
            File file = new File (myDir, fname);
            if (file.exists ()) file.delete ();

             // Open a connection to that URL.
            URLConnection ucon = url.openConnection();
            InputStream inputStream = null;
            HttpURLConnection httpConn = (HttpURLConnection)ucon;
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = httpConn.getInputStream();
            }


            //  Define InputStreams to read from the URLConnection.

            // InputStream is = ucon.getInputStream();

             // Read bytes to the Buffer until there is nothing more to read(-1).


            FileOutputStream fos = new FileOutputStream(file);
            int size = 1024*1024;
            byte[] buf = new byte[size];
            int byteRead,bytesDownloaded=0;
            while (((byteRead = inputStream.read(buf)) != -1)) {
                fos.write(buf, 0, byteRead);
                bytesDownloaded += byteRead;
            }

            // Convert the Bytes read to a String.

            fos.close();

        }catch(IOException io)
        {
            io.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }*/
    }//downloadImagesToSdCard

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()


    private void showdialogcountry() {
        try {
            View customTitle = View.inflate(getActivity(), R.layout.titlecountry, null);
            builder1.setCustomTitle(customTitle);
            //  builder1.setTitle("Select Country");
            builder1.setSingleChoiceItems(strcountry, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ETcountry.setText(strcountry[i]);
                    dialogInterface.cancel();
                }
            });
            builder1.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialogcountry

    private void showChangePasswordDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.change_password_dialog, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        //  etnewpwd = (EditText) promptsView.findViewById(R.id.etnewpwd);
       /* etconfirmpwd = (EditText) promptsView.findViewById(R.id.etconfirmpwd);



         // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                // get user input and set it to result
                // edit text
                validationsalertbuilder();
                if(check1==3)
                {
                    ETmyPassword.setText(etnewpwd.getText().toString().trim());
                    Toast.makeText(getActivity(), "Password change successfully", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getActivity(), "password and confirm password does not match", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });
*/
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                Button negativeButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                //negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                //final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                //final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.colorAccent);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.colorAccent);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.bgColor);
                if (Build.VERSION.SDK_INT >= 16) {
                    //  negativeButton.setBackground(negativeButtonDrawable);
                    //  positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                    // negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    //positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });


        // show it
        alertDialog.show();
    }//showChangePasswordDialog

  /*
   public void validationsalertbuilder() {
        check1=0;
        if(!etnewpwd.getText().toString().equals(etconfirmpwd.getText().toString()))
        {
            etconfirmpwd.setError("password and confirm password does not match");
        }
        else {
            check1++;
        }
        if (etnewpwd.getText().toString().length() == 0  ) {
            etnewpwd.setError("Username name or password is incorrect");

        }
        else{
            check1++;
        }
        if (etconfirmpwd.getText().toString().length()==0) {
            etconfirmpwd.setError("Username name or password is incorrect");

        }else{check1++;}
        return;
    }
   */

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void selectgender() {
        int id = radioSexGroup.getCheckedRadioButtonId();
        RadioButton rbgender = (RadioButton) getView().findViewById(id);
        gender = rbgender.getText().toString().trim();
    }

    public void insert() {
      /*  try {
            new Encode_image().execute();
        }
        catch (Exception e1)
        {
            Toast.makeText(getActivity(),"Error in Uploading Image",Toast.LENGTH_SHORT).show();
        }*/

        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("name", ETname.getText().toString().trim());
        params.put("gender", gender);
        params.put("address", ETaddress.getText().toString().trim());
        params.put("city", ETcity.getText().toString().trim());
        params.put("state", ETstate.getText().toString().trim());
        params.put("country", ETcountry.getText().toString().trim());
        params.put("zip", ETzipcode.getText().toString().trim());
        params.put("email", ETEmail.getText().toString().trim());
        params.put("password", ETmyPassword.getText().toString().trim());
        params.put("mobile", ETmobile.getText().toString().trim());
        params.put("alt_mobile", ETaltmobile.getText().toString().trim());
        params.put("website", ETwebsite.getText().toString().trim());


        client.post(Config.urlmyprofile, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    showToast("Profile updated", Toast.LENGTH_LONG);
                    //   Intent intent=new Intent(getActivity(),RegisterLoginActivity.class);
                    // startActivity(intent);
                    editor.remove("name");
                    editor.remove("email");
                    editor.putString("name", ETname.getText().toString().trim());
                    editor.putString("email", ETEmail.getText().toString().trim());
                    editor.commit();

                    UpdateSQLite();

                    if ((!preEmail.equalsIgnoreCase(ETEmail.getText().toString().trim())) || (!preMobile.equalsIgnoreCase(ETmobile.getText().toString().trim()) || (!preAltMobile.equalsIgnoreCase(ETaltmobile.getText().toString().trim())) || (!preWebsite.equalsIgnoreCase(ETwebsite.getText().toString().trim())))) {
                        artistName = ETname.getText().toString().trim();
                        String name = "CINTAA Diary Artist Profile Update";
                        String username = Config.cintaaMail;
                        String password = Config.cintaaMailPwd;
                        String email = Config.profileUpdateMail;
                        String subject = "Profile Update - " + artistName;
                        sendMail(name, username, password, email, subject);
                    }

                    ETname.setText("");
                    ETaddress.setText("");
                    ETcity.setText("");
                    ETstate.setText("");
                    ETzipcode.setText("");
                    ETEmail.setText("");
                    ETmyPassword.setText("");
                    ETmobile.setText("");
                    ETaltmobile.setText("");
                    ETwebsite.setText("");
                    String s1 = response.getString("success");

                    Fragment fr = new HomeFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
                    //    Toast.makeText(getActivity(), s1, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }

    private void UpdateSQLite() {

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_UserProfile set name=? ,gender=? ,address=? ,city=? ,state=? ,country=? ,zip=? ,email=? ,password=? ,mobile=? ,alt_mobile=? ,website=?;");
        //name ,gender ,address ,city ,state ,country ,zip ,email ,password ,mobile ,alt_mobile ,website

        sqLiteStatement.bindString(1, ETname.getText().toString().trim());
        sqLiteStatement.bindString(2, gender);
        sqLiteStatement.bindString(3, ETaddress.getText().toString().trim());
        sqLiteStatement.bindString(4, ETcity.getText().toString().trim());
        sqLiteStatement.bindString(5, ETstate.getText().toString().trim());
        sqLiteStatement.bindString(6, ETcountry.getText().toString().trim());
        sqLiteStatement.bindString(7, ETzipcode.getText().toString().trim());
        sqLiteStatement.bindString(8, ETEmail.getText().toString().trim());
        sqLiteStatement.bindString(9, ETmyPassword.getText().toString().trim());
        sqLiteStatement.bindString(10, ETmobile.getText().toString().trim());
        sqLiteStatement.bindString(11, ETaltmobile.getText().toString().trim());
        sqLiteStatement.bindString(12, ETwebsite.getText().toString().trim());

        int n = sqLiteStatement.executeUpdateDelete();

        // Toast.makeText(getActivity(),"SQLite Success"+result,Toast.LENGTH_LONG).show();

    }//UpdateSQLite

    public void validation() {
        check = 0;
        {
            if (ETname.getText().toString().length() == 0) {
                ETname.setError("Name cannot be blank!");

            } else {
                check++;
            }
            if (ETaddress.getText().toString().length() == 0) {
                ETaddress.setError("Address cannot be blank!");

            } else {
                check++;
            }
            if (ETcity.getText().toString().length() == 0) {
                ETcity.setError("Enter Your City!");

            } else {
                check++;
            }
            if (ETstate.getText().toString().length() == 0) {
                ETstate.setError("Enter state!");

            } else {
                check++;
            }
            if (ETcountry.getText().toString().length() == 0) {
                ETcountry.setError("select country!");

            } else {
                check++;
            }
            if (!isValidEmaillId(ETEmail.getText().toString().trim())) {
                ETEmail.setError("Please Enter Valid Email Address");

            } else {
                check++;
            }
            if (ETmyPassword.getText().toString().length() == 0) {
                ETmyPassword.setError("Password is required!");

            } else {
                check++;
            }
            return;
        }
    }

    public void getdata() {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLmyprofilegetdata, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("Myprofiledata");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j1 = jsonArray.getJSONObject(i);
                            String name = j1.getString("name");
                            String gender = j1.getString("gender");
                            String address = j1.getString("address");
                            String city = j1.getString("city");
                            String state = j1.getString("state");
                            String zip = j1.getString("zip");
                            String country = j1.getString("country");
                            String email = j1.getString("email");
                            String password = j1.getString("password");
                            String mobile = j1.getString("mobile");
                            String website = j1.getString("website");
                            String profile_imgurl = j1.getString("profile_imgurl");
                            String alt_mobile = j1.getString("alt_mobile");
                            ETname.setText(name);
                            if (gender.equals("Male")) {
                                radiomale.setChecked(true);
                            } else {
                                radiofemale.setChecked(true);
                            }
                            ETaddress.setText(address);
                            ETcity.setText(city);
                            ETstate.setText(state);
                            ETzipcode.setText(zip);
                            ETcountry.setText(country);
                            ETEmail.setText(email);
                            ETmyPassword.setText(password);
                            ETmobile.setText(mobile);
                            if (!alt_mobile.equals(null)) {
                                ETaltmobile.setText(alt_mobile);
                            }
                            ETwebsite.setText(website);
                        }
                    }
                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//getdata()

    private void showChangeImgDialog() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View promptsView = li.inflate(R.layout.change_profile_dialog, null);
        // entry_id= sharedPreferences.getString("Completed_list_item","xcz");
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(li.inflate(R.layout.change_profile_dialog
                , null));

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        TextView gallery, remove, camera;
        gallery = (TextView) dialog.findViewById(R.id.tvGallery);
        remove = (TextView) dialog.findViewById(R.id.tvRemove);
        camera = (TextView) dialog.findViewById(R.id.tvCamera);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                permissionCode = 2;
                requestPermission();

                /*Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image*//*");
                startActivityForResult(
                        Intent.createChooser(intent, "Select File"),
                        SELECT_FILE);*/

                //Toast.makeText(getActivity(),"Gallery",Toast.LENGTH_SHORT).show();
                //  alertDialog.dismiss();

                //Save Image

           /*     try {
                    new Encode_image().execute();
                }
                catch (Exception e1)
                {
                    Toast.makeText(getActivity(),"Error in Uploading Image",Toast.LENGTH_SHORT).show();
                }
*/

            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Removedp();

                //Toast.makeText(getActivity(),"Remove",Toast.LENGTH_SHORT).show();
                // alertDialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                permissionCode = 1;
                requestPermission();

                /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
                hasPermissionInManifest(getActivity(), MediaStore.ACTION_IMAGE_CAPTURE);*/
                //Toast.makeText(getActivity(),"Camera",Toast.LENGTH_LONG).show();
                // alertDialog.dismiss();

                //Save Image


            }
        });

        // set dialog message
        dialog.setCancelable(true);

        // show it
        dialog.show();
        //alertDialog.show();

    }//showChangeImgDialog

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.v("tag", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else if (getActivity().checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.v("tag", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA}, 1);
            } else {
                Log.v("Tag", "Permission is granted");
                doTaskOnPermissionGranted();
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Tag", "Permission is granted");
            doTaskOnPermissionGranted();
        }
    }//isStoragePermissionGranted

    public void doTaskOnPermissionGranted() {
        if (permissionCode == 1) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
            hasPermissionInManifest(getActivity(), MediaStore.ACTION_IMAGE_CAPTURE);
        }
        if (permissionCode == 2) {

            /*Intent intent = new Intent(
                    Intent.ACTION_PICK);
            intent.setType("image*//*");
            startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    SELECT_FILE);*/

            Intent intent = new Intent();
// Show only images, no videos or anything else
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        bm = (Bitmap) data.getExtras().get("data");
       /* ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        profileimg.setImageBitmap(bm);


        //////////////////////////
        //Store Image on Server//
        /////////////////////////
        try {
            new Encode_image().execute();
        } catch (Exception e1) {
            showToast("Error in Uploading Image", Toast.LENGTH_SHORT);
        }


    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri uri = data.getData();
        String selectedImagePath = CommonHelper.getPathFromUri(getContext(), uri);
//        String path = String.valueOf(data.getData());
//        String selectedImagePath = "";
//        if (path.contains(".jpeg") || path.contains(".png") || path.contains(".jpg")) {
//            selectedImagePath = path.substring(6,path.length());
//        } else {
//
//           /* String[] projection = {MediaStore.Images.Media.DATA};
//
//            Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//
//
//            int columnIndex = cursor.getColumnIndex(projection[0]);
//            selectedImagePath = cursor.getString(columnIndex); // returns null
//            cursor.close();*/
//            String[] projection = {MediaStore.MediaColumns.DATA};
//            Cursor cursor = getActivity().managedQuery(uri, projection, null, null,null);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            cursor.moveToFirst();
//            selectedImagePath = cursor.getString(column_index);
//        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);
        profileimg.setImageBitmap(bm);
        //////////////////////////
        //Store Image on Server//
        /////////////////////////
        try {
            new Encode_image().execute();
        } catch (Exception e1) {
            showToast("Error in Uploading Image", Toast.LENGTH_SHORT);
        }
    }


    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }//hasPermissionInManifest

    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //makeRequest();
            updateProfileImage();
        }


        private void makeRequest() {

        }


    }//Encode_image


    //Rename image name
    private String createImgName() {
        String imgName = "";

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.ENGLISH);//yyyy-MM-dd HH:mm:ss.SSS
        Date now = new Date();
        String strDate = sdfDate.format(now);
        imgName = member_id + strDate;

        return imgName;
    }

    //Uploads Image on Server through PHP File
    private void updateProfileImage() {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("encoded_string", encoded_string);
        client.post(Config.URLupdateProfileImage, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast("Image Uploaded", Toast.LENGTH_LONG);

                    ///////////////////
                    //Store in SD card//
                    ////////////////////
                    File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA");
                    if (!direct.exists()) {
                        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/CINTAA/");
                        wallpaperDirectory.mkdirs();
                    }
                    String image = "" + member_id + ".png";
                    File outputFile = new File(new File(Environment.getExternalStorageDirectory() + "/CINTAA/"), image);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                        bm.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

            }
        });


    }//updateProfileImage

    private void showprogressdialog() {
        pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private void Removedp() {
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLupdateRemovedp, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast("Image Removed", Toast.LENGTH_LONG);

                    ///////////////////
                    //Delete from SD card//
                    ////////////////////
                    File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA");
                    if (!direct.exists()) {
                        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/CINTAA/");
                        wallpaperDirectory.mkdirs();
                    }
                    String image = "" + member_id + ".png";
                    File outputFile = new File(new File(Environment.getExternalStorageDirectory() + "/CINTAA/"), image);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    selectprofileimage();
                   /* try {
                        FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                        bm.compress(Bitmap.CompressFormat.PNG,100, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

            }
        });

    }//Removedp

    void selectprofileimage() {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        client.post(Config.URLprofileimgandname, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("Nameandimage");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject j1 = jsonArray.getJSONObject(i);
                        profile_url = j1.getString("profile_imgurl");

                    }

                    editor.putString("profile_imgurl", profile_url);
                    editor.commit();
                    Picasso.with(getActivity()).load(profile_url).resize(70, 70).into(profileimg);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//selectprofileinfo

    class AsyncDownloadImageToSDCard extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String downloadUrl = params[0];

            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                //////////////////////////////////
                connection.setRequestMethod("GET");
                //////////////////////////////////
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap mBitmap = BitmapFactory.decodeStream(input);

                //Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
                //Bitmap mBitmap = BitmapFactory.decodeFile(String.valueOf(input));
                //profileimg.setImageBitmap(mBitmap);

                File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA");
                if (!direct.exists()) {
                    File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/CINTAA/");
                    wallpaperDirectory.mkdirs();
                }
                String image = "" + member_id + ".png";
                File outputFile = new File(new File(Environment.getExternalStorageDirectory() + "/CINTAA/"), image);
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();

                    //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }//AsyncDownloadImageToSDCard

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    private void sendMail(String name, String username, String password, String email, String subject) {
        getSignIn(name, username, password, email, subject);
    }

    private void getSignIn(String name, String username, String password, String email, String subject) {
        if (!(isValidEmaillId(email))) {
            //etauthorityemail.setText("Please Enter Valid EmailId *");
        } else {
            if (!isNetworkConnected())  //if connection available
            {
                showToast("Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            } else {
                String message;

                message = "CINTAA Artist Profile Update - " + artistName + "<br/>" +
                        "Member ID - " + member_id + "<br/>" +
                        "Mobile - " + ETmobile.getText().toString().trim() + "<br/>" +
                        "Alternate Mobile - " + ETaltmobile.getText().toString().trim() + "<br/>" +
                        "Email - " + ETEmail.getText().toString().trim() + "<br/>" +
                        "Website - " + ETwebsite.getText().toString().trim() + "<br/>";

                sendMail(email, subject, message, name, username, password);

            }
        }

    }

    private void sendMail(String email, String subject, String messageBody, String name, String username, String password) {
        Session session = createSessionObject(username, password);

        try {
            Message message = createMessage(email, subject, messageBody,
                    session, username, name);
            new MyProfileFragment.SendMailTask(getActivity()).execute(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject,
                                  String messageBody, Session session, String username, String name) throws MessagingException,
            UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                email, email));
        message.setSubject(subject);
        //message.setText(messageBody);
        message.setContent(messageBody, "text/html");
        return message;
    }

    private Session createSessionObject(final String username, final String password) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", Config.smtpHost);
        properties.put("mail.smtp.port", Config.smtpPort);
//        properties.put("mail.smtp.port", "587");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }

    public class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        Activity ctx;

        public SendMailTask(Activity activity) {
            ctx = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ctx = getActivity();
            /*progressDialog = ProgressDialog.show(getActivity(),
                    "Please wait", "Sending mail", true, false);*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        /*Toast.makeText(SnapshotDetailsActivity.this,
                                "Passwod Send To Your Mail Successfully", Toast.LENGTH_LONG)
                                .show();*/
                    }
                });
            } catch (final MessagingException e) {
                e.printStackTrace();
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        ///https://www.google.com/settings/security/lesssecureapps
                        //go throught that link and select less security
                              /*  Toast.makeText(ForgotPasswordActivity.this,
                                        e.getClass() + " : " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();*/
                    }
                });
            }
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem register = menu.findItem(R.id.action_appTour);
        register.setVisible(false);
    }
}
