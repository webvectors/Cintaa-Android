package com.webvectors.cintaa2.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.webvectors.cintaa2.model.ScheduleEntryEditData;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.Dialog_Classes.CalendarAllEvents_Dialog_Class;
import com.webvectors.cintaa2.Dialog_Classes.ScheduleNewEntryOrEditCurrentDialog;
import com.webvectors.cintaa2.activity.HomeActivity;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.OnViewInflateListener;


public class CalendarFragment extends BaseFragment {

    private static CalendarFragment calendarFragment;
    List<Integer> datesList = new ArrayList<>();
    List<String> storedEvents, shiftTimeLocation;
    private CaldroidFragment caldroidFragment;
    ProgressDialog pd;
    String selectedDate, member_id;
    List<String> listShift, listLocation;
    SharedPreferences preferences;
    Dialog dialog;
    private FancyShowCaseView mTipFocusView;

    TextView tvToday, tvallEvents;
    List<String> DShiftTime, DLocation, DFilm, DProdHouse, DCallTime, DShiftTime1, DLocation1, DFilm1, DProdHouse1, DCallTime1;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    List<ScheduleEntryEditData> scheduleData;
    private Cursor cur;

    public CalendarFragment() {
        // Required empty public constructor
    }

    public static CalendarFragment getFragment() {

        return calendarFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        pd = new ProgressDialog(getActivity());
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        Log.e("--------", "calendarFragment");
        initialise(view);

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
//        String actionBarTitle = "Calendar";
        String actionBarTitle =getString(R.string.schedule_entry);
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        shiftTimeLocation = new ArrayList<>();
        storedEvents = new ArrayList<>();
        listShift = new ArrayList<>();
        listLocation = new ArrayList<>();
        storedEvents.clear();

        try {
            //entryId, scheduleDate, filmName, productionHouse, producerName
            cursor = sqLiteDatabaseRead.rawQuery("select entryId, scheduleDate, filmName, productionHouse, producerName,scheduleStatus from tbScheduleDetails where memberId =?", new String[]{member_id});
            if (cursor.moveToFirst()) {
                do {
                    String entryId, dateOfShoot, filmName, productionHouse, producerName, scheduleStatus;
                    entryId = cursor.getString(0);
                    dateOfShoot = cursor.getString(1);
                    filmName = cursor.getString(2);
                    productionHouse = cursor.getString(3);
                    producerName = cursor.getString(4);
                    Boolean callStatus = false;
                    scheduleStatus = cursor.getString(5);
                    try {
                        cur = sqLiteDatabaseRead.rawQuery("select callin_time from sqtb_diary_Callin where entry_id=?", new String[]{entryId});
                        if (cur.moveToFirst()) {
                            callStatus = true;
                        }
                    } finally {
                        if (cur != null)
                            cur.close();
                    }
                    scheduleData.add(new ScheduleEntryEditData(entryId, dateOfShoot, filmName, productionHouse, producerName, callStatus, scheduleStatus));
                } while (cursor.moveToNext());
                InitCalendar(savedInstanceState, scheduleData);
            } else {
            /*if (isNetworkConnected())
            {
				//get all stored event dates from database
				selectAllEvents(savedInstanceState);
			} else
			{
				showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);
			}*/
                InitCalendar(savedInstanceState, scheduleData);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        tvallEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogue();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
                    return true;
                }
                return false;
            }
        });


        return view;
    }//onCreateView


    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }
    }//onSaveInstanceState


    private void initialise(View view) {
        scheduleData = new ArrayList<ScheduleEntryEditData>();
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvToday = (TextView) view.findViewById(R.id.tvToday);
        tvToday.setTypeface(custom_font);
        tvallEvents = (TextView) view.findViewById(R.id.tvallEvents);
        tvallEvents.setTypeface(custom_font);

        final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd MMM", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        String currentDate = dateFormatter1.format(cal.getTime());
        tvToday.setText("Today, " + currentDate);
        tvallEvents.setEnabled(false);

    }//initialise

    private void showDialogue() {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("CalendarSelected_date", selectedDate);
        editor.putInt("CalendarEvents_List", DShiftTime.size());

        for (int s = 0; s < DShiftTime.size(); s++) {
            editor.putString("CalendarEvents_List_DShiftTime_" + s, DShiftTime.get(s));
            editor.putString("CalendarEvents_List_DLocation_" + s, DLocation.get(s));
            editor.putString("CalendarEvents_List_DFilm_" + s, DFilm.get(s));
            editor.putString("CalendarEvents_List_DProdHouse_" + s, DProdHouse.get(s));
            editor.putString("CalendarEvents_List_DCallTime_" + s, DCallTime.get(s));
        }
        editor.commit();

        new CalendarAllEvents_Dialog_Class().show(getFragmentManager(), "tag");

    }//showDialogue

    private void showprogressdialog() {
        pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }


    private void selectAllEvents(final Bundle savedInstanceState) {

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);

        client.post(Config.UrlSelectAllCalenderEvents, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {

                    scheduleData.clear();
                    JSONArray jsonArray = response.getJSONArray("sht_date");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String entry_id, film_tv_name, house_name, producer_name, shoot_date;
                        Boolean callStatus = false;
                        JSONObject j1 = jsonArray.getJSONObject(i);
                        entry_id = j1.getString("entry_id");
                        film_tv_name = j1.getString("film_tv_name");
                        house_name = j1.getString("house_name");
                        producer_name = j1.getString("producer_name");
                        shoot_date = j1.getString("shoot_date");
                        callStatus = j1.getBoolean("callStatus");
                        //entryId,dateOfShoot,filmName,productionHouse,producerName
                        scheduleData.add(new ScheduleEntryEditData(entry_id, shoot_date, film_tv_name, house_name, producer_name, callStatus));
                    }
                    insertCalendarDataSqlite(scheduleData);
                    InitCalendar(savedInstanceState, scheduleData);
                    tvallEvents.setEnabled(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                InitCalendar(savedInstanceState, scheduleData);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//selectAllEvents

    private void insertCalendarDataSqlite(List<ScheduleEntryEditData> scheduleData) {
        //Delete Old Values from SQLite
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbScheduleDetails");
        int res = sqLiteStatement.executeUpdateDelete();

        for (int k = 0; k < scheduleData.size(); k++) {
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbScheduleDetails values(?,?,?,?,?,?,?,?)");
            //memberId, entryId, scheduleDate, filmName, productionHouse, producerName from tbScheduleDetails
            sqLiteStatement.bindString(1, member_id);
            sqLiteStatement.bindString(2, scheduleData.get(k).getEntryId());
            sqLiteStatement.bindString(3, "" + scheduleData.get(k).getDateOfShoot());
            sqLiteStatement.bindString(4, scheduleData.get(k).getFilmName());
            sqLiteStatement.bindString(5, scheduleData.get(k).getProductionHouse());
            sqLiteStatement.bindString(6, scheduleData.get(k).getProducerName());
            sqLiteStatement.bindString(7, "Pending");
            sqLiteStatement.bindString(8, "done");
            long result = sqLiteStatement.executeInsert();

            if (scheduleData.get(k).getCallStatus()) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                // entry_id, callin_time, callin_lat, callin_lng, Status
                sqLiteStatement.bindString(1, scheduleData.get(k).getEntryId());
                sqLiteStatement.bindString(2, "0");
                sqLiteStatement.bindString(3, "0");
                sqLiteStatement.bindString(4, "0");
                sqLiteStatement.bindString(5, "done");
            }
        }
    }//InsertCalendarData_Sqlite

    private void InitCalendar(Bundle savedInstanceState, List<ScheduleEntryEditData> scheduleData) {
        final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        caldroidFragment = new CaldroidFragment();

        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState, "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        Bundle args = new Bundle();
        //Costum look setting
        args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
        caldroidFragment.setArguments(args);

        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        caldroidFragment.setArguments(args);


        try {
            setCustomResourceForDates(scheduleData);
        } catch (ParseException e) {
            e.printStackTrace();
        }

////////***************Here getChildFragmentManager is Replaced with getFragmentManager*******************//
        FragmentTransaction t = getChildFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();
////////***************getChildFragmentManager is Replaced with getFragmentManager*******************//

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                functionSelectDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
                String text = "month: " + month + " year: " + year;
                //    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClickDate(Date date, View view) {
                //Long click + formatter.format(date)
            }

            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
                    //Caldroid view is created
                }
            }
        };
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
    }//InitCalendar

    private void functionSelectDate(Date date) {
        final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date currentDate = cal.getTime();
        if (date.before(currentDate)) {
            selectedDate = formatter.format(date);
            String currentDay = formatter.format(currentDate);
            if (selectedDate.equalsIgnoreCase(currentDay)) {
                //Is Current Date
                //showToast("Current Date Entry Cannot be Edited",Toast.LENGTH_LONG);
                //getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                selectedDate = formatter.format(date);
                List<ScheduleEntryEditData> scheduleDataOnDate = checkScheduleExists(selectedDate);
                if (scheduleDataOnDate.size() > 0) {
                    showDialogueNewEntryOrEditCurrent(scheduleDataOnDate);
                } else {
                    Fragment fragment = new ScheduleEntryEditFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("scheduleEntryDate", selectedDate);
                    fragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                }
            } else {
                //Is Date Before Current Date
                selectedDate = formatter.format(date);
                List<ScheduleEntryEditData> scheduleDataOnDate = checkScheduleExists(selectedDate);
                if (scheduleDataOnDate.size() > 0) {
                    showDialogPastScheduleDetails(scheduleDataOnDate);
                }
            }
        } else {
            //Is Date After Current Date
            selectedDate = formatter.format(date);
            List<ScheduleEntryEditData> scheduleDataOnDate = checkScheduleExists(selectedDate);
            if (scheduleDataOnDate.size() > 0) {
                showDialogueNewEntryOrEditCurrent(scheduleDataOnDate);
            } else {
                Fragment fragment = new ScheduleEntryEditFragment();
                Bundle bundle = new Bundle();
                bundle.putString("scheduleEntryDate", selectedDate);
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        }
    }//functionSelectDate

    private List<ScheduleEntryEditData> checkScheduleExists(String selectedDate) {
        List<ScheduleEntryEditData> scheduleDataOnDate = new ArrayList<ScheduleEntryEditData>();
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select entryId, filmName, productionHouse, producerName, scheduleStatus from tbScheduleDetails where memberId =? and scheduleDate = ?", new String[]{member_id, selectedDate});
            if (cursor.moveToFirst()) {
                do {
                    String entryId, dateOfShoot, filmName, productionHouse, producerName, scheduleStatus;
                    entryId = cursor.getString(0);
                    filmName = cursor.getString(1);
                    productionHouse = cursor.getString(2);
                    producerName = cursor.getString(3);
                    Boolean callStatus = false;
                    scheduleStatus = cursor.getString(4);
                    Cursor cur = sqLiteDatabaseRead.rawQuery("select callin_time from sqtb_diary_Callin where entry_id='" + entryId + "'", null);
                    if (cur.moveToFirst()) {
                        callStatus = true;
                    }
                    scheduleDataOnDate.add(new ScheduleEntryEditData(entryId, selectedDate, filmName, productionHouse, producerName, callStatus, scheduleStatus));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return scheduleDataOnDate;
    }//checkScheduleExists

    private void showDialogPastScheduleDetails(List<ScheduleEntryEditData> scheduleDataOnDate) {
        ArrayList<String> entryId = new ArrayList<String>();
        for (ScheduleEntryEditData schedule : scheduleDataOnDate) {
            entryId.add(schedule.getEntryId());
        }
        CalendarAllEvents_Dialog_Class dialog = new CalendarAllEvents_Dialog_Class();
        Bundle args = new Bundle();
        args.putString("dateOfShoot", "" + scheduleDataOnDate.get(0).getDateOfShoot());
        args.putStringArrayList("entryId", entryId);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), "tag");
    }//showDialogPastScheduleDetails

    private void showDialogueNewEntryOrEditCurrent(List<ScheduleEntryEditData> scheduleDataOnDate) {
        ArrayList<String> entryId = new ArrayList<String>();
        ArrayList<String> filmName = new ArrayList<String>();
        ArrayList<String> callStatus = new ArrayList<String>();
        ArrayList<String> scheduleStatus = new ArrayList<String>();
        for (int i = 0; i < scheduleDataOnDate.size(); i++) {
            entryId.add(scheduleDataOnDate.get(i).getEntryId());
            filmName.add(scheduleDataOnDate.get(i).getFilmName());
            callStatus.add(String.valueOf(scheduleDataOnDate.get(i).getCallStatus()));
            scheduleStatus.add(scheduleDataOnDate.get(i).getScheduleStatus());
        }
        ScheduleNewEntryOrEditCurrentDialog dialog = new ScheduleNewEntryOrEditCurrentDialog();
        Bundle args = new Bundle();
        args.putString("dateOfShoot", "" + scheduleDataOnDate.get(0).getDateOfShoot());
        args.putStringArrayList("entryId", entryId);
        args.putStringArrayList("filmName", filmName);
        args.putStringArrayList("callStatus", callStatus);
        args.putStringArrayList("scheduleStatus", scheduleStatus);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), "tag");
    }//showDialogueNewEntryOrEditCurrent

    //Sets event color on the calendar
    private void setCustomResourceForDates(List<ScheduleEntryEditData> scheduleData) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date currentDate = cal.getTime();
        if (caldroidFragment != null) {
            ColorDrawable calendarCurrbg = new ColorDrawable(getResources().getColor(R.color.calendarEventDetailbg));
            caldroidFragment.setBackgroundDrawableForDate(calendarCurrbg, currentDate);
            caldroidFragment.setTextColorForDate(R.color.white, currentDate);
        }
        //calulate difference between current date and the storedEvent date in number of days
        List<Integer> datesList = dateDifference(scheduleData);
        for (int date = 0; date < datesList.size(); date++) {
            cal = Calendar.getInstance();
            Date EventDay;
            cal.add(Calendar.DATE, datesList.get(date));
            EventDay = cal.getTime();
            if (caldroidFragment != null) {
                ColorDrawable calendarEventdate = new ColorDrawable(getResources().getColor(R.color.calendarEventDatebg));
                caldroidFragment.setBackgroundDrawableForDate(calendarEventdate, EventDay);
                caldroidFragment.setTextColorForDate(R.color.white, EventDay);
            }
        }
    }//setCustomResourceForDates

    //Calculate difference between current date and date of event in number of days
    private List<Integer> dateDifference(List<ScheduleEntryEditData> scheduleData) throws ParseException {
        Date currDate, eventDate;
        final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        String currentDate = dateFormatter1.format(cal.getTime());
        currDate = dateFormatter1.parse(currentDate);
        for (int i = 0; i < scheduleData.size(); i++) {
            eventDate = dateFormatter1.parse(scheduleData.get(i).getDateOfShoot());
            /*eventDate = scheduleData.get(i).getDateOfShoot();*/
            long diff = eventDate.getTime() - currDate.getTime();
            int m = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            datesList.add(m);
        }
        return datesList;
    }//dateDifference

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    public void hideTip() {
        if (mTipFocusView != null && mTipFocusView.isShown())
            mTipFocusView.hide();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calendarFragment = this;
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:
                showTip1();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mTipFocusView != null && mTipFocusView.isShown())
                mTipFocusView.hide();
        }
    };

    private void showTip1() {
        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(View view) {
                        view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTipFocusView.hide();
                                showTipCalanderTip();
                            }
                        });

                        TextView tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);
                        tvWelcome.setText(R.string.schedule_entry);
                        tvWelcome.setVisibility(View.VISIBLE);

                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                        tvTip.setText(R.string.tip_schedule_create_view_edit);

                        view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                    }
                })
                .closeOnTouch(false)
                .build();
        mTipFocusView.show();
    }

    private void showTipCalanderTip() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_calander, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTipEditDialog();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_schedule_navigate_datewise);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    }

    private void showTipEditDialog() {
        mTipFocusView.hide();
        HomeActivity.ivSettingMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_edit_dialog, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showTip3();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_schedule_edit_dialog);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivSettingMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivSettingMenu.performClick();
    } //Tip2

    private void showTip3() {
        mTipFocusView.hide();

        HomeActivity.ivMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setText(R.string.btn_close);
                                    btnSkip.setOnClickListener(mSkipClickListener);

                                    View view2 = (View) view.findViewById(R.id.view);
                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_create_new_or_edit_existing_schedule);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusOn(HomeActivity.ivMenu)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);


        HomeActivity.ivMenu.performClick();


    } // Tip3
}
