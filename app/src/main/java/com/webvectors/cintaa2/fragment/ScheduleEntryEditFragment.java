package com.webvectors.cintaa2.fragment;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.games.Game;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.utility.GlobalClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.OnViewInflateListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleEntryEditFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {


    AutoCompleteTextView etfilmname, etproductionHouse, etProducerName;
    EditText etdtshoot;
    TextView tvTitle, tvsaveandcreate, tvcancel, tvcalltime;
    Button btnsave, btnCancel, btnUpdate, btnDelete;
    LinearLayout llCreateMode, llEditeMode, llCallTime;
    private ArrayAdapter<String> adapterFilmName, adapterProductionHouse, adapterProducerName;
    List<String> filmNameList, productionHouseList, producerNameList;
    ProgressDialog pd;
    private FancyShowCaseView mTipFocusView;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    SharedPreferences preferences;
    String member_id, alert_flag, initialCallTime = "callTime", initialFilmName = "fm", initialProductionHouse = "pd", initialProducerName = "pn";
    Boolean saveAndCreate = false, sqtbDiaryEntryIsInsert = true;
    private LinearLayout lldateshoot, llprodhouse, llfilmName, llprodName;
    private ScrollView mainScroll;

    public ScheduleEntryEditFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        Log.e("-----", "ScheduleEntryEditFragment");
        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface fonts = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "Schedule Entry";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", fonts), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);

        String scheduleEntryDate = getArguments().getString("scheduleEntryDate");
        final String entryID = getArguments().getString("entryID");
        String filmName = getArguments().getString("filmName");
        GlobalClass.printLog(getActivity(), "entryID:-AAAAAAAAA----" + entryID);
        View view = inflater.inflate(R.layout.fragment_schedule_entry_edit, container, false);
        initialise(view);
        etdtshoot.setText(scheduleEntryDate);
        if (entryID != null) {
            llEditeMode.setVisibility(View.VISIBLE);
            llCreateMode.setVisibility(View.GONE);
            try {
                cursor = sqLiteDatabaseRead.rawQuery("select filmName, productionHouse, producerName from tbScheduleDetails where " +
                        "entryId = '" + entryID + "'", null);
                if (cursor.moveToFirst()) {
                    etfilmname.setText(cursor.getString(0));
                    initialFilmName = cursor.getString(0);
                    etproductionHouse.setText(cursor.getString(1));
                    initialProductionHouse = cursor.getString(1);
                    etProducerName.setText(cursor.getString(2));
                    initialProducerName = cursor.getString(2);
                    btnsave.setVisibility(View.GONE);

                    cursor = sqLiteDatabaseRead.rawQuery("select call_time from sqtb_diary_entry where entry_id='" + entryID + "'", null);
                    if (cursor.moveToFirst()) {
                        tvcalltime.setText(cursor.getString(0));
                        initialCallTime = cursor.getString(0);
                        sqtbDiaryEntryIsInsert = false;
                    }
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } else {
            llCreateMode.setVisibility(View.VISIBLE);
            llEditeMode.setVisibility(View.GONE);
        }

        try {
            alert_flag = "ON";
            cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    alert_flag = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        llCallTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcalltime();

            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate() == 3) {
                    saveAndCreate = false;
                    if (Config.isNetworkConnected(getActivity())) {
                        createSchedule();
                    } else {
                        final String entry_id = getCurrentTimeStamp();
                        insertSchedule(entry_id, false);
                        showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
                        getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                    }
                } else {
                    showToast("Required Fields are Missing", Toast.LENGTH_LONG);
                }
            }
        });
        tvsaveandcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate() == 3) {
                    saveAndCreate = true;
                    if (Config.isNetworkConnected(getActivity())) {
                        createSchedule();
                    } else {
                        final String entry_id = getCurrentTimeStamp();
                        insertSchedule(entry_id, false);
                        showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
                    }

                } else {
                    showToast("Required Fields are Missing", Toast.LENGTH_LONG);
                }
            }
        });
        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate() == 3) {
                    cursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where " +
                            "entryId='" + entryID + "'", null);
                    if (cursor.moveToFirst() & cursor != null) {
                        if (cursor.getString(0).equals("no")) {
                            updateScheduleSQLite(entryID, false, Config.isNetworkConnected(getActivity()));
                        } else {
                            if (Config.isNetworkConnected(getActivity())) {
                                updateSchedule(entryID);
                            } else {
                                updateScheduleSQLite(entryID, true, false);
                                showToast("Network Not Available, Changes Made in Offline Mode", Toast.LENGTH_SHORT);
                                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                            }
                        }
                    }
                } else {
                    showToast("Required Fields are Missing", Toast.LENGTH_SHORT);
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalClass.printLog(getActivity(), "clikkkkkk-------------------------------");
                deleteSchedule(entryID);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();
                    return true;
                }
                return false;
            }
        });

        return view;
    }//onCreate

    private void initialise(View view) {
        filmNameList = new ArrayList<String>();
        productionHouseList = new ArrayList<String>();
        producerNameList = new ArrayList<String>();

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        Typeface customFont = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        etdtshoot = (EditText) view.findViewById(R.id.etdtshoot);
        etdtshoot.setTypeface(customFont);
        etfilmname = (AutoCompleteTextView) view.findViewById(R.id.etfilmname);
        etfilmname.setTypeface(customFont);
        etproductionHouse = (AutoCompleteTextView) view.findViewById(R.id.etproductionHouse);
        etproductionHouse.setTypeface(customFont);
        etProducerName = (AutoCompleteTextView) view.findViewById(R.id.etProducerName);
        etProducerName.setTypeface(customFont);
        /*tvTitle = (TextView)view.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(customFont);*/
        tvsaveandcreate = (TextView) view.findViewById(R.id.tvsaveandcreate);
        tvsaveandcreate.setTypeface(customFont);
        tvcancel = (TextView) view.findViewById(R.id.tvcancel);
        tvcancel.setTypeface(customFont);
        btnsave = (Button) view.findViewById(R.id.btnsave);
        btnsave.setTypeface(customFont);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(customFont);
        btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        btnUpdate.setTypeface(customFont);
        llCreateMode = (LinearLayout) view.findViewById(R.id.llCreateMode);
        llEditeMode = (LinearLayout) view.findViewById(R.id.llEditeMode);
        btnDelete = (Button) view.findViewById(R.id.btnDelete);
        btnDelete.setTypeface(customFont);
        llCallTime = (LinearLayout) view.findViewById(R.id.llCallTime);
        mainScroll = (ScrollView) view.findViewById(R.id.mainScroll);
        lldateshoot = (LinearLayout) view.findViewById(R.id.lldateshoot);
        llprodhouse = (LinearLayout) view.findViewById(R.id.llprodhouse);
        llfilmName = (LinearLayout) view.findViewById(R.id.llfilmName);
        llprodName = (LinearLayout) view.findViewById(R.id.llprodName);

        tvcalltime = (TextView) view.findViewById(R.id.tvcalltime);
        tvcalltime.setTypeface(customFont);

        fillAutoComplete();
    }//initialise

    private void fillAutoComplete() {
        filmNameList.clear();
        producerNameList.clear();
        productionHouseList.clear();
        // scheduleDate,
        cursor = sqLiteDatabaseRead.rawQuery("select filmName, productionHouse, producerName from tbScheduleDetails", null);
        if (cursor.moveToFirst()) {
            do {
                filmNameList.add(cursor.getString(0));
                productionHouseList.add(cursor.getString(1));
                producerNameList.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor = sqLiteDatabaseRead.rawQuery("select productionHouse, producerName from tbProductionHouse", null);
        if (cursor.moveToFirst()) {
            do {
                productionHouseList.add(cursor.getString(0));
                producerNameList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        List<String> filmNameRList = eliminateRedundancy(filmNameList);
        List<String> productionHouseRList = eliminateRedundancy(productionHouseList);
        List<String> producerNameRList = eliminateRedundancy(producerNameList);

        String[] productionHouse = new String[productionHouseRList.size()];
        productionHouseRList.toArray(productionHouse);
        String[] filmName = new String[filmNameRList.size()];
        filmNameRList.toArray(filmName);
        String[] producerName = new String[producerNameRList.size()];
        producerNameRList.toArray(producerName);

        if (getActivity() != null) {
            adapterFilmName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, filmName);
            adapterProductionHouse = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, productionHouse);
            adapterProducerName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, producerName);

            etproductionHouse.setThreshold(1);
            etproductionHouse.setAdapter(adapterProductionHouse);
            etproductionHouse.setOnItemSelectedListener(this);
            etproductionHouse.setOnItemClickListener(this);

            etfilmname.setThreshold(1);
            etfilmname.setAdapter(adapterFilmName);
            etfilmname.setOnItemSelectedListener(this);
            etfilmname.setOnItemClickListener(this);

            etProducerName.setThreshold(1);
            etProducerName.setAdapter(adapterProducerName);
        }
    }//fillAutoComplete

    private List<String> eliminateRedundancy(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); ) {
                if (list.get(j).equals(list.get(i))) {
                    list.remove(j);
                } else
                    j++;
            }
        }
        return list;
    }//eliminateRedundancy

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        fillSuggestions((String) adapterView.getItemAtPosition(i));
       /* for(int j=0; j<filmNameList.size(); j++){
            if(filmNameList.get(j).equals(adapterView.getItemAtPosition(i))){
                etproductionHouse.setText(productionHouseList.get(j));
                etProducerName.setText(producerNameList.get(j));
            }
        }
        for(int j=0; j<productionHouseList.size(); j++){
            if(productionHouseList.get(j).equals(adapterView.getItemAtPosition(i))){
                etProducerName.setText(producerNameList.get(j));
            }
        }*/
    }//onItemClick

    private void fillSuggestions(String filmOrProductionHouse) {
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select productionHouse, producerName from tbScheduleDetails where filmName = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etproductionHouse.setText(cursor.getString(0));
                etProducerName.setText(cursor.getString(1));
            }

            cursor = sqLiteDatabaseRead.rawQuery("select producerName from tbScheduleDetails where productionHouse = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etProducerName.setText(cursor.getString(0));
            }

            cursor = sqLiteDatabaseRead.rawQuery("select producerName from tbProductionHouse where productionHouse =?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etProducerName.setText(cursor.getString(0));
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//fillSuggestions

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }//onItemSelected

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }//onNothingSelected

    public String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return member_id + "_" + strDate;
    }//getCurrentTimeStamp

    private void showprogressdialog() {
        pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.loading_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    private void createSchedule() {

        final String entry_id = getCurrentTimeStamp();

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("member_id", member_id);
        params.put("ScheduleDate", etdtshoot.getText().toString().trim());
        params.put("filmName", etfilmname.getText().toString().trim());
        params.put("productionHouseName", etproductionHouse.getText().toString().trim());
        params.put("producerName", etProducerName.getText().toString().trim());

        client.post(Config.URLinsertNewSchedule, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    showToast("Entry done successfully", Toast.LENGTH_LONG);

                    insertSchedule(entry_id, true);
                    if (!saveAndCreate) {
                        getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                showToast("Network Error, Entry Saved Offline !!", Toast.LENGTH_LONG);
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                insertSchedule(entry_id, false);
                if (!saveAndCreate) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                }
            }
        });

    }//createSchedule

    private void insertSchedule(String entry_id, Boolean online) {
        //entryId, scheduleDate, filmName, productionHouse, producerName
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbScheduleDetails values(?,?,?,?,?,?,?,?)");
        sqLiteStatement.bindString(1, member_id);
        sqLiteStatement.bindString(2, entry_id);
        sqLiteStatement.bindString(3, etdtshoot.getText().toString().trim());
        sqLiteStatement.bindString(4, etfilmname.getText().toString().trim());
        sqLiteStatement.bindString(5, etproductionHouse.getText().toString().trim());
        sqLiteStatement.bindString(6, etProducerName.getText().toString().trim());
        sqLiteStatement.bindString(7, "pending");
        if (online) {
            sqLiteStatement.bindString(8, "done");
        } else {
            sqLiteStatement.bindString(8, "no");
        }
        long result = sqLiteStatement.executeInsert();

        if (tvcalltime.getText().toString().trim().length() > 0) {
            String date = etdtshoot.getText().toString().trim();
            String hours = tvcalltime.getText().toString().trim();
            final String dateHours = date + " " + hours;
            long seconds = CalculateSecondsFromCurrentDate(dateHours);
            int requestCode = startAlert(seconds, entry_id);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry(entry_id, member_id, " +
                    "call_time, requestCode_Alarm, alert_flag) values(?,?,?,?,?)");
            sqLiteStatement.bindString(1, entry_id);
            sqLiteStatement.bindString(2, member_id);
            sqLiteStatement.bindString(3, tvcalltime.getText().toString());
            sqLiteStatement.bindLong(4, requestCode);
            sqLiteStatement.bindString(5, alert_flag);
            long res = sqLiteStatement.executeInsert();
        }

        fillAutoComplete();
        etfilmname.setText("");
        etproductionHouse.setText("");
        etProducerName.setText("");
        tvcalltime.setText("");
    }//insertSchedule

    private void updateSchedule(final String entryID) {
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entryID);
        params.put("member_id", member_id);
        params.put("ScheduleDate", etdtshoot.getText().toString().trim());
        params.put("filmName", etfilmname.getText().toString().trim());
        params.put("productionHouseName", etproductionHouse.getText().toString().trim());
        params.put("producerName", etProducerName.getText().toString().trim());

        client.post(Config.URLupdateSchedule, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    showToast("Changes Saved successfully", Toast.LENGTH_LONG);
                    updateScheduleSQLite(entryID, true, true);
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                showToast("Error in Connection", Toast.LENGTH_LONG);
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }//updateSchedule

    private void updateScheduleSQLite(String entryID, Boolean isOnServer, Boolean isNetworkAvailable) {
        if (isOnServer && isNetworkAvailable) {
            //entryId, scheduleDate, filmName, productionHouse, producerName
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
                    "filmName='" + etfilmname.getText().toString().trim() + "', " +
                    "productionHouse='" + etproductionHouse.getText().toString().trim() + "', " +
                    "producerName='" + etProducerName.getText().toString().trim() + "', " +
                    "ServerEntryStatus='done' where entryId = '" + entryID + "'");
            long result = sqLiteStatement.executeUpdateDelete();
        } else if (!isOnServer) {
            //entryId, scheduleDate, filmName, productionHouse, producerName
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
                    "filmName='" + etfilmname.getText().toString().trim() + "', " +
                    "productionHouse='" + etproductionHouse.getText().toString().trim() + "', " +
                    "producerName='" + etProducerName.getText().toString().trim() + "', ServerEntryStatus='no' where entryId = '" + entryID + "'");
            long result = sqLiteStatement.executeUpdateDelete();
        } else if (isOnServer && (!isNetworkAvailable)) {
            if ((!initialFilmName.equals(etfilmname.getText().toString().trim())) || (!initialProducerName.equals(etProducerName.getText().toString().trim())) || (!initialProductionHouse.equals(etproductionHouse.getText().toString().trim()))) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
                        "filmName='" + etfilmname.getText().toString().trim() + "', " +
                        "productionHouse='" + etproductionHouse.getText().toString().trim() + "', " +
                        "producerName='" + etProducerName.getText().toString().trim() + "', ServerEntryStatus='update' where entryId = '" + entryID + "'");
                long result = sqLiteStatement.executeUpdateDelete();
            }
        }

        if ((tvcalltime.getText().toString().trim().length() > 0) & (!initialCallTime.equals(tvcalltime.getText().toString().trim()))) {
            SetAlertOFF(entryID);
            String date = etdtshoot.getText().toString().trim();
            String hours = tvcalltime.getText().toString().trim();
            final String dateHours = date + " " + hours;
            long seconds = CalculateSecondsFromCurrentDate(dateHours);
            int requestCode = startAlert(seconds, entryID);

            if (sqtbDiaryEntryIsInsert) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry(entry_id, member_id, " +
                        "call_time, requestCode_Alarm, alert_flag) values(?,?,?,?,?)");
                sqLiteStatement.bindString(1, entryID);
                sqLiteStatement.bindString(2, member_id);
                sqLiteStatement.bindString(3, tvcalltime.getText().toString());
                sqLiteStatement.bindLong(4, requestCode);
                sqLiteStatement.bindString(5, alert_flag);
                long res = sqLiteStatement.executeInsert();
            } else {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_diary_entry set call_time=?, alert_flag=?," +
                        "requestCode_Alarm=? where entry_id='" + entryID + "'");
                sqLiteStatement.bindString(1, tvcalltime.getText().toString().trim());
                sqLiteStatement.bindString(2, alert_flag);
                sqLiteStatement.bindLong(3, requestCode);
                long result = sqLiteStatement.executeUpdateDelete();
            }
        }
        etfilmname.setText("");
        etproductionHouse.setText("");
        etProducerName.setText("");
        tvcalltime.setText("");
    }//updateScheduleSQLite

    private void deleteSchedule(String entryID) {
        GlobalClass.printLog(getActivity(), "------deleteSchedule-------------------------" + entryID);

        try {
            cursor = sqLiteDatabaseRead.rawQuery("select ServerEntryStatus from tbScheduleDetails where entryId=?", new String[]{entryID});
            GlobalClass.printLog(getActivity(), "------cursor-------------------------" + cursor.getCount());

            if (cursor.moveToFirst() & cursor != null) {
                if (cursor.getString(0).equals("no")) {
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbScheduleDetails where entryId='" + entryID + "'");
                    long result = sqLiteStatement.executeUpdateDelete();
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_entry where entry_id='" + entryID + "'");
                    result = sqLiteStatement.executeUpdateDelete();
                    showToast("Schedule Deleted Successfully", Toast.LENGTH_LONG);
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                } else {
                    if (Config.isNetworkConnected(getActivity())) {
                        deleteScheduleServer(entryID);
                    } else {
                        showToast("Netwrok Not Available", Toast.LENGTH_SHORT);
                    }
                }
            } else {

                GlobalClass.printLog(getActivity(), "------cursor--------------else");

                if (Config.isNetworkConnected(getActivity())) {
                    deleteScheduleServer(entryID);
                } else {
                    showToast("Netwrok Not Available", Toast.LENGTH_SHORT);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }//deleteSchedule

    private void deleteScheduleServer(final String entryID) {
        GlobalClass.printLog(getActivity(), "deleteScheduleServer________________entryID:-----" + entryID);

        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entryID);
        params.put("member_id", member_id);//2017-02-02 18:00:52.248
        client.post(Config.URLdeleteSchedule, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    GlobalClass.printLog(getActivity(), "response:-----" + response);

                    GlobalClass.printLog(getActivity(), "s1:-----" + s1);
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbScheduleDetails where entryId='" + entryID + "'");
                    long result = sqLiteStatement.executeUpdateDelete();
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_entry where entry_id='" + entryID + "'");
                    result = sqLiteStatement.executeUpdateDelete();
                    GlobalClass.printLog(getActivity(), "result:-----" + result);

                    showToast("Schedule Deleted Successfully", Toast.LENGTH_LONG);
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from tbScheduleDetails where entryId='" + entryID + "'");
                long result = sqLiteStatement.executeUpdateDelete();
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("delete from sqtb_diary_entry where entry_id='" + entryID + "'");
                result = sqLiteStatement.executeUpdateDelete();
                showToast("Schedule Deleted Successfully", Toast.LENGTH_LONG);
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }//deleteScheduleServer

    private int validate() {
        int check = 0;
        if (etfilmname.getText().toString().trim().length() > 0) {
            check += 1;
        } else {
            etfilmname.setError("Film Name is Required");
        }
        if (etproductionHouse.getText().toString().trim().length() > 0) {
            check += 1;
        } else {
            etproductionHouse.setError("Production House Name is Required");
        }
        if (etProducerName.getText().toString().trim().length() > 0) {
            check += 1;
        } else {
            check += 1;
            //etProducerName.setError("Producer's Name is Required");
        }
        return check;
    }//validate

    com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener timeSet = new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
            String minuteString = minute < 10 ? "0" + minute : "" + minute;
            String secondString = second < 10 ? "0" + second : "" + second;
            String day_night = "AM";
            int hours = hourOfDay;
            if (hourOfDay >= 12) {
                day_night = "PM";

            }

            if (hourOfDay > 12) {

                hours = hourOfDay - 12;
            }

            String time = hours + ":" + minuteString + " " + day_night;
            tvcalltime.setText(time);
        }
    };

    public void showcalltime() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                timeSet,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(true);
        tpd.enableMinutes(true);
        tpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }//showcalltime

    //Calculate Time in seconds from Current Date to Given Date
    private long CalculateSecondsFromCurrentDate(String dateHours) {

        long seconds = 0;
        Date now, eventDate, currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
            eventDate = sdfformat.parse(dateHours);

            //in milliseconds
            long diff = eventDate.getTime() - currentDate.getTime();

            long diffSeconds = diff / 1000;
            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);

            long on_head = diffSeconds + diffMinutes + diffHours + diffDays;
            String alert = "00:00";
            cursor = sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    alert = cursor.getString(0);
                } while (cursor.moveToNext());
            }

            String[] units = alert.split(":"); //will break the string up into an array
            long hours = Integer.parseInt(units[0]); //hours element
            long minutes = Integer.parseInt(units[1]); //minutes element
            long before_head = 60 * 60 * hours + minutes * 60; //add up our values

            before_head = on_head - before_head;

            if (before_head > 0) {
                seconds = before_head;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate

    public int startAlert(long seconds, String entry_id) {
        String check_alert_setting = "ON";
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    check_alert_setting = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        int requestCode = (int) (new Date().getTime() / 1000);
        // If alerts are ON in setting menu then only set Alarm for the Event
        if (check_alert_setting.equals("ON") && seconds > 0) {
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
//            intent.putExtra("entry_ids",entry_id);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            // alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds * 1000), pendingIntent);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (seconds * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }
        return requestCode;
    }//startAlert

    private void SetAlertOFF(String entryID) {
        int requestCode;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where entry_id='" + entryID + "'", null);
            if (cursor.moveToFirst()) {
                requestCode = cursor.getInt(0);
                Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//SetAlertOFF

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:
                tipShootDate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mTipFocusView != null && mTipFocusView.isShown())
                mTipFocusView.hide();
        }
    };

    private void tipShootDate() {
        lldateshoot.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipFilmName();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_select_shoot_date_here);
                                }
                            })
                            .focusOn(lldateshoot)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .closeOnTouch(false)
                            .build();
                    mTipFocusView.show();


                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, lldateshoot.getTop());
            }
        });

        lldateshoot.performClick();
    }

    private void tipFilmName() {
        mTipFocusView.hide();
        etfilmname.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipProdHouse();
                                        }
                                    });

                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_film_or_tv_serial_name);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llfilmName)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, lldateshoot.getBottom());
            }
        });
        etfilmname.performClick();

    }

    private void tipProdHouse() {
        mTipFocusView.hide();
        llprodhouse.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipProdName();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_production_house);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llprodhouse)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llfilmName.getBottom());
            }
        });
        llprodhouse.performClick();

    }

    private void tipProdName() {
        mTipFocusView.hide();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipCallInTime();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_producer_name);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llprodName)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llprodhouse.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipCallInTime() {

        mTipFocusView.hide();
        final Handler handler;
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, etProducerName.getBottom());

            }
        };
        handler.postDelayed(r, 200);
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipSave();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);


                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_select_in_time);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llCallTime)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);

        llprodName.performClick();
    }

    private void tipSave() {
        mTipFocusView.hide();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    if (btnsave.getVisibility() == View.VISIBLE) {
                        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                                .customView(R.layout.layout_tip_top, new OnViewInflateListener() {
                                    @Override
                                    public void onViewInflated(View view) {
                                        view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                        TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                        btnSkip.setOnClickListener(mSkipClickListener);
                                        btnSkip.setText(R.string.btn_close);
                                        View view2 = (View) view.findViewById(R.id.view);
                                        view2.setVisibility(View.GONE);
                                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                        tvTip.setText(R.string.tip_click_save);
                                    }
                                })
                                .closeOnTouch(false)
                                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                                .roundRectRadius(90)
                                .focusOn(btnsave)
                                .build();
                    } else if (btnUpdate.getVisibility() == View.VISIBLE) {
                        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                                .customView(R.layout.layout_tip_top, new OnViewInflateListener() {
                                    @Override
                                    public void onViewInflated(View view) {
                                        view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                        TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                        btnSkip.setOnClickListener(mSkipClickListener);
                                        btnSkip.setText(R.string.btn_close);
                                        View view2 = (View) view.findViewById(R.id.view);
                                        view2.setVisibility(View.GONE);
                                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                        tvTip.setText(R.string.tip_click_save);
                                    }
                                })
                                .closeOnTouch(false)
                                .roundRectRadius(90)
                                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                                .focusOn(btnUpdate)
                                .build();
                    }
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, mainScroll.getBottom());
            }
        });
        llprodName.performClick();
    }
}
