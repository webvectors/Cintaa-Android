package com.webvectors.cintaa2.fragment;


import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.Dialog_Classes.Change_pwd_Class;
import com.webvectors.cintaa2.Dialog_Classes.GenerateReportDialogue;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment {
    TextView tvbackup,tvsettingalertbefore,tvsettingpersonalinfo,tvsettingusername,tvsettingemail,tvsettingsecurity,tvsettingnotification,tvsettingchangepassword,tvsettingalert,tvsettingsocial,tvsettingfb,tvsettingtwitter,tvsettinggoogleplus;
    ImageView ivsettinglightnot,ivsettingdarknot,ivsettinglightalert,ivsettingdarkalert;
    Button btnbackup;//btnbackup,tvbackup
    ProgressDialog pd;
    LinearLayout llchangepassword,llfbconnect,lltwitterconnect,llgoogleplusconnect;
    EditText etnewpwd,etoldpwd,etconfirmpwd,etalertbeforetimepicker;
    SwitchCompat switchCompatnot,switchCompatalert;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    AlertDialog.Builder builder;
    String member_id,profile_url,name,email;
    int check1=0;
    Intent i;
    CircleImageView profileimg;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite,sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_setting, container, false);
        pd=new ProgressDialog(getActivity());
        pd.setCancelable(false);
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        builder= new AlertDialog.Builder(getActivity());
        member_id=  preferences.getString("member_id","abc");
        profile_url=  preferences.getString("profile_imgurl","xy12z");
        name=  preferences.getString("name","xy32z");
        email=  preferences.getString("email","xy32z");

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "Setting";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();

        initialise(v);

        tvsettingusername.setText(name);
        tvsettingemail.setText(email);
        File path = new File("/sdcard/CINTAA/"+member_id+".png");
        if(path.exists()){
            Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            profileimg.setImageBitmap(mBitmap);
        }else {
            Picasso.with(getActivity()).load(profile_url).resize(70,70).into(profileimg);

            try {
                // downloadImagesToSdCard(profile_url);
                new AsyncDownloadImageToSDCard().execute(profile_url);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        String alert_flag = "ON",notification = "ON";
        cursor=sqLiteDatabaseRead.rawQuery("select * from sqtb_settings",null);
        if(cursor.moveToFirst())
        {
            do {
                notification = cursor.getString(0);
                alert_flag = cursor.getString(1);
                etalertbeforetimepicker.setText(cursor.getString(2)+" Hour");

            }while (cursor.moveToNext());
        }

        if(!alert_flag.equals("ON")){
            ivsettingdarkalert.setVisibility(View.VISIBLE);
            ivsettinglightalert.setVisibility(View.INVISIBLE);
            switchCompatalert.setChecked(false);
            etalertbeforetimepicker.setEnabled(false);
        }else{
            ivsettinglightalert.setVisibility(View.VISIBLE);
            ivsettingdarkalert.setVisibility(View.INVISIBLE);
            switchCompatalert.setChecked(true);
            etalertbeforetimepicker.setEnabled(true);
        }

        if(!notification.equals("ON")){
            ivsettingdarknot.setVisibility(View.VISIBLE);
            ivsettinglightnot.setVisibility(View.INVISIBLE);
            switchCompatnot.setChecked(false);
        }else{
            ivsettinglightnot.setVisibility(View.VISIBLE);
            ivsettingdarknot.setVisibility(View.INVISIBLE);
            switchCompatnot.setChecked(true);
        }


        switchCompatnot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked)
                {
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_settings set notification='OFF';");
                    int n = sqLiteStatement.executeUpdateDelete();
                    showToast("Notifications: OFF",Toast.LENGTH_SHORT);
                    ivsettingdarknot.setVisibility(View.VISIBLE);
                    ivsettinglightnot .setVisibility(View.INVISIBLE);
                }
                else {
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_settings set notification='ON';");
                    int n = sqLiteStatement.executeUpdateDelete();
                    showToast("Notifications: ON",Toast.LENGTH_SHORT);
                    ivsettinglightnot.setVisibility(View.VISIBLE);
                    ivsettingdarknot.setVisibility(View.INVISIBLE);
                }
            }

        });



        switchCompatalert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked)
                {
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_settings set alert='OFF';");
                    int n = sqLiteStatement.executeUpdateDelete();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_diary_entry set alert_flag='OFF' where  member_id='"+member_id+"';");
                    int m = sqLiteStatement.executeUpdateDelete();

                    SetAllAlertOFF();
                    etalertbeforetimepicker.setEnabled(false);
                    showToast("Alerts: OFF",Toast.LENGTH_SHORT);
                    ivsettingdarkalert.setVisibility(View.VISIBLE);
                    ivsettinglightalert.setVisibility(View.INVISIBLE);
                }
                else {
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_settings set alert='ON';");
                    int n = sqLiteStatement.executeUpdateDelete();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_diary_entry set alert_flag='ON' where  member_id='"+member_id+"';");
                    int m = sqLiteStatement.executeUpdateDelete();

                    SetAllAlertON();
                    etalertbeforetimepicker.setEnabled(true);
                    showToast("Alerts: ON",Toast.LENGTH_SHORT);
                    ivsettinglightalert.setVisibility(View.VISIBLE);
                    ivsettingdarkalert.setVisibility(View.INVISIBLE);
                }

            }
        });
        llfbconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.facebook.com";
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        lltwitterconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.twitter.com";
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        llgoogleplusconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://plus.ic_youtube.com";
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        llchangepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangePasswordDialog();
            }
        });

        btnbackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //new Backup_dialog().show(getFragmentManager(), "tag");

                DialogFragment dialogue = new GenerateReportDialogue();
                dialogue.setCancelable(false);
                dialogue.show(getFragmentManager(), "tag");
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(),"ok").commit();

                    keyCode=0;
                    return true;
                }
//               getFragmentManager().beginTransaction().replace(R.id.content_nav, new OrderFragment(),"ok").addToBackStack(null).commit();
else {
                    return false;
                }
            }
        } );

        etalertbeforetimepicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    ShowAlertBeforeDialog();
                {}
            }
        });
        etalertbeforetimepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowAlertBeforeDialog();

            }
        });
        tvsettingfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Config.urlFB));
                startActivity(i);
            }
        });

        return v;
    }//onCreate

    private void ShowAlertBeforeDialog(){
        final Dialog d = new Dialog(getActivity());
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.alert_before_dialog);
        Button btnset = (Button) d.findViewById(R.id.btnset);
        Button btncancel = (Button) d.findViewById(R.id.btncancel);
        TextView tvhours, tvminutes, tvtitle;
        tvhours = (TextView) d.findViewById(R.id.tvhours);
        tvminutes = (TextView) d.findViewById(R.id.tvminutes);
        tvtitle = (TextView)d.findViewById(R.id.tvtitle);
        final NumberPicker nphrs = (NumberPicker) d.findViewById(R.id.numberPickerhrs);
        final NumberPicker npmins = (NumberPicker) d.findViewById(R.id.numberPickermins);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvhours.setTypeface(custom_font);
        tvminutes.setTypeface(custom_font);
        tvtitle.setTypeface(custom_font);
        btncancel.setTypeface(custom_font);
        btnset.setTypeface(custom_font);
        nphrs.setMaxValue(23);
        nphrs.setMinValue(0);
        npmins.setMaxValue(59);
        npmins.setMinValue(0);
       // nphrs.setWrapSelectorWheel(false);
        //nphrs.setOnValueChangedListener(null);
        btnset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String hrs = String.valueOf(nphrs.getValue());
                String mins = String.valueOf(npmins.getValue());
                if(nphrs.getValue()<10){
                    hrs = "0"+String.valueOf(nphrs.getValue());
                }
                if(npmins.getValue()<10){
                    mins = "0"+String.valueOf(npmins.getValue());
                }
                String time = hrs+":"+mins;
                etalertbeforetimepicker.setText(time+" Hour");
                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_settings set alert_before='"+time+"';");
                int n = sqLiteStatement.executeUpdateDelete();
                SetAllAlertON();
                d.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();


    }//ShowAlertBeforeDialog

    private void SetAllAlertOFF(){

        int requestCode;

        cursor=sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where alert_flag='OFF' and member_id='"+member_id+"'" ,null);
        if(cursor.moveToFirst())
        {
            do {
                requestCode = cursor.getInt(0);

                Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode",String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();

            }while (cursor.moveToNext());
        }


    }//SetAllAlertOFF

    private void SetAllAlertON(){
        List<Integer> requestCode = new ArrayList<Integer>();
        List<String> dateHour = new ArrayList<String>();

        cursor=sqLiteDatabaseRead.rawQuery("select shoot_date,call_time,requestCode_Alarm from sqtb_diary_entry where alert_flag='ON' and member_id='"+member_id+"'",null);
        if(cursor.moveToFirst())
        {
            do {
                String date = cursor.getString(0);
                String hour = cursor.getString(1);
                dateHour.add(date+" "+hour);
                requestCode.add(cursor.getInt(2));

            }while (cursor.moveToNext());
        }
        if(requestCode.size()>0 && dateHour.size()>0){
            List<Long> seconds = CalculateSecondsFromCurrentDate(dateHour);

            startAlert(requestCode,seconds);
        }

    }//SetAllAlertON


    //Calculate Time in seconds from Current Date to Given Date
    private List<Long> CalculateSecondsFromCurrentDate(List<String> dateHours)  {

        List<Long> seconds = new ArrayList<Long>();

        Date now,eventDate,currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        for(int i=0; i<dateHours.size();i++){

            try {
                eventDate = sdfformat.parse(dateHours.get(i));

                //in milliseconds
                long diff = eventDate.getTime() - currentDate.getTime();

                long diffSeconds = diff / 1000;
                long diffMinutes = diff / (60 * 1000);
                long diffHours = diff / (60 * 60 * 1000);
                long diffDays = diff / (24 * 60 * 60 * 1000);

                long on_head = diffSeconds+diffMinutes+diffHours+diffDays;
                String alert = "00:00";
                cursor=sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings",null);
                if(cursor.moveToFirst())
                {
                    do {
                        alert = cursor.getString(0);
                    }while (cursor.moveToNext());
                }

           /*String time = "02:30"; //mm:ss
String[] units = time.split(":"); //will break the string up into an array
int minutes = Integer.parseInt(units[0]); //first element
int seconds = Integer.parseInt(units[1]); //second element
int duration = 60 * minutes + seconds; //add up our values
*/
                String[] units = alert.split(":"); //will break the string up into an array
                long hours = Integer.parseInt(units[0]); //hours element
                long minutes = Integer.parseInt(units[1]); //minutes element
                long before_head = 60 * 60 * hours + minutes * 60; //add up our values

                before_head = on_head - before_head;

                if(before_head>0){
                    seconds.add(before_head);
                }

   /*             if(diffSeconds+diffMinutes+diffHours+diffDays>0){
                    seconds.add(diffSeconds+diffMinutes+diffHours+diffDays);
                }
*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate

    public void startAlert(List<Integer> requestCode, List<Long> seconds) {

        //int requestCode = (int) (new Date().getTime()/1000);

        for(int i=0; i<seconds.size();i++){
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode",String.valueOf(requestCode.get(i)));
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode.get(i), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds.get(i) * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }

    }//startAlert

    private void initialise(View v)
    {
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        tvsettingpersonalinfo=(TextView)v.findViewById(R.id.tvsettingpersonalinfo);
        tvsettingpersonalinfo.setTypeface(custom_font);
        tvsettingusername=(TextView)v.findViewById(R.id.tvsettingusername);
        tvsettingusername.setTypeface(custom_font);
        tvsettingemail=(TextView)v.findViewById(R.id.tvsettingemail);
        tvsettingemail.setTypeface(custom_font);
        tvsettingsecurity=(TextView)v.findViewById(R.id.tvsettingsecurity);
        tvsettingsecurity.setTypeface(custom_font);
        tvsettingalert=(TextView)v.findViewById(R.id.tvsettingalert);
        tvsettingalert.setTypeface(custom_font);
        tvsettingsocial=(TextView)v.findViewById(R.id.tvsettingsocialnet);
        tvsettingsocial.setTypeface(custom_font);
        tvsettingfb=(TextView)v.findViewById(R.id.tvsettingfb);
        tvsettingfb.setTypeface(custom_font);
        tvsettingtwitter=(TextView)v.findViewById(R.id.tvsettingtwitter);
        tvsettingtwitter.setTypeface(custom_font);
        tvsettinggoogleplus=(TextView)v.findViewById(R.id.tvsettinggoogleplus);
        tvsettinggoogleplus.setTypeface(custom_font);
        tvsettingnotification=(TextView)v.findViewById(R.id.tvsettingnotification);
        tvsettingnotification.setTypeface(custom_font);
        tvsettingchangepassword=(TextView)v.findViewById(R.id.tvsettingchangepassword);
        tvsettingchangepassword.setTypeface(custom_font);
        profileimg=(CircleImageView)v.findViewById(R.id.ivsettingprofpic);
        ivsettinglightnot=(ImageView)v.findViewById(R.id.ivsettinglightnot);
        ivsettingdarknot=(ImageView)v.findViewById(R.id.ivsettingdarknot);;
        ivsettinglightalert=(ImageView)v.findViewById(R.id.ivsettinglightalert);
        ivsettingdarkalert=(ImageView)v.findViewById(R.id.ivsettingdarkalert);
        llfbconnect=(LinearLayout)v.findViewById(R.id.llfbconnect);
        lltwitterconnect=(LinearLayout)v.findViewById(R.id.lltwitterconnect);
        llgoogleplusconnect=(LinearLayout)v.findViewById(R.id.llgoogleplusconnect);
        switchCompatnot=(SwitchCompat)v.findViewById(R.id.settingswitchnoti);
        switchCompatalert=(SwitchCompat)v.findViewById(R.id.settingswitchalert);
        llchangepassword=(LinearLayout) v.findViewById(R.id.llchangepassword);
        etalertbeforetimepicker=(EditText) v.findViewById(R.id.etalertbeforetimepicker);
        etalertbeforetimepicker.setTypeface(custom_font);
        tvsettingalertbefore=(TextView)v.findViewById(R.id.tvsettingalertbefore);
        tvsettingalertbefore.setTypeface(custom_font);

        btnbackup = (Button)v.findViewById(R.id.btnbackup);
        btnbackup.setTypeface(custom_font);
        tvbackup = (TextView)v.findViewById(R.id.tvbackup);
        tvbackup.setTypeface(custom_font);

    }


    public void showChangePasswordDialog()
    {

        new Change_pwd_Class().show(getFragmentManager(), "tag"); // or getFragmentManager() in API 11+
       /* // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
       // final View promptsView = li.inflate(R.layout.change_password_dialog, null);
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.change_password_dialog
                , null));

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set prompts.xml to alertdialog builder
        //alertDialogBuilder.setView(promptsView);

        etnewpwd = (EditText) dialog.findViewById(R.id.etnewpwd);
        etconfirmpwd = (EditText) dialog.findViewById(R.id.etconfirmpwd);
        TextView tvchangepwd = (TextView) dialog.findViewById(R.id.tvchangepwd);
        TextView tvoldpwd = (TextView) dialog.findViewById(R.id.tvoldpwd);
        etoldpwd = (EditText) dialog.findViewById(R.id.etoldpwd);

        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        etconfirmpwd.setTypeface(custom_font);
        etnewpwd.setTypeface(custom_font);
        tvoldpwd.setTypeface(custom_font);
        etoldpwd.setTypeface(custom_font);
        tvchangepwd.setTypeface(custom_font);
        btnCancel.setTypeface(custom_font);
        btnOk.setTypeface(custom_font);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validationsalertbuilder();
                if(check1==4)
                {
                    setpassword();
                    dialog.dismiss();

                }

            }
        });
        // set dialog message



        // create alert dialog






        // show it
        dialog.show();*/
    }
    public void validationsalertbuilder() {


        SharedPreferences preferences2;
        preferences2= PreferenceManager.getDefaultSharedPreferences(getActivity());
        String old_pwd=  preferences.getString("password","abc");
        check1=0;
        if(!old_pwd.equals(etoldpwd.getText().toString().trim()))
        {
            etoldpwd.setError("Current Password is Wrong");
            showToast("Current Password is Wrong",Toast.LENGTH_LONG);
        }
        else {
            check1++;
        }

        if(!etnewpwd.getText().toString().equals(etconfirmpwd.getText().toString()))
        {
            etconfirmpwd.setError("New Password and Confirm Password Does Not Match");
            showToast("New Password and Confirm Password Does Not Match",Toast.LENGTH_LONG);
        }
        else {
            check1++;
        }
        if (etnewpwd.getText().toString().length() == 0  ) {
            etnewpwd.setError("New Password is Missing");
            showToast("New Password is Missing",Toast.LENGTH_LONG);

        }
        else{
            check1++;
        }
        if (etconfirmpwd.getText().toString().length()==0) {
            etconfirmpwd.setError("Confirm Password is Missing");
            showToast("Confirm Password is Missing",Toast.LENGTH_LONG);

        }else{check1++;}
        return;
    }
    public void setpassword()
    {
        pd.show();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("member_id", member_id);
        params.put("password", etconfirmpwd.getText().toString().trim());
        final String new_pass = etconfirmpwd.getText().toString().trim();

        client.post(Config.URLchangepassword, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    // Toast.makeText(getActivity(), "Profile updated", Toast.LENGTH_LONG).show();
                    //   Intent intent=new Intent(getActivity(),RegisterLoginActivity.class);
                    // startActivity(intent);

                    SharedPreferences preferences1;
                    SharedPreferences.Editor editor1;
                    preferences1= PreferenceManager.getDefaultSharedPreferences(getActivity());
                    editor1=preferences1.edit();
                    editor1.putString("password",new_pass);
                    editor1.commit();

                    showToast("Password change successfully", Toast.LENGTH_LONG);
                    etnewpwd.setText("");
                    etconfirmpwd.setText("");

                    String s1 = response.getString("success");

                    //  Intent intent=new Intent(getActivity(),HomeActivity.class);
                    //  startActivity(intent);
                    //    Toast.makeText(getActivity(), s1, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }

    class AsyncDownloadImageToSDCard extends AsyncTask<String,String ,String> {

        @Override
        protected String doInBackground(String... params) {
            String downloadUrl = params[0];

            try{
                URL url = new URL(downloadUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                //////////////////////////////////
                connection.setRequestMethod("GET");
                //////////////////////////////////
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap mBitmap = BitmapFactory.decodeStream(input);

                //Bitmap mBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
                //Bitmap mBitmap = BitmapFactory.decodeFile(String.valueOf(input));
                //profileimg.setImageBitmap(mBitmap);

                File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA");
                if (!direct.exists()) {
                    File wallpaperDirectory = new File("/sdcard/CINTAA/");
                    wallpaperDirectory.mkdirs();
                }
                String image="" + member_id + ".png";
                File outputFile = new File(new File("/sdcard/CINTAA/"),image );
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                    mBitmap.compress(Bitmap.CompressFormat.PNG,100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();

                    //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }catch (Exception e){

            }


            return null;
        }
    }//AsyncDownloadImageToSDCard

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem register = menu.findItem(R.id.action_appTour);
        register.setVisible(false);
    }
}

