package com.webvectors.cintaa2.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.webvectors.cintaa2.utility.AlertBroadcastReceiver;
import com.webvectors.cintaa2.utility.Config;
import com.webvectors.cintaa2.utility.CustomTypefaceSpan;
import com.webvectors.cintaa2.HelperClasses.ChromeHelpPopup;
import com.webvectors.cintaa2.model.ScheduleEntryEditData;
import com.webvectors.cintaa2.activity.HomeActivity;
import com.webvectors.cintaa2.adapter.PlaceArrayAdapter;
import com.webvectors.cintaa2.R;
import com.webvectors.cintaa2.HelperClasses.SQLiteDB;
import com.webvectors.cintaa2.utility.GlobalClass;
import com.webvectors.cintaa2.utility.PermissionModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cz.msebera.android.httpclient.Header;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.OnViewInflateListener;


public class DiaryentryFragment extends BaseFragment implements
        AdapterView.OnItemClickListener,
        AdapterView.OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    static DiaryentryFragment diaryentryFragment;
    EditText etprodhouname, etshifttime, etdtshoot, etcalltime, etshootloc, etcharname, etrate, etdueday, etremark;
    EditText etconveyance, etcallin, etpackup, etauthorityemail;
    Button btncallin, btncallinsubmit, btnpackup, btnpackupsubmit, btsubmit;
    TextView tvconveyance, tvInTimePackupTime, tvAuthorityDetails, tvauthoritynm, tvAuthorityNumber, tvAuthoritySign, tvProductionHouseEmail;
    ImageView ivauthoritysign, ivproducer, ivproductionHouse;
    LinearLayout llconveyancedetail, llph_email, llSubPage, llsubmitPage;
    AlertDialog.Builder builder;
    AlertDialog.Builder builder1, builder2;
    String[] stshifttime, stratetype, AryStprodhouname, Arystalies_name, Aryph_alies;
    Button btsave;
    AutoCompleteTextView autoproduction, etfilmname, etprodname, etauthoritynm, etauthoritynumb;
    private ArrayAdapter<String> adapterFilmName, adapterProductionHouse, adapterProducerName, adapterAuthorityName, adapterAuthorityNo;
    List<String> filmNameList, productionHouseList, producerNameList, authorityNameList, authorityNoList;
    //lldateshoot,
    LinearLayout btcancel, llshifttime, llprodName, llprodhouse, llAddressOfShoot, llNameOfCharacter, llcalltime, llshootlocation, lldateshoot, llfilmName, llPagePackup;
    TextView tvWelcome, tvsave, tvcancel, tvRdays, tvdays, etmaplocation;
    ProgressDialog pd;
    int check = 0, size = 0;
    String addressofloc;
    private ArrayAdapter<String> adapter;
    String profile_url, name, username, email, password, subject, ArtistName = null, signImageUrl = null;
    SharedPreferences preferences;
    String member_id, Sthouse_id = "";
    List<String> house_id, stprodhouname, stproducer_name, stalies_name, ph_alies, shift_start;

    List<Address> addresses;
    Geocoder geocoder;
    String address1, city, country, state, postalCode, knownName;
    double MyLat = 0;
    double MyLong = 0;
    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    String sublat, sublon, entry_id, encoded_string;
    Boolean UpdateEntry = false, isCallinSubmitted = false;

    SignaturePad mSignaturePad;
    Button mClearButton;
    Button mSaveButton;
    Bitmap signatureBitmap;

    String Page = "false", submit_time, packup_time;

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)
            .setFastestInterval(16)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    public static String curlocation;
    String[] lat;
    String[] lng;
    double latdb = 0, lngdb = 0;

    SQLiteDB sqLiteDB;
    SQLiteDatabase sqLiteDatabaseWrite, sqLiteDatabaseRead;
    SQLiteStatement sqLiteStatement;
    Cursor cursor;
    //if isCallinAvailable is true then fire update Query else insert Query
    Boolean isCallinAvailable = false, isDeviceLocation = false;


    View view;
    private FancyShowCaseView mTipFocusView;
    private ScrollView mainScroll;
    private RadioGroup rgCallinTime, rgPackTime;
    private RadioButton radioCallinAm, radioCallinPm, radioPackupPm, radioPackupAm;
    private Cursor cursor2;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_diaryentry, container, false);
        builder = new AlertDialog.Builder(getActivity());

        //apply permissions for all the devices
        PermissionModule permissionModule = new PermissionModule(getActivity());
        permissionModule.checkPermissions();

        initialise(view);
        // GetPreferencesOnBackPress();
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        buildGoogleApiClient();

        Toolbar toolbar = (Toolbar) super.getActivity().findViewById(R.id.appbar1);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        String actionBarTitle = "Diary Entry";
        SpannableStringBuilder ssb = new SpannableStringBuilder(actionBarTitle);
        ssb.setSpan(new CustomTypefaceSpan("", font), 0, actionBarTitle.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        toolbar.setTitle(ssb);

        sqLiteDB = new SQLiteDB(getActivity());
        sqLiteDatabaseWrite = sqLiteDB.getWritableDatabase();
        sqLiteDatabaseRead = sqLiteDB.getReadableDatabase();


        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        member_id = preferences.getString("member_id", "abc");
        profile_url = preferences.getString("profile_imgurl", "xy12z");
        name = preferences.getString("name", "xy32z");

        stprodhouname = new ArrayList<>();
        house_id = new ArrayList<>();
        stproducer_name = new ArrayList<>();
        stalies_name = new ArrayList<>();
        ph_alies = new ArrayList<>();
        Aryph_alies = new String[]{};


        if (!GetPreferences()) {
            GlobalClass.printLog(getActivity(), "---if---AAAAAAAAAA-----------");
            //If user enters diary entry page from dashboard page
            final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Calendar cal = Calendar.getInstance();
            String currentDate = dateFormatter1.format(cal.getTime());
            etdtshoot.setText("" + currentDate);
            checkSchedule();
        } else {
            //Else user has returned diary entry page from Map page
            if (UpdateEntry) {
                GlobalClass.printLog(getActivity(), "--else-if---BBBBBB-----------");

                final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                Calendar cal = Calendar.getInstance();
                String currentDate = dateFormatter1.format(cal.getTime());
                etdtshoot.setText("" + currentDate);
                isFieldsEnable(!UpdateEntry);
            }
        }
        tvRdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialogratetype();
            }
        });

        //SelectData();
        selectShiftTime();
      /*  mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                //.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(getActivity(), GOOGLE_API_CLIENT_ID, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mAutocompleteTextView_to_loc = (AutoCompleteTextView)  view.findViewById(R.id.etshootloc);
        mAutocompleteTextView_to_loc.setThreshold(1);
        mAutocompleteTextView_to_loc.setOnItemClickListener(mAutocompleteClickListenerDestination);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView_to_loc.setAdapter(mPlaceArrayAdapter);*/

        // stprodhouname=new String[]{"Amir Khan Productions","Anurag Kashyap Films","Balaji Motion Pictures","Dharma Productions"};
       /*llprodhouse.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
        public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    showdialog();
            }
        });
        llprodhouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog();
            }
        });

*/
        builder1 = new AlertDialog.Builder(getActivity());

        llshifttime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (hasFocus)
                    showdialog1();
            }
        });
        llshifttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog1();
            }
        });

        llcalltime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (hasFocus)
                    showcalltime();
            }
        });
        llcalltime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                showcalltime();

            }
        });
        builder2 = new AlertDialog.Builder(getActivity());
        stratetype = new String[]{"per hour", "per day"};
      /*  llratetype.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    showdialogratetype();
            }
        });
        llratetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialogratetype();
            }
        });*/
        //      etcallin.setEnabled(true);
        setCallinEnableTrue();
        if (etcallin.getText().toString().trim().length() > 0) {
            isCallinSubmitted = true;
//            etcallin.setEnabled(false);
            setCallinEnableFalse();
        }

//        etpackup.setEnabled(true);
        setPackUPEnableTrue();
        if (etpackup.getText().toString().trim().length() > 0) {
            isCallinSubmitted = true;
            //                etpackup.setEnabled(false);
            setPackUPEnableFalse();
        }


        btsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                validation();
                if (check == 9) {

//                    if (validateTime()) {     //.. commented because now intime is always valid, so no need to check it
                    if (latdb != 0 && lngdb != 0) {
                        DestroyPreferences();
                        if (UpdateEntry) {
                            GlobalClass.printLog(getActivity(), "--if (UpdateEntry)---DDDDDD-----------");


                            //..
                            if (Config.isNetworkConnected(getActivity())) {
                                GlobalClass.printLog(getActivity(), "--if (updateDiaryEntry)---DDDDDD-----------");

                                updateDiaryEntry();
                            } else {
                                GlobalClass.printLog(getActivity(), "--else (updateDiaryEntry)---DDDDDD-----------");

                                showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
                                InsertSQLite(entry_id, false, !isCallinAvailable);
                                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();
                            }
                        } else {
                            GlobalClass.printLog(getActivity(), "--else--------EEEEEEE-----------");

                            if (Config.isNetworkConnected(getActivity())) {
                                GlobalClass.printLog(getActivity(), "--else-----if--insertDiaryEntry-----EEEEEEE-----------");

                                insertDiaryEntry();
                            } else {
                                GlobalClass.printLog(getActivity(), "--else-----else--insertDiaryEntry-----EEEEEEE-----------");

                                entry_id = getCurrentTimeStamp();
                                showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
                                insert_tbScheduleDetails(entry_id, false);
                                InsertSQLite(entry_id, false, !isCallinAvailable);
                                Fragment fr = new HomeFragment();
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
                            }
                        }


                    } else {
                        // new ValidDiaryEntry_Dialog_Class().show(getFragmentManager(),"tag");
                        ShowDialogNoLatLng();
                    }

//                    } else {
//                        showToast("Invalid Call in time", Toast.LENGTH_LONG);
//                        etcalltime.setError("Call time must be in between given shift time");
//                    }
                } else {
                    // Toast.makeText(getActivity().getApplicationContext(),"Required Fields are Empty !!",Toast.LENGTH_LONG).show();

                    showToast("Required Fields are Empty !!", Toast.LENGTH_LONG);

//					View toastView = ToastMessage.getView();
//					ToastMessage.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//					toastView.setBackgroundResource(R.drawable.toast_drawable);
//					ToastMessage.show();
                }


            }
        });
        /*tvsave.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v)
			{
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				validation();
				if (check == 9)
				{
					if (isNetworkConnected())
					{
						if (validateTime())
						{
							if (latdb != 0 && lngdb != 0)
							{
								DestroyPreferences();
								saveAndCreateNext = true;
								insertDiaryEntry();
								//insertDiaryEntrySaveONLY();
							} else
							{
								// new ValidDiaryEntry_Dialog_Class().show(getFragmentManager(),"tag");
								ShowDialogNoLatLngOnly();
							}
						} else
						{
							showToast("Invalid Call in time", Toast.LENGTH_LONG);
							etcalltime.setError("Call time must be in between given shift time");
						}

					} else
					{
						showToast("\"Network connection is closed...Please enable data connection\"", Toast.LENGTH_LONG);

					}


				} else
				{
					showToast("Required Fields are Empty !!", Toast.LENGTH_LONG);

				}
			}
		});*/

/*
        btcancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

				DestroyPreferences();
				Fragment fr = new HomeFragment();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
			}
		});
*/


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                // StorePreferencesOnBackPress();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                Page = preferences.getString("diary_StayHere", "false");

                if (!Page.equals("True")) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        GlobalClass.printLog(getActivity(), "--(keyCode == KeyEvent.KEYCODE_BACK)-----FFFFFF-----------");

                        DestroyPreferences();
                        getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), "ok").commit();

                        return true;
                    }
                }
                DestroyPreferences();
                Page = "false";
                return false;
            }
        });

        llshootlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                isDeviceLocation = false;
                if (Config.isNetworkConnected(getActivity())) {
                    StorePreferences();
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapLocationFragment()).addToBackStack("Tag").commit();
                } else {
                    if (MyLat != 0 && MyLong != 0) {
                        showToast("Network Connection is Closed, Device Location is Fetched", Toast.LENGTH_SHORT);
                        latdb = MyLat;
                        lngdb = MyLong;
                        isDeviceLocation = true;
                        etmaplocation.setText(latdb + "; " + lngdb);
                    } else {
                        ShowDialogNoLatLng();
                    }
                }
            }
        });

        llshootlocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                isDeviceLocation = false;
                if (Config.isNetworkConnected(getActivity())) {
                    StorePreferences();
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapLocationFragment()).addToBackStack("Tag").commit();
                } else {
                    if (MyLat != 0 && MyLong != 0) {
                        showToast("Network Connection is Closed, Device Location is Fetched", Toast.LENGTH_SHORT);
                        latdb = MyLat;
                        lngdb = MyLong;
                        isDeviceLocation = true;
                        etmaplocation.setText(latdb + "; " + lngdb);
                    } else {
                        ShowDialogNoLatLng();
                    }
                }
            }
        });

        etshootloc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    isDeviceLocation = false;
                    if (Config.isNetworkConnected(getActivity())) {
                        StorePreferences();
                        getFragmentManager().beginTransaction().replace(R.id.content_frame, new MapLocationFragment()).addToBackStack("Tag").commit();
                    } else {
                        if (MyLat != 0 && MyLong != 0) {
                            showToast("Network Connection is Closed, Device Location is Fetched", Toast.LENGTH_SHORT);
                            latdb = MyLat;
                            lngdb = MyLong;
                            isDeviceLocation = true;
                            etmaplocation.setText(latdb + "; " + lngdb);
                        } else {
                            ShowDialogNoLatLng();
                        }
                    }
                    handled = true;
                }
                return handled;
            }
        });


        etcharname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    showdialog1();

                    handled = true;
                }
                return handled;
            }
        });

	 /*etprodname.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_SEND)
				{
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					showcal();
					handled = true;
				}
				return handled;
			}
		});*/

        btncallin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shoot_date = etdtshoot.getText().toString().trim();
                SimpleDateFormat sdfDate = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);//dd/MM/yyyy
//                SimpleDateFormat sdfDate2 = new SimpleDateFormat("h:mm a");

                Calendar cal = Calendar.getInstance();

                String currentDate = sdfDate.format(cal.getTime());
                Date currDate = new Date();
                Date end_date = new Date();

                try {
                    /*currDate = sdfDate.parse(currentDate);
                    end_date = sdfDate.parse(shoot_date);*/
                    currDate = sdfDate.parse(currentDate);
                    end_date = sdfDate.parse(etcalltime.getText().toString().trim());
                } catch (java.text.ParseException e) {
                    //	showToast(e.toString(),Toast.LENGTH_LONG);
                    e.printStackTrace();
                }
                long diff = end_date.getTime() - currDate.getTime();
                //int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                //showToast(diff+"",Toast.LENGTH_SHORT);
                //if (diff <= 0){
                if (true) {
                /*
                if(currDate.after(end_date) || currDate.equals(end_date)){*/
                    //showToast(DaysFromShoot +"",Toast.LENGTH_LONG);

                    //.. changes here to set toggle button for time
//                    callin_time = getCurrentTime();
                    String callin_time = sdfDate.format(cal.getTime());
                    GlobalClass.printLog(getActivity(), "callin_time: " + callin_time);
                    etcallin.setText(callin_time.replace("AM", "").replace("PM", "").trim());

                    if (callin_time.contains(getString(R.string.am))) {
                        radioCallinAm.setChecked(true);
                    } else if (callin_time.contains(getString(R.string.pm))) {
                        radioCallinPm.setChecked(true);
                    }

                } else {
                    showToast("In Time Cannot be Recorded before Call Time of Shoot", Toast.LENGTH_LONG);
                }

            }
        });
        btncallinsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etcallin.getText().toString().trim().length() == 0) {
                    etcallin.setError(getString(R.string.in_time_is_not_set));
                    showToast(getString(R.string.in_time_is_not_set), Toast.LENGTH_LONG);

                } else if (!etcallin.getText().toString().trim().contains(":")) {
                    etcallin.setError(getString(R.string.in_time_is_not_valid));
                    showToast(getString(R.string.in_time_is_not_valid), Toast.LENGTH_LONG);
                } else if (etcallin.getText().toString().trim().length() > 5) {
                    etcallin.setError(getString(R.string.in_time_is_not_valid));
                    showToast(getString(R.string.in_time_is_not_valid), Toast.LENGTH_LONG);
                } else {
                    if (isNetworkConnected()) {
                        try {
                            // Getting address from found locations.
                            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
                            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            knownName = addresses.get(0).getFeatureName();
                            // you can get more details other than this . like country code, state code, etc.

                            addressofloc = address1 + "; " + city + "; " + state + "; " + country;
                            storeCallinToDB(addressofloc, MyLat, MyLong);

                            llPagePackup.setVisibility(View.VISIBLE);

                            etfilmname.setEnabled(false);
                            llprodhouse.setEnabled(false);

                        } catch (Exception e) {
                            showToast(getString(R.string.enable_your_gps), Toast.LENGTH_SHORT);
                            e.printStackTrace();
                        }

                    } else {
                        //Insert Callin Details To SQLite
                        InsertCallinSQLiteOFFLine(entry_id, String.valueOf(MyLat), String.valueOf(MyLong));
                    }


                }
                return;

            }
        });

        btnpackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etcallin.getText().toString().trim().length() == 0 || !isCallinSubmitted) {
                    etcallin.setError(getString(R.string.in_time_is_not_submitted));
                    showToast(getString(R.string.in_time_is_not_submitted), Toast.LENGTH_LONG);

                } else {
                    String shoot_date = etdtshoot.getText().toString().trim();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");//dd/MM/yyyy old date formate

                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdfDate2 = new SimpleDateFormat("hh:mm a");

                    String currentDate = sdfDate.format(cal.getTime());
                    Date currDate = new Date();
                    Date end_date = new Date();

                    try {

                        currDate = sdfDate.parse(currentDate);
                        end_date = sdfDate.parse(shoot_date);

                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }

                    long diff = end_date.getTime() - currDate.getTime();
                    int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                    if (DaysFromShoot <= 0) {

                        //.. Made changes here to set toggle button for time
//                        packup_time = getCurrentTime();
                        packup_time = sdfDate2.format(cal.getTime());
                        GlobalClass.printLog(getActivity(), "packup_time: " + packup_time);
                        etpackup.setText(packup_time.replace("AM", "").replace("PM", "").trim());

                        if (packup_time.contains(getString(R.string.am))) {
                            radioPackupAm.setChecked(true);

                        } else if (packup_time.contains(getString(R.string.pm))) {
                            radioPackupPm.setChecked(true);
                        }

                    } else {
                        showToast("Packup Time Cannot be Recorded before Date of Shoot", Toast.LENGTH_LONG);
                    }
                }
                return;
            }
        });

        btnpackupsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String packupTime = etpackup.getText().toString().trim().replace(":", "");
                if (packupTime.length() == 0) {
                    etpackup.setError(getString(R.string.packup_time_is_not_set));
                    showToast(getString(R.string.packup_time_is_not_set), Toast.LENGTH_LONG);
                } else if (!etpackup.getText().toString().trim().contains(":")) {
                    etpackup.setError(getString(R.string.packup_time_is_not_valid));
                    showToast(getString(R.string.packup_time_is_not_valid), Toast.LENGTH_LONG);
                } else if (packupTime.length() > 4) {
                    etpackup.setError(getString(R.string.packup_time_is_not_valid));
                    showToast(getString(R.string.packup_time_is_not_valid), Toast.LENGTH_LONG);
                } else {
                    if (isNetworkConnected()) {
                        try {
                            // Getting address from found locations.
                            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
                            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            knownName = addresses.get(0).getFeatureName();
                            String address = address1 + "; " + city + "; " + state + "; " + country;
                            storePackupToDB(address, MyLat, MyLong);
                        } catch (Exception e) {
                            showToast(getString(R.string.enable_your_gps), Toast.LENGTH_SHORT);
                            e.printStackTrace();
                        }
                    } else {
                        //Insert Packup Details To SQLite
                        InsertPackupSQLiteOFFLine(entry_id, String.valueOf(MyLat), String.valueOf(MyLong));
                        //showToast("Network Connection is Closed, Packup Stored Offline", Toast.LENGTH_SHORT);
                    }
                }
                return;
            }
        });

        btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateSubmitPackup(entry_id)) {
                    if ((etauthoritynumb.getText().toString().length() == 0) && (etauthoritynm.getText().toString().length() == 0)) {
                        etauthoritynm.setError("Name of Signing Authority Cannot be Empty");
                        etauthoritynumb.setError("Number of Authority is Required");
                        showToast("Authority Name or Number is Missing", Toast.LENGTH_LONG);
                    } else {
                        if (ivauthoritysign.getDrawable() == null) {
                            showToast("Authority Sign is Missing", Toast.LENGTH_LONG);
                        } else {
                            if (etauthoritynumb.getText().toString().trim().length() == 10) {

                                if (etauthorityemail.getText().toString().trim().length() > 0 && isValidEmaillId(etauthorityemail.getText().toString().trim())) {

                                    String house_name = autoproduction.getText().toString().trim();
                                    String producer_name = etprodname.getText().toString().trim();
                                    String house_email = etauthorityemail.getText().toString().trim();
                                    String authority_name = etauthoritynm.getText().toString().trim();
                                    String authority_no = etauthoritynumb.getText().toString().trim();
                                    InsertProductionDataToDB(producer_name, house_name, house_email, authority_name, authority_no);


                                    String shoot_date = etdtshoot.getText().toString().trim();
                                    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);//dd/MM/yyyy

                                    Calendar cal = Calendar.getInstance();
                                    String currentDate = sdfDate.format(cal.getTime());
                                    Date currDate = new Date();
                                    Date end_date = new Date();

                                    try {

                                        currDate = sdfDate.parse(currentDate);
                                        end_date = sdfDate.parse(shoot_date);

                                    } catch (java.text.ParseException e) {
                                        e.printStackTrace();
                                    }

                                    long diff = end_date.getTime() - currDate.getTime();
                                    int DaysFromShoot = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                                    if (DaysFromShoot <= 0) {

                                        submit_time = getCurrentTimeSignature();
                                        try {
                                            new Encode_image().execute();
                                        } catch (Exception e1) {
                                            showToast("Error in Uploading Image", Toast.LENGTH_SHORT);
                                        }

                                    } else {
                                        showToast("Event Cannot be Submited before Date of Shoot", Toast.LENGTH_LONG);
                                    }
                                } else {
                                    etauthorityemail.setError("Input Valid Email");
                                    showToast("Input Valid Email", Toast.LENGTH_LONG);
                                }
                            } else {
                                etauthoritynumb.setError("Input Valid Number");
                                showToast("Input Valid Number", Toast.LENGTH_LONG);
                            }
                        }//End-if-else-Authority-Sign
                    }//End-if-else-Authority-Detail-Validity
                }//End-If-validateSubmitPackup

                return;
            }
        });

        ivauthoritysign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showsignaturedialog();
            }
        });

        fillAutoComplete();

        return view;
    }//onCreate

    private void InsertProductionDataToDB(String producer_name, String house_name, String house_email, String authority_name, String authority_no) {

        GlobalClass.printLog(getActivity(), "InsertProductionDataToDB method");

        GlobalClass.printLog(getActivity(), "producer_name " + producer_name);
        GlobalClass.printLog(getActivity(), "house_name " + house_name);
        GlobalClass.printLog(getActivity(), "house_email " + house_email);

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbProductionHouse values(?,?,?)");
        sqLiteStatement.bindString(1, house_name);
        sqLiteStatement.bindString(2, producer_name);
        sqLiteStatement.bindString(3, house_email);
        long r = sqLiteStatement.executeInsert();

        GlobalClass.printLog(getActivity(), "InsertProductionDataToDB method r " + r);

        try {
            //... insert AuthorityName and No. to database in tbAuthorityDetails
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbAuthorityDetails values(?,?)");
            sqLiteStatement.bindString(1, authority_name);
            sqLiteStatement.bindString(2, authority_no);
            long s = sqLiteStatement.executeInsert();
        } catch (Exception e) {
            GlobalClass.printLog(getActivity(), "----+++++++++++++++++++++++++++++++++++++++++++++=-" + e);
            sqLiteDatabaseRead.execSQL("CREATE TABLE IF NOT EXISTS tbAuthorityDetails (authorityName text,authorityNo text)");
        }


    }//... Enter Producer name, email, production house name to database in tbProductionHouse table

    public static DiaryentryFragment getFragment() {
        return diaryentryFragment;
    }

    private void StorePreferencesOnBackPress() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("diary_txt_filmnm_onBack", etfilmname.getText().toString().trim());
        editor.putString("diary_txt_phouse_onBack", autoproduction.getText().toString().trim());
        editor.putString("diary_txt_prodnm_onBack", etprodname.getText().toString().trim());
        editor.putString("diary_txt_shootdate_onBack", etdtshoot.getText().toString().trim());
        editor.putString("diary_txt_shootaddress_onBack", etshootloc.getText().toString().trim());
        editor.putString("diary_txt_shootloc_onBack", etmaplocation.getText().toString().trim());
        editor.putString("diary_txt_charcnm_onBack", etcharname.getText().toString().trim());
        editor.putString("diary_txt_shifttime_onBack", etshifttime.getText().toString().trim());
        editor.putString("diary_txt_calltime_onBack", etcalltime.getText().toString().trim());
        editor.putString("diary_txt_rateper_onBack", etrate.getText().toString().trim());
        editor.putString("diary_txt_duetime_onBack", etdueday.getText().toString().trim());
        editor.putString("diary_txt_remark_onBack", etremark.getText().toString().trim());
        editor.putString("diary_txt_lat_onBack", String.valueOf(latdb));
        editor.putString("diary_txt_lon_onBack", String.valueOf(lngdb));

        editor.commit();

    }//StorePreferencesOnBackPress

    private void GetPreferencesOnBackPress() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (preferences.getString("diary_txt_filmnm_onBack", "none").equalsIgnoreCase("none")) {
            GetPreferences();
        } else {
            etfilmname.setText(preferences.getString("diary_txt_filmnm_onBack", ""));
            autoproduction.setText(preferences.getString("diary_txt_phouse_onBack", ""));
            etprodname.setText(preferences.getString("diary_txt_prodnm_onBack", ""));
            etdtshoot.setText(preferences.getString("diary_txt_shootdate_onBack", ""));
            etshootloc.setText(preferences.getString("diary_txt_shootaddress_onBack", ""));
            String adrs = preferences.getString("diary_txt_shootloc_onBack", "");
            latdb = Double.parseDouble(preferences.getString("diary_txt_lat_onBack", "0"));
            lngdb = Double.parseDouble(preferences.getString("diary_txt_lon_onBack", "0"));
            if (adrs.equals("getting address...") || adrs.equals("")) {
                etmaplocation.setText("");
                latdb = 0;
                lngdb = 0;
            } else {
                etmaplocation.setText(adrs);
                etshootloc.requestFocus();
                // etcharname.setActivated(true);
            }

            etcharname.setText(preferences.getString("diary_txt_charcnm_onBack", ""));
            etshifttime.setText(preferences.getString("diary_txt_shifttime_onBack", ""));
            etcalltime.setText(preferences.getString("diary_txt_calltime_onBack", ""));
            etrate.setText(preferences.getString("diary_txt_rateper_onBack", ""));
            etdueday.setText(preferences.getString("diary_txt_duetime_onBack", ""));
            if (!preferences.getString("diary_txt_remark_onBack", "").equalsIgnoreCase("xRemarker"))
                etremark.setText(preferences.getString("diary_txt_remark_onBack", ""));

            DestroyPreferencesOnBackPress();
        }


    }//GetPreferencesOnBackPress

    private void DestroyPreferencesOnBackPress() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("diary_txt_filmnm_onBack");
        editor.remove("diary_txt_phouse_onBack");
        editor.remove("diary_txt_prodnm_onBack");
        editor.remove("diary_txt_shootdate_onBack");
        editor.remove("diary_txt_shootaddress_onBack");
        editor.remove("diary_txt_shootloc_onBack");
        editor.remove("diary_txt_charcnm_onBack");
        editor.remove("diary_txt_shifttime_onBack");
        editor.remove("diary_txt_calltime_onBack");
        editor.remove("diary_txt_rateper_onBack");
        editor.remove("diary_txt_duetime_onBack");
        editor.remove("diary_txt_remark_onBack");
        editor.remove("diary_txt_lat_onBack");
        editor.remove("diary_txt_lon_onBack");

        editor.commit();

    }//DestroyPreferencesOnBackPress

    private void StorePreferences() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("diary_txt_entry_id", entry_id);
        editor.putString("diary_txt_filmnm", etfilmname.getText().toString().trim());
        editor.putString("diary_txt_phouse", autoproduction.getText().toString().trim());
        editor.putString("diary_txt_prodnm", etprodname.getText().toString().trim());
        editor.putString("diary_txt_shootdate", etdtshoot.getText().toString().trim());
        editor.putString("diary_txt_shootaddress", etshootloc.getText().toString().trim());
        editor.putString("diary_txt_shootloc", etmaplocation.getText().toString().trim());
        editor.putString("diary_txt_charcnm", etcharname.getText().toString().trim());
        editor.putString("diary_txt_shifttime", etshifttime.getText().toString().trim());
        editor.putString("diary_txt_calltime", etcalltime.getText().toString().trim());
        editor.putString("diary_txt_rateper", etrate.getText().toString().trim());
        editor.putString("diary_txt_duetime", etdueday.getText().toString().trim());
        editor.putString("diary_txt_remark", etremark.getText().toString().trim());
        editor.putString("diary_txt_lat", String.valueOf(latdb));
        editor.putString("diary_txt_lon", String.valueOf(lngdb));
        editor.putString("diary_txt_UpdateEntry", String.valueOf(UpdateEntry));
        editor.putString("diary_txt_isCallinAvailable", String.valueOf(isCallinAvailable));

        editor.commit();
    }//StorePreferences

    private Boolean GetPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if (preferences.getString("diary_txt_filmnm", "").length() == 0 & preferences.getString("diary_txt_shootloc", "").length() == 0) {
            return false;
        } else {
            entry_id = preferences.getString("diary_txt_entry_id", "");
            etfilmname.setText(preferences.getString("diary_txt_filmnm", ""));
            autoproduction.setText(preferences.getString("diary_txt_phouse", ""));
            etprodname.setText(preferences.getString("diary_txt_prodnm", ""));
            etdtshoot.setText(preferences.getString("diary_txt_shootdate", ""));
            etshootloc.setText(preferences.getString("diary_txt_shootaddress", ""));
            String adrs = preferences.getString("diary_txt_shootloc", "");
            latdb = Double.parseDouble(preferences.getString("diary_txt_lat", "0"));
            lngdb = Double.parseDouble(preferences.getString("diary_txt_lon", "0"));
            if (adrs.equals("Fetching Address...") || adrs.length() == 0) {
                etmaplocation.setText("");
                latdb = 0;
                lngdb = 0;
            } else {
                etmaplocation.setText(adrs);
                etcharname.requestFocus();
                // etcharname.setActivated(true);
            }

            etcharname.setText(preferences.getString("diary_txt_charcnm", ""));
            etshifttime.setText(preferences.getString("diary_txt_shifttime", ""));
            etcalltime.setText(preferences.getString("diary_txt_calltime", ""));
            etrate.setText(preferences.getString("diary_txt_rateper", ""));
            etdueday.setText(preferences.getString("diary_txt_duetime", ""));

            if (!preferences.getString("diary_txt_remark", "").equalsIgnoreCase("xRemarker"))
                etremark.setText(preferences.getString("diary_txt_remark", ""));

            UpdateEntry = Boolean.parseBoolean(preferences.getString("diary_txt_UpdateEntry", "false"));
            isCallinAvailable = Boolean.parseBoolean(preferences.getString("diary_txt_isCallinAvailable", "false"));
            DestroyPreferences();

            return true;
        }
        //  Toast.makeText(getActivity(),"LAT: "+latdb+"\n LON: "+lngdb,Toast.LENGTH_LONG).show();
    }//GetPreferences

    private void DestroyPreferences() {
        GlobalClass.printLog(getActivity(), "--DestroyPreferences method-----GGGGGGGG------");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("diary_txt_entry_id");
        editor.remove("diary_txt_filmnm");
        editor.remove("diary_txt_phouse");
        editor.remove("diary_txt_prodnm");
        editor.remove("diary_txt_shootdate");
        editor.remove("diary_txt_shootaddress");
        editor.remove("diary_txt_shootloc");
        editor.remove("diary_txt_charcnm");
        editor.remove("diary_txt_shifttime");
        editor.remove("diary_txt_calltime");
        editor.remove("diary_txt_rateper");
        editor.remove("diary_txt_duetime");
        editor.remove("diary_txt_remark");
        editor.remove("diary_txt_lat");
        editor.remove("diary_txt_lon");
        editor.remove("diary_txt_UpdateEntry");
        editor.remove("diary_StayHere");
        editor.remove("diary_txt_isCallinAvailable");

        editor.commit();

    }//DestroyPreferences

    //here requires api should be mandatory due to some depericated libraries
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void isFieldsEnable(Boolean enable) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (enable) {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ivproducer.setBackgroundDrawable(getResources().getDrawable(R.drawable.left_round_icon));
                ivproductionHouse.setBackgroundDrawable(getResources().getDrawable(R.drawable.left_round_icon));
                etfilmname.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable));
                autoproduction.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable));
                etprodname.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable));
            } else {
                ivproducer.setBackground(getResources().getDrawable(R.drawable.left_round_icon));
                ivproductionHouse.setBackground(getResources().getDrawable(R.drawable.left_round_icon));
                etfilmname.setBackground(getResources().getDrawable(R.drawable.right_round_drawable));
                autoproduction.setBackground(getResources().getDrawable(R.drawable.right_round_drawable));
                etprodname.setBackground(getResources().getDrawable(R.drawable.right_round_drawable));
            }
            etfilmname.setEnabled(true);
            autoproduction.setEnabled(true);
            etprodname.setEnabled(true);

            ChromeHelpPopup chromeHelpPopup = new ChromeHelpPopup(getActivity(), "No Schedule Entry Found, Make A New Diary Entry Here.");
            chromeHelpPopup.show(etdtshoot);
        } else {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ivproducer.setBackgroundDrawable(getResources().getDrawable(R.drawable.left_round_icon_disabled));
                ivproductionHouse.setBackgroundDrawable(getResources().getDrawable(R.drawable.left_round_icon_disabled));
                etfilmname.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
                autoproduction.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
                etprodname.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
            } else {
                ivproducer.setBackground(getResources().getDrawable(R.drawable.left_round_icon_disabled));
                ivproductionHouse.setBackground(getResources().getDrawable(R.drawable.left_round_icon_disabled));
                etfilmname.setBackground(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
                autoproduction.setBackground(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
                etprodname.setBackground(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
            }
            etfilmname.setEnabled(true);
            autoproduction.setEnabled(true);
            etprodname.setEnabled(true);
        }
    }//fieldsEnable

    private void checkSchedule() {
        final SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        String currentDate = dateFormatter1.format(cal.getTime());//current date
        cal.add(Calendar.DATE, -1);
        String yesterdayDate = dateFormatter1.format(cal.getTime());//previous date
        List<ScheduleEntryEditData> scheduleData = new ArrayList<ScheduleEntryEditData>();
        String diaryDate; //date to be shown on diary page

        cursor = null;//Here Cursor checks for the schedule from pervious and current date
        cursor = sqLiteDatabaseRead.rawQuery("select sd.entryId, sd.filmName, sd.productionHouse, sd.producerName, sd.scheduleStatus, sd.scheduleDate, dec.callin_time " +
                "from tbScheduleDetails sd LEFT OUTER JOIN sqtb_diary_Callin dec ON sd.entryId = dec.entry_id where memberId = ? and (scheduleDate = ? or scheduleDate = ?) and scheduleStatus='pending'  ", new String[]{member_id, yesterdayDate, currentDate});

/*
        diaryDate = yesterdayDate;
		if(cursor.getCount()==0){
			cursor = null;//if No schedule found for pervious date then check for on going shcedule from today
			cursor = sqLiteDatabaseRead.rawQuery("select entryId, filmName, productionHouse, producerName,scheduleStatus, scheduleDate " +
					"from tbScheduleDetails where memberId = ? and scheduleDate = ? and scheduleStatus='pending'", new String[]{member_id,currentDate});
			diaryDate = currentDate;
		}
*/
        try {
            if (cursor.moveToFirst()) {
                UpdateEntry = true;
                isFieldsEnable(false);
                do {
                    String entryId, filmName, productionHouse, producerName, scheduleStatus;
                    if ((null != cursor.getString(6)) && (cursor.getString(5).equalsIgnoreCase(yesterdayDate))) {
                        //if this data is of pervious date and callin time is marked in it then add to list scheduleData
                        entryId = cursor.getString(0);
                        filmName = cursor.getString(1);
                        productionHouse = cursor.getString(2);
                        producerName = cursor.getString(3);
                        Boolean callStatus = false;
                        scheduleStatus = cursor.getString(4);
                        diaryDate = cursor.getString(5);
                        if (null != diaryDate) {
                            //entryId,dateOfShoot,filmName,productionHouse,producerName
                            scheduleData.add(new ScheduleEntryEditData(entryId, diaryDate, filmName, productionHouse, producerName, callStatus, scheduleStatus));
                        }
                    } else if (cursor.getString(5).equalsIgnoreCase(currentDate)) {
                        //Else-if this data is of current date only then add to list scheduleData
                        entryId = cursor.getString(0);
                        filmName = cursor.getString(1);
                        productionHouse = cursor.getString(2);
                        producerName = cursor.getString(3);
                        Boolean callStatus = false;
                        scheduleStatus = cursor.getString(4);
                        diaryDate = cursor.getString(5);
                        if (null != diaryDate) {
                            //entryId,dateOfShoot,filmName,productionHouse,producerName
                            scheduleData.add(new ScheduleEntryEditData(entryId, diaryDate, filmName, productionHouse, producerName, callStatus, scheduleStatus));
                        }
                    }
                } while (cursor.moveToNext());

                if (null != scheduleData && scheduleData.size() > 0) {
                    //if list scheduleData has some data to show
                    entry_id = scheduleData.get(0).getEntryId();
                    etfilmname.setText(scheduleData.get(0).getFilmName());
                    autoproduction.setText(scheduleData.get(0).getProductionHouse());
                    etprodname.setText(scheduleData.get(0).getProducerName());
                    etdtshoot.setText("" + scheduleData.get(0).getDateOfShoot());
                    checkRecords(entry_id, scheduleData.get(0).getScheduleStatus());
                    //handleMultiSchedule() is used to handle page for schedule data from different dates
                    handleMultiSchedule(scheduleData);
                } else {
                    //Else perform same action that as of if the cursor was empty
                    UpdateEntry = false;
                    isFieldsEnable(true);
                    btsave.setVisibility(View.VISIBLE);
                    llSubPage.setVisibility(View.GONE);
                }
            } else {
                //Else this is new Diary Entry So UpdateEntry variable is assigned value false so that the insert query will be applied for this diary entry
                UpdateEntry = false;
                isFieldsEnable(true);
                btsave.setVisibility(View.VISIBLE);
                llSubPage.setVisibility(View.GONE);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

    }//checkSchedule

    //Here requires api is mandatory due to some depericated libraries
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void reCheckSchedule(String currentDate) {
        try {
            List<ScheduleEntryEditData> scheduleData = new ArrayList<ScheduleEntryEditData>();
            cursor = sqLiteDatabaseRead.rawQuery("select entryId, filmName, productionHouse, producerName, scheduleStatus " +
                    "from tbScheduleDetails where memberId = ? and scheduleDate = ? and scheduleStatus = 'pending'", new String[]{member_id, currentDate});
            if (cursor.moveToFirst()) {
                UpdateEntry = true;
                isFieldsEnable(false);
                do {
                    String entryId, filmName, productionHouse, producerName, scheduleStatus;
                    entryId = cursor.getString(0);
                    filmName = cursor.getString(1);
                    productionHouse = cursor.getString(2);
                    producerName = cursor.getString(3);
                    Boolean callStatus = false;
                    scheduleStatus = cursor.getString(4);
                    //entryId,dateOfShoot,filmName,productionHouse,producerName
                    scheduleData.add(new ScheduleEntryEditData(entryId, currentDate, filmName, productionHouse, producerName, callStatus, scheduleStatus));
                } while (cursor.moveToNext());

                checkRecords(scheduleData.get(0).getEntryId(), scheduleData.get(0).getScheduleStatus());
                handleMultiSchedule(scheduleData);
            } else {
                UpdateEntry = false;
                isFieldsEnable(true);
                btsave.setVisibility(View.VISIBLE);
                llSubPage.setVisibility(View.GONE);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//reCheckSchedule

    private void handleMultiSchedule(final List<ScheduleEntryEditData> scheduleData) {
        if (scheduleData.size() > 1) {
            showSchedules(scheduleData);
        }
        llfilmName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSchedules(scheduleData);
            }
        });
    }//handleMultiSchedule

    private void showSchedules(final List<ScheduleEntryEditData> scheduleData) {
        //Show List of schedules that are present in list scheduleData
        List<String> filmName = new ArrayList<>();
        for (int i = 0; i < scheduleData.size(); i++) {
            if (null != scheduleData.get(i).getDateOfShoot() && null != scheduleData.get(i).getFilmName()) {
                filmName.add(scheduleData.get(i).getDateOfShoot() + " - " + scheduleData.get(i).getFilmName());
            }
        }
        View customTitle = View.inflate(getActivity(), R.layout.titlefilmname, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setCustomTitle(customTitle);
        builder1.setCancelable(true);
        final String[] arr = new String[filmName.size()];
        filmName.toArray(arr);
        int selected = -1;
        String currentValue = etfilmname.getText().toString().trim();
        for (int i = 0; i < arr.length; i++) {
            if (currentValue.equals(arr[i])) {
                selected = i;
                break;
            }
        }
        builder1.setSingleChoiceItems(arr, selected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (ScheduleEntryEditData data : scheduleData) {
                    if (arr[i].contains("" + data.getDateOfShoot()) && arr[i].contains(data.getFilmName())) {
                        etfilmname.setText(data.getFilmName());
                        etdtshoot.setText("" + data.getDateOfShoot());
                        entry_id = data.getEntryId();
                        autoproduction.setText(data.getProductionHouse());
                        etprodname.setText(data.getProducerName());
                        //check for data and auto-populate the fields for the schedule selected from this list scheduleData
                        checkRecords(entry_id, data.getScheduleStatus());
                    }
                }

                dialogInterface.cancel();
            }
        });
        builder1.show();
    }//showSchedules(scheduleData);

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void checkRecords(String entry_id, String scheduleStatus) {
        try {
            isCallinAvailable = false;
            //etmaplocation.setText("");
            etcharname.setText("");
            etshifttime.setText("");
            etcalltime.setText("");
            etrate.setText("");
            etdueday.setText("");
            etremark.setText("");
            etmaplocation.setText("");
            etshootloc.setText("");

            llshootlocation.setEnabled(true);
            etcharname.setEnabled(true);
            llshifttime.setEnabled(true);
            llcalltime.setEnabled(true);
            btncallin.setEnabled(true);
            btncallinsubmit.setEnabled(true);
            btnpackup.setEnabled(true);
            btnpackupsubmit.setEnabled(true);
            etrate.setEnabled(true);
            etdueday.setEnabled(true);
            etremark.setEnabled(true);
            tvRdays.setEnabled(true);
            btsave.setVisibility(View.VISIBLE);
            llSubPage.setVisibility(View.GONE);

            //sqtb_diary_entry   entry_id, member_id, shoot_date, film_tv_name, shift_time,
            // call_time, shoot_location, lat, lng, characterName, shiftTime, rate, paymentAfter, remark
            cursor = sqLiteDatabaseRead.rawQuery("select shoot_date, film_tv_name, shift_time, call_time, " +
                    "characterName, rate, paymentAfter, remark, shoot_location,rateType,shoot_map_location from sqtb_diary_entry where " +
                    "entry_id=? and member_id=?", new String[]{entry_id, member_id});


            if (cursor.moveToFirst()) {
                etshifttime.setText(cursor.getString(2));
                etcalltime.setText(cursor.getString(3));
                etcharname.setText(cursor.getString(4));
                try {
                    etrate.setText(cursor.getString(5));
                    if (!cursor.getString(7).equalsIgnoreCase("xRemarker")) {
                        etremark.setText(cursor.getString(7));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    etrate.setText("");
                    etremark.setText("");
                }
//            etrate.setText(cursor.getString(5));
                etdueday.setText(cursor.getString(6));

                etshootloc.setText(cursor.getString(8));
                etmaplocation.setText(cursor.getString(10));

                if (etcalltime.getText().toString().length() > 0) {
                    isCallinAvailable = true;
                }

                if (etcharname.getText().toString().trim().length() > 0) {
//				tvRdays.setText(cursor.getString(9));
                    //Here are some changes handle the incomming data
                    try {
                        if (cursor.getString(9) != null) {
                            tvRdays.setText(cursor.getString(9));
                        } else {
                            tvRdays.setText(" Per Day");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvRdays.setText(" Per Day");
                    }

                    llshootlocation.setEnabled(false);
                    etcharname.setEnabled(false);
                    llshifttime.setEnabled(false);
                    llcalltime.setEnabled(false);
                    etrate.setEnabled(false);
                    etdueday.setEnabled(false);
                    //etremark.setEnabled(false);
                    tvRdays.setEnabled(false);
                    btsave.setVisibility(View.GONE);
                    llSubPage.setVisibility(View.VISIBLE);
                    //..

                    llconveyancedetail.setVisibility(View.GONE);
                    if (etrate.getText().toString().trim().length() > 0) {
                        try {
                            if (Double.parseDouble(etrate.getText().toString().trim()) <= Config.conveyanceLimit) {
                                llconveyancedetail.setVisibility(View.VISIBLE);
                                try {
                                    cursor = sqLiteDatabaseRead.rawQuery("select conveyance from tbConveyance", null);
                                    if (cursor.moveToFirst()) {
                                        etconveyance.setText(cursor.getString(0));
                                    }
                                } finally {
                                    if (cursor != null)
                                        cursor.close();
                                }
                            }
                        } catch (NumberFormatException e) {
                        }

                    }
//.. to get productionEmail from database
                    try {
                        cursor = sqLiteDatabaseRead.rawQuery("select productionEmail from tbProductionHouse where productionHouse=? and producerName=?",
                                new String[]{autoproduction.getText().toString().trim(), etprodname.getText().toString().trim()});
                        if (cursor.moveToFirst()) {
                            etauthorityemail.setText(cursor.getString(0));
                            etauthorityemail.setEnabled(true);

                            //... Remove all below comments to disable etauthorityemail after get value
//                            etauthorityemail.setEnabled(false);
//                            final int sdk = android.os.Build.VERSION.SDK_INT;
//                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                                etauthorityemail.setBackgroundDrawable(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
//                            } else {
//                                etauthorityemail.setBackground(getResources().getDrawable(R.drawable.right_round_drawable_disabled));
//                            }
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }

                    checkCallInPackup(entry_id);

                    if (scheduleStatus.equals("done")) {
                        llsubmitPage.setVisibility(View.GONE);
                    } else {
                        llsubmitPage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//checkRecords

    private Boolean validateSubmitPackup(String entry_id) {
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select packup_time from sqtb_diary_Packup where entry_id=?", new String[]{entry_id});
            if (cursor.moveToFirst()) {
                return true;
            } else {
                showToast("Packup Time is Not Submitted", Toast.LENGTH_SHORT);
                return false;
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//validateSubmitPackup

    private void checkCallInPackup(String entry_id) {
        try {

            //sqtb_diary_Callin (entry_id text, callin_time
            //sqtb_diary_Packup (entry_id text, packup_time
            cursor = sqLiteDatabaseRead.rawQuery("select callin_time from sqtb_diary_Callin where entry_id=?", new String[]{entry_id});

            //... replace am pm
            if (cursor.moveToFirst()) {
                etcallin.setText(cursor.getString(0).replace("AM", "").replace("PM", "").trim());

                if (cursor.getString(0).contains(getString(R.string.am))) {
                    radioCallinAm.setChecked(true);
                } else if (cursor.getString(0).contains(getString(R.string.pm))) {
                    radioCallinPm.setChecked(true);
                }


//                etcallin.setText(cursor.getString(0));
                btncallin.setVisibility(View.GONE);
                btncallinsubmit.setVisibility(View.GONE);
                isCallinSubmitted = true;


                setCallinEnableFalse();

            } else {

//                etcallin.setEnabled(true);
                setCallinEnableTrue();
                etcallin.setText("");
                btncallin.setVisibility(View.VISIBLE);
                btncallinsubmit.setVisibility(View.VISIBLE);
                isCallinSubmitted = false;
            }

            cursor = sqLiteDatabaseRead.rawQuery("select packup_time from sqtb_diary_Packup where entry_id=?", new String[]{entry_id});
            if (cursor.moveToFirst()) {
                etpackup.setText(cursor.getString(0).replace("AM", "").replace("PM", "").trim());
                if (cursor.getString(0).contains(getString(R.string.am))) {
                    radioPackupAm.setChecked(true);
                } else if (cursor.getString(0).contains(getString(R.string.pm))) {
                    radioPackupPm.setChecked(true);
                }

//                etpackup.setText(cursor.getString(0));
//                etpackup.setEnabled(false);
                setPackUPEnableFalse();
                rgPackTime.setEnabled(false);
                radioPackupAm.setEnabled(false);
                radioPackupPm.setEnabled(false);
                btnpackup.setVisibility(View.GONE);
                btnpackupsubmit.setVisibility(View.GONE);

            } else {
                etpackup.setText("");
                //        etpackup.setEnabled(true);
                setPackUPEnableTrue();
                btnpackup.setVisibility(View.VISIBLE);
                btnpackupsubmit.setVisibility(View.VISIBLE);
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }

        llPagePackup.setVisibility(View.VISIBLE);
        if (etcallin.getText().toString().trim().length() == 0) {
            llPagePackup.setVisibility(View.GONE);
        }
    }//checkCallInPackup

    private void setCallinEnableFalse() {
        etcallin.setEnabled(false);
        rgCallinTime.setEnabled(false);
        radioCallinAm.setEnabled(false);
        radioCallinPm.setEnabled(false);
    }

    private void setCallinEnableTrue() {
        etcallin.setEnabled(true);
        rgCallinTime.setEnabled(true);
        radioCallinAm.setEnabled(true);
        radioCallinPm.setEnabled(true);
    }

    private void setPackUPEnableFalse() {
        etpackup.setEnabled(false);
        rgPackTime.setEnabled(false);
        radioPackupAm.setEnabled(false);
        radioPackupPm.setEnabled(false);
    }

    private void setPackUPEnableTrue() {
        etpackup.setEnabled(true);
        rgPackTime.setEnabled(true);
        radioPackupAm.setEnabled(true);
        radioPackupPm.setEnabled(true);
    }

	/*private void ShowDialogNoLatLng()
    {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		dialogBuilder.setMessage("Shoot Location is missing, Auto Callin will be Disabled for this Entry.");

		dialogBuilder.setCancelable(false);

		dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				DestroyPreferences();
				if(UpdateEntry){
					if(Config.isNetworkConnected(getActivity())){
						updateDiaryEntry();
					}else {
						showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
						InsertSQLite(entry_id,false,!isCallinAvailable);
						Fragment fr = new HomeFragment();
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
					}
				}else {
					if(Config.isNetworkConnected(getActivity())){
						insertDiaryEntry();
					}else{
						entry_id = getCurrentTimeStamp();
						showToast("Network Not Available, Entry Saved Offline", Toast.LENGTH_LONG);
						insert_tbScheduleDetails(entry_id,false);
						InsertSQLite(entry_id,false,!isCallinAvailable);
						Fragment fr = new HomeFragment();
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
					}
				}
				dialog.cancel();
			}
		});

		dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});


		//Create alert dialog object via builder
		AlertDialog alertDialogObject = dialogBuilder.create();

		alertDialogObject.setOnShowListener(new DialogInterface.OnShowListener()
		{
			@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onShow(final DialogInterface dialog)
			{
				Button negativeButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
				Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);

				// this not working because multiplying white background (e.g. Holo Light) has no effect
				negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

				final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
				final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
				final int positiveButtonDrawabletextback = getResources().getColor(R.color.btn_backColor);
				final int negativeButtonDrawabletextback = getResources().getColor(R.color.btn_text);
				final int positiveButtonDrawabletext = getResources().getColor(R.color.btn_text);
				final int negativeButtonDrawabletext = getResources().getColor(R.color.btn_backColor);
				if (Build.VERSION.SDK_INT >= 16)
				{
					negativeButton.setBackground(negativeButtonDrawable);
					positiveButton.setBackground(positiveButtonDrawable);
					positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
					negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
					positiveButton.setTextColor(positiveButtonDrawabletext);
					negativeButton.setTextColor(negativeButtonDrawabletext);
				} else
				{
					negativeButton.setBackgroundDrawable(negativeButtonDrawable);
					positiveButton.setBackgroundDrawable(positiveButtonDrawable);
					positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
					negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
					positiveButton.setTextColor(positiveButtonDrawabletext);
					negativeButton.setTextColor(negativeButtonDrawabletext);
				}

				negativeButton.invalidate();
				positiveButton.invalidate();
			}
		});
		alertDialogObject.show();

	}//ShowDialogNoLatLng*/

    private void ShowDialogNoLatLng() {
        latdb = 1;
        lngdb = 1;

        LayoutInflater li = LayoutInflater.from(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(li.inflate(R.layout.location_entry_by_user, null));

        TextView tvLabel = (TextView) dialog.findViewById(R.id.tvLabel);
        final EditText etLocation = (EditText) dialog.findViewById(R.id.etLocation);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        etLocation.setTypeface(custom_font);
        tvLabel.setTypeface(custom_font);
        btnOk.setTypeface(custom_font);

        if (!isDeviceLocation) {
            etLocation.setText(etmaplocation.getText().toString());
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etLocation.getText().toString().trim().length() > 0) {
                    etmaplocation.setText(etLocation.getText().toString().trim());
                    dialog.dismiss();
                } else {
                    showToast("Entry Location of Shoot", Toast.LENGTH_SHORT);
                    etLocation.setError("Entry Location of Shoot");
                }
            }
        });

        dialog.show();
    }//ShowDialogNoLatLng

    private void ShowDialogNoLatLngOnly() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Shoot Location is missing, Auto Callin will be Disabled for this Entry.");

        dialogBuilder.setCancelable(false);

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                insertDiaryEntrySaveONLY();
                dialog.cancel();
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();

        alertDialogObject.setOnShowListener(new DialogInterface.OnShowListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onShow(final DialogInterface dialog) {
                Button negativeButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);

                // this not working because multiplying white background (e.g. Holo Light) has no effect
                negativeButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

                final Drawable negativeButtonDrawable = getResources().getDrawable(R.drawable.tabselelct);
                final Drawable positiveButtonDrawable = getResources().getDrawable(R.drawable.tabunselect);
                final int positiveButtonDrawabletextback = getResources().getColor(R.color.bgColor);
                final int negativeButtonDrawabletextback = getResources().getColor(R.color.colorAccent);
                final int positiveButtonDrawabletext = getResources().getColor(R.color.colorAccent);
                final int negativeButtonDrawabletext = getResources().getColor(R.color.bgColor);
                if (Build.VERSION.SDK_INT >= 16) {
                    negativeButton.setBackground(negativeButtonDrawable);
                    positiveButton.setBackground(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                } else {
                    negativeButton.setBackgroundDrawable(negativeButtonDrawable);
                    positiveButton.setBackgroundDrawable(positiveButtonDrawable);
                    positiveButton.setBackgroundColor(positiveButtonDrawabletextback);
                    negativeButton.setBackgroundColor(negativeButtonDrawabletextback);
                    positiveButton.setTextColor(positiveButtonDrawabletext);
                    negativeButton.setTextColor(negativeButtonDrawabletext);
                }

                negativeButton.invalidate();
                positiveButton.invalidate();
            }
        });
        alertDialogObject.show();

    }//ShowDialogNoLatLngOnly


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    //    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestination = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
//            final String placeId = String.valueOf(item.placeId);
//            curlocation = String.valueOf(item.description);
//            //Log.i(LOG_TAG, "Selected: " + item.description);
//            //  PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
//            // placeResult.setResultCallback(mUpdatePlaceDetailsCallbackDestination);
//            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
//            etcharname.requestFocus();
//            /*Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
//            i.putExtra("DEST",dest);*/
//        }
//
//    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackDestination = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            LatLng mlatlng = place.getLatLng();

            String[] parts;
            String part1, part2;
            parts = mlatlng.toString().split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            lat = s1.split("\\(");
            lng = s2.split("\\)");

            latdb = Double.parseDouble(lat[1]);
            lngdb = Double.parseDouble(lng[0]);


        }
    };/*
    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(getActivity());
        }
        super.onStop();
    }
    private void stopAutoManage() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.stopAutoManage(getActivity());
    }
    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        if (ActivityCompat.checkSelfPermission(super.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(super.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,REQUEST,this);  // LocationListener
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
    }
*/

    /* @Override
     public void onConnectionSuspended(int arg0) {
		 // TODO Auto-generated method stub
		 mPlaceArrayAdapter.setGoogleApiClient(null);
	 }

	 @Override
	 public void onConnectionFailed(ConnectionResult arg0) {
		 // TODO Auto-generated method stub

	 }*/
    private boolean validateTime() {
        //Check if Call time is between Given Shift Time
        SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a", Locale.ENGLISH);

        String startTime, endTime, callTime, timeType = null;
        Date startTimeDate, endTimeDate, callTimeDate;
        startTimeDate = new Date();
        endTimeDate = new Date();
        callTimeDate = new Date();

        callTime = etcalltime.getText().toString();

        // this first if condition dont have endTime..
        if (etshifttime.getText().toString().length() == 8) {

            startTime = etshifttime.getText().toString().substring(0, 7);
            endTime = etshifttime.getText().toString().substring(0, 7);
            timeType = etshifttime.getText().toString().substring(5, 7);

            try {
                startTimeDate = sdfTime.parse(startTime);
                endTimeDate = sdfTime.parse(endTime);
                callTimeDate = sdfTime.parse(callTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

        } else if (etshifttime.getText().toString().length() == 18) {

            startTime = etshifttime.getText().toString().substring(0, 7);
            endTime = etshifttime.getText().toString().substring(11);
            timeType = etshifttime.getText().toString().substring(5, 7);

            try {
                startTimeDate = sdfTime.parse(startTime);
                endTimeDate = sdfTime.parse(endTime);
                callTimeDate = sdfTime.parse(callTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

        } else if (etshifttime.getText().toString().length() == 20) {

            startTime = etshifttime.getText().toString().substring(0, 8);
            endTime = etshifttime.getText().toString().substring(12);
            timeType = etshifttime.getText().toString().substring(6, 8);


            try {
                startTimeDate = sdfTime.parse(startTime);
                endTimeDate = sdfTime.parse(endTime);
                callTimeDate = sdfTime.parse(callTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

        }

        boolean result = false;
//        if (timeType.equalsIgnoreCase("AM")) {
//            result = false;
//            if ((startTimeDate.equals(callTimeDate) || startTimeDate.before(callTimeDate)) && endTimeDate.after(callTimeDate)) {
//                result = true;
//            }
//        } else if (timeType.equalsIgnoreCase("PM")) {
//            result = true;
//            if ((endTimeDate.equals(callTimeDate) || endTimeDate.before(callTimeDate)) && startTimeDate.after(callTimeDate)) {
//                result = false;
//            }
//        }
        //Callin will always be valid..
        result = true;
        return result;
    }//ValidateTime

    private void selectproductionhouse() {
        for (int m = 0; m < size; m++) {
            if (!Arystalies_name[m].equals("null")) {
                ph_alies.add(AryStprodhouname[m] + "/" + Arystalies_name[m]);
            } else {
                ph_alies.add(AryStprodhouname[m]);
            }
        }
        Aryph_alies = new String[ph_alies.size()];
        ph_alies.toArray(Aryph_alies);
        if (getActivity() != null)
            adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, Aryph_alies);
        autoproduction.setThreshold(1);
        //Set adapter to AutoCompleteTextView
        autoproduction.setAdapter(adapter);
        autoproduction.setOnItemSelectedListener(this);
        autoproduction.setOnItemClickListener(this);
    }//selectproductionhouse

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        fillSuggestions((String) arg0.getItemAtPosition(arg2));
    }

    //.. Fills automatic data
    private void fillSuggestions(String filmOrProductionHouse) {
        try {

            cursor = sqLiteDatabaseRead.rawQuery("select productionHouse, producerName from tbScheduleDetails where " +
                    "filmName = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                autoproduction.setText(cursor.getString(0));
                etprodname.setText(cursor.getString(1));
            }

            cursor = sqLiteDatabaseRead.rawQuery("select producerName from tbScheduleDetails where " +
                    "productionHouse = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etprodname.setText(cursor.getString(0));
            }

            cursor = sqLiteDatabaseRead.rawQuery("select producerName from tbProductionHouse where " +
                    "productionHouse = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etprodname.setText(cursor.getString(0));
            }

//... set authority no. directly
            cursor = sqLiteDatabaseRead.rawQuery("select authorityNo from tbAuthorityDetails where " +
                    "authorityName = ?", new String[]{filmOrProductionHouse});
            if (cursor.moveToFirst()) {
                etauthoritynumb.setText(cursor.getString(0));
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//fillSuggestions

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        if (pd != null) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }


    private void initialise(View view) {
        filmNameList = new ArrayList<String>();
        productionHouseList = new ArrayList<String>();
        producerNameList = new ArrayList<String>();
        authorityNoList = new ArrayList<String>();
        authorityNameList = new ArrayList<String>();

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "lane.ttf");
        llprodhouse = (LinearLayout) view.findViewById(R.id.llprodhouse);
        llprodName = (LinearLayout) view.findViewById(R.id.llprodName);
        llshifttime = (LinearLayout) view.findViewById(R.id.llshifttime);
        tvdays = (TextView) view.findViewById(R.id.tvdays);
        tvdays.setTypeface(custom_font);
        tvRdays = (TextView) view.findViewById(R.id.tvRdays);
        tvRdays.setTypeface(custom_font);

        //llratetype =(LinearLayout)view.findViewById(R.id.llratetype);
        //llratetype =(LinearLayout)view.findViewById(R.id.etdtshoot);
        //llratetype.setEnabled(false);
        //lldateshoot = (LinearLayout) view.findViewById(R.id.lldateshoot);
        llcalltime = (LinearLayout) view.findViewById(R.id.llinprogresscalltime);
        autoproduction = (AutoCompleteTextView) view.findViewById(R.id.Autoproduction);
        autoproduction.setTypeface(custom_font);
        etfilmname = (AutoCompleteTextView) view.findViewById(R.id.etfilmname);
        etfilmname.setTypeface(custom_font);
        etprodname = (AutoCompleteTextView) view.findViewById(R.id.etprodname);
        etprodname.setTypeface(custom_font);
        etshootloc = (EditText) view.findViewById(R.id.etshootloc);
        etshootloc.setTypeface(custom_font);
        etcalltime = (EditText) view.findViewById(R.id.etcalltime);
        etcalltime.setTypeface(custom_font);
        etcharname = (EditText) view.findViewById(R.id.etcharname);
        etcharname.setTypeface(custom_font);
        etrate = (EditText) view.findViewById(R.id.etrate);
        etrate.setTypeface(custom_font);
        etdueday = (EditText) view.findViewById(R.id.etdueday);
        etdueday.setTypeface(custom_font);
        etremark = (EditText) view.findViewById(R.id.etremark);
        etremark.setTypeface(custom_font);
        // etprodhouname=(EditText)view.findViewById(R.id.etprodhouname);
        // etprodhouname.setTypeface(custom_font);
        etshifttime = (EditText) view.findViewById(R.id.etshifttime);
        etshifttime.setTypeface(custom_font);
        etdtshoot = (EditText) view.findViewById(R.id.etdtshoot);
        etdtshoot.setTypeface(custom_font);
       /* //etratetype = (EditText) view.findViewById(R.id.etRateType);
        etratetype = (EditText) view.findViewById(R.id.etdtshoot);
        etratetype.setTypeface(custom_font);
        ///////////////////
        etratetype.setEnabled(false);*/
        btsave = (Button) view.findViewById(R.id.btsave);
        btsave.setTypeface(custom_font);

        btcancel = (LinearLayout) view.findViewById(R.id.btcancel);

        tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);
        tvWelcome.setTypeface(custom_font);
        tvsave = (TextView) view.findViewById(R.id.tvsaveandcreate);
        tvsave.setTypeface(custom_font);
        tvcancel = (TextView) view.findViewById(R.id.tvcancel);
        tvcancel.setTypeface(custom_font);

        etmaplocation = (TextView) view.findViewById(R.id.etmaplocation);
        etmaplocation.setTypeface(custom_font);

        llshootlocation = (LinearLayout) view.findViewById(R.id.llshootlocation);
        llAddressOfShoot = (LinearLayout) view.findViewById(R.id.llAddressOfShoot);
        llNameOfCharacter = (LinearLayout) view.findViewById(R.id.llNameOfCharacter);

        etconveyance = (EditText) view.findViewById(R.id.etconveyance);
        etconveyance.setTypeface(custom_font);
        etcallin = (EditText) view.findViewById(R.id.etcallin);
        etcallin.setTypeface(custom_font);

        rgCallinTime = (RadioGroup) view.findViewById(R.id.rgCallinTime);
        radioCallinAm = (RadioButton) view.findViewById(R.id.radioCallinAm);
        radioCallinPm = (RadioButton) view.findViewById(R.id.radioCallinPm);

        rgPackTime = (RadioGroup) view.findViewById(R.id.rgPackTime);
        radioPackupAm = (RadioButton) view.findViewById(R.id.radioPackupAm);
        radioPackupPm = (RadioButton) view.findViewById(R.id.radioPackupPm);

        etpackup = (EditText) view.findViewById(R.id.etpackup);
        etpackup.setTypeface(custom_font);
        etauthoritynm = (AutoCompleteTextView) view.findViewById(R.id.etauthoritynm);
        etauthoritynm.setTypeface(custom_font);
        etauthoritynumb = (AutoCompleteTextView) view.findViewById(R.id.etauthoritynumb);
        etauthoritynumb.setTypeface(custom_font);
        etauthorityemail = (EditText) view.findViewById(R.id.etauthorityemail);
        etauthorityemail.setTypeface(custom_font);

        btncallin = (Button) view.findViewById(R.id.btncallin);
        btncallin.setTypeface(custom_font);
        btncallinsubmit = (Button) view.findViewById(R.id.btncallinsubmit);
        btncallinsubmit.setTypeface(custom_font);
        btnpackup = (Button) view.findViewById(R.id.btnpackup);
        btnpackup.setTypeface(custom_font);
        btnpackupsubmit = (Button) view.findViewById(R.id.btnpackupsubmit);
        btnpackupsubmit.setTypeface(custom_font);
        btsubmit = (Button) view.findViewById(R.id.btsubmit);
        btsubmit.setTypeface(custom_font);

        tvconveyance = (TextView) view.findViewById(R.id.tvconveyance);
        tvconveyance.setTypeface(custom_font);
        tvInTimePackupTime = (TextView) view.findViewById(R.id.tvInTimePackupTime);
        tvInTimePackupTime.setTypeface(custom_font);
        tvAuthorityDetails = (TextView) view.findViewById(R.id.tvAuthorityDetails);
        tvAuthorityDetails.setTypeface(custom_font);
        tvauthoritynm = (TextView) view.findViewById(R.id.tvauthoritynm);
        tvauthoritynm.setTypeface(custom_font);
        tvAuthorityNumber = (TextView) view.findViewById(R.id.tvAuthorityNumber);
        tvAuthorityNumber.setTypeface(custom_font);
        tvAuthoritySign = (TextView) view.findViewById(R.id.tvAuthoritySign);
        tvAuthoritySign.setTypeface(custom_font);
        tvProductionHouseEmail = (TextView) view.findViewById(R.id.tvProductionHouseEmail);
        tvProductionHouseEmail.setTypeface(custom_font);

        llconveyancedetail = (LinearLayout) view.findViewById(R.id.llconveyancedetail);
        llph_email = (LinearLayout) view.findViewById(R.id.llph_email);
        mainScroll = (ScrollView) view.findViewById(R.id.mainScroll);

        ivauthoritysign = (ImageView) view.findViewById(R.id.ivauthoritysign);

        llfilmName = (LinearLayout) view.findViewById(R.id.llfilmName);
        lldateshoot = (LinearLayout) view.findViewById(R.id.lldateshoot);
        llSubPage = (LinearLayout) view.findViewById(R.id.llSubPage);
        llsubmitPage = (LinearLayout) view.findViewById(R.id.llsubmitPage);

        llPagePackup = (LinearLayout) view.findViewById(R.id.llPagePackup);

        ivproducer = (ImageView) view.findViewById(R.id.ivproducer);
        ivproductionHouse = (ImageView) view.findViewById(R.id.ivproductionHouse);


        pd = new ProgressDialog(getActivity());

//		readLocation(etdtshoot, "onCreate()");

    }//initialise

    //
//	private void readLocation(TextView tv, String status){
//		DisplayMetrics displayMetrics = new DisplayMetrics();
//		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//		int offsetX = displayMetrics.widthPixels - mainScroll.getMeasuredWidth();
//		int offsetY = displayMetrics.heightPixels - mainScroll.getMeasuredHeight();
//
//		int[] locationInWindow = new int[2];
//		object.getLocationInWindow(locationInWindow);
//		int[] locationOnScreen = new int[2];
//		object.getLocationOnScreen(locationOnScreen);
//
//		tv.setText(
//				"\n" + status +"\n"
//						+ "getLocationInWindow() - " + locationInWindow[0] + " : " + locationInWindow[1] + "\n"
//						+ "getLocationOnScreen() - " + locationOnScreen[0] + " : " + locationOnScreen[1] + "\n"
//						+ "Offset x: y - " + offsetX + " : " + offsetY);
//
//	}
    private void fillAutoComplete() {
        filmNameList.clear();
        producerNameList.clear();
        productionHouseList.clear();
        authorityNoList.clear();
        authorityNameList.clear();


        GlobalClass.printLog(getActivity(), "--fillAutoComplete METHOD-----HHHHHHHHH-----------");

        // scheduleDate,
        cursor = sqLiteDatabaseRead.rawQuery("select filmName, productionHouse, producerName from tbScheduleDetails", null);
        if (cursor.moveToFirst()) {
            do {
                filmNameList.add(cursor.getString(0));
                productionHouseList.add(cursor.getString(1));
                producerNameList.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        try {
            cursor2 = sqLiteDatabaseRead.rawQuery("select authorityName, authorityNo from tbAuthorityDetails", null);
            if (cursor2 != null && cursor2.moveToFirst()) {
                do {

                    GlobalClass.printLog(getActivity(), "--fillAutoComplete METHOD-----IIIIIIIIIII-----------");

                    authorityNameList.add(cursor2.getString(0));
                    authorityNoList.add(cursor2.getString(1));
                } while (cursor2.moveToNext());
            }
        } catch (Exception e) {
            GlobalClass.printLog(getActivity(), "----+++++++++++++++++++++++++++++++++++++++++++++=-" + e);
            sqLiteDatabaseRead.execSQL("CREATE TABLE IF NOT EXISTS tbAuthorityDetails (authorityName text,authorityNo text)");
        }


        cursor = sqLiteDatabaseRead.rawQuery("select productionHouse, producerName from tbProductionHouse", null);
        if (cursor.moveToFirst()) {
            do {
                GlobalClass.printLog(getActivity(), "--fillAutoComplete METHOD-----JJJJJJJJJ-----------");

                productionHouseList.add(cursor.getString(0));
                producerNameList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }


        List<String> filmNameRList = eliminateRedundancy(filmNameList);
        List<String> productionHouseRList = eliminateRedundancy(productionHouseList);
        List<String> producerNameRList = eliminateRedundancy(producerNameList);
        List<String> authorityNameRList = eliminateRedundancy(authorityNameList);
        List<String> authorityNoRList = eliminateRedundancy(authorityNoList);

        String[] productionHouse = new String[productionHouseRList.size()];
        productionHouseRList.toArray(productionHouse);

        String[] filmName = new String[filmNameRList.size()];
        filmNameRList.toArray(filmName);

        String[] producerName = new String[producerNameRList.size()];
        producerNameRList.toArray(producerName);

        String[] authorityName = new String[authorityNameRList.size()];
        authorityNameRList.toArray(authorityName);

        String[] authorityNo = new String[authorityNoRList.size()];
        authorityNoRList.toArray(authorityNo);

        if (getActivity() != null) {
            adapterFilmName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, filmName);
            adapterProductionHouse = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, productionHouse);
            adapterProducerName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, producerName);
            if (authorityName != null && authorityName.length > 0) {
                adapterAuthorityName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, authorityName);
                adapterAuthorityNo = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, authorityNo);
            }

            autoproduction.setThreshold(1);
            autoproduction.setAdapter(adapterProductionHouse);
            autoproduction.setOnItemSelectedListener(this);
            autoproduction.setOnItemClickListener(this);

            etfilmname.setThreshold(1);
            etfilmname.setAdapter(adapterFilmName);
            etfilmname.setOnItemSelectedListener(this);
            etfilmname.setOnItemClickListener(this);

            etauthoritynm.setThreshold(1);
            etauthoritynm.setAdapter(adapterAuthorityName);
            etauthoritynm.setOnItemSelectedListener(this);
            etauthoritynm.setOnItemClickListener(this);

            etauthoritynumb.setThreshold(1);
            etauthoritynumb.setAdapter(adapterAuthorityNo);
            etauthoritynumb.setOnItemSelectedListener(this);
            etauthoritynumb.setOnItemClickListener(this);

            etprodname.setThreshold(1);
            etprodname.setAdapter(adapterProducerName);
        }
    }//fillAutoComplete

    private List<String> eliminateRedundancy(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); ) {
                if (list.get(j).equals(list.get(i))) {
                    list.remove(j);
                } else
                    j++;
            }
        }
        return list;
    }//eliminateRedundancy

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }//isNetworkConnected()

    private void selectShiftTime() {
        GlobalClass.printLog(getActivity(), "--selectShiftTime---CCCCCC-----------");

        shift_start = new ArrayList<String>();

        cursor = sqLiteDatabaseRead.rawQuery("select shift_start from sqtb_shift_time", null);


        if (cursor.moveToFirst()) {
            do {
//..  changed "7:00 AM to 7:00 PM" to 7:00 AM
                String[] parts = cursor.getString(0).split("to");
                String part1 = parts[0];
                shift_start.add(part1);
//                shift_start.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        stshifttime = new String[shift_start.size()];
        shift_start.toArray(stshifttime);

    }//selectShiftTime

    //select's proucer name and production house name from database
    public void SelectData() {
        //showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        client.post(Config.UrlselectProductionHouseProducer, params, new JsonHttpResponseHandler() {

            @Override

            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("ProductionHouseData");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject j1 = jsonArray.getJSONObject(i);

                        house_id.add(j1.getString("house_id"));
                        stprodhouname.add(j1.getString("house_name"));
                        stproducer_name.add(j1.getString("producer_name"));
                        stalies_name.add(j1.getString("alies_name"));
                    }


                    AryStprodhouname = new String[stprodhouname.size()];
                    stprodhouname.toArray(AryStprodhouname);
                    Arystalies_name = new String[stalies_name.size()];
                    stalies_name.toArray(Arystalies_name);
                    size = AryStprodhouname.length;
                    selectproductionhouse();
                    //displaydata();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Network Connection Failed", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//SelectData

    public String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return member_id + "_" + strDate;
    }

    public void insertDiaryEntry() {
        entry_id = getCurrentTimeStamp();
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("member_id", member_id);
        params.put("shoot_date", etdtshoot.getText().toString().trim());
        params.put("played_character", etcharname.getText().toString().trim());
        params.put("shoot_location", etshootloc.getText().toString().trim());
        params.put("film_tv_name", etfilmname.getText().toString().trim());
        params.put("shift_time", etshifttime.getText().toString().trim());
        params.put("call_time", etcalltime.getText().toString().trim());
        params.put("rate_type", tvRdays.getText().toString().trim());
        params.put("rate", etrate.getText().toString().trim());
        params.put("due_days", etdueday.getText().toString().trim());
        params.put("remark", "");//etremark.getText().toString().trim()
        params.put("schedule_status", "pending");
        params.put("lat", latdb);
        params.put("lng", lngdb);
        params.put("shoot_map_location", etmaplocation.getText().toString().trim());
        params.put("new_house_name", autoproduction.getText().toString().trim());
        params.put("new_producer_name", etprodname.getText().toString().trim());

        //etprodhouname,etprodname

        client.post(Config.URLDiaryEntryInsert, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    if (s1.equals("1")) {
                        insert_tbScheduleDetails(entry_id, true);
                        ////////////////////////
                        InsertSQLite(entry_id, true, !isCallinAvailable);
                        ////////////////////////
                        showToast("Entry done successfully", Toast.LENGTH_LONG);
                        /*etcalltime.setText("");
                        etshifttime.setText("");
						etdtshoot.setText("");
						//etratetype.setText("");
						etfilmname.setText("");
						etprodname.setText("");
						etshootloc.setText("");
						etcharname.setText("");
						etrate.setText("");
						etdueday.setText("");
						etremark.setText("");*/
                        DestroyPreferences();
                        //Create Background Service for Auto In Time Recording
                        //getActivity().startService(new Intent(getActivity(), BackgroundService.class));
                        Fragment fr = new DiaryentryFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Network Error, Entry Saved Offline !!", Toast.LENGTH_LONG);
                insert_tbScheduleDetails(entry_id, false);
                InsertSQLite(entry_id, false, !isCallinAvailable);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
                Fragment fr = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
            }
        });


    }//insertDiaryEntry

    private void insert_tbScheduleDetails(String entry_id, Boolean online) {
        //entryId, scheduleDate, filmName, productionHouse, producerName, scheduleStatus
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbScheduleDetails values(?,?,?,?,?,?,?,?)");
        sqLiteStatement.bindString(1, member_id);
        sqLiteStatement.bindString(2, entry_id);
        sqLiteStatement.bindString(3, etdtshoot.getText().toString().trim());
        sqLiteStatement.bindString(4, etfilmname.getText().toString().trim());
        sqLiteStatement.bindString(5, autoproduction.getText().toString().trim());
        sqLiteStatement.bindString(6, etprodname.getText().toString().trim());
        sqLiteStatement.bindString(7, "pending");
        if (online) {
            sqLiteStatement.bindString(8, "done");
        } else {
            sqLiteStatement.bindString(8, "no");
        }
        long result = sqLiteStatement.executeInsert();

    }//insert_tbScheduleDetails

    public void updateDiaryEntry() {
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("member_id", member_id);
        params.put("shoot_date", etdtshoot.getText().toString().trim());
        params.put("played_character", etcharname.getText().toString().trim());
        //params.put("shoot_location", etshootloc.getText().toString().trim());
        params.put("shoot_location", etshootloc.getText().toString().trim());
        params.put("film_tv_name", etfilmname.getText().toString().trim());
        params.put("shift_time", etshifttime.getText().toString().trim());
        params.put("call_time", etcalltime.getText().toString().trim());
        params.put("rate_type", tvRdays.getText().toString().trim());
        params.put("rate", etrate.getText().toString().trim());
        params.put("due_days", etdueday.getText().toString().trim());
        params.put("remark", "");
        params.put("schedule_status", "pending");
        params.put("lat", latdb);
        params.put("lng", lngdb);
        params.put("shoot_map_location", etmaplocation.getText().toString().trim());
        params.put("new_house_name", autoproduction.getText().toString().trim());
        params.put("new_producer_name", etprodname.getText().toString().trim());

        client.post(Config.UrlUpdateDiaryEntryInsert, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    if (s1.equals("1")) {
                        InsertSQLite(entry_id, true, !isCallinAvailable);
                        updateScheduleSQLite(entry_id, true, true);

                        showToast("Entry done successfully", Toast.LENGTH_LONG);
                        etcalltime.setText("");
                        etshifttime.setText("");
                        etdtshoot.setText("");
                        etfilmname.setText("");
                        etprodname.setText("");
                        etshootloc.setText("");
                        etcharname.setText("");
                        etrate.setText("");
                        etdueday.setText("");
                        etremark.setText("");
                        DestroyPreferences();
                        //Create Background Service for Auto In Time Recording
                        //getActivity().startService(new Intent(getActivity(), BackgroundService.class));
                    }

                    //....CALLING fragment again
                    Fragment fr = new DiaryentryFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Network Error, Entry Saved Offline !!", Toast.LENGTH_LONG);
                InsertSQLite(entry_id, false, !isCallinAvailable);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
                Fragment fr = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
            }
        });
    }//updateDiaryEntry

    private void updateScheduleSQLite(String entryID, Boolean isOnServer, Boolean isNetworkAvailable) {
        if (isOnServer && isNetworkAvailable) {
            //entryId, scheduleDate, filmName, productionHouse, producerName
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
                    "filmName='" + etfilmname.getText().toString().trim() + "', " +
                    "productionHouse='" + autoproduction.getText().toString().trim() + "', " +
                    "producerName='" + etprodname.getText().toString().trim() + "', " +
                    "ServerEntryStatus='done' where entryId = '" + entryID + "'");
            long result = sqLiteStatement.executeUpdateDelete();
        } else if (!isOnServer) {
            //entryId, scheduleDate, filmName, productionHouse, producerName
            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
                    "filmName='" + etfilmname.getText().toString().trim() + "', " +
                    "productionHouse='" + autoproduction.getText().toString().trim() + "', " +
                    "producerName='" + etprodname.getText().toString().trim() + "', ServerEntryStatus='no' where entryId = '" + entryID + "'");
            long result = sqLiteStatement.executeUpdateDelete();
        }
//        else if (isOnServer && (!isNetworkAvailable)) {
//            if ((!initialFilmName.equals(etfilmname.getText().toString().trim())) || (!initialProducerName.equals(etprodname.getText().toString().trim())) || (!initialProductionHouse.equals(autoproduction.getText().toString().trim()))) {
//                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update tbScheduleDetails set " +
//                        "filmName='" + etfilmname.getText().toString().trim() + "', " +
//                        "productionHouse='" + autoproduction.getText().toString().trim() + "', " +
//                        "producerName='" + etprodname.getText().toString().trim() + "', ServerEntryStatus='update' where entryId = '" + entryID + "'");
//                long result = sqLiteStatement.executeUpdateDelete();
//            }
//        }

//        if ((tvcalltime.getText().toString().trim().length() > 0) & (!initialCallTime.equals(.getText().toString().trim()))) {
//            SetAlertOFF(entryID);
//            String date = etdtshoot.getText().toString().trim();
//            String hours = tvcalltime.getText().toString().trim();
//            final String dateHours = date + " " + hours;
//            long seconds = CalculateSecondsFromCurrentDate(dateHours);
//            int requestCode = startAlert(seconds, entryID);
//
//            if (sqtbDiaryEntryIsInsert) {
//                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry(entry_id, member_id, " +
//                        "call_time, requestCode_Alarm, alert_flag) values(?,?,?,?,?)");
//                sqLiteStatement.bindString(1, entryID);
//                sqLiteStatement.bindString(2, member_id);
//                sqLiteStatement.bindString(3, tvcalltime.getText().toString());
//                sqLiteStatement.bindLong(4, requestCode);
//                sqLiteStatement.bindString(5, alert_flag);
//                long res = sqLiteStatement.executeInsert();
//            } else {
//                sqLiteStatement = sqLiteDatabaseWrite.compileStatement("Update sqtb_diary_entry set call_time=?, alert_flag=?," +
//                        "requestCode_Alarm=? where entry_id='" + entryID + "'");
//                sqLiteStatement.bindString(1, tvcalltime.getText().toString().trim());
//                sqLiteStatement.bindString(2, alert_flag);
//                sqLiteStatement.bindLong(3, requestCode);
//                long result = sqLiteStatement.executeUpdateDelete();
//            }
//        }
        etfilmname.setText("");
        autoproduction.setText("");
        etprodname.setText("");
//        tvcalltime.setText("");
    }//updateScheduleSQLite

    //Calculate Time in seconds from Current Date to Given Date
    private long CalculateSecondsFromCurrentDate(String dateHours) {

        long seconds = 0;
        Date now, eventDate, currentDate;

        SimpleDateFormat sdfformat = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
        now = new Date();
        String current = sdfformat.format(now);

        currentDate = new Date();
        eventDate = new Date();

        try {
            currentDate = sdfformat.parse(current);
            eventDate = sdfformat.parse(dateHours);

            //in milliseconds
            long diff = eventDate.getTime() - currentDate.getTime();

            long diffSeconds = diff / 1000;
            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);

            long on_head = diffSeconds + diffMinutes + diffHours + diffDays;
            String alert = "00:00";
            cursor = sqLiteDatabaseRead.rawQuery("select alert_before from sqtb_settings", null);
            if (cursor.moveToFirst()) {
                do {
                    alert = cursor.getString(0);
                } while (cursor.moveToNext());
            }

            String[] units = alert.split(":"); //will break the string up into an array
            long hours = Integer.parseInt(units[0]); //hours element
            long minutes = Integer.parseInt(units[1]); //minutes element
            long before_head = 60 * 60 * hours + minutes * 60; //add up our values

            before_head = on_head - before_head;

            if (before_head > 0) {
                seconds = before_head;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return seconds;

    }//CalculateSecondsFromCurrentDate

    public int startAlert(long seconds, String entry_id) {

        String check_alert_setting = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                check_alert_setting = cursor.getString(0);
            } while (cursor.moveToNext());
        }


        int requestCode = (int) (new Date().getTime() / 1000);
        // If alerts are ON in setting menu then only set Alarm for the Event
        if (check_alert_setting.equals("ON") && seconds > 0) {
            long interval = 3000;//Reapeat Alarm after every 3 second
            Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
            intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
//            intent.putExtra("entry_ids",entry_id);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            // alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ (seconds * 1000), pendingIntent);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (seconds * 1000), pendingIntent);
            //Toast.makeText(getActivity(), "Alarm after " + seconds + " seconds.. Code: "+requestCode,Toast.LENGTH_LONG).show();
        }

        return requestCode;
    }//startAlert

    private void SetAlertOFF(String entryID) {
        int requestCode;
        try {
            cursor = sqLiteDatabaseRead.rawQuery("select requestCode_Alarm from sqtb_diary_entry where entry_id=?", new String[]{entryID});
            if (cursor.moveToFirst()) {
                requestCode = cursor.getInt(0);
                Intent intent = new Intent(getActivity(), AlertBroadcastReceiver.class);
                intent.putExtra("Alert_requestCode", String.valueOf(requestCode));
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                // Toast.makeText(this, "Alarm Canceled.. Code: "+requestCode,Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }//SetAlertOFF

    //on button submit click
    private void InsertSQLite(String entry_id, Boolean isonline, Boolean isInsert) {
        String alert_flag = "ON";
        cursor = sqLiteDatabaseRead.rawQuery("select alert from sqtb_settings", null);
        if (cursor.moveToFirst()) {
            do {
                alert_flag = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        if (isInsert) {
            String date = etdtshoot.getText().toString().trim();
            String hours = etcalltime.getText().toString().trim();
            final String dateHours = date + " " + hours;
            long seconds = CalculateSecondsFromCurrentDate(dateHours);
            int requestCode = startAlert(seconds, entry_id);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_entry values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            sqLiteStatement.bindString(1, entry_id);
            sqLiteStatement.bindString(2, member_id);
            sqLiteStatement.bindString(3, etdtshoot.getText().toString().trim());
            sqLiteStatement.bindString(4, etfilmname.getText().toString().trim());
            sqLiteStatement.bindString(5, etshifttime.getText().toString().trim());
            sqLiteStatement.bindString(6, etcalltime.getText().toString().trim());
            if (!isDeviceLocation) {
                sqLiteStatement.bindString(7, etshootloc.getText().toString().trim());
            } else {
                sqLiteStatement.bindString(7, "");
            }
            sqLiteStatement.bindString(8, String.valueOf(latdb));
            sqLiteStatement.bindString(9, String.valueOf(lngdb));
            sqLiteStatement.bindString(10, "pending");
            sqLiteStatement.bindString(11, alert_flag);
            sqLiteStatement.bindLong(12, requestCode);
            sqLiteStatement.bindString(13, "undone");
            sqLiteStatement.bindString(14, etcharname.getText().toString().trim());
            sqLiteStatement.bindString(15, etrate.getText().toString().trim());
            sqLiteStatement.bindString(16, etdueday.getText().toString().trim());
            sqLiteStatement.bindString(17, "");//etremark.getText().toString().trim()
            if (isonline) {
                sqLiteStatement.bindString(18, "done");
            } else {
                sqLiteStatement.bindString(18, "no");
            }
            sqLiteStatement.bindString(19, tvRdays.getText().toString().trim());
            sqLiteStatement.bindString(20, etmaplocation.getText().toString().trim());

            long result = sqLiteStatement.executeInsert();
        } else {
            SetAlertOFF(entry_id);
            String date = etdtshoot.getText().toString().trim();
            String hours = etcalltime.getText().toString().trim();
            final String dateHours = date + " " + hours;
            long seconds = CalculateSecondsFromCurrentDate(dateHours);
            int requestCode = startAlert(seconds, entry_id);

            sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set shoot_date=?, film_tv_name=?, " +
                    "shift_time=?, call_time=?, shoot_location=?, lat=?, lng=?, schedule_status=?, alert_flag=?, requestCode_Alarm=?," +
                    "Task_callin=?, characterName=?, rate=?, paymentAfter=?, remark=?, ServerEntryStatus=?, rateType=? , shoot_map_location=?" +
                    " where entry_id='" + entry_id + "' and member_id='" + member_id + "'");
            sqLiteStatement.bindString(1, etdtshoot.getText().toString().trim());
            sqLiteStatement.bindString(2, etfilmname.getText().toString().trim());
            sqLiteStatement.bindString(3, etshifttime.getText().toString().trim());
            sqLiteStatement.bindString(4, etcalltime.getText().toString().trim());
            if (!isDeviceLocation) {
                sqLiteStatement.bindString(5, etshootloc.getText().toString().trim());
            } else {
                sqLiteStatement.bindString(5, "");
            }
            sqLiteStatement.bindString(6, String.valueOf(latdb));
            sqLiteStatement.bindString(7, String.valueOf(lngdb));
            sqLiteStatement.bindString(8, "pending");
            sqLiteStatement.bindString(9, alert_flag);
            sqLiteStatement.bindLong(10, requestCode);
            sqLiteStatement.bindString(11, "undone");
            sqLiteStatement.bindString(12, etcharname.getText().toString().trim());
            sqLiteStatement.bindString(13, etrate.getText().toString().trim());
            sqLiteStatement.bindString(14, etdueday.getText().toString().trim());
            sqLiteStatement.bindString(15, etremark.getText().toString().trim());
            if (isonline) {
                sqLiteStatement.bindString(16, "done");
            } else {
                sqLiteStatement.bindString(16, "no");
            }
            sqLiteStatement.bindString(17, tvRdays.getText().toString().trim());
            sqLiteStatement.bindString(18, etmaplocation.getText().toString().trim());
            long res = sqLiteStatement.executeUpdateDelete();
        }
    }//InsertSQLite

    private void showprogressdialog() {
        pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.please_wait));
        pd.setMessage(getString(R.string.downloading_required_data));
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
        //pd.dismiss();
    }

    public void insertDiaryEntrySaveONLY() {
        final String entry_id = getCurrentTimeStamp();
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("member_id", member_id);
        //params.put("house_id", Sthouse_id);
        params.put("shoot_date", etdtshoot.getText().toString().trim());
        params.put("played_character", etcharname.getText().toString().trim());
        params.put("shoot_location", etshootloc.getText().toString().trim());
        params.put("film_tv_name", etfilmname.getText().toString().trim());
        params.put("shift_time", etshifttime.getText().toString().trim());
        params.put("call_time", etcalltime.getText().toString().trim());
        // params.put("rate_type", etratetype.getText().toString().trim());
        params.put("rate_type", "per day");
        params.put("rate", etrate.getText().toString().trim());
        params.put("due_days", etdueday.getText().toString().trim());
        params.put("remark", etremark.getText().toString().trim());
        params.put("schedule_status", "pending");
        params.put("lat", latdb);
        params.put("lng", lngdb);
        params.put("shoot_map_location", etmaplocation.getText().toString().trim());

        params.put("new_house_name", autoproduction.getText().toString().trim());
        params.put("new_producer_name", etprodname.getText().toString().trim());

        client.post(Config.URLDiaryEntryInsert, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    if (s1.equals("1")) {
                        ////////////////////////
                        InsertSQLite(entry_id, true, !isCallinAvailable);
                        ////////////////////////
                        showToast("Entry done successfully", Toast.LENGTH_LONG);
                        etcalltime.setText("");
                        etshifttime.setText("");
                        etdtshoot.setText("");
                        autoproduction.setText("");
                        etfilmname.setText("");
                        etprodname.setText("");
                        etshootloc.setText("");
                        etcharname.setText("");
                        etrate.setText("");
                        etdueday.setText("");
                        etremark.setText("");
                        DestroyPreferences();
                        //Create Background Service for Auto In Time Recording
                        //getActivity().startService(new Intent(getActivity(), BackgroundService.class));

                        Fragment fr = new DiaryentryFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fr).commit();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Error in Connection", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });


    }//insertDiaryEntrySaveONLY

    private void ShowdialogMapLoc() {


    }//ShowdialogMapLoc

    private void showdialog() {
        ph_alies.clear();
        for (int m = 0; m < size; m++) {
            if (!Arystalies_name[m].equals("null")) {
                ph_alies.add(AryStprodhouname[m] + "/" + Arystalies_name[m]);
            } else {
                ph_alies.add(AryStprodhouname[m]);
            }
        }
        Aryph_alies = new String[ph_alies.size()];
        ph_alies.toArray(Aryph_alies);

        //Code for popup dialog.
        try {
            View customTitle = View.inflate(getActivity(), R.layout.titleproductionhouse, null);
            builder.setCustomTitle(customTitle);
            // builder.setTitle("Select Production House");
            builder.setSingleChoiceItems(Aryph_alies, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etprodhouname.setText(Aryph_alies[i]);
                    etprodname.setText(stproducer_name.get(i));
                    Sthouse_id = house_id.get(i);
                    dialogInterface.cancel();
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialog

    private void showdialog1() {
        try {
            View customTitle = View.inflate(getActivity(), R.layout.titleshifttime, null);
            builder1.setCustomTitle(customTitle);
            // builder1.setTitle("Select Shift Time");
            int selected = -1;
            String currentValue = etshifttime.getText().toString().trim();
            for (int i = 0; i < stshifttime.length; i++) {
                if (currentValue.equals(stshifttime[i])) {
                    selected = i;
                    break;
                }
            }
            builder1.setSingleChoiceItems(stshifttime, selected, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etshifttime.setText(stshifttime[i]);
                    if (etcalltime.getText().toString().trim().length() == 0)
                        showcalltime();
                    //etrate.requestFocus();
                    dialogInterface.cancel();
                }
            });
            builder1.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialog1

    private void showdialogratetype() {
        try {
            View customTitle = View.inflate(getActivity(), R.layout.titleratetype, null);
            builder2.setCustomTitle(customTitle);
            int selected = -1;
            final String rateTypes[] = new String[]{"per Day", "per Film/TV Serial"};
            String currentValue = tvRdays.getText().toString().trim();
            for (int i = 0; i < rateTypes.length; i++) {
                if (currentValue.equals(rateTypes[i])) {
                    selected = i;
                    break;
                }
            }
            builder2.setSingleChoiceItems(rateTypes, selected, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //tvRdays.setText(rateTypes[i]);
                    //Here are some changes handle the incomming data
                    try {
                        if (rateTypes[i] != null) {
                            tvRdays.setText(rateTypes[i]);
                        } else {
                            tvRdays.setText(" Per Day");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvRdays.setText(" Per Day");
                    }

                    dialogInterface.cancel();
                }
            });
            builder2.show();
        } catch (Exception e) {
            Log.v("tag", "dialg error desc " + e.toString());
        }
    }//showdialogratetype

    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener listenerd = new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

            int mnth = monthOfYear + 1;
            String month = "" + mnth;
            if (mnth < 10) {
                month = "0" + (mnth);
            }

            String datee = "" + dayOfMonth;
            if (dayOfMonth < 10) {
                datee = "0" + dayOfMonth;
            }

            String date = datee + "/" + month + "/" + year;
            etdtshoot.setText(date);
            etshootloc.requestFocus();
        }
    };

    public void showcal() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                listenerd,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.bgColor));


        dpd.vibrate(true);

        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

    }//showcal

   /* @Override
    public void onClick(View v) {

    }*/

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        etdtshoot.setText(date);
        etshootloc.requestFocus();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }


  /*  DatePickerDialog.OnDateSetListener datelistener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            int month = monthOfYear + 1;
            String date,Daydate = ""+dayOfMonth;

            if(dayOfMonth<10){
                Daydate = "0"+dayOfMonth;
            }

            if(month>9) {date = Daydate + "/" + month + "/" + year;
            }else{
                date = Daydate + "/0" + month + "/" + year;
            }
            etdtshoot.setText(date);
        }
    };*/

    public void validation() {
        check = 0;
        {
            if (etfilmname.getText().toString().length() == 0) {
                etfilmname.setError("Enter the film or serial name!");

            } else {
                check++;
            }
            if (autoproduction.getText().toString().length() == 0) {
                autoproduction.setError("Select production house!");

            } else {
                check++;
            }
            if (etdtshoot.getText().toString().length() == 0) {
                etdtshoot.setError("Select date of shoot!");

            } else {
                check++;
            }
            if (etshootloc.getText().toString().length() == 0) {
                check++;

            } else {
                check++;
            }

            if (etcharname.getText().toString().length() == 0) {
                etcharname.setError("Enter charactor name!");

            } else {
                check++;
            }
            if (etshifttime.getText().toString().length() == 0) {
                etshifttime.setError("Enter shoot location!");

            } else {
                check++;
            }
            if (etcalltime.getText().toString().length() == 0) {
                etcalltime.setError("Select callin time!");

            } else {
                check++;
            }
           /* if (etratetype.getText().toString().length() == 0) {
                etratetype.setError("Select rate type!");

            } else {
                check++;
            }*/
            if (etrate.getText().toString().length() == 0) {
                //etrate.setError("Enter rate!");
                check++;

            } else {
                check++;
            }

            if (etdueday.getText().toString().length() == 0) {
                etdueday.setError("Enter number of due days!");

            } else {
                check++;
            }
            return;
        }
    }//validation


    com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener timeSet = new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
            String minuteString = minute < 10 ? "0" + minute : "" + minute;
            String secondString = second < 10 ? "0" + second : "" + second;
            String day_night = "AM";
            int hours = hourOfDay;
            if (hourOfDay >= 12) {
                day_night = "PM";
            }

            if (hourOfDay > 12) {

                hours = hourOfDay - 12;
            }

            String time = hours + ":" + minuteString + " " + day_night;
            etcalltime.setText(time);
            etrate.requestFocus();
        }
    };

    public void showcalltime() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                timeSet,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(true);
        tpd.enableMinutes(true);
        tpd.setAccentColor(getResources().getColor(R.color.bgTitleTab));
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }//showcalltime

    private String getCurrentTimeSignature() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentTime

    private String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }//getCurrentTime

    //Code of Touch Signature
    public void showsignaturedialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 1000f;
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setContentView(li.inflate(R.layout.signature, null));
        mClearButton = (Button) dialog.findViewById(R.id.clear_button);
        mSaveButton = (Button) dialog.findViewById(R.id.save_button);
        mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                // Toast.makeText(dialog.getContext(), "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureBitmap = mSignaturePad.getSignatureBitmap();
                ivauthoritysign.setImageBitmap(signatureBitmap);

                dialog.dismiss();
            }
        });
        dialog.show();

    }//showsignaturedialog

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second


        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return;
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            sublat = String.valueOf(mLastLocation.getLatitude());
            sublon = String.valueOf(mLastLocation.getLongitude());
            updateUI();
        } else {
            //checkGps();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    @Override
    public void onLocationChanged(Location location) {
        sublat = String.valueOf(location.getLatitude());
        sublon = String.valueOf(location.getLongitude());
        updateUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        checkGpsIsOn();
    }

    private void checkGpsIsOn() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        if (mGoogleApiClient != null) {
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }

                }
            });
        }

    }//checkGpsIsOn

    void updateUI() {
        MyLat = Double.parseDouble(sublat);
        MyLong = Double.parseDouble(sublon);
    }//updateUI

    public void checkGps() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.gps_dialog);
        dialog.setTitle("GPS");
        dialog.setCancelable(false);


        Button gpsSettings = (Button) dialog.findViewById(R.id.gps_sett_button);
        gpsSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
            }
        });

        Button gpsCancel = (Button) dialog.findViewById(R.id.gps_cancel_button);
        gpsCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                btncallinsubmit.setEnabled(false);
                btnpackupsubmit.setEnabled(false);
            }
        });
        dialog.show();
    }//checkGps

    private class Encode_image extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String sub = createImgName();
            String img_name = etauthoritynm.getText().toString().trim().replaceAll("\\s+", "") + "_" + sub.replaceAll("\\s+", "");
            if (isNetworkConnected()) {
                updateAuthoritySign(img_name);
            } else {
                storeOfflineAuthorization(img_name);
            }
        }
    }//Encode_image

    //Rename image name
    private String createImgName() {
        String imgName = "";
        SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyyHHmmssSSS", Locale.ENGLISH);//yyyy-MM-dd HH:mm:ss.SSS
        Date now = new Date();
        String strDate = sdfDate.format(now);
        imgName = member_id + strDate;
        return imgName;
    }

    private void updateAuthoritySign(final String img_name) {
        GlobalClass.printLog(getActivity(), "-----------img_name----" + img_name);
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("encoded_string", encoded_string);
        params.put("img_name", img_name);
        params.put("entry_id", entry_id);
        params.put("authority_sign_date", submit_time);
        params.put("authority_nm", etauthoritynm.getText().toString().trim());
        params.put("authority_number", etauthoritynumb.getText().toString().trim());
        params.put("authority_email", etauthorityemail.getText().toString().trim());
        params.put("productionHouse", autoproduction.getText().toString().trim());
        params.put("producerName", etprodname.getText().toString().trim());
        params.put("member_id", member_id);
        params.put("remark", etremark.getText().toString().trim());
        GlobalClass.printLog(getActivity(), "-----------params---" + params);

        client.post(Config.URLupdateauthoritysign, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String s1 = response.getString("success");
                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbScheduleDetails set " +
                            "scheduleStatus='completed' where entryId = '" + entry_id + "'");
                    long result = sqLiteStatement.executeUpdateDelete();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set " +
                            "remark=? where entry_id = '" + entry_id + "'");
                    sqLiteStatement.bindString(1, etremark.getText().toString().trim());
                    result = sqLiteStatement.executeUpdateDelete();


                    showToast(getString(R.string.event_complete), Toast.LENGTH_LONG);

                    String memberEmail = null;
                    try {
                        cursor = sqLiteDatabaseRead.rawQuery("select email,name from sqtb_UserProfile", null);
                        if (cursor.moveToFirst()) {
                            memberEmail = cursor.getString(0);
                            ArtistName = cursor.getString(1);
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }

                    //Send Email to Production
                    signImageUrl = img_name;
                    name = "CINTAA Diary";
                    username = Config.cintaaMail;
                    password = Config.cintaaMailPwd;
                    email = etauthorityemail.getText().toString();
                    subject = "Work Schedule Report - " + ArtistName;
                    sendMail(name, username, password, email, subject);

                    if (memberEmail != null) {
                        sendMail(name, username, password, memberEmail, subject);
                    }

                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                GlobalClass.printLog(getActivity(), "-----------t----" + t);
                GlobalClass.printLog(getActivity(), "-----------statusCode---" + statusCode);
                GlobalClass.printLog(getActivity(), "-----------res---" + res);

                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("could not connect to network", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });

    }//updateAuthoritySign

    private void storeCallinToDB(String address, final double Lat, final double Lng) {

        showprogressdialog();
        int selectedId = rgCallinTime.getCheckedRadioButtonId();
        RadioButton radiobutton = (RadioButton) view.findViewById(selectedId);

//                    etcallin.getText().toString().trim() =  + " " + radiobutton.getText();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("callin_time", etcallin.getText().toString().trim() + " " + radiobutton.getText());
        params.put("callin_location", address);
        params.put("callin_lat", Lat);
        params.put("callin_lng", Lng);
        params.put("callin_entry_type", "ManualOnline");

        client.post(Config.URLupdateCallinTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    String s1 = response.getString("success");
                    showToast("In Time Recorded Successfully", Toast.LENGTH_LONG);
                    btncallinsubmit.setEnabled(false);
                    btncallin.setEnabled(false);

                    btncallinsubmit.setVisibility(View.GONE);
                    btncallin.setVisibility(View.GONE);

                    btncallin.setAlpha((float) 0.5);
                    btncallinsubmit.setAlpha((float) 0.5);

                    isCallinSubmitted = true;
                    //            etcallin.setEnabled(false);
                    setCallinEnableFalse();
                    int selectedId = rgCallinTime.getCheckedRadioButtonId();
                    RadioButton radiobutton = (RadioButton) view.findViewById(selectedId);

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
                    sqLiteStatement.bindString(1, entry_id);
                    //... Set AM PM with it
                    sqLiteStatement.bindString(2, etcallin.getText().toString().trim() + " " + radiobutton.getText());
                    sqLiteStatement.bindString(3, String.valueOf(Lat));
                    sqLiteStatement.bindString(4, String.valueOf(Lng));
                    sqLiteStatement.bindString(5, "done");
                    long res = sqLiteStatement.executeInsert();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set Task_callin = 'done' where entry_id = '" + entry_id + "'");
                    long result = sqLiteStatement.executeUpdateDelete();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

				/*Intent intent=new Intent(getActivity(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				getActivity().finish();*/
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Could Not Connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });
    }//storeCallinToDB

    private void InsertCallinSQLiteOFFLine(String entry_id, String MyLat, String MyLong) {

        int selectedId = rgCallinTime.getCheckedRadioButtonId();
        RadioButton radiobutton = (RadioButton) view.findViewById(selectedId);

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Callin values(?,?,?,?,?)");
        //entry_id, callin_time, callin_lat, callin_lng, Status
        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, etcallin.getText().toString().trim() + " " + radiobutton.getText());
        sqLiteStatement.bindString(3, MyLat);
        sqLiteStatement.bindString(4, MyLong);
        sqLiteStatement.bindString(5, "Pending");
        long res = sqLiteStatement.executeInsert();

        //Invokes Background Service to in Time Update to Server
        //getActivity().startService(new Intent(getActivity(), BackgroundService_OffLineCallinPackup.class));


        //Functions Performed in OnSuccess of Callin Submit
        showToast("In Time Recorded Successfully in OffLine Mode", Toast.LENGTH_LONG);
        btncallinsubmit.setEnabled(false);
        btncallin.setEnabled(false);

        btncallinsubmit.setVisibility(View.GONE);
        btncallin.setVisibility(View.GONE);

        btncallin.setAlpha((float) 0.5);
        btncallinsubmit.setAlpha((float) 0.5);
        isCallinSubmitted = true;
        //            etcallin.setEnabled(false);
        setCallinEnableFalse();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set Task_callin = 'done' where entry_id = '" + entry_id + "'");
        long result = sqLiteStatement.executeUpdateDelete();

		/*Intent intent=new Intent(getActivity(),HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		getActivity().finish();*/

    }//InsertCallinSQLiteOFFLine

    private void storePackupToDB(String address, final double Lat, final double Lng) {
        int selectedId = rgPackTime.getCheckedRadioButtonId();
        final RadioButton radiobutton2 = (RadioButton) view.findViewById(selectedId);
        //...
        showprogressdialog();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("entry_id", entry_id);
        params.put("packup_time", etpackup.getText().toString().trim() + " " + radiobutton2.getText());
        params.put("packup_location", address);
        params.put("packup_lat", Lat);
        params.put("packup_lng", Lng);
        params.put("packup_entry_type", "ManualOnline");

        client.post(Config.URLupdatePackupTime, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    String s1 = response.getString("success");
                    showToast("Packup Recorded Successfully", Toast.LENGTH_LONG);
                    btnpackupsubmit.setEnabled(false);
                    btnpackup.setEnabled(false);

                    btnpackupsubmit.setVisibility(View.GONE);
                    btnpackup.setVisibility(View.GONE);

                    btnpackup.setAlpha((float) 0.5);
                    btnpackupsubmit.setAlpha((float) 0.5);

                    isCallinSubmitted = true;
                    //                etpackup.setEnabled(false);
                    setPackUPEnableFalse();

                    sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Packup values(?,?,?,?,?)");
                    //entry_id, packup_time, packup_lat, packup_lng, Status
                    sqLiteStatement.bindString(1, entry_id);
                    sqLiteStatement.bindString(2, etpackup.getText().toString().trim() + " " + radiobutton2.getText());
                    sqLiteStatement.bindString(3, String.valueOf(Lat));
                    sqLiteStatement.bindString(4, String.valueOf(Lng));
                    sqLiteStatement.bindString(5, "done");
                    long res = sqLiteStatement.executeInsert();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }

				/*Intent intent=new Intent(getActivity(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				getActivity().finish();*/
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                showToast("Could Not Connect", Toast.LENGTH_LONG);
                if (pd != null) {
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }
        });


    }//storePackupToDB

    private void InsertPackupSQLiteOFFLine(String entry_id, String MyLat, String MyLong) {
        int selectedId = rgPackTime.getCheckedRadioButtonId();
        RadioButton radiobutton2 = (RadioButton) view.findViewById(selectedId);

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into sqtb_diary_Packup values(?,?,?,?,?)");
        //entry_id, packup_time, packup_lat, packup_lng, Status
        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, etpackup.getText().toString().trim() + " " + radiobutton2.getText());
        sqLiteStatement.bindString(3, MyLat);
        sqLiteStatement.bindString(4, MyLong);
        sqLiteStatement.bindString(5, "Pending");
        long res = sqLiteStatement.executeInsert();

        isCallinSubmitted = true;
        //                etpackup.setEnabled(false);
        setPackUPEnableFalse();

        //Invokes Background Service to Packup Time Update to Server
        //getActivity().startService(new Intent(getActivity(), BackgroundService_OffLineCallinPackup.class));

        //Functions Performed in OnSuccess of Packup Submit
        showToast("Packup Recorded Successfully in OffLine Mode", Toast.LENGTH_LONG);
        btnpackupsubmit.setEnabled(false);
        btnpackup.setEnabled(false);

        btnpackupsubmit.setVisibility(View.GONE);
        btnpackup.setVisibility(View.GONE);

        btnpackup.setAlpha((float) 0.5);
        btnpackupsubmit.setAlpha((float) 0.5);

		/*Intent intent=new Intent(getActivity(),HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		getActivity().finish();*/
    }//InsertPackupSQLiteOFFLine

    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private void sendMail(String name, String username, String password, String email, String subject) {
        getSignIn(name, username, password, email, subject);
    }

    private void getSignIn(String name, String username, String password, String email, String subject) {
        int selectedId = rgCallinTime.getCheckedRadioButtonId();
        RadioButton radiobutton = (RadioButton) view.findViewById(selectedId);

        int selectedId2 = rgPackTime.getCheckedRadioButtonId();
        RadioButton packupRadio = (RadioButton) view.findViewById(selectedId2);

        if (!(isValidEmaillId(email))) {
            etauthorityemail.setText("Please Enter Valid EmailId *");
        } else {
            if (!isNetworkConnected())  //if connection available
            {
                showToast("Please Check Your Internet Connection", Toast.LENGTH_SHORT);
            } else {
                String message;
                if (etrate.getText().toString().trim().length() > 0) {
                    message = "" +
                            "<table  border=\"1\" cellpadding=\"5\">" +
                            "<tr><th colspan=\"2\" bgcolor=\"#8f8f8f\">Work Schedule Report</th></tr>" +
                            "<tr>" +
                            "<td>CINTAA Member ID</td>  <td>" + member_id + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Name</td>  <td>" + ArtistName + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Film/TV Serial</td>  <td>" + etfilmname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Production House</td>  <td>" + autoproduction.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Producer Name</td>  <td>" + etprodname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Date of Shoot</td>  <td>" + etdtshoot.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Location of Shoot</td>  <td>" + etmaplocation.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<td>Address of Shoot</td>  <td>" + etshootloc.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Character Name</td>  <td>" + etcharname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Shift Time</td>  <td>" + etshifttime.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Call Time</td>  <td>" + etcalltime.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Rate</td>  <td>" + etrate.getText().toString().trim() + " " + tvRdays.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Due After</td>  <td>" + etdueday.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Remark</td>  <td>" + etremark.getText().toString().trim() + "</td>" +
                            "</tr>";
                    try {
                        if (Double.parseDouble(etrate.getText().toString().trim()) <= Config.conveyanceLimit) {
                            message = message +
                                    "<tr>" +
                                    "<td>Conveyance Fees</td>  <td>" + etconveyance.getText().toString().trim() + "</td>" +
                                    "</tr>";
                        }
                    } catch (NumberFormatException e) {
                    }


                    message = message +
                            "<tr>" +
                            "<td>In Time</td>  <td>" + etcallin.getText().toString().trim() + " " + radiobutton.getText() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Pack-up Time</td>  <td>" + etpackup.getText().toString().trim() + " " + packupRadio.getText() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Name</td>  <td>" + etauthoritynm.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Number</td>  <td>" + etauthoritynumb.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Sign</td>  <td><img src=\"" + Config.imgPngUrl + signImageUrl + ".png\" alt=\"Sign\" height=\"80\"></td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Date of Sign</td>  <td>" + submit_time + "</td>" +
                            "</tr>" +
                            "</table> <br/>" +
                            "This email message is intended only for CINTAA members and their employers and does not tantamount to spamming as it contains information that is confidential. If you are not the intended recipient, any disclosure, distribution or other use of this email message is prohibited. <br/><br/>" +
                            "<img src=\"" + Config.mailLogoUrl + "\" alt=\"Logo\" height=\"40\"> <br/><br/>" +
                            "<small>Cine & TV Artistes' Association (CINTAA)</small><br/>" +
                            "<small>221, Kartik Complex, 2nd Flr.,</small><br/>" +
                            "<small>Opp.Laxmi Ind. Estate, New Link Rd.,</small><br/>" +
                            "<small>Andheri (W), Mumbai-400 053.</small><br/>" +
                            "<small>Tel.# 26730511/10</small><br/>";
                } else {
                    //<img src=""+Config.mailLogoUrl+"" alt="Logo" height="80"> <br/><br/>
                    //"<b>Cine & TV Artistes' Association</b> <br/><br/>" +
                    message = "" +
                            "<table  border=\"1\" cellpadding=\"5\">" +
                            "<tr><th colspan=\"2\" bgcolor=\"#8f8f8f\">Work Schedule Report</th></tr>" +
                            "<tr>" +
                            "<td>CINTAA Member ID</td>  <td>" + member_id + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Name</td>  <td>" + ArtistName + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Film/TV Serial</td>  <td>" + etfilmname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Production House</td>  <td>" + autoproduction.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Producer Name</td>  <td>" + etprodname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Date of Shoot</td>  <td>" + etdtshoot.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Address of Shoot</td>  <td>" + etmaplocation.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Character Name</td>  <td>" + etcharname.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Shift Time</td>  <td>" + etshifttime.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Call Time</td>  <td>" + etcalltime.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Rate</td>  <td> NA </td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Due After</td>  <td>" + etdueday.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Remark</td>  <td>" + etremark.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>In Time</td>  <td>" + etcallin.getText().toString().trim() + " " + radiobutton.getText() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Pack-up Time</td>  <td>" + etpackup.getText().toString().trim() + " " + packupRadio.getText() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Name</td>  <td>" + etauthoritynm.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Number</td>  <td>" + etauthoritynumb.getText().toString().trim() + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Authority Sign</td>  <td><img src=\"" + Config.imgPngUrl + signImageUrl + ".png\" alt=\"Sign\" height=\"80\"></td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Date of Sign</td>  <td>" + submit_time + "</td>" +
                            "</tr>" +
                            "</table> <br/>" +
                            "This email message is intended only for CINTAA members and their employers and does not tantamount to spamming as it contains information that is confidential. If you are not the intended recipient, any disclosure, distribution or other use of this email message is prohibited. <br/><br/>" +
                            "<img src=\"" + Config.mailLogoUrl + "\" alt=\"Logo\" height=\"40\"> <br/><br/>" +
                            "<small>Cine & TV Artistes' Association (CINTAA)</small><br/>" +
                            "<small>221, Kartik Complex, 2nd Flr.,</small><br/>" +
                            "<small>Opp.Laxmi Ind. Estate, New Link Rd.,</small><br/>" +
                            "<small>Andheri (W), Mumbai-400 053.</small><br/>" +
                            "<small>Tel.# 26730511/13</small><br/>";
                }

                GlobalClass.printLog(getActivity(), "-------------Config.imgPngUrl + signImageUrl:" + Config.imgPngUrl + signImageUrl);
                sendMail(email, subject, message, name, username, password);

            }
        }

    }

    private void sendMail(String email, String subject, String messageBody, String name, String username, String password) {
        Session session = createSessionObject(username, password);

        try {
            Message message = createMessage(email, subject, messageBody,
                    session, username, name);
            new SendMailTask(getActivity()).execute(message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject, String messageBody, Session session, String username, String name) throws MessagingException,
            UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username, name));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        //message.setText(messageBody);
        message.setContent(messageBody, "text/html");
        return message;
    }

    private Session createSessionObject(final String username, final String password) {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", Config.smtpHost);
        properties.put("mail.smtp.socketFactory.port", Config.smtpPort);
        properties.put("mail.smtp.port", Config.smtpPort);
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.put("mail.debug", "true");
        properties.put("mail.store.protocol", "pop3");
        properties.put("mail.transport.protocol", "smtp");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }

//    private Session createSessionObject(final String username, final String password) {
//        Properties properties = new Properties();
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.smtp.host", Config.smtpHost);
//        properties.put("mail.smtp.port", Config.smtpPort);
////...        properties.put("mail.smtp.auth", "true");
////...        properties.put("mail.smtp.starttls.enable", "true");
////...        properties.put("mail.smtp.host", "smtp.gmail.com");
////...        properties.put("mail.smtp.port", "587");
//
//        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(username, password);
//            }
//        });
//        return session;
//    }

    public class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        Activity ctx;

        public SendMailTask(Activity activity) {
            ctx = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ctx = getActivity();
//            progressDialog = ProgressDialog.show(getActivity(),
//                    getString(R.string.please_wait), getString(R.string.sending_mail), true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            if (progressDialog != null && progressDialog.isShowing())
//                progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            GlobalClass.printLog(getActivity(), "--------------message:-----" + messages);
            try {
                Transport.send(messages[0]);
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        /*Toast.makeText(SnapshotDetailsActivity.this,
                                "Passwod Send To Your Mail Successfully", Toast.LENGTH_LONG)
                                .show();*/
                    }
                });
            } catch (final MessagingException e) {
                GlobalClass.printLog(getActivity(), "--------------message:-----" + e);

                e.printStackTrace();
                ctx.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        ///https://www.google.com/settings/security/lesssecureapps
                        //go throught that link and select less security
                              /*  Toast.makeText(ForgotPasswordActivity.this,
                                        e.getClass() + " : " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();*/
                    }
                });
            }
            return null;
        }
    }

    private void storeOfflineAuthorization(String img_name) {
        // tbAuthorise (entryId, member_id, name, number, sign, encodedString, signDate, email, prodHouse, prodName, severStatus)
        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("insert into tbAuthorise values(?,?,?,?,?,?,?,?,?,?,?,?)");
        sqLiteStatement.bindString(1, entry_id);
        sqLiteStatement.bindString(2, member_id);
        sqLiteStatement.bindString(3, etauthoritynm.getText().toString().trim());
        sqLiteStatement.bindString(4, etauthoritynumb.getText().toString().trim());
        sqLiteStatement.bindString(5, img_name);
        sqLiteStatement.bindString(6, encoded_string);
        sqLiteStatement.bindString(7, submit_time);
        sqLiteStatement.bindString(8, etauthorityemail.getText().toString().trim());
        sqLiteStatement.bindString(9, autoproduction.getText().toString().trim());
        sqLiteStatement.bindString(10, etprodname.getText().toString().trim());
        sqLiteStatement.bindString(11, "no");
        sqLiteStatement.bindString(12, etremark.getText().toString().trim());
        long res = sqLiteStatement.executeInsert();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update tbScheduleDetails set " +
                "scheduleStatus='completed' where entryId = '" + entry_id + "'");
        long result = sqLiteStatement.executeUpdateDelete();

        sqLiteStatement = sqLiteDatabaseWrite.compileStatement("update sqtb_diary_entry set " +
                "remark=? where entry_id = '" + entry_id + "'");
        sqLiteStatement.bindString(1, etremark.getText().toString().trim());
        result = sqLiteStatement.executeUpdateDelete();

        showToast("Network is Not Available, Details Stored Offline", Toast.LENGTH_SHORT);

        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
        /*File direct = new File(Environment.getExternalStorageDirectory() + "/CINTAA/appData");
        if (!direct.exists()) {
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/CINTAA/appData/");
            wallpaperDirectory.mkdirs();
        }
        String image = "" + img_name ;
        File outputFile = new File(new File(Environment.getExternalStorageDirectory() + "/CINTAA/appData/"), image);
        if (outputFile.exists()) {
            outputFile.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

            //Toast.makeText(getApplicationContext(),outputFile.toString(),Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }//storeOfflineAuthorization


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        diaryentryFragment = this;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appTour:

                if (btsave.getVisibility() == View.VISIBLE) {
                    tipShootDate();
                } else if (llSubPage.getVisibility() == View.VISIBLE) {
                    if (llconveyancedetail.getVisibility() == View.VISIBLE) {
                        tipConveyance();
                    } else {
                        tipCallInTime();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    View.OnClickListener mSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            hideTip();
        }
    };

    public void hideTip() {
        if (mTipFocusView != null && mTipFocusView.isShown())
            mTipFocusView.hide();
    }

    private void tipShootDate() {
        lldateshoot.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipFilmName();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_select_shoot_date_here);
                                }
                            })
                            .focusOn(lldateshoot)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .closeOnTouch(false)
                            .build();
                    mTipFocusView.show();


                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, tvWelcome.getBottom());
            }
        });

        lldateshoot.performClick();
    }

    private void tipFilmName() {
        hideTip();
        etfilmname.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipProdHouse();
                                        }
                                    });

                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_film_or_tv_serial_name);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llfilmName)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, lldateshoot.getBottom());
            }
        });
        etfilmname.performClick();

    }

    private void tipProdHouse() {
        hideTip();
        llprodhouse.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipProdName();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_production_house);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llprodhouse)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llfilmName.getBottom());
            }
        });
        llprodhouse.performClick();

    }

    private void tipProdName() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipshootLocation();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_producer_name);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llprodName)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llprodhouse.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipshootLocation() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipshootMapView();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_photo_shoot_location);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llshootlocation)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llprodhouse.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipshootMapView() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_map_view, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipAddressOfShoot();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_shootMapView);
                                }
                            })
                            .closeOnTouch(false)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llprodhouse.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipAddressOfShoot() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipNameOfCharacter();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_address_of_shoot);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llAddressOfShoot)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, llshootlocation.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipNameOfCharacter() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            if (btsave.getVisibility() == View.VISIBLE) {
                                            tipShifttime();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_character_name);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llNameOfCharacter)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        llprodName.performClick();

    }

    private void tipShifttime() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipCallTime();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.enter_shift_time);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llshifttime)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        llprodName.performClick();

    }

    private void tipCallTime() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_bottom, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            if (btsave.getVisibility() == View.VISIBLE) {
                                            tipRate();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_call_time);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(llcalltime)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        llprodName.performClick();

    }

    private void tipRate() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_top, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            if (btsave.getVisibility() == View.VISIBLE) {
                                            tipDueDate();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_rate);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(etrate)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        llprodName.performClick();

    }

    private void tipDueDate() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_top, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            if (btsave.getVisibility() == View.VISIBLE) {
                                            tipSave();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_due_date);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(etdueday)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        llprodName.performClick();

    }

    private void tipSave() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    if (btsave.getVisibility() == View.VISIBLE) {
                        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                                .customView(R.layout.layout_tip_top, new OnViewInflateListener() {
                                    @Override
                                    public void onViewInflated(View view) {
                                        view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                        TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                        btnSkip.setOnClickListener(mSkipClickListener);
                                        btnSkip.setText(R.string.btn_close);
                                        View view2 = (View) view.findViewById(R.id.view);
                                        view2.setVisibility(View.GONE);
                                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                        tvTip.setText(R.string.tip_click_save);
                                    }
                                })
                                .closeOnTouch(false)
                                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                                .roundRectRadius(90)
                                .focusOn(btsave)
                                .build();
                    } else {
                        mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                                .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                    @Override
                                    public void onViewInflated(View view) {
                                        view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                        TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                        btnSkip.setOnClickListener(mSkipClickListener);
                                        btnSkip.setText(R.string.btn_close);
                                        View view2 = (View) view.findViewById(R.id.view);
                                        view2.setVisibility(View.GONE);
                                        TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                        tvTip.setText(R.string.tip_click_save);
                                    }
                                })
                                .closeOnTouch(false)
                                .roundRectRadius(90)
                                .build();
                    }
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, mainScroll.getBottom());
            }
        });
        llprodName.performClick();
    }

    private void tipConveyance() {
        hideTip();
        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipCallInTime();
                                        }
                                    });
                                    view.findViewById(R.id.btnSkip).setOnClickListener(mSkipClickListener);
                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_conveyance_charges);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(etconveyance)
                            .build();
                    mTipFocusView.show();
                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
                mainScroll.smoothScrollTo(0, mainScroll.getBottom());
            }
        });
        llprodName.performClick();

    }

    private void tipCallInTime() {
        hideTip();

        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_callin_time, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipAuthorityname();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);


                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_select_in_time);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
//                            .focusOn(etcallin)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
//        mainScroll.post(new Runnable() {
//            public void run() {
////                mainScroll.smoothScrollTo(0, mainScroll.getBottom());
//                mainScroll.smoothScrollTo(0, etcallin.getBottom());
//            }
//        });
        llprodName.performClick();
    }

    private void tipAuthorityname() {
        hideTip();

        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_authority_name, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipAuthorityContact();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);


                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_authoroty_name);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
//                            .focusOn(etauthoritynm)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
//        mainScroll.post(new Runnable() {
//            public void run() {
////                mainScroll.smoothScrollTo(0, etauthoritynm.getBottom());
//                mainScroll.fullScroll(View.FOCUS_DOWN);
//            }
//        });
        llprodName.performClick();
    }

    private void tipAuthorityContact() {
        hideTip();

        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_authority_contact, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipAuthorityProductionEmail();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);


                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_authoroty_contact);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
//                            .focusOn(etauthoritynm)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
//        mainScroll.post(new Runnable() {
//            public void run() {
////                mainScroll.smoothScrollTo(0, etauthoritynm.getBottom());
//                mainScroll.fullScroll(View.FOCUS_DOWN);
//            }
//        });
        llprodName.performClick();
    }

    private void tipAuthorityProductionEmail() {
        hideTip();

        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_with_production_email, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            tipSubmitAll();
                                        }
                                    });
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);


                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_production_email);
                                }
                            })
                            .closeOnTouch(false)
//                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
//                            .roundRectRadius(90)
//                            .focusOn(etauthoritynm)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
//        mainScroll.post(new Runnable() {
//            public void run() {
////                mainScroll.smoothScrollTo(0, etauthoritynm.getBottom());
//                mainScroll.fullScroll(View.FOCUS_DOWN);
//            }
//        });
        llprodName.performClick();
    }

    private void tipSubmitAll() {
        hideTip();

        llprodName.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getActivity().isFinishing()) {
                    mTipFocusView = new FancyShowCaseView.Builder(getActivity())
                            .customView(R.layout.layout_tip_center, new OnViewInflateListener() {
                                @Override
                                public void onViewInflated(View view) {
                                    view.findViewById(R.id.btnNextTip).setVisibility(View.GONE);
                                    TextView btnSkip = (TextView) view.findViewById(R.id.btnSkip);
                                    btnSkip.setOnClickListener(mSkipClickListener);
                                    btnSkip.setText(R.string.btn_close);

                                    View view2 = (View) view.findViewById(R.id.view);
                                    view2.setVisibility(View.GONE);

                                    TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                                    tvTip.setText(R.string.tip_save_all);
                                }
                            })
                            .closeOnTouch(false)
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .roundRectRadius(90)
                            .focusOn(btsubmit)
                            .build();
                    mTipFocusView.show();

                }
            }
        }, 500);
        mainScroll.post(new Runnable() {
            public void run() {
//                mainScroll.smoothScrollTo(0, etauthoritynm.getBottom());
                mainScroll.fullScroll(View.FOCUS_DOWN);
            }
        });
        llprodName.performClick();
    }

}




